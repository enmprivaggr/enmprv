# Making a release

A release consists of three binary programs built from the source code as available in a public git repository. The three programs are:
- a p1 encryptor, `p1encrypt`,
- a key adder, `keyadder`, and
- a privacy aggregator, `aggrmeters`.

A release also consists of configuration files.
The three programw each have their own:
- .toml file with runtime parameters, and
- .yaml file with log file formats.


# Develop/Test

Test the three binaries in a development environment.

This development environment consists of two computers: 
- a local machine (laptop/desktop) with the development git repository
  to change the programs,
- a raspberry pi to run the `p1encrypt` program. 
  This raspberry is connected to a P1 port of a smart meter.
  This raspberry is accessed by ssh from the local machine.
- a wireguard tunnel between the local machine and the raspberry.

The unit tests can be run on the local machine by running `test/local-enmprv.sh`.
These tests should pass.

Then test the `simulated_meter_nrs` feature. With  this feature,
the privacy aggregation via the key adder should work:

- run the command
```bash
test/aggrmeters.sh
```
  in a terminal window and let the aggrmeters program run.

- run the command
```bash
test/keyadd.sh
```
   in another terminal window and let the keyadder program run.

- in a third terminal window, run the command 
```bash
test/p1encrypt.sh devsim
```
  to run the p1encrypt program with the `simulate_meter_nrs` feature.
  In case the startup stops early another instance of this program may
  be running, stop this by Ctrl-C in its window, or by 
```bash
test/p1encrypt.sh disable
```

With these three programs running, after a few minutes the aggrmeters program should show a log of a 
real time decrypted aggregation. 
For an example of this decrypted aggregation output, 
see [DevelopmentScripts.md](DevelopmentScripts.md).

These running programs are development versions built by `cargo build`,
not by `cargo build --release`.

Finally, verify that `dev/meterplots.py` plots measured values in 
quarter of an hour intervals.

# Increase the release number

Commit the latest changes on the master branch in the development repository.

In the following files update the release number 
to the new release number k.l.m:
- in `Cargo.toml`, remove -dev from k.l.m-dev here,
  and possibly adapt k, l and/or m to the new release number.
- in `test/p1encrypt.sh`, update the release tag from the previous release number
  to the new release number. 
  The new tag name should be a 'v' followed by the release number,
  like v0.1.12 .
- On the development machine, do a `cargo build --release` to check the build,
  and to have `Cargo.lock` include the new version number.


Add these changes as a release commit on the master branch.

Add the release tag locally on this commit 
and a short commit message for the release.

Push the master branch to the public repository.

Push the release tag to the public repository.

In top level directory of the test repository run
```bash
test/p1encrypt.sh release
```

On the Raspberry Pi, this will:
- checkout the release tag from the public repository 
  into a the directory `enmprv-rel`.
- run `cargo check` and `cargo build --release` 
  to create the `p1encrypt` release binary 
  in the `enmprv-rel/target/release/` directory.
  When the Cargo.lock crate dependencies and/or the rust compiler have been updated since the previous release, these cargo steps will take some time.
- run the `p1encrypt` release binary.
  This run normally ends with an early exit in case the previous p1encrypt service is running.

# Using the release binaries

The release binaries are now available on the raspberry in:
- `enmprv-rel/target/release/p1encrypt`
- `enmprv-rel/target/release/keyadder`
- `enmprv-rel/target/release/aggrmeters`

To check the versions of the binaries, ssh into the raspberry
and run each of these binaries with argument -v to the check
their version.

The .toml and .yaml development configuration files are here:
- `enmprv-rel/src/bin/p1encrypt/p1encrypt.toml`
- `enmprv-rel/src/bin/p1encrypt/p1enbglog4rs.yaml`
- `enmprv-rel/src/bin/keyadder/keyadder.toml`
- `enmprv-rel/src/bin/keyadder/keyaddbglog4rs.yaml`
- `enmprv-rel/src/bin/aggrmeters/aggrmeters.toml`
- `enmprv-rel/src/bin/aggrmeters/agrmbglog4rs.toml`

To build the release binaries on the local development machine,
checkout the release tag, run `cargo build --release`, and run the binaries with argument -v to check their version.

To communicate with each other these programs use wireguard tunnels.
The network addresses on these tunnels are defined in the .toml configuration files.

These network addresses will need to be adapted to the tunnel network addresses
of the machines on which these programs are run.

The wireguard tunnels for the release binaries are currently set up manually.

# Continue development with a -dev version

After the checkout of the release tag and after the build on the raspberry,
change the new version number from k.l.m to k.l.(m+1)-dev in `Cargo.toml`, 
and commit this on master branch to continue development.
