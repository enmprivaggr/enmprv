# Hardware set up of a Raspberry Pi as a p1encryptor

This is more detailed than setupemp.py, but not checked in detail (20250217).

## Introduction

The goal is to setup a new raspberry pi 4 to work as a p1encryptor.

Hardware needed:
- raspberry pi 4
- power supply for the pi via USB B
- HDMI/miniHDMI cable to initially boot the pi and connect a screen,
- a keyboard and mouse with USB to initially control the pi after booting,
- connection cable USB to p1 port on smart meter
- a network cable to connect the pi to the local network close the smart meter,
  or use a wifi connection there.
- micro SD for an OS image for the raspberry
- a computer to prepare this micro SD with an OS image.

After ssh connection to the pi is setup the following are no more needed:
- the HDMI/miniHDMI cable, and HDMI screen,
- the USB keyboard and mouse.

Use a microSD with a preinstalled Raspberry PI OS image., or prepare the image as follows.
Follow the steps here to create a bootable image on the microSD card:
https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up/2
Download and use the imager rpi on the computer to prepare the micro SD.
Prepare the micro SD with a Raspberry Pi OS operating system.

Connect the raspberry, see
https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up/3 
- insert microSD card
- connect USB keyboard and mouse
- connect HDMI cable to screen

Setup the raspyberry with the setup program, see:
https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up/4
- connect the power supply
- use the setup program to set the local country and time zone
- possibly set the language to English to better follow the guidance here
- create an initial user and password, and take note of the user name and password.
- change the screen settings as needed,
- setup a wifi connection when wifi is available.
  When possible connect to the same one to be used when
  connected to the p1 port of the smart meter.
- update the software on the pi,
- restart to finish the setup.


After the restart, use the raspberry with the keyboard, mouse and screen.
You will be logged in as the initial user.
- finish any software updates in case they are available by clicking

Then continue with the Raspberry Pi Configuration:

- get network ifconfig info: inet ipv4 address for later access by ssh.
- if necessary, for example when there are more raspberry pi's on the local network,
  set the hostname from raspberrypi to something else.
  reboot to actually change this hostname.
- enable interface SSH on the raspberry.
- set public ssh keys in ~./ssh/authorized_keys for easy ssh access,
  one way to do this is to ssh from the raspberry to a local machine
  and copy and paste .ssh/id_rsa.pub from there.
- test ssh access by ssh user@ipv4address on the local machine.
- set the timezone to the local one, for example Amsterdam.
- if necessary, set the keyboard type. With ssh access only, this is not needed.
- set the Wireless LAN Country, if necessary.

Then reboot the raspberry pi.
- use ssh user@hostname to login via ssh from a local machine.
- use the RaspBerry PI Configuration tool to:
-- set the raspberry to boot to CLI, this will save some cpu for later use as server.
-- disable the Auto Login, (make sure to know the username and password),
   this improves security for later unattended use as server.
- login via the login shell, give the command:
-- shutdown now
- wait for the shutdown,
- disconnect the power,
- disconnect the keyboard, mouse and screen.


Next steps:
- move the raspberry close to smart meter, 
- connect the p1 cable to an usb port,
- when possible, connect the raspberry with a network cable to the local network
  (for example a modem in close to the meter), or enable wifi access close
  to the smart meter.

- check that the p1 connection to the raspberry is at the left upper USB port,
- ssh into the raspberry, and give this command to check the p1 connection to the raspberry:
-- cat /dev/ttyUSB0
- in case this shows strange characters with parity errors, interrupt by ^C, and give this command:
-- stty -F /dev/ttyUSB0 evenp -icrnl

- in case there is not output from the cat command:
-- stty -F /dev/ttyUSB0 115200

Now the cat command should show p1 telegrams from /dev/ttyUSB0 .

The p1encrypt program will also perform these stty settings
when it starts up.





