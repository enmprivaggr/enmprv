# Road map

The prototype currently shows that real time aggregation of meter values from the P1 ports of smart meters
can be done with full privacy for the meter values. 
P1 ports provide new measurements values every 1-10 seconds, and the prototype aggregates these every 12 seconds.

In the normal situation the P4 port values are used:
- these values are available the next day for each quarter of an hour, and
- these values are used directly, without encryption.

This prototype shows two possible improvements over the normal situation:
- real time use of the meter values, and 
- privacy for the meter values.

Starting from this prototype, this road map contains goals for further development.

One goal is to get more developers involved.
In release v0.2.0 this is supported by simplifying the setup of a development environment.

Another goal is to build up user trust in the aggregated meter values by 
showing financial information derived from the P1 port that is the same
as the actual bills derived from the P4 port. 
This is also possible with full privacy for the meter values.

Once there is sufficient user trust in the provided financial information,
the next goal is to de-aggregate the energy balance responsibility towards
the participants.
This should allow them to reduce their energy bills by exercising
this balance responsibility with their energy devices,
with full privacy for their meter values from P1 port.

The previous goals involve scaling up to more users.
When the number of users grows to about hundred or more it is time 
to try and reduce the hardware cost per user, for example by
developing a P1 dongle to replace the current Raspberry Pi.

The goals related to the (non developer-)users are elaborated below.

## Building up user trust

The users initially have three possible roles:
- participants, having his/her meter values aggregated,
- key adder users, providing the privacy for the meter values, and
- privacy aggregator users, using the real time aggregates.

Any user can have the role of participant, and possibly also the role of key adder or the role of privacy aggregator.
These last two roles cannot be combined by the same user because that would enable collusion and break the privacy.

Building up trust in aggregated meter values and in privacy for local meter values involves the following:

- Using a PKI (Public Key Infrastructure) to define policies for the participants, 
  key adders user and aggregator user.

  For example:
  - users should only be certified to participate when they agree with aggregator to
    encrypt the real measurement values from the P1 port of their meter,
  - the key adder, that provide the privacy for the meter values, should agree with the participants 
    on the minimum of of participants that can be used for aggregation.

  A PKI will also involve an extra role for a common trusted party.


- Showing monthly cost info derived from P1 values to compare to actual monthly bills from P4 values.

  For the P1 values this be done by the participants themselves, and also by the privacy aggregator user.

  For the P4 values this is normally the (monthly) energy bill, 
  so the role of privacy aggregator is preferably taken by the energy supplier of the participants.

  When the cost info is the same as the actual bill, trust can be gained.

  Showing this cost info requires:
  - a database at the p1encryptor (this is already present, but it may need additions)
  - databases for earlier values at keyadder and prvaggr, and 
  - addition of sources of energy prices.

  The monthly cost info is to be shown for:
  - high/low tariffs with privacy for high/low
  - hourly dynamic prives with privacy for hourly meter values.

The privacy is provided by the key adder user. The method to provide the privacy
and the software implementation that is used for this will need to be evaluated.
  

## Learning to take responsibility on energy markets

This mainly consists of de-aggregating the current central role of the Balance Responsible Party (BRP)
towards the local participants. Currently small participants delegate this responsibility to their energy supplier.

De-aggregating this balance responsibility involves:
- participants taking responsibility for their own balance, and 
- local aggregators taking responsilibility for the local balance.

The market prices are set by Tennet, and these prices continuously balance the energy production and consumption on the market.

The goal here is to let participants learn to bid on electricity markets:
- the day ahead market for hourly values to have minimal hourly imbalance,
- the intraday market for quarter values to minimize imbalance costs.

Therefore when a (local) bid on these markets is accepted this creates a (local) daily energy program for the energy consumption/production.

Pass this daily energy program on to HomeAssistant to control local hardware for energy storage/use. 
The goal is to avoid vendor lock in for this hardware.
Homeassistant also has a clear privacy goal so this appears to fit well.

For privacy of the participants it will also be necessary to aggregate their bids with full privacy,
and for this aggregation the same method can be used as for aggregating the meter values.

Estimation of business cases for energy storage (battery/heat/molecules) 
is possible from the history of aggregated values and energy prices.
The point here is that the actual costs of these different forms energy (local sector coupling) 
can be provided with full privacy for the meter values.


## Costs and risks of hardware to run this

A Raspberry Pi is good for development of the P1 port encryption for the participants, 
but less is more:
- develop a P1 port dongle for P1 privacy encryption,
- do real time meter value privacy encryption in the smart meter itself.

For the role of the key adder it is to be investigated which party and which hardware is most suitable,
especially when scaling up.

The many network connections used for this become more critical when scaling up.
It is expected that most network bandwith will be needed by the privacy aggregator.
There especially the network connections with the privacy aggregator will need evaluation.
