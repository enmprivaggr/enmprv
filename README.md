
# Energy meter privacy by multi party encrypted addition of meter values

This repository contains a prototype for real time aggregation of smart (energy)
meter values with full privacy for the meter values.

For their privacy the participants encrypt each measured value by adding 
a new large random number as encryption key.
A key adder receives these encryption keys and provides the privacy by requiring
a minimal number of participants for each aggregation.

This privacy method can also be used to determine energy costs for single participant
over a period of time with variable prices. 
For this the key addition over the period is weighed by the variable prices.

The prototype system is self hosted on Raspberry Pi's: some participants can also take the role of either 
the key adder or the aggregator.

For further development there is a [road map](RoadMap.md) with development goals, and
and [here](DevelopmentScripts.md) is overview of currently used development scripts. 

The privacy method is described in more detail in 
[this report](https://gitlab.com/YpeKingma/publications/-/tree/master/meterpriv/Kingma2020MeterPrivEN.pdf).
(For good viewing, please download the pdf and use a local pdf viewer.)
Dutch versions of the report and slides for the report are available
[here](https://gitlab.com/YpeKingma/publications).

See Figures 1 and 2 of this report for a quick overview. Figure 2 shows two meters
that each have their P1 port connected to a small computer that encrypts a meter value by adding a random value as an encryption key. This computer is called the p1 encryptor here.
The key adder S receives all these encryption keys, sums them up into a total key, and sends this total as a decryption key to the decryptor/aggregator T.
The decryptor of the total receives the encrypted meter values, adds them, and subtracts the total key that receives from the key adder. The result is the aggregation of the meter values.

An earlier report on the prototype (September 2021) is [here](Report202109.md).


# License

Licensed under either of:

- Apache License, Version 2.0, [here](LICENSE-APACHE),
also [here](https://www.apache.org/licenses/LICENSE-2.0), or
- MIT license, [here](LICENSE-MIT), 
also [here](https://opensource.org/licenses/MIT),

at your option.

# Contribution

Unless you explicitly state otherwise, any Contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.
