# Changelog

## 0.2.0 - 2025-02-22

### Added

- Add this CHANGELOG.md using inspiration from https://common-changelog.org/
- Add a [python script](dev/setupemp.py) for the setup of a development environment.
  See the python code comments in there for details of this environment.
- Add a [road map](RoadMap.md) for further developments.
- Add an [overview of the scripts](DevelopmentScripts.md) used for development.

### Changed
- Update Rust code to edition 2024, rustc 1.85.0.
- Simplify the database for measurement data on the p1encryptor 
  to use meter numbers instead of network EAN codes.
- Avoid the use of meter numbers by the key adder,
  instead use the p1encryptor public keys for the meters.
- On the development machine:
  - update Ubuntu to noble, 24.04.1 LTS.
  - update PostgreSQL (for compilation) from 11 to 16
- On the Raspberry Pi running the p1encryptor:
  - update Raspbian to bookworm, see:
    https://downloads.raspberrypi.com/raspios_arm64/images/raspios_arm64-2024-07-04/2024-07-04-raspios-bookworm-arm64.img.xz .
  - upgrade PostgreSQL (for measurement data) from 11 to 15.

# 0.16.0 - 2024-02-09, and earlier releases

- The releases up to 0.2.0 were used with at most two meters,
  with manual setup/installation of wireguard tunnels, program binaries and systemctl services.
- Development moved to https://gitlab.com/privacysamen/enmprv in August 2021.
- Development originally started in another repository in April 2021.

