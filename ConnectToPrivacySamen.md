# Warning

This text is outdated.
Connecting to Privacy Samend needs to be much more automated and 
based on a PKI system that is to be introduced into the development.


# Connecting to Privacy Samen


[Privacy Samen](https://privacysamen.nl)
 (Dutch for Privacy Together) is a non profit organisation. 
It provides a system for real time aggregation of smart meter values,
with full meter value privacy for the participants.
Privacy Samen also aims to provide the economic value of the meter values
back the participants as much as possible.

The system that Privacy Samen provides consists of 
a key adder and an aggregator that are connected the to participants, 
and to each other.
This system is a prototype to investigate the feasibility of real time aggregation with full privacy
and trusted results.

The meter values of the participants are taken from the P1 ports of their smart meters. 
These values are encrypted before they are sent to the aggregator.
This encryption provides the privacy for the meter values, 
the meter values themselves stay at home.
Each meter value is encrypted by adding a new encryption key.
The encryption keys are large and randomly chosen.

The key adder receives the individual encryption keys from the participants and
provides sum decryption keys to the aggregator.
The key adder enforces the privacy by resticting the aggregator 
to only decrypt sums when enough partipants provide their meter values.
The aggregator decrypts the sum of the meter values 
by subtracting the decryption key provided by the key adder.

To allow the aggregation results to be trusted, the participants and the key adder
digitally sign each value communicated, and the signatures are checked
when each value is received.

Each communication is done via a trusted channel, for security and privacy.

In this way, with enough participants, the participants provide each other with privacy for 
their meter values, and the real time sum of the meter values can be used for their benefit.

Real time here means that about five times per minute the participants encrypt their meter values and the aggregator decrypts the sum.

By making a connection to this system and by using your connection you are exchanging information with Privacy Samen. 
This information exchange comes with mutual responsibilities.
Privacy Samen is currently (December 2023)
formulating the responsilities for the roles of the participants, the key adder and the aggregator.

We hope you will connect and help us to fill in the roles and responsibilities.

For now, we only allow new participants that are members of our local energy cooperation
[Deelstroom Delft](https://deelstroomdelft.nl/).

The Privacy Samen system itself is running on two Raspberry Pi's in two different locations.
One Raspberry runs the key adder, and the other runs the privacy aggregator.
Both Raspberries also run P1 encryptor programs connected to their local smart meters.



# Building, installing and connecting a P1 encrypting program


Here are the steps to build, install and connect a P1 encrypting program.

The steps here assume that the Raspberry Pi OS is used, 
see the [raspberry documentation](https://www.raspberrypi.com/documentation/computers/os.html).

You will need:
- A Raspberry Pi running Linux, with a USB connector, with access to internet.
- A P1 USB cable to connect your smart meter to the Raspberry. 
  Use the top left USB port on the connector side of the Raspberry.
- Internet access for the Raspberry.

During normal use, internet is used by the P1 encryption program to access the key adder and aggregator hosts provided by Privacy Samen.

In the steps below, internet is also used to:
- update your Raspberry with the latest Pi OS programs,
- install the Rust programming language on the Raspberry,
- download the program (for now) in source code form, from gitlab,
- exchange email with the system administrator to obtain configuration files.

You will need to be familiar the command shell in Linux, normally bash.
The steps here can be done either directly on the Raspberry desktop or via ssh from another computer.
As the Raspberry will be connected to the smart meter, using ssh from another computer is preferred.

In the future, the steps will hopefully be much more automated, for example by having a .deb file to install from.


## Preparing the Raspberry

Some of the shell commands shown here also contain comment dashes #. 
These introduce comments to the end of the line that are not part the commands.

The commands occasionally use raspy2 as the host name in the example shell prompt.
The user name in the prompt has been replaced by three dots before the @. 
Shell prompts are shown to distinguish the command from its standard output on the immediately following lines.

The user is supposed to have sudo rights; for the user **pi** this is normally the case on a Raspberry.
The user with sudo rights is referred to as the normal user below.

As a first step, update and upgrade as recommended
[here](https://www.raspberrypi.com/documentation/computers/os.html),
and check the remaining free disk space with the following shell commands:
```
sudo apt update
sudo apt full-upgrade
df -h
```
You will need at least 3 GB free disk space in the /dev/root file system.
The precise amount of free space needed for this is not known at the moment.

The P1 encryptor program uses the following programs:
- openssl, 
- wireguard, 
- timedatectl,
- ip, and
- stty.

Install openssl to create your key to digitally sign the encrypted measurements,
and install wireguard for the tunnel connections:
```
which openssl # when not available, install:
sudo apt install openssl
which wg # idem:
sudo apt install wireguard
```
Check that your Raspberry system time shows NTPSynchronized=yes:
```
...@raspy2:~ $ timedatectl show
Timezone=Europe/Amsterdam
LocalRTC=no
CanNTP=yes
NTP=yes
NTPSynchronized=yes
# and more output lines
```
Verify that the ip and stty programs are available:
```
...@raspy2:~ $ which ip
/usr/sbin/ip
...@raspy2:~ $ which stty
/usr/sbin/stty
```

In case any of the above programs are not available, install them.

To check that the P1 cable is connected properly to the smart meter,
use the following command.
It should show the text of some P1 telegrams in your shell.
These P1 telegrams normally arrive every 3 to 10 seconds:
```
cat /dev/ttyUSB0
#
# P1 telegram text shown here
#
^C # ctrl-C to stop the cat program.
```
In case nothing shows up, first check the connection of the P1 cable to the smart meter.
Then try changing the baudrate of /dev/ttyUSB0 to 115200 or 9600:
```
stty -F /dev/ttyUSB0 115200
# or:
stty -F /dev/ttyUSB0 9600
```
And repeat the ```cat``` command above.

You should see some output characters of P1 telegrams now, and that means that the P1 port is connected correctly to your Raspberry.

Don't worry when the output is not really readable. Lateron the P1 encryptor program
will try changing not only the tty baudrate and but also the tty parity.


### Create user mmenc to run the P1 encrypting program

The user mmenc, the MeasureMents ENCryptor, will be running the P1 encrypting program.


#### Delete existing mmenc user

In the case that the mmenc user already exists, you may consider stopping here.
The steps here are only meant for the case of connecting to the Privacy Samen system for the first time. 
When the mmenc user already exists most likely you are updating an existing connected P1 encryptor,
and the steps for updating have not yet been documented.

Warning: in the case that the user mmenc already exists, the user may have measurements stored in the postgresql database (see below). It is currently not known how postgresql deals with these stored measurements when the mmenc user is deleted.

Nevertheless, in case you decide to remove the existing mmenc user, kill all processes that the mmenc user is running, and then remove the user:
```
sudo killall -SIGKILL --user mmenc
sudo deluser --remove-all-files mmenc
```

#### Create new mmenc user

Add the mmenc user as a new user, and add the user to the dialout group to 
allow reading from the USB port that the P1 cable is connected to:
```
sudo adduser --disabled-login mmenc < /dev/null
sudo adduser mmenc dialout
```
Then create this directories in the home directory of the mmenc user:
- p1encrypt-rs/ to hold the p1encrypt-rs program and its configuration files,
- p1encrypt-rs/log/ to hold the log files of the p1encrypt-rs program, and
- keys/ to hold the private and public key of mmenc, and the public keys of the key adder and the aggregator programs.
```
sudo su mmenc
# now as user mmenc:
cd
mkdir --parents p1encrypt-rs/
mkdir --parents p1encrypt-rs/log/
mkdir --mode=700 --parents keys/
^D # ctrl-D to end the su mmenc session 
```


### Optional: postgresql database for P1 measurements

In case you want the p1encryptor to use a database to store your P1 measurements, you will need to install postgresql on the Raspberry:
```
...@raspy2:~ $ sudo apt install postgresql
```

The database for use by the P1 encrypting program still needs to be created, see below.

## Generate your openssl private and public keys

In the commands below use the current date (year, month day) and hour instead of YYYYMMDDhh, 
for example 2023120214:
```
sudo su mmenc
# as user mmenc:
cd ~/keys

# generate the private ssl key:
openssl genrsa -out YYYYMMDDhh-private2048.pem 2048

# generate the public ssl key:
openssl rsa -in YYYYMMDDhh-private2048.pem -pubout -out YYYYMMDDhh-public2048.pem

^D # ctrl-D to end the su mmenc session
```


## Generate wireguard key files

Wireguard tunnels are used to make sure that the exchanged information
can only be accessed at the tunnel endpoints. 
See [wireguard.com](https://wireguard.com) for background.

In the setup here, wireguard will use an IP4 address from the range **10.1.0.0/16** on your Raspberry.
Please contact your system administrator in case this address range is already in use on your Raspberry, another address range can be used.


Create the wgconf/ directory for the normal user to hold the wireguard configuration file and the files with wireguard private and public keys:
```
cd
mkdir --mode=700 wgconf
```

As before, in the commands below use the current date (year, month day) and hour instead of YYYYMMDDhh, for example 2023120214.

Generate the wireguard private/public key files in the wgconf/ directory of the normal user:

```
cd wgconf
wg genkey > YYYYMMDDhh-wgprivkey
cat YYYYMMDDhh-wgprivkey | wg pubkey > YYYYMMDDhh-wgpubkey
```


## Backup your private keys

Before starting the email exchange for configuration files, make a backup 
of the private keys that you generated above in a safe place.

Your private keys are like passwords, and
the system administrator cannot help you to recover them in case of loss.



## Building the P1 encryptor program from source


(This step can hopefully be replaced by using
[gitlab releases](https://docs.gitlab.com/ee/user/project/releases/#create-a-release-in-the-releases-page)
to make the release artifacts available.)


First install git and rust on the Raspberry:
```
sudo apt install git
```
Then install rust by following [these steps](https://www.rust-lang.org/tools/install).

As user mmenc on the Pi, choose a directory for the git repository with the source code,
here we assume that you use the home directory:
```
sudo su mmenc
cd # to mmenc home
git clone https://gitlab.com/privacysamen/enmprv

# checkout the code release that you want to build, here the example is v0.1.15:
cd enmprv
git checkout v0.1.15
cargo build --release
```
The last step will take at least a few minutes, building rust programs is known to be somewhat slow.
Then check the presence of a p1encrypt-rs program and check that its reported version coincides with the git checkout above:
```
...@raspy2:~/enmprv $ file target/release/p1encrypt-rs
target/release/p1encrypt-rs: ELF 32-bit LSB shared object, ARM, EABI5 version 1 (SYSV), ...

...@raspy2:~/enmprv $ ./target/release/p1encrypt-rs -v
./target/release/p1encrypt-rs version 0.1.15
^D # end su mmenc session

```

The cargo build step above also builds the keyadder-rs and aggrmeters-rs programs. 
These do the key adding and the privacy aggregation for the Privacy Samen system.
You can also use these programs, see the license file in the repository.


### Optional: create the local database for use by the P1 encrypting program

This database will be put on the microssd card of your Raspberry.

This step uses a shell script from the local git repository in ~mmenc/enmprv.
This script must be run as the normal user that can sudo:
```
cd ~mmenc/enmprv/dev
./p1initdb.sh
```

Over time, the data size for the P1 measurements may grow to about 4GB, so make sure this space is also available, use df -h as above. 

(It is possible to reduce the data size for the P1 measurements by reducing the default times the measurements are kept. These defaults are in the p1encrypt.toml configuration file.)


## Email exchange with the system administrator for configuration files

You can request your system administrator for configuration files by email.
In this request please indicate that you agree that the information you provide
will be used in the way described below.
In the request email also attach the following files:
- your openssl public key, ~mmenc/keys/YYYYMMHHdd-public2048.pem, and
- your wireguard public key, ~/wgconf/YYYYMMHHdd-wgpubkey.

Never send any private key by mail.

In the reply you will receive these files to run the p1encryptor:
- p1encrypt.toml with general configuration
- p1elog4rs.yaml to configure the log output,
- wgenmprv0.conf to configure the wireguard tunnels, and
- the public key of the key adder.

Save these files in the indicated directories:
- p1encrypt.toml and p1elog4rs.yaml: save in ~mmenc/p1encrypt-rs/
- keyadd-public2048.pem: save in ~mmenc/keys/
- wgenmrpv0.conf: save in ~/wgconf/

At the moment the aggregator does not digitally sign its results,
and it does not have a key pair.
This will change in the future.

### Aside: how the administrator deals with your request

The date of your email request and
the public keys you sent in the request 
will be stored in the Privacy Samen system.

The administrator chooses the following unique values that are used
to operate the privacy aggregation of your P1 meter values:
- two fake EAN numbers, 
- a log identifier of the P1 encrypting program, and
- a VPN address.

These values will also be stored.

#### Fake EAN numbers

The administrator chooses:
- a unique fake EAN number for your electricity connection, and
- a unique fake EAN number for your gas connection.

The smart meter that you connect to your P1 port will be assumed to be measuring
the electricity and gas for these fake EAN numbers. 
These fake EAN numbers have 20 decimal digits,
whereas normal EAN numbers have at most 18 decimal digits.
We use fake EAN numbers to avoid using personal information.

More background on normal EAN numbers can be found at
[energievergelijk.nl](https://www.energievergelijk.nl/onderwerpen/ean-code)
and
[eancodeboek.nl](https://www.eancodeboek.nl/).


####  Identifier for your P1 encryptor program

The administrator also chooses an identifier for the P1 encryptor running on your Raspberry.
This identifier consists of p1e- followed by some hexadecimal digits 0-9a-f, for example p1e-1a005 .
It will be used in the names and in the content of the log files of your P1 encryptor program.

#### VPN address

Finally, the administrator chooses the IP address of your Raspberry on the virtual private network (VPN) formed by the wireguard tunnels for the Privacy Samen system.
As mentioned above, this is currently an IP4 address taken from **10.1.0.0/16**.


### Adding your Raspberry to the Privacy Samen system

The administrator then updates the Privacy Samen system with these steps:

1. Add your public ssl key and your fake EAN numbers
   to the configuration files of the keyadder-rs and aggrmeters-rs programs.

   This will allow your digital signatures to checked. 

2. Put your public wireguard key and the VPN address your Raspberry into the wireguard Peer info
   of the hosts running the keyadder-rs and the aggrmeters-rs programs.

   This will allow your raspberry to connect safely to the hosts of the Privacy Samen system.

3. Put your p1e- identifier into your log configuration file.


Finally the administrator sends your configuration files by email in reply to your request email.


## Set up the wireguard tunnels to the key adder host and the aggregator host

The wgenmprv0.conf file still needs your private wireguard key.

The wgenmprv0.conf file looks like this, the actual values after the = signs will be different.
The two [Peer] sections in there are to connect to the key adder, and to the aggregator.
Please be carefull with the actual Endpoint addresses: keep them private.
```
[Interface]
Address = 10.1.0.3/32
PrivateKey = 

[Peer]
PublicKey = eyPsXC/Z6cTB7FL/Hkjydn1TLm9W9g96uKkJzU38eQ8=
AllowedIps= 10.1.0.5/32
Endpoint = ipaddress:port

[Peer]
PublicKey = sqIOYhBKPiOAkPYHAElCWwUqlu4wTjigIYzt6CjUy3A=
AllowedIPs = 10.1.0.6/32
Endpoint = ipaddress:port
```
Insert the characters of your private wireguard key from the wgprivkey just after
the line, leaving one space after the = sign:
```
PrivateKey = 
```

Now the wg tunnels can be started:
```
wg-quick up ./wgenmprv0.conf
```

The tunnels are working normally when both hosts can be pinged through them:
```
...@raspy2:~ $ ping 10.1.0.5
PING 10.1.0.5 (10.1.0.5) 56(84) bytes of data.
64 bytes from 10.1.0.5: icmp_seq=1 ttl=64 time=20.1 ms
64 bytes from 10.1.0.5: icmp_seq=2 ttl=64 time=17.3 ms
...
^C
```
and similar for the example aggregator at 10.1.0.6.
```
...@raspy2:~ $ ping 10.1.0.6
PING 10.1.0.6 (10.1.0.6) 56(84) bytes of data.
64 bytes from 10.1.0.6: icmp_seq=1 ttl=64 time=28.1 ms
...
^C
...
```

Then prepare for automatic startup at boot:
```
sudo cp wgenmprv0.conf /etc/wireguard/
```

Stop the tunnel to prepare for restart as system service:

```
wg-quick down ./wgenmprv0.conf
```
Restart as system service:

```
sudo systemctl restart wg-quick@wgenmprv0.service
```
Enable at boot:
```
sudo systemctl enable wg-quick@wgenmprv0.service
```
Show the status:
```
sudo systemctl status wg-quick@wgenmprv0.service
```

## Run the P1 encryptor program as a system service

Copy the latest build p1encrypt-rs program to directory ~mmenc/p1encrypt-rs/
```
sudo su mmenc
# as mmenc user:
cd ~/enmprv/target/release
cp p1encrypt-rs ~/p1encrypt-rs/
cd ~/p1encrypt-rs/

# Check the contents of the p1encrypt-rs/ directory:
...@raspy2:~/p1encrypt-rs $ ls
log  p1ebglog4rs.yaml  p1encrypt-rs  p1encrypt-rs.service  p1encrypt.toml
^D # end the su mmenc session
```
All the above listed directory entries should be there.


Add the P1 encryptor program as a system service:
```
cd ~mmenc/p1encrypt-rs/
sudo ln --force p1encrypt-rs.service /etc/systemd/system
sudo systemctl daemon-reload
```
Start the P1 encryptor program as a systemctl service, and make it start at boot:
```
sudo systemctl start p1encrypt-rs.service
sudo systemctl enable p1encrypt-rs.service
```

Check the log output of the running P1 encryptor program:

```
cd ~mmenc/p1encrypt-rs/log/
ls -t # use the log file that is listed first:
tail -f p1e-1a005.log
# here p1e-1a005 is the identifier of the P1 encryptor program.
```
The log output for starting a single P1 telegram looks like this:
```
2023-12-03T20:17:23.224Z p1e-1a005 TRACE p1encrypt_rs::p1input:864 starting p1 telegram, utc_time_stamp: 2023-12-03T20:17:23.224Z
2023-12-03T20:17:23.608Z p1e-1a005 INFO p1encrypt_rs::p1input:242  67188.750 el.kWh.to at 2023-12-03T20:17:23.224Z
2023-12-03T20:17:23.609Z p1e-1a005 INFO p1encrypt_rs::p1input:242   9559.328 el.kWh.by at 2023-12-03T20:17:23.224Z
2023-12-03T20:17:23.609Z p1e-1a005 INFO p1encrypt_rs::p1input:242      0.730  el.kW.to at 2023-12-03T20:17:23.224Z
2023-12-03T20:17:23.609Z p1e-1a005 INFO p1encrypt_rs::p1input:242      0.000  el.kW.by at 2023-12-03T20:17:23.224Z
2023-12-03T20:17:23.609Z p1e-1a005 INFO p1encrypt_rs::p1input:242  20591.943  ng.nl.m3 at 2023-12-03T20:00:00.000Z
```

A bit further down the reply from the aggregator is shown.
The first line ends with a dot, which indicates that the last encrypted P1 telegram was received correctly by the aggregator.
The log file then shows the rest of the reply from the aggregator, which is the last aggregation result in which the program p1e-1a005 took part:

```
2023-12-03T20:17:23.690Z p1e-1a005 INFO p1encrypt_rs::p1aggr:340 .
ean: aggr_01
mutc: 2023-12-03T20:17:15.889Z
num_msrmnts: 2
total_delay_ms: 9584
el.kWh.to: 94489.607
el.kWh.by: 9559.332
el.kW.to: 1.440
el.kW.by: 0.000
total_delay_ms: 2071778
ng.nl.m3: 29827.266
```

In these lines, the symbols have the following meaning:
- **ean:**  the name of aggregated group
- **mutc:**  the UTC time of the aggregation (here about 8 seconds after the UTC time of the reply to the P1 encryptor)
- **num_msrmnts:**  the number of aggregated measurements, i.e. the number of smart meters involved
- **total_delay_ms** total delay of the measurements following it
- **el.kWh.to:** the total cumulative electric energy delivered to the smart meters, idem for **.by**,
                (low and normal tariff are reported together)
- **el.kW.to:** the total electric power delivered to the smart meters, idem for **.by**
- **ng.nl.m3:** total natural gas m3 delivered to the meters.

For the electricity measurements, the avarage delay at the moment the aggregation was done
is the **total_delay_ms** divided by the **num_msrmnts**.
In this case the avarage delay for the electricity measurements is almost 5 seconds.

The total gas measurement has a larger delay (average 2071.778/2 = 1036 seconds) 
because the gas measurements change only once per hour.
They are reported in the P1 telegrams with their original measurement times.
