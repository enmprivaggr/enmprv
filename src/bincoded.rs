use std::{
    collections::HashSet,
    fmt::{self, Display},
    hash::Hash,
    iter::repeat_with,
    slice::Iter,
};

use rand::random;
use serde::{Deserialize, Serialize};

use crate::{measuredvalue::MeasuredValue, openssl::PubKeyHash, util::hash_u32};

const BIN_CODE_SIZE_LIMIT: usize = 40000;

#[must_use]
pub fn get_bincode_config() -> bincode::config::Configuration<
    bincode::config::LittleEndian,
    bincode::config::Varint,
    bincode::config::Limit<40000>,
> {
    bincode::config::legacy()
        .with_variable_int_encoding()
        .with_limit::<BIN_CODE_SIZE_LIMIT>()
}

/// Entities measured by a smart meter
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Eq, Hash)]
#[allow(non_camel_case_types)]
pub enum P1MeasurementType {
    // Only change by appending (unless using MeasurementType only in a new db).
    NgNl_m3, // Natural Gas, NL, cumulative, 9.769 kWh per normalized m3, caloric value also via https://www.eancodeboek.nl/
    El_kW_to, // electric, kW power to meter from the grid, non cumulative
    El_kW_by, // electric, kW power by meter to the grid, non cumulative
    El_kWh_to_tariff_1, // cumulative, delivered to the meter by the grid
    El_kWh_to_tariff_2, // cumulative, delivered to the meter by the grid
    El_kWh_by_tariff_1, // cumulative, delivered by the meter to the grid
    El_kWh_by_tariff_2, // cumulative, delivered by the meter to the grid
    El_voltage_l1, // non cumulative, line 1
    El_voltage_l2,
    El_voltage_l3,
    El_current_l1,
    El_current_l2,
    El_current_l3,
    // Only change MeasurementType by appending here
}

impl P1MeasurementType {
    const VALUES: [Self; 13] = [
        Self::NgNl_m3,
        Self::El_kW_to,
        Self::El_kW_by,
        Self::El_kWh_to_tariff_1,
        Self::El_kWh_to_tariff_2,
        Self::El_kWh_by_tariff_1,
        Self::El_kWh_by_tariff_2,
        Self::El_voltage_l1,
        Self::El_voltage_l2,
        Self::El_voltage_l3,
        Self::El_current_l1,
        Self::El_current_l2,
        Self::El_current_l3,
    ];

    pub fn iter() -> Iter<'static, Self> {
        Self::VALUES.iter()
    }

    pub fn is_cumulative(&self) -> bool {
        use P1MeasurementType::*;
        match self {
            NgNl_m3 | El_kWh_to_tariff_1 | El_kWh_to_tariff_2 | El_kWh_by_tariff_1
            | El_kWh_by_tariff_2 => true,
            El_kW_to | El_kW_by | El_voltage_l1 | El_voltage_l2 | El_voltage_l3 | El_current_l1
            | El_current_l2 | El_current_l3 => false,
        }
    }
}

/// Vector element sent from p1encryptor to aggregator to avoid repetitive measurement fields.
#[derive(Serialize, Deserialize, Debug)]
pub enum MeasurementInfo {
    MeterSerialNr(String),
    Mutc(String),
    EncryptedValue(P1MeasurementType, MeasuredValue), // measurement type and encrypted value.
    DirectValue(P1MeasurementType, MeasuredValue),    // measurement type and value.
}

/// Aggregated measurement type
#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
#[allow(non_camel_case_types)]
pub enum AggrMeasurementType {
    El_kW_to_aggr,
    El_kW_by_aggr,
    NgNl_m3_aggr,
    El_kWh_to_tariff_1_aggr,
    El_kWh_by_tariff_1_aggr,
    El_kWh_to_tariff_2_aggr,
    El_kWh_by_tariff_2_aggr,
    // CHECKME: should collective self consumption that needs an interval be defined here?
    El_kW_coll,
    El_kWh_tariff_1_coll,
    El_kWh_tariff_2_coll,
}

impl AggrMeasurementType {
    const VALUES: [Self; 10] = [
        Self::El_kW_to_aggr,
        Self::El_kW_by_aggr,
        Self::El_kW_coll,
        Self::NgNl_m3_aggr,
        Self::El_kWh_to_tariff_1_aggr,
        Self::El_kWh_by_tariff_1_aggr,
        Self::El_kWh_tariff_1_coll,
        Self::El_kWh_to_tariff_2_aggr,
        Self::El_kWh_by_tariff_2_aggr,
        Self::El_kWh_tariff_2_coll,
    ];

    pub fn iter() -> Iter<'static, Self> {
        Self::VALUES.iter()
    }
}

/// Aggregate a measurement type without self consumption
pub fn aggr_msmrnt_type(msrmnt_type: P1MeasurementType) -> Option<AggrMeasurementType> {
    use AggrMeasurementType::*;
    use P1MeasurementType::*;
    Some(match msrmnt_type {
        El_kW_to => El_kW_to_aggr,
        El_kW_by => El_kW_by_aggr,
        NgNl_m3 => NgNl_m3_aggr,
        El_kWh_to_tariff_1 => El_kWh_to_tariff_1_aggr,
        El_kWh_to_tariff_2 => El_kWh_to_tariff_2_aggr,
        El_kWh_by_tariff_1 => El_kWh_by_tariff_1_aggr,
        El_kWh_by_tariff_2 => El_kWh_by_tariff_2_aggr,
        _ => return None,
    })
}

#[derive(Serialize, Deserialize, Debug)]
pub struct MeterPubKeyHashMeasTimeTypes {
    pub meter_pkh: PubKeyHash,
    pub meas_dt_stamp: String,
    pub msrmnt_types: HashSet<P1MeasurementType>,
}

#[must_use]
pub fn slice_u8_to_string(u: &[u8]) -> String {
    String::from_utf8(u.to_owned()).unwrap_or_else(|_e| format!("{u:?}"))
}

impl Display for MeterPubKeyHashMeasTimeTypes {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "MeterPubKeyHashMeasTimeTypes({}, meas_dt {}, ({}))",
            &self.meter_pkh,
            &self.meas_dt_stamp,
            self.msrmnt_types
                .iter() // CHECKME: sort first, or use BTreeSet ?
                .map(|mt| format!("{mt:?}"))
                .collect::<Vec<_>>()
                .join(", "),
        )
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum IncrementalRequest {
    Intermediate, // start a request, or add to a started request, do not expect a reply.
    Final,        // add to an earlier request, if any, and expect a reply
}

// Request from aggregator to key adder:
#[derive(Serialize, Deserialize, Debug)]
pub struct EncKeyAddRequest {
    pub request_stage: IncrementalRequest,
    pub meter_pkh_meas_dt_types_vec: Vec<MeterPubKeyHashMeasTimeTypes>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum EncryptionResult {
    PrivacyViolation,
    TotalEncryptionKey(MeasuredValue),
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MeasurementTypeTotalEncKey {
    pub msrmnt_type: P1MeasurementType,
    pub total_encryption_key: EncryptionResult,
    pub total_measurements: u64,
    pub refused_meter_set_pkhs: Vec<PubKeyHash>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EncKeyAddReply {
    pub msrmnt_type_total_key_vec: Vec<MeasurementTypeTotalEncKey>,
    pub meter_pkhs_no_encryption_info: Vec<PubKeyHash>,
}

//  Measurement time interval for the shared encryption seed.
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct ShareSeedInterval {
    pub not_before_dt_stamp: String, // (normally 24 bytes) utc string, always with millisecs and trailing Z
    pub not_after_dt_stamp: String,  // idem,
}

// Interval borders should be expressed in millisecond resolution,
// these interval borders are normally provided by util::time_stamp_to_string().
// Since not_before_dt and not_after_dt include themselves in the intervals,
// these intervals should be seperated by at least 1 millisecond.
pub const INTERVAL_SEPARATOR_MILLIS: i64 = 1;

impl Display for ShareSeedInterval {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let r = format!(
            "not_before_dt {}, not_after_dt {}",
            &self.not_before_dt_stamp, &self.not_after_dt_stamp,
        );
        write!(f, "ShareSeedInterval({r})")
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct SharedSeed(pub Vec<u8>); // normally 32 bytes, 256 bits, randomly generated by the requestor.

impl SharedSeed {
    pub fn new(seed_size: usize) -> Self {
        Self(random_seed(seed_size))
    }
}

fn random_seed(num_bytes: usize) -> Vec<u8> {
    // random::<u8>() may involve a delay until randomness is available.
    repeat_with(random).take(num_bytes).collect()
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct ShareSeedEncryptionInfo {
    pub hash_function_name: String, // bytes of name string, for now "blake2s256" (February 2023).
    pub seed: SharedSeed,
    pub ten_pow_modulus: u16, // modulus of 10^ten_pow_modulus for max encryption key.
    pub ten_pow_divisor: u16, // encryption key after modulus is divided by 10^ten_pow_divisor.
}

impl Display for SharedSeed {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let r = format!("{:08X}", hash_u32(&self.0));
        write!(f, "SharedSeed({r})")
    }
}

impl Display for ShareSeedEncryptionInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let r = format!(
            "hash_function name {}, {}, ten_pow_modulus {}, ten_pow_divisor {}",
            &self.hash_function_name, &self.seed, self.ten_pow_modulus, self.ten_pow_divisor,
        );
        write!(f, "ShareSeedEncryptionInfo({r})")
    }
}

// Request from p1encryptor to key adder to share an encryption seed for encryption of measured values.
// The encryption of a measurement uses the seed, and the meter serial nr, the type,
// the time and the value of the measurement.
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct ShareSeedRequest {
    pub meter_pkh: PubKeyHash,
    pub interval: ShareSeedInterval,
    pub encryption_info: ShareSeedEncryptionInfo,
}

impl Display for ShareSeedRequest {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "ShareSeedRequest(meter {}, {}, {})",
            &self.meter_pkh, self.interval, self.encryption_info,
        )
    }
}

// The reply to a request to share an encryption seed for measured values.
// Some general error conditions:
// - The meter serial nr is not allowed by the key adder.
// - The interval length is too short or too long.
// - The interval ends too far into the past, or it starts too far into the future.
// - Unreasonable/unknown values for seed, hash_function, ten_pow_modulus or ten_pow_divisor.
// Some conditions involving the same meter serial nr in an earlier request:
// - A duplicate of an earlier valid request is allowed.
// - The requested interval overlaps with an earlier requested interval
//   and some measurements have already been encrypted using the earlier seed.
//   This is an error, the request is denied.
// - The requested interval overlaps with an earlier requested interval, and
//   no measurements have already been encrypted in the currently requested interval.
//   In this case a warning is given to indicate that the validity period of the earlier,
//   now overlapping, request is reduced to a millisecond before the current request.
// - The requested interval starts more than 1 millisecond after the latest interval ends.
//   In this case a warning is given about the gap between these intervals.
#[derive(Serialize, Deserialize, Debug)]
pub struct ShareSeedReply {
    pub message: Option<String>,
}

/// To avoid repetitive fields in a vector of AggregatedMeasurementsAnonymous
#[derive(Serialize, Deserialize, Debug)]
pub enum AggrAnonMeasurementInfo {
    AggrId(String),
    AggrUtc(String),
    TotalDelayMs(i64),
    NumAggregated(u64),
    AggrValue(AggrMeasurementType, MeasuredValue),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum AggrReplyToTelegram {
    NotVerified, // p1 telegram
    LastAggregationWithMeterSerialNr(Vec<AggrAnonMeasurementInfo>),
    InternalError, // internal queue full, locked on last aggregation result poisoned.
}

#[cfg(test)]
mod tests {
    use super::P1MeasurementType;
    use anyhow::anyhow;

    impl TryFrom<usize> for P1MeasurementType {
        type Error = anyhow::Error;
        fn try_from(value: usize) -> Result<Self, Self::Error> {
            match Self::VALUES.get(value) {
                None => Err(anyhow!("MeasurementType usize too big {value}")),
                Some(mt) => Ok(*mt),
            }
        }
    }

    #[test]
    fn test_mt_usize_consistency() {
        use P1MeasurementType::*;

        const VALUES_IN_DB: [P1MeasurementType; 13] = [
            // copy of MeasurementType::VALUES to simulate mt_usize() values in a db
            NgNl_m3,
            El_kW_to,
            El_kW_by,
            El_kWh_to_tariff_1,
            El_kWh_to_tariff_2,
            El_kWh_by_tariff_1,
            El_kWh_by_tariff_2,
            El_voltage_l1,
            El_voltage_l2,
            El_voltage_l3,
            El_current_l1,
            El_current_l2,
            El_current_l3,
        ];

        assert_eq!(P1MeasurementType::iter().len(), VALUES_IN_DB.iter().len());
        for (mt_new, mt_old) in P1MeasurementType::iter().zip(VALUES_IN_DB.iter()) {
            assert_eq!(mt_new, mt_old);
        }
    }
}
