use std::{
    fmt::{self, Display, Formatter},
    time::Duration,
};

use chrono::TimeDelta;
use serde::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct DurationConfig {
    // Non negative duration in seconds. Only u16 and u8 fields, default 0.
    weeks: Option<u16>, // about 1260 years
    days: Option<u8>,
    hours: Option<u8>,
    minutes: Option<u8>,
    seconds: Option<u8>,
}

impl Display for DurationConfig {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut rv = Vec::<String>::new();
        if self.weeks() > 0 {
            rv.push(format!("weeks = {}", self.weeks()));
        }
        if self.days() > 0 {
            rv.push(format!("days = {}", self.days()));
        }
        if self.hours() > 0 {
            rv.push(format!("hours = {}", self.hours()));
        }
        if self.minutes() > 0 {
            rv.push(format!("minutes = {}", self.minutes()));
        }
        if self.seconds() > 0 || rv.is_empty() {
            rv.push(format!("seconds = {}", self.seconds()));
        }
        write!(f, "{{{}}}", rv.join(", "))
    }
}

impl DurationConfig {
    fn weeks(&self) -> u16 {
        self.weeks.unwrap_or(0)
    }

    fn days(&self) -> u8 {
        self.days.unwrap_or(0)
    }

    fn hours(&self) -> u8 {
        self.hours.unwrap_or(0)
    }

    fn minutes(&self) -> u8 {
        self.minutes.unwrap_or(0)
    }

    fn seconds(&self) -> u8 {
        self.seconds.unwrap_or(0)
    }

    #[must_use]
    pub fn std_duration(&self) -> Duration {
        // max Duration is u64::MAX seconds, about 584542 million years.
        Duration::from_secs(
            self.seconds() as u64
                + 60 * (self.minutes() as u64
                    + 60 * (self.hours() as u64
                        + 24 * (self.days() as u64 + 7 * self.weeks() as u64))),
        )
    }

    #[must_use]
    pub fn time_delta(&self) -> TimeDelta {
        // chrono::time_delta::MAX is i64::MAX seconds, half of u64::MAX.
        TimeDelta::weeks(self.weeks().into())
            + TimeDelta::days(self.days().into())
            + TimeDelta::hours(self.hours().into())
            + TimeDelta::minutes(self.minutes().into())
            + TimeDelta::seconds(self.seconds().into())
    }
}
