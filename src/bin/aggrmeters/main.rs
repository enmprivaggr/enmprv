use std::{
    collections::{BTreeMap, HashMap, hash_map},
    env,
    fs::File,
    io::Read,
    process::exit,
    sync::{Arc, Mutex, mpsc},
    thread,
};

use anyhow::{Error, Result, anyhow};
use log::{error, info, trace, warn};
use serde::Deserialize;

use enmprv::{
    checkclocksync::RepeatCheckClockSync,
    durationconfig::DurationConfig,
    measurement::{MeasurementTime, P1Measurement},
    openssl::{OpenSslSigVerifier, OpenSslSigner, OpenSslSignerFiles, PubKeyHash},
    util::{IpAddressPortConfig, RunState},
};

use crate::aggrka::AggrMeasurement;

mod aggrka;
mod aggrp1;

// Measurements from a message from a p1encryptor:
#[derive(Debug, Clone)]
pub(crate) struct MeasurementsMessage {
    pub msrmnts_non_enc: Vec<P1Measurement>,
    pub msrmnts_enc: Vec<P1Measurement>,
    pub total_bytes: usize,
}

impl MeasurementsMessage {
    fn total_num_msrmnts(&self) -> usize {
        self.msrmnts_non_enc.len() + self.msrmnts_enc.len()
    }
}

// Message on queue from aggregator p1 input and interval creator to aggregator key adder interface:
pub(crate) enum AggregationMessage {
    EndInterval,
    Measurements(MeasurementsMessage),
}

const CONFIG_FILE_NAME: &str = "aggrmeters.toml";
const LOG_CONFIG_FILE_NAME: &str = "agrmlog4rs.yaml";

#[derive(Deserialize, Debug)]
pub struct PrivAggrConfig {
    wait_clock_sync: DurationConfig,
    repeat_wait_clock_sync: DurationConfig,
    p1_encryptors_local_ip: IpAddressPortConfig,
    ten_pow_max_decrypt: u16,
    aggregator_id: String,
    key_adder_remote_ip: IpAddressPortConfig,
    max_msrmnts_key_add_request: u64,
    max_keep_eq_ids: u64,
    aggregation_interval: DurationConfig,
    max_secs_ahead: i64,
    key_adder_receive_timeout: DurationConfig,
    max_queue_size: u64,
    key_adder_public_key: String,
    temp_dir: String,
    meter_sets_sig_verifiers: BTreeMap<String, MeterSetVerifierConfig>,
    ssl_signer_files: OpenSslSignerFiles,
}

#[derive(Deserialize, Debug)]
pub struct MeterSetVerifierConfig {
    electricity: String, // meter serial nr
    gas: Option<String>, // idem
    public_key_file_name: String,
}

fn pkh_and_verifier_by_meter_serial_nr_from_config(
    config: &PrivAggrConfig,
) -> Result<HashMap<String, (PubKeyHash, Arc<OpenSslSigVerifier>)>> {
    use hash_map::Entry::*;
    let mut result_ok = HashMap::<String, (PubKeyHash, Arc<OpenSslSigVerifier>)>::new();
    let mut config_name_by_pkh = HashMap::new();
    for (config_name, meter_set_verifier) in &config.meter_sets_sig_verifiers {
        let ssl_sig_verifier =
            OpenSslSigVerifier::new(&meter_set_verifier.public_key_file_name, &config.temp_dir)?;
        let pkh = ssl_sig_verifier.pub_key_hash()?;
        match config_name_by_pkh.entry(pkh.clone()) {
            Vacant(c_ve) => {
                c_ve.insert(config_name);
            }
            Occupied(c_oe) => {
                return Err(anyhow!(
                    "non unique public key hash for {} and {config_name}",
                    c_oe.get()
                ));
            }
        }
        let ssl_sig_verifier_rc = Arc::new(ssl_sig_verifier);
        let mut meter_serial_nrs = vec![&meter_set_verifier.electricity];
        meter_serial_nrs.extend(&meter_set_verifier.gas);
        trace!(
            "config {config_name} {pkh} of {} for {}",
            meter_set_verifier.public_key_file_name,
            meter_serial_nrs
                .iter()
                .map(|&s| s.to_owned())
                .collect::<Vec<_>>()
                .join(" ")
        );
        for meter_serial_nr in meter_serial_nrs {
            match result_ok.entry(meter_serial_nr.to_owned()) {
                Vacant(r_ve) => {
                    r_ve.insert((pkh.to_owned(), ssl_sig_verifier_rc.clone()));
                }
                Occupied(_) => {
                    return Err(anyhow!(
                        "non unique meter serial nr {} at {}",
                        meter_serial_nr,
                        config_name,
                    ));
                }
            }
        }
    }
    Ok(result_ok)
}

fn usage_exit(exit_code: i32) -> ! {
    eprintln!("argument: -v");
    eprintln!("  show invocation and version number");
    eprintln!("arguments: [config_file [log_config_file]]");
    eprintln!("  config_file:     default {CONFIG_FILE_NAME}");
    eprintln!("  log_config_file: default {LOG_CONFIG_FILE_NAME}");
    exit(exit_code)
}

fn error_exit(e: Error) -> ! {
    error!("{e:#}, exiting");
    exit(1)
}

fn main() {
    let mut args = env::args();
    let binary_name = args.next().unwrap_or("meter aggregator".to_owned());
    let config_file_name = args.next().unwrap_or_else(|| CONFIG_FILE_NAME.to_string());
    let version = env!("CARGO_PKG_VERSION");
    if config_file_name == "-v" {
        println!("{binary_name} version {version}");
        exit(0);
    }
    if config_file_name == "-h" {
        usage_exit(0);
    }
    let log_config_file_name = args
        .next()
        .unwrap_or_else(|| LOG_CONFIG_FILE_NAME.to_string());
    if args.next().is_some() {
        usage_exit(1);
    }

    log4rs::init_file(
        &log_config_file_name,
        log4rs::config::Deserializers::default(),
    )
    .unwrap_or_else(|e| {
        eprintln!("{e:?} at log4rs::init_file {log_config_file_name}, exiting");
        exit(1);
    });
    info!(
        "starting meter aggregator version {version} as {binary_name} user {} pid {}",
        std::env::var("USER").unwrap_or("".to_owned()),
        std::process::id()
    );
    let run_state = match RunState::new() {
        Err(e) => error_exit(e.context("Runstate::new()")),
        Ok(r) => r,
    };
    run_state.start_exit_thread();

    let mut toml_buf = Vec::<u8>::new();
    {
        let mut config_file = File::open(&config_file_name).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(format!("File::open {config_file_name}")));
        });
        let _ = config_file.read_to_end(&mut toml_buf).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(format!("read_to_end {config_file_name}")));
        });
    }
    let toml_str = std::str::from_utf8(&toml_buf).unwrap_or_else(|e| {
        error_exit(Error::new(e).context(config_file_name.to_owned()));
    });
    let config = Arc::new(
        toml::from_str::<PrivAggrConfig>(toml_str).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(config_file_name));
        }),
    );

    let aggr_signer = Arc::new(
        OpenSslSigner::new(&config.ssl_signer_files, &config.temp_dir).unwrap_or_else(|e| {
            error_exit(e.context("OpenSslSigner::new()"));
        }),
    );

    let pkh_and_verifier_by_meter_serial_nr =
        match pkh_and_verifier_by_meter_serial_nr_from_config(&config) {
            Ok(r) => Arc::new(r),
            Err(e) => error_exit(e.context("pkh_and_verifier_by_meter_serial_nr_from_config")),
        };

    let keyadder_sig_verifier =
        OpenSslSigVerifier::new(&config.key_adder_public_key, &config.temp_dir).unwrap_or_else(
            |e| {
                error_exit(e.context("open_ssl_sig_verifier() for keyadder_sig_verifier"));
            },
        );

    let aggregation_interval = config.aggregation_interval.std_duration();
    if aggregation_interval.as_secs() == 0 {
        error_exit(anyhow!("zero aggregation interval, too small"));
    }

    let key_adder_socket_addr = match config.key_adder_remote_ip.socket_address() {
        Err(e) => {
            error_exit(anyhow!(
                "{e:?} at socket_address {:?}",
                &config.key_adder_remote_ip
            ));
        }
        Ok(socket_addr) => socket_addr,
    };

    let repeat_check_clock_sync = match RepeatCheckClockSync::new(
        config.wait_clock_sync.std_duration(),
        config.repeat_wait_clock_sync.std_duration(),
    ) {
        Err(e) => {
            error_exit(e.context("RepeatCheckClockSync::new"));
        }
        Ok(rccs) => rccs,
    };

    let last_aggr_result = Arc::<Mutex<Vec<AggrMeasurement>>>::new(Mutex::new(Vec::new()));

    let (sender_mkvs, receiver_msrmnt_msgs) =
        mpsc::sync_channel::<AggregationMessage>(config.max_queue_size as usize);
    let sender_intervals = sender_mkvs.clone();

    let config_clone = Arc::clone(&config);
    let last_aggr_result_clone = Arc::clone(&last_aggr_result);
    let pkh_and_verifier_by_meter_serial_nr_clone =
        Arc::clone(&pkh_and_verifier_by_meter_serial_nr);

    let send_thread = thread::spawn(move || {
        if let Err(e) = aggrp1::verify_msgs_from_sock_to_reply_and_queue(
            &config_clone,
            &pkh_and_verifier_by_meter_serial_nr_clone,
            &sender_mkvs,
            &last_aggr_result_clone,
        ) {
            error_exit(e.context("aggrp1::verify_msgs_from_sock_to_queue"));
        }
    });

    let interval_thread = thread::spawn(move || {
        loop {
            thread::sleep(aggregation_interval);
            if let Err(queue_send_err) = sender_intervals.try_send(AggregationMessage::EndInterval)
            {
                match queue_send_err {
                    mpsc::TrySendError::Full(_full_error) => {
                        warn!("queue full_error, no aggregation interval ending");
                    }
                    mpsc::TrySendError::Disconnected(ref _option_mkvs) => {
                        error_exit(Error::new(queue_send_err).context("aggregation queue"));
                    }
                }
            }
            if let Err(e) = repeat_check_clock_sync.recheck_wait() {
                error_exit(anyhow!("{e:?} after aggregation interval"));
            }
        }
    });

    let config_clone = Arc::clone(&config);
    let pkh_and_verifier_by_meter_serial_nr_clone =
        Arc::clone(&pkh_and_verifier_by_meter_serial_nr);
    if let Err(e) = aggrka::aggr_msrmnts_from_queue(
        &config_clone,
        &receiver_msrmnt_msgs,
        &pkh_and_verifier_by_meter_serial_nr_clone,
        &aggr_signer,
        &key_adder_socket_addr,
        &keyadder_sig_verifier,
        &Arc::clone(&last_aggr_result),
    ) {
        error_exit(e.context("aggr_msrmnts_from_queue"));
    }

    if let Err(send_thread_join_error) = send_thread.join() {
        error_exit(anyhow!("send_thread_join_error {send_thread_join_error:?}"));
    }

    let Err(interval_thread_join_error) = interval_thread.join();
    error_exit(anyhow!(
        "interval_thread_join_error {interval_thread_join_error:?}"
    ));
}
