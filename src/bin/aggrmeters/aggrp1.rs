use std::{
    collections::{HashMap, HashSet},
    io::ErrorKind,
    net::{SocketAddr, UdpSocket},
    sync::{Arc, Mutex, mpsc},
    thread,
    time::{self, Duration},
};

use anyhow::{Error, Result, anyhow};
use bincode::serde::decode_from_slice;
use chrono::{DateTime, Utc};
use log::{error, warn};

use enmprv::{
    bincoded::{
        AggrAnonMeasurementInfo, AggrReplyToTelegram, MeasurementInfo, P1MeasurementType,
        get_bincode_config,
    },
    openssl::{OpenSslSigVerifier, OpenSslSigner, PubKeyHash},
    util::{
        dt_utc_from_str, dt_utc_to_string, ip_addr_in_wg_local_intf, time_stamp_milli_secs,
        udp_bind,
    },
};

use crate::{
    AggregationMessage, MeasurementTime, MeasurementsMessage, P1Measurement, PrivAggrConfig,
    aggrka::{AggrAnonMeasurement, AggrMeasurement},
};

struct ReceivedMessage<'a> {
    received_dt: DateTime<Utc>,
    src_addr_port: SocketAddr,
    num_bytes_received: usize,
    receive_buffer: &'a [u8],
}

fn sig_verifier_for_meters(
    meter_nrs: &HashSet<String>,
    sig_verifier_by_meter_nr: &HashMap<&String, Arc<OpenSslSigVerifier>>,
) -> Result<Arc<OpenSslSigVerifier>> {
    // There may be only one signature verifier for the meter serial nrs in a message.
    let mut sig_verifier_opt: Option<Arc<OpenSslSigVerifier>> = None;
    for meter_nr in meter_nrs {
        if let Some(sig_verifier_for_meter) = sig_verifier_by_meter_nr.get(meter_nr) {
            if let Some(prev_sig_verifier) = sig_verifier_opt.take() {
                if &prev_sig_verifier != sig_verifier_for_meter {
                    return Err(anyhow!("multiple signature verifiers for {meter_nr}",));
                } else {
                    sig_verifier_opt = Some(prev_sig_verifier);
                }
            } else {
                sig_verifier_opt = Some(sig_verifier_for_meter.clone());
            }
        } else {
            return Err(anyhow!("no signature verifier for {meter_nr}"));
        }
    }
    let Some(sig_verifier) = sig_verifier_opt.take() else {
        return Err(anyhow!("no signature verifier for {meter_nrs:?}"));
    };
    Ok(sig_verifier)
}

impl<'a> ReceivedMessage<'a> {
    fn new(
        received_dt: DateTime<Utc>,
        src_addr_port: SocketAddr,
        num_bytes_received: usize,
        receive_buffer: &'a [u8],
    ) -> Self {
        ReceivedMessage {
            received_dt,
            src_addr_port,
            num_bytes_received,
            receive_buffer,
        }
    }

    fn decode_and_verify(
        &self,
        sig_verifier_by_meter_nr: &HashMap<&String, Arc<OpenSslSigVerifier>>,
        msrmnt_max_secs_ahead: i64, // verify measurements not too far in the future.
    ) -> Result<MeasurementsMessage> {
        const NUM_VERSION_BYTES: usize = 2;
        if self.num_bytes_received < NUM_VERSION_BYTES + 6 {
            return Err(anyhow!(
                "message too short: {:?}",
                &self.receive_buffer[..self.num_bytes_received]
            ));
        }
        let received_data = &self.receive_buffer[..self.num_bytes_received][NUM_VERSION_BYTES..];
        match self.receive_buffer[..NUM_VERSION_BYTES] {
            [0, 0] => {
                self.decode_and_verify_0(
                    received_data,
                    sig_verifier_by_meter_nr,
                    msrmnt_max_secs_ahead, // verify measurements not too far in the future.
                )
            }
            _ => Err(anyhow!(
                "unknown message version {:?}",
                &self.receive_buffer[..NUM_VERSION_BYTES]
            )),
        }
    }

    fn decode_and_verify_0(
        &self,
        received_data: &[u8], // self.receive_buffer without version info
        sig_verifier_by_meter_nr: &HashMap<&String, Arc<OpenSslSigVerifier>>,
        msrmnt_max_secs_ahead: i64, // verify measurements not too far in the future.
    ) -> Result<MeasurementsMessage> {
        const NUM_SIZE_BYTES: usize = 2;
        if received_data.len() < NUM_SIZE_BYTES + 2 {
            return Err(anyhow!("received_data too short {received_data:?}"));
        }
        let msrmnt_infos_msg_size =
            (received_data[0] as usize) + ((received_data[1] as usize) << 8);
        if received_data.len() < msrmnt_infos_msg_size + 3 {
            // 2 bytes length, at least 1 byte signature.
            return Err(anyhow!(
                "received_data too long, total length: {}, msmrnt infos length: {msrmnt_infos_msg_size}",
                received_data.len()
            ));
        }
        let msrmnts_msg_bytes = &received_data[NUM_SIZE_BYTES..][..msrmnt_infos_msg_size];
        let signature_bytes = &received_data[NUM_SIZE_BYTES..][msrmnt_infos_msg_size..];
        let msrmnt_infos = match decode_from_slice::<Vec<MeasurementInfo>, _>(
            msrmnts_msg_bytes,
            get_bincode_config(),
        ) {
            Err(e) => {
                error!("{e:?}");
                return Err(e.into());
            }
            Ok((mis, size)) => {
                if size == msrmnts_msg_bytes.len() {
                    mis
                } else {
                    return Err(anyhow!(
                        "msrmnt_infos decode_from_slice incomplete, {} of {}",
                        size,
                        msrmnts_msg_bytes.len()
                    ));
                }
            }
        };

        let mut msrmnts_enc: Vec<P1Measurement> = Vec::new();
        let mut msrmnts: Vec<P1Measurement> = Vec::new();

        let mut last_serial_nr_opt: Option<String> = None;
        let mut last_mutc_opt: Option<MeasurementTime> = None;
        let mut meter_nrs = HashSet::<String>::new();
        let max_allowed_msmrnt_timestamp = self.received_dt.timestamp() + msrmnt_max_secs_ahead;

        for msrmnt_info in &msrmnt_infos {
            use MeasurementInfo::*;
            match msrmnt_info {
                MeterSerialNr(serial_nr) => {
                    last_serial_nr_opt = Some(serial_nr.to_string());
                    meter_nrs.insert(serial_nr.to_string()); // ignore duplicate meter serial nrs.
                }
                Mutc(time_stamp) => match dt_utc_from_str(time_stamp.as_str()) {
                    Err(e) => {
                        return Err(e.context(format!("time_stamp_from_str {time_stamp}")));
                    }
                    Ok(dt_utc) => {
                        if dt_utc.timestamp() > max_allowed_msmrnt_timestamp {
                            return Err(anyhow!(
                                "measurement time {dt_utc:?} more than {msrmnt_max_secs_ahead:?} secs after receive time {:?} (version 1)",
                                self.received_dt
                            ));
                        }
                        last_mutc_opt = Some(dt_utc);
                    }
                },
                EncryptedValue(msrmnt_type, encrypted_value) => {
                    let Some(ref serial_nr) = last_serial_nr_opt else {
                        return Err(anyhow!("missing serial nr before {msrmnt_info:?}"));
                    };
                    let Some(mutc) = last_mutc_opt else {
                        return Err(anyhow!("missing mutc before {msrmnt_info:?}"));
                    };
                    msrmnts_enc.push(P1Measurement {
                        meter_nr: serial_nr.to_owned(),
                        p1_msrmnt_type: *msrmnt_type,
                        dt_utc: mutc,
                        value: encrypted_value.clone(),
                    })
                }
                DirectValue(msrmnt_type, measured_value) => {
                    let Some(ref serial_nr) = last_serial_nr_opt else {
                        return Err(anyhow!("missing meter serial nr before {msrmnt_info:?}"));
                    };
                    let Some(mutc) = last_mutc_opt else {
                        return Err(anyhow!("missing mutc before {msrmnt_info:?}"));
                    };
                    msrmnts.push(P1Measurement {
                        meter_nr: serial_nr.to_owned(),
                        p1_msrmnt_type: *msrmnt_type,
                        dt_utc: mutc,
                        value: measured_value.clone(),
                    })
                }
            }
        }

        if msrmnts.is_empty() && msrmnts_enc.is_empty() {
            return Err(anyhow!("no measurements in measurement infos"));
        }

        check_unique_msrmnt_types(&msrmnts).map_err(|e| e.context("unencrypted"))?;
        check_unique_msrmnt_types(&msrmnts_enc).map_err(|e| e.context("encrypted"))?;

        let sig_verifier = sig_verifier_for_meters(&meter_nrs, sig_verifier_by_meter_nr)?;

        match sig_verifier.verify_signature(msrmnts_msg_bytes, signature_bytes) {
            Err(e) => Err(e.context("verify_signature")),
            Ok(verified) => {
                if verified {
                    Ok(MeasurementsMessage {
                        msrmnts_enc,
                        msrmnts_non_enc: msrmnts,
                        total_bytes: received_data.len() + 2, // include version bytes
                    })
                } else {
                    Err(anyhow!("incorrect signature"))
                }
            }
        }
    }
}

/// Check that the given measurements contain at most one measured value for a measurement type.
fn check_unique_msrmnt_types(msrmnts: &[P1Measurement]) -> Result<()> {
    let mut present_msrmnt_types = HashSet::<P1MeasurementType>::new();
    for msrmnt in msrmnts {
        if !present_msrmnt_types.insert(msrmnt.p1_msrmnt_type) {
            return Err(anyhow!(
                "measurement type {:?} present more than once",
                msrmnt.p1_msrmnt_type
            ));
        }
    }
    Ok(())
}

pub(crate) fn verify_msgs_from_sock_to_reply_and_queue(
    config: &PrivAggrConfig,
    pkh_and_verifier_by_meter_serial_nr: &HashMap<String, (PubKeyHash, Arc<OpenSslSigVerifier>)>,
    sync_sender: &mpsc::SyncSender<AggregationMessage>,
    last_aggr_result: &Mutex<Vec<AggrMeasurement>>,
) -> Result<()> {
    let local_addr_port = config.p1_encryptors_local_ip.addr_port_str()?;
    let max_secs_ahead = config.max_secs_ahead;
    let aggr_ssl_signer = OpenSslSigner::new(&config.ssl_signer_files, &config.temp_dir)?;
    let sig_verifier_by_meter_nr: HashMap<&String, Arc<OpenSslSigVerifier>> =
        HashMap::from_iter(pkh_and_verifier_by_meter_serial_nr.iter().map(
            |(meter_serial_nr, (_pkh, sig_verifier))| (meter_serial_nr, sig_verifier.clone()),
        ));

    loop {
        let p1_encryptors_udp_socket = match udp_bind(&local_addr_port) {
            Ok(sock) => sock,
            Err(e) => {
                error!("{e:#} at: udp_bind, sleeping one aggregation_interval");
                std::thread::sleep(config.aggregation_interval.std_duration());
                continue;
            }
        };
        if let Err(e) = p1_encryptors_udp_socket.set_read_timeout(Some(Duration::new(15, 0))) {
            return Err(Error::new(e).context("set_read_timeout"));
        }
        // check the local wg interface after each udp socket creation:
        match ip_addr_in_wg_local_intf(&config.p1_encryptors_local_ip.address) {
            Err(e) => {
                return Err(e.context(format!(
                    "ip_addr_in_wg_local_intf {}",
                    config.p1_encryptors_local_ip.address
                )));
            }
            Ok(res) => {
                if !res {
                    return Err(anyhow!(
                        "error: no local wireguard ip interface for {}",
                        config.p1_encryptors_local_ip.address
                    ));
                }
            }
        }

        loop {
            // receive udp messages as long as p1_encryptors_udp_socket works normally
            let mut receive_buffer = [0u8; 1024];
            match p1_encryptors_udp_socket.recv_from(&mut receive_buffer) {
                Err(sock_err) => {
                    match sock_err.kind() {
                        ErrorKind::WouldBlock => {
                            warn!("timeout at recv_from (is the wg tunnel up?)");
                            match p1_encryptors_udp_socket.take_error() {
                                Err(error) => error!("UdpSocket.take_error {error:?}"),
                                Ok(Some(error)) => error!("UdpSocket take_error Some {error:?}"),
                                Ok(..) => {} // normal timeout
                            }
                            break; // stop using p1_encryptors_udp_socket
                        }
                        ErrorKind::Interrupted => {
                            warn!("interrupted at recv_from (sleeping?), retrying after 60 secs");
                            thread::sleep(time::Duration::from_secs(60));
                        }
                        _ => {
                            return Err(Error::new(sock_err).context("recv_from"));
                        }
                    };
                }
                Ok((num_bytes_received, src_addr_port)) => {
                    let rcvd_msg = ReceivedMessage::new(
                        time_stamp_milli_secs(),
                        src_addr_port,
                        num_bytes_received,
                        &receive_buffer,
                    );
                    // CHECKME: keep a temporary list of src_addr_port's that failed signature verification
                    // so the packet can be dropped early?
                    if num_bytes_received == rcvd_msg.receive_buffer.len() {
                        warn!(
                            "message too long: {num_bytes_received} bytes from {src_addr_port:?} at {:?}, dropped",
                            rcvd_msg.received_dt
                        );
                    } else {
                        let reply_to_telegram: AggrReplyToTelegram;
                        match rcvd_msg.decode_and_verify(&sig_verifier_by_meter_nr, max_secs_ahead)
                        {
                            Err(e) => {
                                error!("{e:#} at decode_and_verify");
                                // reply V to p1 encryptor: decode_and_verify failed
                                reply_to_telegram = AggrReplyToTelegram::NotVerified;
                            }
                            Ok(msrmnts_msg) => {
                                if let Err(queue_send_err) = sync_sender
                                    .try_send(AggregationMessage::Measurements(msrmnts_msg.clone()))
                                {
                                    match queue_send_err {
                                        mpsc::TrySendError::Full(_full_error) => {
                                            // _full_error contains the dropped message
                                            warn!(
                                                "TrySendError::Full, dropping verified message from {:?} received at {:?}",
                                                rcvd_msg.src_addr_port, rcvd_msg.received_dt
                                            );
                                            reply_to_telegram = AggrReplyToTelegram::InternalError;
                                        }
                                        mpsc::TrySendError::Disconnected(_option_msrmnt_msg) => {
                                            return Err(anyhow!(
                                                "TrySendError::Disconnected sync_sender"
                                            ));
                                        }
                                    }
                                } else {
                                    let received_meter_nrs = msrmnts_msg
                                        .msrmnts_non_enc
                                        .iter()
                                        .chain(msrmnts_msg.msrmnts_enc.iter())
                                        .map(|msrmnt| msrmnt.meter_nr.clone())
                                        .collect::<HashSet<_>>();

                                    match last_aggr_result.lock() {
                                        Ok(res) => {
                                            let mut result_with_received_meter_nr: Vec<
                                                AggrAnonMeasurement,
                                            > = (*res)
                                                .iter()
                                                .filter_map(|aggr_msrmnt| {
                                                    if received_meter_nrs.iter().any(|nr| {
                                                        aggr_msrmnt.meter_nrs.contains(nr)
                                                    }) {
                                                        Some(AggrAnonMeasurement {
                                                            aggr_id: aggr_msrmnt.aggr_id.to_owned(),
                                                            aggr_value: aggr_msrmnt
                                                                .aggr_value
                                                                .clone(),
                                                            aggr_dt_utc: aggr_msrmnt.aggr_dt_utc,
                                                            aggr_msrmnt_type: aggr_msrmnt
                                                                .aggr_msrmnt_type,
                                                            total_delay_ms: aggr_msrmnt
                                                                .total_delay_ms,
                                                            num_msrmnts: aggr_msrmnt.meter_nrs.len()
                                                                as u64,
                                                        })
                                                    } else {
                                                        None
                                                    }
                                                })
                                                .collect::<Vec<_>>();
                                            result_with_received_meter_nr.sort_by_key(|am| {
                                                (am.total_delay_ms, am.aggr_msrmnt_type)
                                            });
                                            reply_to_telegram =
                                                AggrReplyToTelegram::LastAggregationWithMeterSerialNr(
                                                    aggr_anon_msrmnt_infos(
                                                        &result_with_received_meter_nr,
                                                    ),
                                                );
                                        }
                                        Err(e) => {
                                            error!("{e:?} on last_aggr_result");
                                            reply_to_telegram = AggrReplyToTelegram::InternalError;
                                        }
                                    }
                                }
                            }
                        }
                        let reply_mes = match bincode::serde::encode_to_vec(
                            &reply_to_telegram,
                            get_bincode_config(),
                        ) {
                            Ok(buf) => buf,
                            Err(e) => {
                                error!("{e:?}");
                                continue;
                            }
                        };
                        let signature = match aggr_ssl_signer.generate_signature(&reply_mes) {
                            Ok(sig) => sig,
                            Err(e) => {
                                error!("{e:?}");
                                continue;
                            }
                        };
                        let version_bytes: [u8; 2] = [0, 0];
                        let mut bytes = Vec::<u8>::new();
                        bytes.extend(version_bytes);
                        let reply_mes_len = reply_mes.len();
                        // encode as two bytes, least significant first.
                        if reply_mes_len > u16::MAX as usize {
                            error!("reply_mes length {reply_mes_len} is longer than u16::MAX");
                            continue;
                        }
                        bytes.extend([reply_mes_len as u8, (reply_mes_len >> 8) as u8]);
                        bytes.extend(&reply_mes);
                        bytes.extend(&signature);
                        if let Err(e) =
                            send_mes_udp(&bytes, &p1_encryptors_udp_socket, &src_addr_port)
                        {
                            error!("{e:?}");
                        }
                    }
                }
            }
        }
    }
}

fn send_mes_udp(mes_bytes: &[u8], udp_socket: &UdpSocket, addr_port: &SocketAddr) -> Result<()> {
    match udp_socket.send_to(mes_bytes, addr_port) {
        Ok(size) => {
            if size == mes_bytes.len() {
                Ok(())
            } else {
                Err(anyhow!("send_to sent {size} of {} bytes", mes_bytes.len()))
            }
        }
        Err(e) => Err(Error::new(e)),
    }
}

fn aggr_anon_msrmnt_infos(ag_msrmnts: &[AggrAnonMeasurement]) -> Vec<AggrAnonMeasurementInfo> {
    let mut res = Vec::new();
    let mut last_aggr_id_opt: Option<&str> = None;
    let mut last_mutc_opt: Option<&DateTime<Utc>> = None;
    let mut last_delay_ms_opt: Option<i64> = None;
    let mut last_num_msrmnts: u64 = 0;
    use AggrAnonMeasurementInfo::*;
    for ag_msrmnt in ag_msrmnts {
        let put_aggr_id = if let Some(last_aggr_id) = last_aggr_id_opt {
            if last_aggr_id == ag_msrmnt.aggr_id {
                false
            } else {
                last_aggr_id_opt = Some(&ag_msrmnt.aggr_id);
                true
            }
        } else {
            last_aggr_id_opt = Some(&ag_msrmnt.aggr_id);
            true
        };
        if put_aggr_id {
            res.push(AggrId(ag_msrmnt.aggr_id.to_owned()));
        }
        let put_aggr_utc = if let Some(last_mutc) = last_mutc_opt {
            if *last_mutc == ag_msrmnt.aggr_dt_utc {
                false
            } else {
                last_mutc_opt = Some(&ag_msrmnt.aggr_dt_utc);
                true
            }
        } else {
            last_mutc_opt = Some(&ag_msrmnt.aggr_dt_utc);
            true
        };
        if put_aggr_utc {
            res.push(AggrUtc(dt_utc_to_string(&ag_msrmnt.aggr_dt_utc)));
        }
        if last_num_msrmnts != ag_msrmnt.num_msrmnts {
            last_num_msrmnts = ag_msrmnt.num_msrmnts;
            res.push(NumAggregated(ag_msrmnt.num_msrmnts));
        }
        let put_delay_ms = if let Some(last_delay_ms) = last_delay_ms_opt {
            if last_delay_ms == ag_msrmnt.total_delay_ms {
                false
            } else {
                last_delay_ms_opt = Some(ag_msrmnt.total_delay_ms);
                true
            }
        } else {
            last_delay_ms_opt = Some(ag_msrmnt.total_delay_ms);
            true
        };
        if put_delay_ms {
            res.push(TotalDelayMs(ag_msrmnt.total_delay_ms));
        }
        res.push(AggrValue(
            ag_msrmnt.aggr_msrmnt_type,
            ag_msrmnt.aggr_value.clone(),
        ));
    }
    res
}
