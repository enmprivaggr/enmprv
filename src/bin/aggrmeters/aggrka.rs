use std::{
    collections::{
        BTreeSet, HashMap, HashSet,
        hash_map::Entry::{Occupied, Vacant},
    },
    io::{self, Write},
    net::{SocketAddr, TcpStream},
    sync::{Arc, Mutex, mpsc},
};

use anyhow::{Context, Error, Result, anyhow};
use bincode::serde::decode_from_slice;
use chrono::{DateTime, TimeDelta, Utc};
use log::{debug, error, info, log, trace, warn};
use num::{BigInt, One};

use enmprv::{
    bincoded::{
        AggrMeasurementType, EncKeyAddReply, EncKeyAddRequest, EncryptionResult,
        IncrementalRequest, MeterPubKeyHashMeasTimeTypes, P1MeasurementType, aggr_msmrnt_type,
        get_bincode_config,
    },
    measuredvalue::MeasuredValue,
    measurement::{MeasurementTime, P1Measurement},
    openssl::{OpenSslSigVerifier, OpenSslSigner, PubKeyHash},
    util::{
        dt_utc_to_string, receive_version_size_message, send_signed_message, tcp_stream_in_wg_intf,
        time_stamp_milli_secs,
    },
};

use crate::{AggregationMessage, MeasurementsMessage, PrivAggrConfig};

#[derive(Debug, Clone, PartialEq)]
pub(crate) struct AggrMeasurement {
    pub aggr_id: String,              // aggregator id.
    pub aggr_dt_utc: MeasurementTime, // aggregation time, in millisecond resolution
    pub aggr_msrmnt_type: AggrMeasurementType,
    pub aggr_value: MeasuredValue,  // aggregated measurement value.
    pub total_delay_ms: i64,        // from measurement utc's to aggregation utc.
    pub meter_nrs: HashSet<String>, // used for reporting the full aggregation, for example for further aggregation.
}

/// To avoid repetitive fields in a vector of AggrMeasurements
#[derive(Debug)]
pub enum AggrMeasurementInfo {
    AggrId(String),
    AggrUtc(String),
    TotalDelayMs(i64),
    OriginalMeterSerialNrs(HashSet<String>),
    AggrValue(AggrMeasurementType, MeasuredValue),
}

fn aggr_msrmnt_infos(ag_msrmnts: &[AggrMeasurement]) -> Vec<AggrMeasurementInfo> {
    let mut res = Vec::new();
    let mut last_aggr_id_opt: Option<&str> = None;
    let mut last_mutc_opt: Option<&DateTime<Utc>> = None;
    let mut last_delay_ms_opt: Option<i64> = None;
    let mut last_meter_nrs_opt: Option<HashSet<String>> = None;
    use AggrMeasurementInfo::*;
    for ag_msrmnt in ag_msrmnts {
        let put_aggr_id = if let Some(last_aggr_id) = last_aggr_id_opt {
            if last_aggr_id == ag_msrmnt.aggr_id {
                false
            } else {
                last_aggr_id_opt = Some(&ag_msrmnt.aggr_id);
                true
            }
        } else {
            last_aggr_id_opt = Some(&ag_msrmnt.aggr_id);
            true
        };
        if put_aggr_id {
            res.push(AggrId(ag_msrmnt.aggr_id.to_owned()));
        }
        let put_aggr_utc = if let Some(last_mutc) = last_mutc_opt {
            if *last_mutc == ag_msrmnt.aggr_dt_utc {
                false
            } else {
                last_mutc_opt = Some(&ag_msrmnt.aggr_dt_utc);
                true
            }
        } else {
            last_mutc_opt = Some(&ag_msrmnt.aggr_dt_utc);
            true
        };
        if put_aggr_utc {
            res.push(AggrUtc(dt_utc_to_string(&ag_msrmnt.aggr_dt_utc)));
        }
        let put_aggr_meter_nrs = match last_meter_nrs_opt {
            Some(ref last_meter_nrs) => {
                if last_meter_nrs == &ag_msrmnt.meter_nrs {
                    false
                } else {
                    last_meter_nrs_opt = Some(ag_msrmnt.meter_nrs.clone());
                    true
                }
            }
            _ => {
                last_meter_nrs_opt = Some(ag_msrmnt.meter_nrs.clone());
                true
            }
        };
        if put_aggr_meter_nrs {
            res.push(OriginalMeterSerialNrs(ag_msrmnt.meter_nrs.clone()));
        }
        let put_delay_ms = if let Some(last_delay_ms) = last_delay_ms_opt {
            if last_delay_ms == ag_msrmnt.total_delay_ms {
                false
            } else {
                last_delay_ms_opt = Some(ag_msrmnt.total_delay_ms);
                true
            }
        } else {
            last_delay_ms_opt = Some(ag_msrmnt.total_delay_ms);
            true
        };
        if put_delay_ms {
            res.push(TotalDelayMs(ag_msrmnt.total_delay_ms));
        }
        res.push(AggrValue(
            ag_msrmnt.aggr_msrmnt_type,
            ag_msrmnt.aggr_value.clone(),
        ));
    }
    res
}

/// The result of aggregating measurements, anonymised without the original ref_id's.
#[derive(Debug)]
pub(crate) struct AggrAnonMeasurement {
    pub aggr_id: String,                       // aggregator id.
    pub aggr_dt_utc: MeasurementTime,          // aggregation time, in millisecond resolution
    pub aggr_msrmnt_type: AggrMeasurementType, // type of aggregated measurements
    pub total_delay_ms: i64,                   // from measurement utc's to aggregation utc.
    pub num_msrmnts: u64,                      // number of aggregated measurements
    pub aggr_value: MeasuredValue,             // aggregated measurement value.
}

fn aggr_key_add_req_from_mrsmnts(
    ndkr_msrmnts_vec: &mut [P1Measurement], // mut to allow sorting.
    request_stage: IncrementalRequest,
    pkh_and_verifier_by_meter_serial_nr: &HashMap<String, (PubKeyHash, Arc<OpenSslSigVerifier>)>,
) -> Result<Option<EncKeyAddRequest>> {
    use std::cmp::Ordering::*;
    ndkr_msrmnts_vec.sort_unstable_by(|a, b| match a.meter_nr.cmp(&b.meter_nr) {
        Less => Less,
        Greater => Greater,
        Equal => a.dt_utc.cmp(&b.dt_utc),
    });
    if let Some(first_msrmnt) = ndkr_msrmnts_vec.first() {
        let mut key_add_request = EncKeyAddRequest {
            request_stage,
            meter_pkh_meas_dt_types_vec: Vec::new(),
        };

        let mut last_meter_nr = &first_msrmnt.meter_nr;
        let mut last_meter_pkh: &PubKeyHash;
        if let Some((meter_pkh, _)) = pkh_and_verifier_by_meter_serial_nr.get(last_meter_nr) {
            last_meter_pkh = meter_pkh;
        } else {
            return Err(anyhow!("no meter pkh for {last_meter_nr}"));
        };
        let mut last_dt_utc = first_msrmnt.dt_utc;
        let mut encr_msrmnt_types_vec = HashSet::new();
        encr_msrmnt_types_vec.insert(first_msrmnt.p1_msrmnt_type);
        for msrmnt in &ndkr_msrmnts_vec[1..] {
            if &msrmnt.meter_nr == last_meter_nr && msrmnt.dt_utc == last_dt_utc {
                encr_msrmnt_types_vec.insert(msrmnt.p1_msrmnt_type);
            } else {
                let meas_dt_stamp = dt_utc_to_string(&last_dt_utc);
                key_add_request
                    .meter_pkh_meas_dt_types_vec
                    .push(MeterPubKeyHashMeasTimeTypes {
                        meter_pkh: last_meter_pkh.to_owned(),
                        meas_dt_stamp,
                        msrmnt_types: encr_msrmnt_types_vec,
                    });
                if last_meter_nr != &msrmnt.meter_nr {
                    last_meter_nr = &msrmnt.meter_nr;
                    if let Some((meter_pkh, _)) =
                        pkh_and_verifier_by_meter_serial_nr.get(last_meter_nr)
                    {
                        last_meter_pkh = meter_pkh;
                    } else {
                        return Err(anyhow!("no meter pkh for {last_meter_nr}"));
                    };
                }
                last_dt_utc = msrmnt.dt_utc;
                encr_msrmnt_types_vec = HashSet::new();
                encr_msrmnt_types_vec.insert(msrmnt.p1_msrmnt_type);
            }
        }
        let meas_dt_stamp = dt_utc_to_string(&last_dt_utc);
        key_add_request
            .meter_pkh_meas_dt_types_vec
            .push(MeterPubKeyHashMeasTimeTypes {
                meter_pkh: last_meter_pkh.to_owned(),
                meas_dt_stamp,
                msrmnt_types: encr_msrmnt_types_vec,
            });
        Ok(Some(key_add_request))
    } else {
        // no measurements to be decrypted:
        match request_stage {
            IncrementalRequest::Intermediate => Ok(None),
            IncrementalRequest::Final => {
                // Final EncKeyAddRequest may be empty
                Ok(Some(EncKeyAddRequest {
                    request_stage,
                    meter_pkh_meas_dt_types_vec: Vec::new(),
                }))
            }
        }
    }
}

type MeasurementTimeBySerialNrType = HashMap<(String, P1MeasurementType), MeasurementTime>;

fn keep_last_meas_time(
    msrmnt: &P1Measurement,
    last_meas_time_by_serial_nr_type: &mut MeasurementTimeBySerialNrType,
) -> Result<()> {
    match last_meas_time_by_serial_nr_type
        .entry((msrmnt.meter_nr.to_string(), msrmnt.p1_msrmnt_type))
    {
        Vacant(entry) => {
            entry.insert(msrmnt.dt_utc);
            Ok(())
        }
        Occupied(mut entry) => {
            let prev_msmrnt_dt_utc = entry.get();
            if msrmnt.dt_utc >= *prev_msmrnt_dt_utc {
                // msrmnt times can be equal when encrypted and non encrypted values are present in same msrmnt_msg
                entry.insert(msrmnt.dt_utc);
                Ok(())
            } else {
                Err(anyhow!(
                    "ignored message with measurement {msrmnt:?} before previous one at {prev_msmrnt_dt_utc:?}"
                ))
            }
        }
    }
}

type MeasurementBySerialNrType = HashMap<(String, P1MeasurementType), P1Measurement>;

fn collect_msrmnts_msg_for_interval(
    msrmnts_msg: &MeasurementsMessage,
    last_meas_time_by_serial_nr_type: &mut MeasurementTimeBySerialNrType,
    interval_msrmnt_by_serial_nr_type: &mut MeasurementBySerialNrType,
    interval_msrmnt_enc_by_serial_nr_type: &mut MeasurementBySerialNrType,
    ndkr_msrmnts_vec: &mut Vec<P1Measurement>,
    show_msrmnt_dots: bool,
) -> Result<()> {
    // Filter incoming measurements in msrmnts_msg:
    // ignore the message when it is before a previous one:

    let mut msmrnts_ok: bool = true;
    for msrmnt in &msrmnts_msg.msrmnts_non_enc {
        if let Err(e) = keep_last_meas_time(msrmnt, last_meas_time_by_serial_nr_type) {
            error!("{e:#} for unencrypted msrmnt");
            msmrnts_ok = false;
        }
    }
    for msrmnt in &msrmnts_msg.msrmnts_enc {
        if let Err(e) = keep_last_meas_time(msrmnt, last_meas_time_by_serial_nr_type) {
            error!("{e:#} for encrypted msrmnt");
            msmrnts_ok = false;
        }
    }
    if !msmrnts_ok {
        return Err(anyhow!("at least one late measurement"));
    }

    for msrmnt in &msrmnts_msg.msrmnts_non_enc {
        match interval_msrmnt_by_serial_nr_type
            .entry((msrmnt.meter_nr.clone(), msrmnt.p1_msrmnt_type))
        {
            Vacant(entry) => {
                entry.insert(msrmnt.clone());
                if show_msrmnt_dots {
                    print!("."); // accept msrmnt
                }
            }
            Occupied(mut entry) => {
                *entry.get_mut() = msrmnt.clone();
                if show_msrmnt_dots {
                    print!("+"); // override earlier msrmnt
                }
            }
        }
    }
    for msrmnt in &msrmnts_msg.msrmnts_enc {
        match interval_msrmnt_enc_by_serial_nr_type
            .entry((msrmnt.meter_nr.clone(), msrmnt.p1_msrmnt_type))
        {
            Vacant(entry) => {
                entry.insert(msrmnt.clone());
                if show_msrmnt_dots {
                    print!(","); // accept encrypted msrmnt
                }
            }
            Occupied(mut entry) => {
                *entry.get_mut() = msrmnt.clone();
                if show_msrmnt_dots {
                    print!("*"); // override earlier encrypted msrmnt
                }
            }
        };
        // key adder will override earlier decryption requests for earlier msrmnts:
        ndkr_msrmnts_vec.push(msrmnt.clone());
    }
    // flush to show dots/plusses without a newline at the end:
    if io::stdout().flush().is_err() {
        error!("stdout flush failed");
    }
    Ok(())
}

fn report_meter_nrs(aggr_res: &[AggrMeasurement]) {
    let meter_nrs_ordered = aggr_res
        .iter()
        .flat_map(|am| am.meter_nrs.iter())
        .map(String::as_str)
        .collect::<BTreeSet<&str>>();
    info!(
        "{} meter serial nr(s): {}",
        meter_nrs_ordered.len(),
        Vec::from_iter(meter_nrs_ordered).join(" ")
    );
}

fn unencrypted_aggregation(
    interval_msrmnt_by_serial_nr_type: &MeasurementBySerialNrType,
    now_dt: DateTime<Utc>,
    aggregator_id: &str,
) -> Vec<AggrMeasurement> {
    let mut res = Vec::new();
    for msrmnt_type in P1MeasurementType::iter() {
        if let Some(aggr_msrmnt_type) = aggr_msmrnt_type(*msrmnt_type) {
            let mut aggr_msrmnt_opt: Option<AggrMeasurement> = None;
            for msrmnt in interval_msrmnt_by_serial_nr_type.values() {
                if msrmnt.p1_msrmnt_type == *msrmnt_type {
                    match aggr_msrmnt_opt {
                        Some(ref mut aggr_msrmnt) => {
                            aggr_msrmnt.total_delay_ms += now_dt
                                .signed_duration_since(msrmnt.dt_utc)
                                .num_milliseconds();
                            aggr_msrmnt.aggr_value += msrmnt.value.clone();
                            aggr_msrmnt.meter_nrs.insert(msrmnt.meter_nr.clone());
                        }
                        _ => {
                            let mut aggr_msrmnt = AggrMeasurement {
                                aggr_id: aggregator_id.to_owned(),
                                aggr_dt_utc: now_dt,
                                aggr_msrmnt_type,
                                aggr_value: msrmnt.value.clone(),
                                total_delay_ms: now_dt
                                    .signed_duration_since(msrmnt.dt_utc)
                                    .num_milliseconds(),
                                meter_nrs: HashSet::new(),
                            };
                            aggr_msrmnt.meter_nrs.insert(msrmnt.meter_nr.clone());
                            aggr_msrmnt_opt = Some(aggr_msrmnt);
                        }
                    }
                }
            }
            if let Some(aggr_measurement) = aggr_msrmnt_opt {
                res.push(aggr_measurement);
            }
        } else {
            for msrmnt in interval_msrmnt_by_serial_nr_type.values() {
                if msrmnt.p1_msrmnt_type == *msrmnt_type {
                    error!(
                        "cannot aggregate {:?} in unencrypted {msrmnt:?}",
                        msrmnt.p1_msrmnt_type
                    )
                }
            }
        }
    }

    res
}

fn decrypted_aggregation(
    interval_msrmnts_enc_by_serial_nr_type: &MeasurementBySerialNrType,
    pkh_and_verifier_by_meter_serial_nr: &HashMap<String, (PubKeyHash, Arc<OpenSslSigVerifier>)>,
    key_add_reply: &EncKeyAddReply,
    max_abs_decrypted_per_participant: &MeasuredValue,
    aggregator_id: &str,
) -> Vec<AggrMeasurement> {
    let mut res = Vec::new();
    let now_dt = time_stamp_milli_secs();
    for mt_total_key in &key_add_reply.msrmnt_type_total_key_vec {
        use EncryptionResult::*;
        match &mt_total_key.total_encryption_key {
            PrivacyViolation => {
                info!(
                    "privacy violation, no decryption possible for {:?} measurement(s)",
                    mt_total_key.msrmnt_type
                );
                continue;
            }
            TotalEncryptionKey(decryption_key) => {
                let mut refused_meter_set_pkhs = HashSet::<PubKeyHash>::new();
                for rf_meter_set_pkh in &mt_total_key.refused_meter_set_pkhs {
                    if refused_meter_set_pkhs.insert(rf_meter_set_pkh.to_owned()) {
                        info!(
                            "refused meter set {rf_meter_set_pkh} for {:?}",
                            mt_total_key.msrmnt_type
                        );
                    } else {
                        error!("meter set {rf_meter_set_pkh} refused more than once");
                    }
                }
                let msrmnts_enc: Vec<&P1Measurement> = interval_msrmnts_enc_by_serial_nr_type
                    .values()
                    .filter(|&msrmnt| msrmnt.p1_msrmnt_type == mt_total_key.msrmnt_type)
                    .collect();
                if msrmnts_enc.is_empty() {
                    error!(
                        "no encrypted measurements aggregated for msrmnt_type {:?}",
                        mt_total_key.msrmnt_type
                    );
                    continue; // next mt_total_key
                }
                // take the sum over the encrypted values, except for the refused ones
                let mut decr_msrmnts: usize = 0;
                let mut total_delay_ms: i64 = 0;
                let total_encrypted_val: MeasuredValue = msrmnts_enc
                    .iter()
                    .filter_map(|&msrmnt| {
                        if let Some((meter_pkh, _verifier)) =
                            pkh_and_verifier_by_meter_serial_nr.get(&msrmnt.meter_nr)
                        {
                            if refused_meter_set_pkhs.contains(meter_pkh) {
                                None // refused meter, filter out.
                            } else {
                                decr_msrmnts += 1;
                                let msrmnt_delay_ms = now_dt
                                    .signed_duration_since(msrmnt.dt_utc)
                                    .num_milliseconds();
                                total_delay_ms += msrmnt_delay_ms;
                                Some(&msrmnt.value)
                            }
                        } else {
                            error!("no meter set for meter_nr {}", msrmnt.meter_nr);
                            None // consider the meter refused, this might lead to decryption out of range.
                        }
                    })
                    .sum();
                if decr_msrmnts == 0 {
                    error!(
                        "only ignored meters for {} {:?} measurements",
                        msrmnts_enc.len(),
                        mt_total_key.msrmnt_type
                    );
                    continue;
                }

                let original_meter_nrs = msrmnts_enc
                    .iter()
                    .filter_map(|&msrmnt| {
                        if let Some((meter_pkh, _verifier)) =
                            pkh_and_verifier_by_meter_serial_nr.get(&msrmnt.meter_nr)
                        {
                            if refused_meter_set_pkhs.contains(meter_pkh) {
                                None
                            } else {
                                Some(msrmnt.meter_nr.clone())
                            }
                        } else {
                            None
                        }
                    })
                    .collect::<HashSet<_>>();

                let Some(aggr_msrmnt_type) = aggr_msmrnt_type(mt_total_key.msrmnt_type) else {
                    error!(
                        "no aggregation measurement type for {:?} in {mt_total_key:?}",
                        mt_total_key.msrmnt_type
                    );
                    continue;
                };

                let total_decrypted_val = total_encrypted_val - decryption_key.clone();
                let max_abs_decrypted = max_abs_decrypted_per_participant * (decr_msrmnts as i32);
                if total_decrypted_val.abs() > max_abs_decrypted {
                    report_avarage(
                        log::Level::Error,
                        "decrypted result out of range ",
                        decr_msrmnts,
                        total_delay_ms,
                        total_decrypted_val,
                        aggr_msrmnt_type,
                    );
                    continue;
                }
                res.push(AggrMeasurement {
                    aggr_id: aggregator_id.to_owned(),
                    aggr_dt_utc: now_dt,
                    aggr_msrmnt_type,
                    aggr_value: total_decrypted_val,
                    total_delay_ms,
                    meter_nrs: original_meter_nrs,
                });
            }
        };
    }

    let mut msrmnt_enc_types: HashSet<P1MeasurementType> = HashSet::new();
    for (_, msrmnt_type) in interval_msrmnts_enc_by_serial_nr_type.keys() {
        msrmnt_enc_types.insert(*msrmnt_type);
    }
    if msrmnt_enc_types.len() != key_add_reply.msrmnt_type_total_key_vec.len() {
        warn!(
            "decrypted {} of {} aggregated encrypted measurement types",
            key_add_reply.msrmnt_type_total_key_vec.len(),
            msrmnt_enc_types.len(),
        );
    }
    res
}

fn report_avarage(
    log_level: log::Level,
    enc_type: &str,
    num_msrmnts: usize,
    total_delay_ms: i64,
    total_msrmnt_val: MeasuredValue,
    aggr_msrmnt_type: AggrMeasurementType,
) {
    let av_delay_ms = total_delay_ms / (num_msrmnts as i64);
    if num_msrmnts == 0 {
        error!("no msrmnts for {aggr_msrmnt_type:?}");
        return;
    };

    let av_msrmnt_val = (&total_msrmnt_val) / (num_msrmnts as i32);

    log!(
        log_level,
        "{num_msrmnts} {enc_type} msrmnts, av. delay: {:>7} ms, av.: {:>11} {:?}",
        av_delay_ms,
        av_msrmnt_val.to_string(5),
        aggr_msrmnt_type,
    );
}

/// Provide the aggregated measurement types that are the sources of an aggregated collective consumption type.
fn coll_self_consumption_source_types(
    aggr_self_cons_msmrnt_type: AggrMeasurementType,
) -> Option<(AggrMeasurementType, AggrMeasurementType)> {
    use AggrMeasurementType::*;
    Some(match aggr_self_cons_msmrnt_type {
        El_kW_coll => (El_kW_to_aggr, El_kW_by_aggr),
        // TBD: cumulative self consumption needs an interval
        // El_kWh_self_tariff_1_aggr => (El_kWh_to_tariff_1_aggr, El_kWh_by_tariff_1_aggr),
        // El_kWh_self_tariff_2_aggr => (El_kWh_to_tariff_2_aggr, El_kWh_by_tariff_2_aggr),
        _ => return None,
    })
}

/// Add collective self consumption measurements and subtract them from their source measurements.
fn compute_coll_self_consumption(total_aggr_result: &mut Vec<AggrMeasurement>) {
    let mut coll_cons_source_types_by_target = HashMap::new();
    let mut coll_cons_source_types = HashSet::new();
    for amt in AggrMeasurementType::iter() {
        if let Some(source_types) = coll_self_consumption_source_types(*amt) {
            coll_cons_source_types_by_target.insert(amt, source_types);
            let (source_type1, source_type2) = source_types;
            coll_cons_source_types.insert(source_type1);
            coll_cons_source_types.insert(source_type2);
        }
    }
    let coll_cons_source_types_by_target = coll_cons_source_types_by_target;
    let coll_cons_source_types = coll_cons_source_types;

    // let self_cons_target_type = self_cons_target_type;
    let source_msrmnts_by_type = total_aggr_result
        .iter()
        .filter_map(|am| {
            if coll_cons_source_types.contains(&am.aggr_msrmnt_type) {
                Some((am.aggr_msrmnt_type, am.clone()))
            } else {
                None
            }
        })
        .collect::<HashMap<AggrMeasurementType, AggrMeasurement>>();

    for (target_type, (source_type1, source_type2)) in coll_cons_source_types_by_target {
        match source_msrmnts_by_type.get(target_type) {
            Some(existing_target_msrmnt) => {
                error!("{target_type:?} already there: {existing_target_msrmnt:?}");
            }
            _ => {
                let mut self_cons_value_opt: Option<MeasuredValue> = None;
                if let Some(source_msrmnt1) = source_msrmnts_by_type.get(&source_type1) {
                    if let Some(source_msrmnt2) = source_msrmnts_by_type.get(&source_type2) {
                        let self_cons_value = source_msrmnt1
                            .aggr_value
                            .clone()
                            .min(source_msrmnt2.aggr_value.clone());
                        let dt_diff = source_msrmnt1.aggr_dt_utc - source_msrmnt2.aggr_dt_utc;
                        const MAX_WEIGHT: usize = (i32::MAX as usize) >> 2; // about 537 * 10**6
                        let w1 = source_msrmnt1.meter_nrs.len().min(MAX_WEIGHT) as i32;
                        let w2 = source_msrmnt2.meter_nrs.len().min(MAX_WEIGHT) as i32;
                        // Weigh dt_diff and round to millisecs:
                        let diff_ms = ((dt_diff * w1) / (w1 + w2) + TimeDelta::microseconds(500))
                            .num_milliseconds();
                        let aggr_dt_utc =
                            source_msrmnt2.aggr_dt_utc + TimeDelta::milliseconds(diff_ms);
                        let total_delay_ms = (source_msrmnt1.total_delay_ms * (w1 as i64)
                            + source_msrmnt2.total_delay_ms * (w2 as i64))
                            / (w1 as i64 + w2 as i64);
                        let mut original_meter_nrs = source_msrmnt1.meter_nrs.clone();
                        original_meter_nrs.extend(source_msrmnt2.meter_nrs.clone());
                        total_aggr_result.push(AggrMeasurement {
                            aggr_id: source_msrmnt1.aggr_id.clone(),
                            aggr_dt_utc,
                            aggr_msrmnt_type: *target_type,
                            aggr_value: self_cons_value.clone(),
                            total_delay_ms,
                            meter_nrs: original_meter_nrs,
                        });
                        self_cons_value_opt = Some(self_cons_value);
                    }
                }
                if let Some(self_cons_value) = self_cons_value_opt {
                    total_aggr_result.iter_mut().for_each(|am| {
                        if am.aggr_msrmnt_type == source_type1 {
                            am.aggr_value -= self_cons_value.clone();
                        }
                        if am.aggr_msrmnt_type == source_type2 {
                            am.aggr_value -= self_cons_value.clone();
                        }
                    });
                }
            }
        }
    }
}

fn receive_signed_key_add_reply(
    key_adder_tcp_stream: &mut TcpStream,
    read_timeout_seconds: u64,
    keyadder_sig_verifier: &OpenSslSigVerifier,
) -> Result<EncKeyAddReply> {
    let reply_buf = match receive_version_size_message(key_adder_tcp_stream, read_timeout_seconds) {
        Ok(buf) => buf,
        Err(e) => {
            debug!("{e:#?}");
            if let Some(ioe) = e.downcast_ref::<io::ErrorKind>() {
                debug!("{ioe:#?}");
                match ioe {
                    io::ErrorKind::WouldBlock => {
                        debug!("WouldBlock");
                    }
                    io::ErrorKind::UnexpectedEof => {
                        debug!("UnexpectedEof");
                    }
                    _ => {}
                }
            }
            return Err(e.context("receive_version_size_message reply_buf"));
        }
    };
    let signature = match receive_version_size_message(key_adder_tcp_stream, read_timeout_seconds) {
        Ok(buf) => buf,
        Err(e) => {
            return Err(e.context("receive_version_size_message signature"));
        }
    };
    // verify_signature before deserialize: strongest check first.
    match keyadder_sig_verifier.verify_signature(&reply_buf, &signature) {
        Err(e) => Err(e.context("verify_signature")),
        Ok(sig_ok) => {
            if sig_ok {
                match decode_from_slice::<EncKeyAddReply, _>(&reply_buf, get_bincode_config()) {
                    Err(e) => Err(Error::new(e).context("decode_from_slice")),
                    Ok((key_add_reply, size)) => {
                        if size == reply_buf.len() {
                            Ok(key_add_reply)
                        } else {
                            Err(anyhow!(
                                "key_add_reply decode_from_slice incomplete, {} of {}",
                                size,
                                reply_buf.len()
                            ))
                        }
                    }
                }
            } else {
                Err(anyhow!("verify_signature failed"))
            }
        }
    }
}

#[allow(clippy::too_many_arguments)]
pub(crate) fn aggr_msrmnts_from_queue(
    config: &PrivAggrConfig,
    receiver_msrmnt_msgs: &mpsc::Receiver<AggregationMessage>,
    pkh_and_verifier_by_meter_serial_nr: &HashMap<String, (PubKeyHash, Arc<OpenSslSigVerifier>)>,
    aggr_signer: &Arc<OpenSslSigner>,
    key_adder_socket_addr: &SocketAddr,
    keyadder_sig_verifier: &OpenSslSigVerifier,
    last_aggr_result: &Mutex<Vec<AggrMeasurement>>,
) -> Result<()> {
    let max_abs_decrypted =
        MeasuredValue::from_bi_minus_ten_pow(BigInt::one(), -i32::from(config.ten_pow_max_decrypt));
    let read_timeout = config.key_adder_receive_timeout.std_duration();
    let max_keep_eq_ids = config.max_keep_eq_ids;

    let mut last_meas_time_by_meter_type = HashMap::new(); // to ignore later arrived, but earlier measurements messages
    let mut interval_msrmnt_non_enc_by_meter_type = HashMap::new(); // collect non encrypted msmrnts per interval
    let mut interval_msrmnt_enc_by_meter_type = HashMap::new(); // collect encrypted msrmnts per interval
    let mut ndkr_msrmnts_vec = Vec::new(); // encrypted msmrmnts for which no decryption key request has been sent.

    // when a TcpStream goes out of scope there is an automatic shutdown(Shutdown::Both)
    let mut key_adder_tcp_stream: Option<TcpStream> =
        match tcp_stream_in_wg_intf(key_adder_socket_addr, read_timeout.as_secs()) {
            Ok(tcp_stream) => Some(tcp_stream),
            Err(e) => {
                error!("{e:#} first tcp_stream_in_wg_intf for key_adder_tcp_stream");
                None
            }
        };

    info!("start collecting measurement messages");
    let mut msrmnts_msgs_received: usize = 0;
    let mut total_msrmnts_received: usize = 0;
    let mut total_bytes_received: usize = 0;

    let show_msrmnt_dots = match std::env::var("SILENT_MSRMNTS") {
        Err(_) => true,
        Ok(silent_msrmnts) => silent_msrmnts == "0",
    };

    loop {
        let aggregation_message = match receiver_msrmnt_msgs.recv() {
            Err(e) => {
                return Err(Error::new(e));
            }
            Ok(m) => m,
        };
        match aggregation_message {
            AggregationMessage::Measurements(msrmnts_msg) => {
                msrmnts_msgs_received += 1;
                total_msrmnts_received += msrmnts_msg.total_num_msrmnts();
                total_bytes_received += msrmnts_msg.total_bytes;
                if let Err(e) = collect_msrmnts_msg_for_interval(
                    &msrmnts_msg,
                    &mut last_meas_time_by_meter_type,
                    &mut interval_msrmnt_non_enc_by_meter_type,
                    &mut interval_msrmnt_enc_by_meter_type,
                    &mut ndkr_msrmnts_vec,
                    show_msrmnt_dots,
                ) {
                    error!("{e:#} at collect_msmrnts_msg_for_interval");
                    continue;
                }
                if ndkr_msrmnts_vec.len() >= (config.max_msrmnts_key_add_request as usize) {
                    match aggr_key_add_req_from_mrsmnts(
                        &mut ndkr_msrmnts_vec,
                        IncrementalRequest::Intermediate,
                        pkh_and_verifier_by_meter_serial_nr,
                    ) {
                        Ok(Some(key_add_request)) => {
                            if let Some(ref mut tcp_stream) = key_adder_tcp_stream {
                                if let Err(e) =
                                    send_signed_message(&key_add_request, aggr_signer, tcp_stream)
                                {
                                    error!("{e:?} at send_signed_message partial request");
                                }
                            }
                        }
                        Ok(None) => {
                            warn!(
                                "no partial partial key add request for ndkr_msrmnts_vec.len() {}",
                                ndkr_msrmnts_vec.len()
                            );
                        }
                        Err(e) => return Err(e.context("aggr_key_add_req_from_mrsmnts")),
                    }
                    ndkr_msrmnts_vec.clear();
                }
            }
            AggregationMessage::EndInterval => {
                if show_msrmnt_dots && total_msrmnts_received > 0 {
                    println!(); // end the ...+++ or ,,,*** line for the measurements
                }
                if interval_msrmnt_non_enc_by_meter_type.is_empty()
                    && interval_msrmnt_enc_by_meter_type.is_empty()
                {
                    debug!("no messages available");
                } else {
                    // send final key add request:
                    match aggr_key_add_req_from_mrsmnts(
                        &mut ndkr_msrmnts_vec,
                        IncrementalRequest::Final,
                        pkh_and_verifier_by_meter_serial_nr,
                    )
                    .context("aggr_key_add_req_from_mrsmnts")?
                    {
                        Some(key_add_request) => {
                            if let Some(ref mut tcp_stream) = key_adder_tcp_stream {
                                if let Err(e) =
                                    send_signed_message(&key_add_request, aggr_signer, tcp_stream)
                                {
                                    error!("{e:?} at send_signed_message final request");
                                }
                            }
                        }
                        None => {
                            warn!(
                                "no final partial key add request for ndkr_msrmnts_vec.len() {}",
                                ndkr_msrmnts_vec.len()
                            );
                        }
                    }
                    ndkr_msrmnts_vec.clear();

                    info!(
                        "collected {} measurement messages, with {} measurements, total {} bytes",
                        msrmnts_msgs_received, total_msrmnts_received, total_bytes_received,
                    );

                    // compute and report interval aggregation results.

                    let start_report_dt = time_stamp_milli_secs();
                    let aggr_result_unencrypted_opt: Option<_> =
                        if interval_msrmnt_non_enc_by_meter_type.is_empty() {
                            None
                        } else {
                            let res = unencrypted_aggregation(
                                &interval_msrmnt_non_enc_by_meter_type,
                                start_report_dt,
                                &config.aggregator_id,
                            );
                            for am in &res {
                                report_avarage(
                                    log::Level::Info,
                                    "unencrypted",
                                    am.meter_nrs.len(),
                                    am.total_delay_ms,
                                    am.aggr_value.clone(),
                                    am.aggr_msrmnt_type,
                                );
                            }
                            report_meter_nrs(&res);
                            Some(res)
                        };

                    let aggr_result_decrypted_opt: Option<_> = if interval_msrmnt_enc_by_meter_type
                        .is_empty()
                    {
                        trace!("no encrypted measurements available");
                        None
                    } else {
                        match key_adder_tcp_stream {
                            Some(ref mut tcp_stream) => {
                                match receive_signed_key_add_reply(
                                    tcp_stream,
                                    read_timeout.as_secs(),
                                    keyadder_sig_verifier,
                                ) {
                                    Err(e) => {
                                        error!("{e:#} at receive_signed_key_add_reply");
                                        None
                                    }
                                    Ok(key_add_reply) => {
                                        let decrypt_delay_ms = time_stamp_milli_secs()
                                            .signed_duration_since(start_report_dt)
                                            .num_milliseconds();
                                        info!("decryption delay: {decrypt_delay_ms} ms");

                                        if !key_add_reply.meter_pkhs_no_encryption_info.is_empty() {
                                            warn!(
                                                "no decryption info for {} meter(s): {}",
                                                key_add_reply.meter_pkhs_no_encryption_info.len(),
                                                key_add_reply
                                                    .meter_pkhs_no_encryption_info
                                                    .iter()
                                                    .map(|pkh| format!("{pkh}"))
                                                    .collect::<Vec<_>>()
                                                    .join(" ")
                                            );
                                        }

                                        let res = decrypted_aggregation(
                                            &interval_msrmnt_enc_by_meter_type,
                                            pkh_and_verifier_by_meter_serial_nr,
                                            &key_add_reply,
                                            &max_abs_decrypted,
                                            &config.aggregator_id,
                                        );
                                        Some(res)
                                    }
                                }
                            }
                            _ => {
                                error!("no key_adder_tcp_stream for encrypted measurements");
                                None
                            }
                        }
                    };

                    let mut total_aggr_result: Vec<AggrMeasurement> = Vec::new();
                    match (aggr_result_unencrypted_opt, aggr_result_decrypted_opt) {
                        (Some(aggr_result_unencrypted), Some(aggr_result_decrypted)) => {
                            // merge by measurement type, check for equal results of same original meter serial nrs
                            for aggr_mt in AggrMeasurementType::iter() {
                                let mut aggr_result_unencrypted_mt_opt = None;
                                for aggr_msrmnt in &aggr_result_unencrypted {
                                    if aggr_msrmnt.aggr_msrmnt_type == *aggr_mt
                                        && aggr_result_unencrypted_mt_opt.is_none()
                                    {
                                        aggr_result_unencrypted_mt_opt = Some(aggr_msrmnt);
                                        break;
                                    }
                                }
                                let mut aggr_result_decrypted_mt_opt = None;
                                for aggr_msrmnt in &aggr_result_decrypted {
                                    if aggr_msrmnt.aggr_msrmnt_type == *aggr_mt
                                        && aggr_result_decrypted_mt_opt.is_none()
                                    {
                                        aggr_result_decrypted_mt_opt = Some(aggr_msrmnt);
                                        break;
                                    }
                                }
                                if let (
                                    Some(aggr_result_unencrypted_mt),
                                    Some(aggr_result_decrypted_mt),
                                ) =
                                    (aggr_result_unencrypted_mt_opt, aggr_result_decrypted_mt_opt)
                                {
                                    if aggr_result_unencrypted_mt.meter_nrs.len()
                                        == aggr_result_decrypted_mt.meter_nrs.len()
                                    {
                                        let unenc_meter_nrs = aggr_result_unencrypted_mt
                                            .meter_nrs
                                            .iter()
                                            .collect::<HashSet<_>>();
                                        if aggr_result_decrypted_mt
                                            .meter_nrs
                                            .iter()
                                            .all(|nr| unenc_meter_nrs.contains(nr))
                                        {
                                            if aggr_result_unencrypted_mt.aggr_value
                                                == aggr_result_decrypted_mt.aggr_value
                                            {
                                                total_aggr_result
                                                    .push(aggr_result_unencrypted_mt.clone());
                                            } else {
                                                error!(
                                                    "{aggr_mt:?} unequal totals for decrypted and unencrypted"
                                                );
                                            }
                                        } else {
                                            // not the same meter serial nrs, ignore the unencrypted aggregation
                                            total_aggr_result
                                                .push(aggr_result_decrypted_mt.clone());
                                        }
                                    }
                                }
                            }
                        }
                        (Some(aggr_result_unencrypted), None) => {
                            total_aggr_result.extend(aggr_result_unencrypted);
                        }
                        (None, Some(aggr_result_decrypted)) => {
                            total_aggr_result.extend(aggr_result_decrypted);
                        }
                        (None, None) => {}
                    }

                    compute_coll_self_consumption(&mut total_aggr_result);
                    total_aggr_result.sort_by_key(|am| (am.total_delay_ms, am.aggr_msrmnt_type));

                    for agm_info in aggr_msrmnt_infos(&total_aggr_result) {
                        use AggrMeasurementInfo::*;
                        match agm_info {
                            AggrValue(msrmnt_type, value) => {
                                info!("{:>11} {msrmnt_type:?}", value.to_string(5));
                            }
                            OriginalMeterSerialNrs(meter_nrs) => {
                                let mut meter_nrs2: Vec<&str> =
                                    meter_nrs.iter().map(|s| s.as_str()).collect();
                                meter_nrs2.sort();
                                let len = meter_nrs2.len();
                                info!(
                                    "{:>3} meter serial nrs: {}",
                                    len,
                                    meter_nrs2.into_iter().collect::<Vec<_>>().join(" "),
                                );
                            }
                            AggrId(ref _aggr_id) => {
                                // avoid clippy warning on non used AggrId contents, similar below.
                                info!("{agm_info:?}");
                            }
                            AggrUtc(ref _aggr_dt_utc) => {
                                info!("{agm_info:?}");
                            }
                            TotalDelayMs(_total_delay) => {
                                info!("{agm_info:?}");
                            }
                        }
                    }

                    match last_aggr_result.lock() {
                        Ok(mut res) => {
                            *res = total_aggr_result;
                        }
                        Err(e) => {
                            error!("{e:?} on last_aggr_result");
                        }
                    }

                    // prepare next aggregation interval
                    interval_msrmnt_non_enc_by_meter_type.clear();
                    interval_msrmnt_enc_by_meter_type.clear();
                }
                msrmnts_msgs_received = 0;
                total_msrmnts_received = 0;
                total_bytes_received = 0;

                if last_meas_time_by_meter_type.len() > (max_keep_eq_ids as usize) {
                    // Possible attack with many different combinations?
                    error!(
                        "too many combinations of meter serial nr and measurement type: {}",
                        last_meas_time_by_meter_type.len()
                    );
                    // Must clear, now an old message may be accepted:
                    last_meas_time_by_meter_type.clear();
                }
                // create a new key_adder_tcp_stream for next aggregation interval
                key_adder_tcp_stream =
                    match tcp_stream_in_wg_intf(key_adder_socket_addr, read_timeout.as_secs()) {
                        Ok(tcp_stream) => Some(tcp_stream),
                        Err(e) => {
                            error!("{e:#} at later tcp_stream_in_wg_intf key_adder_tcp_stream");
                            None
                        }
                    };
            }
        }
    }
}
