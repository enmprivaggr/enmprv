//! p1encryptor interface to key adder.

//  To share the seed, choose a validity interval length and starting moment.
//  Choose an initial starting time (not_before_dt), this should:
//    - not be in the past for consistency with unknown earlier share seeds.
//    - be usable in a single testing period of say 3 minutes.
//    try initial starting time at one minute after the current minute ends.
//  The initial starting time is implemented here as one minute after the current minute ends.

//  Choose validity interval length to avoid leap seconds outside validity intervals,
//  so this should be in whole minutes when starting at a minute boundary.
//  For testing, 1 minute is practical.
//  For development, a number of whole hours or days.
//  In real life, a larger number of whole weeks.

//  To avoid many p1encryptors connecting to the key adder at the same time after a power up,
//  use a random wait period between 1 and 3 minutes.
//  after failing to connect to the key adder to share a seed.
//  When sharing a new seed, generate a new one each time a connection is tried.

//  At startup of the p1encryptor, share the currently valid seed with the key adder,
//  or choose an initial seed as above.
//  For this it will be necessary to store the current seed for later use after shutdown.
//  Initially during the development, use the shared seed from the config file for this.
//  Lateron add the shared seeds into a db table.

//  When the key adder goes down it may need the currently valid shared seed,
//  this will be indicated by the aggregator reply to a measurement message.
//  Choose minimum time before end of current validity interval, choose a new seed and share it.

use std::{
    cmp::Ordering,
    collections::{
        HashMap, HashSet,
        hash_map::Entry::{Occupied, Vacant},
    },
    net::SocketAddr,
    sync::{
        Arc,
        mpsc::{self, SyncSender},
    },
    time::{Duration, SystemTime},
};

use anyhow::{Context, Error, Result, anyhow};
use bincode::serde::decode_from_slice;
use chrono::{DateTime, Duration as ChronoDuration, NaiveDateTime, NaiveTime, Timelike, Utc};
use log::{error, info, trace, warn};
use serde::Deserialize;
use tokio::sync::mpsc as tokio_mpsc;

use enmprv::{
    bincoded::{
        INTERVAL_SEPARATOR_MILLIS, ShareSeedEncryptionInfo, ShareSeedInterval, ShareSeedReply,
        ShareSeedRequest, SharedSeed, get_bincode_config,
    },
    durationconfig::DurationConfig,
    openssl::{OpenSslSigVerifier, OpenSslSigner, PubKeyHash},
    util::{
        dt_utc_from_str, dt_utc_to_string, receive_signed_buffer, send_signed_message,
        tcp_stream_in_wg_intf, time_stamp_milli_secs,
    },
};

use crate::{P1EncryptConfig, P1Telegram, P1TelegramOrSharedSeedRequest};

#[derive(Deserialize, Debug, Clone)]
pub(crate) struct SharedSeedsConfig {
    startup_wait: DurationConfig,
    random_wait: DurationConfig,
    repeat_interval: DurationConfig,
    pub keep_interval: DurationConfig, // how long to keep a shared seed for encrypting measurements from the past.
    replace_old_seeds_interval: DurationConfig,
    hash_function_name: String,
    seed_size: u16,
    pub validity_period: DurationConfig,
    pub before_next_period: DurationConfig, // provide new share seed before current one expires.
    pub ten_pow_modulus: u16,
    pub ten_pow_divisor: u16,
}

// for a meter_pkh/meas_dt pair determine whether to
// - provide the starting time of a new validity interval with a new random seed, or
// - use a shared seed from the cache
fn new_interval_not_before_dt_opt(
    shared_seeds_status_cache: &SharedSeedsStatusCache,
    meter_pkh: &PubKeyHash,
    meas_dt: &DateTime<Utc>,
    seed_size: usize,
) -> Option<(SharedSeed, DateTime<Utc>)> {
    if let Some((_sent_to_ka, earlier_share_seed_request)) =
        shared_seeds_status_cache.get(meter_pkh)
    {
        // check the validity period of the entry against the msrmnt time.
        let msrmnt_dt_stamp = dt_utc_to_string(meas_dt);
        if msrmnt_dt_stamp.cmp(&earlier_share_seed_request.interval.not_after_dt_stamp)
            == Ordering::Greater
        {
            trace!(
                "{meter_pkh} {} msmrnt is after earlier: {earlier_share_seed_request}",
                &msrmnt_dt_stamp
            );
            let new_seed = SharedSeed::new(seed_size); // this may take some time
            Some((new_seed, *meas_dt))
        } else if msrmnt_dt_stamp.cmp(&earlier_share_seed_request.interval.not_before_dt_stamp)
            == Ordering::Less
        {
            warn!(
                "{meter_pkh} {} msmrnt is before interval: {}",
                &msrmnt_dt_stamp, earlier_share_seed_request.interval,
            );
            None
        } else {
            // shared seed available, new new one random seed needed.
            None
        }
    } else {
        // this might take some time
        let new_seed = SharedSeed::new(seed_size);
        let now = time_stamp_milli_secs();
        Some((new_seed, now))
    }
}

// for a given meter serial nr and a starting time of validity interval, provide a new share seed request
fn new_share_seed_request(
    meter_pkh: &PubKeyHash,
    new_seed: SharedSeed,
    not_before_dt: &DateTime<Utc>,
    config: &P1EncryptConfig,
    seed_validity_period: &ChronoDuration,
) -> Result<ShareSeedRequest> {
    let two_minutes_after = *not_before_dt + ChronoDuration::minutes(2);
    let date_after = two_minutes_after.date_naive();
    let time_after = two_minutes_after.time();
    let Some(whole_minute_after) =
        NaiveTime::from_hms_opt(time_after.hour(), time_after.minute(), 0)
    else {
        return Err(anyhow!("no whole minute for {time_after:?}"));
    };
    let new_start_dt: DateTime<Utc> = DateTime::from_naive_utc_and_offset(
        NaiveDateTime::new(date_after, whole_minute_after),
        Utc,
    );
    let new_not_before_dt = new_start_dt + ChronoDuration::milliseconds(INTERVAL_SEPARATOR_MILLIS);
    let new_not_before_dt_stamp = dt_utc_to_string(&new_not_before_dt);
    let new_not_after_dt = new_start_dt + *seed_validity_period;
    let new_not_after_dt_stamp = dt_utc_to_string(&new_not_after_dt);

    Ok(ShareSeedRequest {
        meter_pkh: meter_pkh.to_owned(),
        encryption_info: ShareSeedEncryptionInfo {
            hash_function_name: config.shared_seeds.hash_function_name.to_owned(),
            seed: new_seed,
            ten_pow_modulus: config.shared_seeds.ten_pow_modulus,
            ten_pow_divisor: config.shared_seeds.ten_pow_divisor,
        },
        interval: ShareSeedInterval {
            not_before_dt_stamp: new_not_before_dt_stamp,
            not_after_dt_stamp: new_not_after_dt_stamp,
        },
    })
}

enum SharedSeedState {
    New,                // when created, or with new interval and random seed.
    ReceivedByKeyAdder, // when the key adder confirmed reception of SharedSeedRequest before interval.not_before_dt .
    SentToEncryptor,    // when put on the queue to module p1aggr.
}

type SharedSeedsStatusCache = HashMap<PubKeyHash, (SharedSeedState, ShareSeedRequest)>;

// replace shared seeds that are in their before_next_period at the end of the validity period.
fn replace_old_shared_seeds(
    shared_seeds_status_cache: &mut SharedSeedsStatusCache,
    before_next_period: &ChronoDuration,
    seed_validity_period: &ChronoDuration,
    seed_size: usize,
) -> Result<()> {
    let latest_validity_dt = time_stamp_milli_secs() + *before_next_period;
    let latest_validity_stamp = dt_utc_to_string(&latest_validity_dt);
    trace!(
        "replacement latest_validity {}",
        dt_utc_to_string(&latest_validity_dt)
    );

    for (shared_seed_state, ssr) in shared_seeds_status_cache.values_mut() {
        if ssr.interval.not_after_dt_stamp < latest_validity_stamp {
            // replace ssr with validity period just after ssr.interval and new key.
            let mut prev_not_after_dt = dt_utc_from_str(&ssr.interval.not_after_dt_stamp)?;
            let mut next_not_after_dt;
            loop {
                // Try next interval until at latest_validity.
                // this loop can take long when the p1 stream was interrupted and this program continues,
                // therefore there is timeout on the input queue per hour
                // that calls this replace_old_shared_seeds().
                next_not_after_dt = prev_not_after_dt + *seed_validity_period;
                if next_not_after_dt > latest_validity_dt {
                    break;
                }
                prev_not_after_dt = next_not_after_dt;
            }

            let next_not_before_dt =
                prev_not_after_dt + ChronoDuration::milliseconds(INTERVAL_SEPARATOR_MILLIS);
            let next_not_before_dt_stamp = dt_utc_to_string(&next_not_before_dt);
            let next_not_after_dt_stamp = dt_utc_to_string(&next_not_after_dt);

            let mut new_ssr = ssr.clone();
            new_ssr.interval.not_before_dt_stamp = next_not_before_dt_stamp;
            new_ssr.interval.not_after_dt_stamp = next_not_after_dt_stamp;
            new_ssr.encryption_info.seed = SharedSeed::new(seed_size);

            *ssr = new_ssr;
            *shared_seed_state = SharedSeedState::New;
        }
    }
    Ok(())
}

fn share_seed(
    share_seed_req: &ShareSeedRequest,
    key_adder_socket_addr: &SocketAddr,
    key_adder_timeout: ChronoDuration,
    read_timeout_seconds: u64,
    signer_by_meter_pkh: &HashMap<PubKeyHash, Arc<OpenSslSigner>>,
    keyadder_sig_verifier: &OpenSslSigVerifier,
) -> Result<()> {
    let time_out_seconds = match u64::try_from(key_adder_timeout.num_seconds()) {
        Ok(s) => s,
        Err(e) => {
            return Err(Error::new(e).context(format!(
                "key_adder_timeout.num_seconds() {key_adder_timeout}"
            )));
        }
    };
    let Some(signer) = signer_by_meter_pkh.get(&share_seed_req.meter_pkh) else {
        return Err(anyhow!("no signer for {share_seed_req}"));
    };
    let key_adder_tcp_stream = &mut tcp_stream_in_wg_intf(key_adder_socket_addr, time_out_seconds)
        .context("tcp_stream_in_wg_intf")?;
    trace!("{signer}");
    send_signed_message(share_seed_req, signer, key_adder_tcp_stream)?;
    trace!("sent {share_seed_req}");

    let reply_buf = receive_signed_buffer(
        key_adder_tcp_stream,
        read_timeout_seconds,
        keyadder_sig_verifier,
    )?;
    let bincode_config = get_bincode_config();
    let (shared_seed_reply, size) =
        decode_from_slice::<ShareSeedReply, _>(&reply_buf, bincode_config)?;
    if size != reply_buf.len() {
        Err(anyhow!(
            "shared_seed_reply decode_from_slice incomplete, {} of {}",
            size,
            reply_buf.len()
        ))
    } else if let Some(mes) = shared_seed_reply.message {
        trace!("message from key adder: {mes}");
        Err(anyhow!("{mes} in ShareSeedReply"))
    } else {
        trace!("received from key adder: ok");
        Ok(())
    }
}

/// Long running thread receiving [P1Telegram]s from a channel and
/// passing [P1TelegramOrSharedSeedRequest]s for these [P1Telegram]s on a channel
/// and sending [ShareSeedRequest]s over a socket to a key adder.
pub(crate) fn share_seeds(
    config: &P1EncryptConfig,
    pkh_and_signer_by_meter_serial_nr: &HashMap<String, (PubKeyHash, Arc<OpenSslSigner>)>,
    from_share_keys_chan: &mut tokio_mpsc::Receiver<P1Telegram>,
    key_adder_socket_addr: &SocketAddr,
    to_encrypt_chan: &SyncSender<P1TelegramOrSharedSeedRequest>,
) -> Result<()> {
    let signer_by_meter_pkh = HashMap::<PubKeyHash, Arc<OpenSslSigner>>::from_iter(
        pkh_and_signer_by_meter_serial_nr
            .values()
            .map(|v| (v.0.to_owned(), v.1.clone())),
    );
    let key_adder_sig_verifier =
        OpenSslSigVerifier::new(&config.key_adder_public_key, &config.temp_dir)?;

    let key_adder_timeout = config.key_adder_timeout.time_delta();
    let read_timeout_seconds = u64::try_from(key_adder_timeout.num_seconds())
        .with_context(|| format!("u64::from num_seconds {key_adder_timeout}"))?;

    info!("random_wait_period {}", config.shared_seeds.random_wait);
    let random_wait_period = config.shared_seeds.random_wait.time_delta();
    info!("repeat_interval {}", config.shared_seeds.repeat_interval);
    let repeat_interval = config.shared_seeds.repeat_interval.time_delta();

    let seed_validity_period: ChronoDuration = config.shared_seeds.validity_period.time_delta();
    if seed_validity_period.num_minutes() <= 0 {
        return Err(anyhow!(
            "cannot use shared seeds seed_validity_period less than 1 minute"
        ));
    }
    let before_next_period = config.shared_seeds.before_next_period.time_delta();
    if before_next_period.num_minutes() <= 0 {
        return Err(anyhow!(
            "cannot use shared seeds before_next_period less than 1 minute"
        ));
    }
    if seed_validity_period <= before_next_period {
        return Err(anyhow!(
            "before_next_period {} {} seed_validity_period {}",
            before_next_period,
            "must be smaller than",
            seed_validity_period
        ));
    }

    // generate a new seed at start up and make it valid for the configured validity period.
    let mut shared_seeds_status_cache = SharedSeedsStatusCache::new();

    let mut next_repeat_interval_start = time_stamp_milli_secs()
        + ChronoDuration::seconds(
            (config.shared_seeds.startup_wait.std_duration().as_secs() as i64)
                + random_wait_period.num_seconds() * i64::from(rand::random::<u8>())
                    / i64::from(u8::MAX),
        );
    info!(
        "next_repeat_interval_start {}",
        dt_utc_to_string(&next_repeat_interval_start)
    );

    let mut next_replace_old_seeds = next_repeat_interval_start;

    let seed_size = config.shared_seeds.seed_size as usize;
    loop {
        let p1_telegram = match from_share_keys_chan.try_recv() {
            Err(tokio_mpsc::error::TryRecvError::Disconnected) => {
                return Err(anyhow!(
                    "recv_timeout, p1_telegram queue no longer available"
                ));
            }
            Err(tokio_mpsc::error::TryRecvError::Empty) => {
                if DateTime::<Utc>::from(SystemTime::now()) > next_replace_old_seeds {
                    info!("timeout on queue, replace_old_shared_seeds");
                    // avoid too long loop in replace_old_shared_seeds():
                    if let Err(e) = replace_old_shared_seeds(
                        &mut shared_seeds_status_cache,
                        &before_next_period,
                        &seed_validity_period,
                        seed_size,
                    ) {
                        error!("{e:#} at replace_old_shared_seeds");
                    }
                    next_replace_old_seeds = time_stamp_milli_secs()
                        + config.shared_seeds.replace_old_seeds_interval.time_delta();
                } else {
                    std::thread::sleep(Duration::from_secs(1));
                }
                continue;
            }
            Ok(p1t) => p1t,
        };

        // immediately pass on the p1_telegram for encryption to aggregator:
        let share_seeds_msrmnts = p1_telegram.msrmnts.clone();
        if let Err(queue_send_err) =
            to_encrypt_chan.try_send(P1TelegramOrSharedSeedRequest::P1Telegram(p1_telegram))
        {
            match queue_send_err {
                mpsc::TrySendError::Full(_full_error) => {
                    warn!("to_encrypt_chan queue full, ignored p1_telegram");
                }
                mpsc::TrySendError::Disconnected(ref _option_mkvs) => {
                    return Err(Error::new(queue_send_err));
                }
            }
        }

        if let Err(e) = replace_old_shared_seeds(
            &mut shared_seeds_status_cache,
            &before_next_period,
            &seed_validity_period,
            seed_size,
        ) {
            error!("{e:#} at replace_old_shared_seeds");
        };

        let mut earliest_meas_dt_by_meter_pkh: HashMap<&PubKeyHash, DateTime<Utc>> = HashMap::new();
        // avoid logging meter nr missing signer public key more than once:
        let mut missing_pubkey_logged: HashSet<&str> = HashSet::new();
        for msrmnt in &share_seeds_msrmnts {
            let Some((meter_pkh, _signer)) =
                pkh_and_signer_by_meter_serial_nr.get(&msrmnt.meter_nr)
            else {
                if !missing_pubkey_logged.contains(msrmnt.meter_nr.as_str()) {
                    error!("no public key for {} in {}", msrmnt.meter_nr, msrmnt);
                    missing_pubkey_logged.insert(&msrmnt.meter_nr);
                }
                continue;
            };
            match earliest_meas_dt_by_meter_pkh.entry(meter_pkh) {
                Vacant(ve) => {
                    ve.insert(msrmnt.dt_utc);
                }
                Occupied(mut oe) => {
                    if oe.get() > &msrmnt.dt_utc {
                        oe.insert(msrmnt.dt_utc);
                    }
                }
            }
        }

        for (meter_pkh, earliest_meas_dt) in earliest_meas_dt_by_meter_pkh {
            trace!(
                "from p1_telegram: {meter_pkh} {}",
                dt_utc_to_string(&earliest_meas_dt)
            );
            if let Some((new_seed, not_before_dt)) = new_interval_not_before_dt_opt(
                &shared_seeds_status_cache,
                meter_pkh,
                &earliest_meas_dt,
                seed_size,
            ) {
                let share_seed_req = match new_share_seed_request(
                    meter_pkh,
                    new_seed,
                    &not_before_dt,
                    config,
                    &seed_validity_period,
                ) {
                    Err(e) => {
                        error!("{e:#} at new_share_seed_request");
                        continue; // next meas_dt_by_meter_pkh
                    }
                    Ok(ssr) => ssr,
                };

                trace!("new {share_seed_req}"); // CHECKME: never seen, 20230911

                shared_seeds_status_cache
                    .insert(meter_pkh.to_owned(), (SharedSeedState::New, share_seed_req));
            }
            if next_repeat_interval_start > DateTime::<Utc>::from(SystemTime::now()) {
                trace!(
                    "next_repeat_interval_start: {}",
                    dt_utc_to_string(&next_repeat_interval_start)
                );
            } else {
                let Some(p1e_ssl_signer) = signer_by_meter_pkh.get(meter_pkh) else {
                    error!("no ssl signer for {meter_pkh}");
                    continue;
                };
                trace!("{meter_pkh} {p1e_ssl_signer}");

                info!("time is after next_repeat_interval_start: {next_repeat_interval_start}");
                // in repeat interval, send all available shared seeds again for the case that the key adder has restarted.
                for (shared_seed_state, ssr) in shared_seeds_status_cache.values_mut() {
                    if let Err(e) = share_seed(
                        ssr,
                        key_adder_socket_addr,
                        key_adder_timeout,
                        read_timeout_seconds,
                        &signer_by_meter_pkh,
                        &key_adder_sig_verifier,
                    ) {
                        error!("{e:#} at share_seed");
                        break; // no need to try more TCP
                    }
                    match *shared_seed_state {
                        SharedSeedState::New => {
                            // CHECKME: should the key adder reply be before the end of the interval?
                            *shared_seed_state = SharedSeedState::ReceivedByKeyAdder;
                        }
                        SharedSeedState::ReceivedByKeyAdder | SharedSeedState::SentToEncryptor => {}
                    }
                }

                next_repeat_interval_start = time_stamp_milli_secs()
                    + repeat_interval
                    + ChronoDuration::seconds(
                        (random_wait_period.num_minutes() * i64::from(rand::random::<u8>())) * 60
                            / i64::from(u8::MAX),
                    );
                info!(
                    "new next_repeat_interval_start: {}",
                    dt_utc_to_string(&next_repeat_interval_start)
                );
            }

            for (shared_seed_state, ssr) in shared_seeds_status_cache.values_mut() {
                match *shared_seed_state {
                    SharedSeedState::New | SharedSeedState::SentToEncryptor => {}
                    SharedSeedState::ReceivedByKeyAdder => {
                        // send share_seed_request to p1aggr for encryption:
                        if let Err(queue_send_err) = to_encrypt_chan
                            .try_send(P1TelegramOrSharedSeedRequest::ShareSeedRequest(ssr.clone()))
                        {
                            match queue_send_err {
                                mpsc::TrySendError::Full(_full_error) => {
                                    warn!("queue full, ignored shared seed");
                                    continue; // try again at next p1_telegram
                                }
                                mpsc::TrySendError::Disconnected(_option_mkvs) => {
                                    return Err(anyhow!(
                                        "P1TelegramOrSharedSeedRequest queue disconnected"
                                    ));
                                }
                            }
                        } else {
                            *shared_seed_state = SharedSeedState::SentToEncryptor;
                        }
                    }
                }
            }
        }
    }
}
