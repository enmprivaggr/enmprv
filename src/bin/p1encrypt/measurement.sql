CREATE TABLE IF NOT EXISTS measurement (
    local_meter_nr                     int         REFERENCES meter NOT NULL,
    msrmnt_type                        smallint    NOT NULL,
    interval_border                    smallint    NOT NULL,
    msrmnt_dt_utc                      timestamp   NOT NULL,
    msrmnt_value                       numeric     NOT NULL,
    CONSTRAINT unique_msrmnt_type_dt_utc
      UNIQUE(
        local_meter_nr,
        msrmnt_dt_utc,
        msrmnt_type));
