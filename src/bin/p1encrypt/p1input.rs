use std::{
    collections::{HashMap, HashSet},
    io::{ErrorKind, Read},
    str::{self, FromStr},
    thread,
    time::Duration,
};

use anyhow::{Error, Result, anyhow};
use chrono::{
    DateTime, Duration as ChronoDuration, Local, LocalResult, NaiveDate, NaiveDateTime, NaiveTime,
    TimeZone, Utc,
};
use log::{error, info, trace, warn};
use num::{Integer, ToPrimitive};
use serial2::{KeepSettings, Parity, SerialPort};
use tokio::sync::mpsc as tokio_mpsc;

use enmprv::{
    bincoded::{MeasurementInfo, P1MeasurementType},
    checkclocksync::RepeatCheckClockSync,
    measuredvalue::MeasuredValue,
    util::{time_stamp_milli_secs, udp_bind},
};

use crate::{P1EncryptConfig, P1Measurement, P1Telegram};

#[derive(Debug, Clone)]
struct LoggedMinMaxValue {
    physical_unit: String,
    min_value: MeasuredValue,
    max_value: MeasuredValue,
}

// P1 telegram as read from p1 terminal, and then queued
#[derive(Debug, Clone)]
pub struct P1TelegramBuilder {
    creation_dt_utc: DateTime<Utc>,
    used_obis_keys: HashSet<String>,
    obis_kf: ObisKeysFunctions,
    last_logged_keys_values: HashMap<String, String>,
    logged_keys_min_max_values: HashMap<String, LoggedMinMaxValue>,
    crc16: u16,
    input_errors: u64,
    meter_type_line_opt: Option<String>,
    last_serial_number_opt: Option<String>,
    last_time_stamp_opt: Option<DateTime<Utc>>,
    last_msrmnt_type_opt: Option<P1MeasurementType>,
    msrmnts: Vec<P1Measurement>,
}

type AddToP1TelegramFromObisKeyValue = fn(&mut P1TelegramBuilder, &str, &str) -> Result<()>; // &str args: obis_key, obis_value

type ObisKeysFunctions = HashMap<&'static str, AddToP1TelegramFromObisKeyValue>;

type StaticStr = &'static str;
struct ObisKeys {
    // Actually encountered obis keys:
    empty_key: StaticStr,
    power_time_stamp: StaticStr,
    device_serial_number: StaticStr,
    power_failures: StaticStr,
    text_message: StaticStr,
    tariff_el: StaticStr,
    device_type: StaticStr,
    time_and_unit: StaticStr,
    time_and_value: StaticStr,
    el_kw_delivered: StaticStr,
    el_kwh_to_tariff: StaticStr,
    el_kw_received: StaticStr,
    el_kwh_by_tariff: StaticStr,
    version_key: StaticStr,
    power_failure_event_log: StaticStr,
    num_voltage_sags_l1: StaticStr,
    num_voltage_sags_l2: StaticStr,
    num_voltage_sags_l3: StaticStr,
    num_voltage_swells_l1: StaticStr,
    num_voltage_swells_l2: StaticStr,
    num_voltage_swells_l3: StaticStr,
    el_current_l1: StaticStr,
    el_current_l2: StaticStr,
    el_current_l3: StaticStr,
    el_voltage_l1: StaticStr,
    el_voltage_l2: StaticStr,
    el_voltage_l3: StaticStr,
    el_kw_delivered_l1: StaticStr,
    el_kw_delivered_l2: StaticStr,
    el_kw_delivered_l3: StaticStr,
    el_kw_received_l1: StaticStr,
    el_kw_received_l2: StaticStr,
    el_kw_received_l3: StaticStr,
}

impl ObisKeys {
    fn new() -> Self {
        ObisKeys {
            empty_key: "",                 // for gas m3
            power_time_stamp: "0-n:1.0.m", // n: channel number 0-4, m mostly zero.
            device_serial_number: "0-n:96.1.m",
            power_failures: "0-n:96.7.m", // m = 9: long power failures, m = 21 power failures.
            text_message: "0-n:96.13.m",  // m=0 or m=1 encountered
            tariff_el: "0-n:96.14.m",     // tariff electricity, m is the tariff.
            device_type: "0-n:24.1.m",
            time_and_unit: "0-n:24.3.m",
            // Encountered in telegram lines:
            //     0-1:96.1.0(3338303034303038323431313331363132)
            //     0-1:24.1.0(03)
            //     0-1:24.3.0(230827170000)(08)(60)(1)(0-1:24.2.0)(m3)
            //     (20204.264)
            // Parse these into a Measurement by collecting:
            //   device id (1st line)
            //   gas type (2nd line)
            //   time stamp (3rd line), verify m3 for gas type,
            //   value (4th line)
            time_and_value: "0-n:24.2.m",
            // 0-n:24.2.1.255, Last 5-minute Meter reading Heat or Cold in 0,01 GJ and capture time
            // Encountered in telegram lines:
            //     0-1:24.1.0(003)
            //     0-1:96.1.0(4730303233353631323236303936333134)
            //     0-1:24.2.1(230826000000S)(09100.175*m3)
            // The m3 is against the spec of 0,01 GJ, just assume it is NL gas in m3.
            // Parse these into a P1Measurement by collecting:
            //   gas type (1st line)
            //   device id (2nd line)
            //   time stamp and value (3rd line), verify m3 for gas type
            el_kwh_to_tariff: "1-n:1.8.m", // Import tariff = m = 1 or 2
            el_kwh_by_tariff: "1-n:2.8.m", // Export tariff = m = 1 or 2
            version_key: "1-n:0.2.m",      // encountered at channel 3, m = 8
            power_failure_event_log: "1-n:99.97.m",
            num_voltage_sags_l1: "1-n:32.32.m", // in phase L1
            num_voltage_sags_l2: "1-n:52.32.m", // in phase L2
            num_voltage_sags_l3: "1-n:72.32.m", // in phase L3
            num_voltage_swells_l1: "1-n:32.36.m",
            num_voltage_swells_l2: "1-n:52.36.m",
            num_voltage_swells_l3: "1-n:72.36.m",
            el_current_l1: "1-n:31.7.m", // Instantaneous current in L1, etc.
            el_current_l2: "1-n:51.7.m",
            el_current_l3: "1-n:71.7.m",
            el_voltage_l1: "1-n:32.7.m", // Instantaneous voltage in L1
            el_voltage_l2: "1-n:52.7.m",
            el_voltage_l3: "1-n:72.7.m",
            el_kw_delivered: "1-n:1.7.m", // Actual kW delivered to meter
            el_kw_delivered_l1: "1-n:21.7.m", // Instantaneous active power L1 (+P), duplicate of el_kw_delivered on single phase?
            el_kw_delivered_l2: "1-n:41.7.m",
            el_kw_delivered_l3: "1-n:61.7.m",
            el_kw_received: "1-n:2.7.m", // Actual kW received from meter
            el_kw_received_l1: "1-n:22.7.m", // Instantaneous active power L1 (-P)
            el_kw_received_l2: "1-n:42.7.m",
            el_kw_received_l3: "1-n:62.7.m",
        }
    }
}

fn new_obis_kf() -> ObisKeysFunctions {
    // Parsing functions, adding parsed into to a P1TelegramBuilder
    type Akv = AddToP1TelegramFromObisKeyValue;
    // for collecting measurements:
    let add_power_time_stamp = P1TelegramBuilder::add_power_time_stamp as Akv;
    let add_meter_serial_number = P1TelegramBuilder::add_meter_serial_number as Akv;
    let add_kwh_msrmnt_to_tariff = P1TelegramBuilder::add_kwh_msrmnt_to_tariff as Akv;
    let add_kwh_msrmnt_by_tariff = P1TelegramBuilder::add_kwh_msrmnt_by_tariff as Akv;
    let add_kw_msrmnt_to = P1TelegramBuilder::add_kw_msrmnt_to as Akv;
    let add_kw_msrmnt_by = P1TelegramBuilder::add_kw_msrmnt_by as Akv;
    let add_device_type = P1TelegramBuilder::add_device_type as Akv;
    let add_time_and_unit = P1TelegramBuilder::add_time_and_unit as Akv;
    let add_time_and_value = P1TelegramBuilder::add_time_and_value as Akv;
    let add_empty_key = P1TelegramBuilder::add_empty_key as Akv;
    // logged only:
    let log_version = P1TelegramBuilder::log_version as Akv;
    let log_tariff = P1TelegramBuilder::log_tariff as Akv;
    let log_text_message = P1TelegramBuilder::log_text_message as Akv;
    let log_power_failures = P1TelegramBuilder::log_power_failures as Akv;
    let log_power_failure_event_log = P1TelegramBuilder::log_power_failure_event_log as Akv;
    let log_voltage_sags_swells_line = P1TelegramBuilder::log_voltage_sags_swells_line as Akv;
    let log_el_current_line = P1TelegramBuilder::log_el_current_line as Akv;
    let log_el_voltage_line = P1TelegramBuilder::log_el_voltage_line as Akv;
    let log_kw_msrmnt_line = P1TelegramBuilder::log_kw_msrmnt_line as Akv;

    // associate obis keys with parsing routines:
    let keys = ObisKeys::new();
    let mut res = ObisKeysFunctions::new();
    for (key, akv) in [
        // for collecting measurements:
        (keys.power_time_stamp, add_power_time_stamp),
        (keys.device_serial_number, add_meter_serial_number),
        (keys.device_type, add_device_type),
        (keys.time_and_unit, add_time_and_unit),
        (keys.time_and_value, add_time_and_value),
        (keys.empty_key, add_empty_key),
        (keys.el_kwh_to_tariff, add_kwh_msrmnt_to_tariff),
        (keys.el_kwh_by_tariff, add_kwh_msrmnt_by_tariff),
        (keys.el_kw_delivered, add_kw_msrmnt_to), // delivered to meter
        (keys.el_kw_received, add_kw_msrmnt_by),  // received from meter
        // logged
        (keys.version_key, log_version),
        (keys.el_kw_delivered_l1, log_kw_msrmnt_line),
        (keys.el_kw_delivered_l2, log_kw_msrmnt_line),
        (keys.el_kw_delivered_l3, log_kw_msrmnt_line),
        (keys.el_kw_received_l1, log_kw_msrmnt_line),
        (keys.el_kw_received_l2, log_kw_msrmnt_line),
        (keys.el_kw_received_l3, log_kw_msrmnt_line),
        (keys.tariff_el, log_tariff),
        (keys.text_message, log_text_message),
        (keys.power_failures, log_power_failures),
        (keys.power_failure_event_log, log_power_failure_event_log),
        (keys.num_voltage_sags_l1, log_voltage_sags_swells_line),
        (keys.num_voltage_sags_l2, log_voltage_sags_swells_line),
        (keys.num_voltage_sags_l3, log_voltage_sags_swells_line),
        (keys.num_voltage_swells_l1, log_voltage_sags_swells_line),
        (keys.num_voltage_swells_l2, log_voltage_sags_swells_line),
        (keys.num_voltage_swells_l3, log_voltage_sags_swells_line),
        (keys.el_current_l1, log_el_current_line),
        (keys.el_current_l2, log_el_current_line),
        (keys.el_current_l3, log_el_current_line),
        (keys.el_voltage_l1, log_el_voltage_line),
        (keys.el_voltage_l2, log_el_voltage_line),
        (keys.el_voltage_l3, log_el_voltage_line),
    ] {
        if let Some(prev_akv) = res.get(&key) {
            error!("double entry for {key}: {prev_akv:?} and {akv:?}")
        } else {
            res.insert(key, akv);
        }
    }
    res
}

fn utc_from_msrmnt_time_str(
    msrmnt_local_time_stamp: &str,
    now: DateTime<Utc>,
) -> Result<DateTime<Utc>> {
    // determine utc from msrmnt_time_str in local time
    // Examples 210925140000, 170108161107
    if !msrmnt_local_time_stamp.chars().all(|c| c.is_ascii_digit()) {
        return Err(anyhow!("not all digits"));
    }
    let u32_from_str = {
        |s| match u32::from_str(s) {
            Ok(u) => Ok(u),
            Err(e) => Err(anyhow!("{e:?} at u32::from_str {s}")),
        }
    };
    let input_len = msrmnt_local_time_stamp.len();
    if input_len < 12 {
        return Err(anyhow!(
            "time stamp {msrmnt_local_time_stamp} shorter than 12"
        ));
    };
    let yy = u32_from_str(&msrmnt_local_time_stamp[..input_len - 10])?;
    let mo = u32_from_str(&msrmnt_local_time_stamp[input_len - 10..input_len - 8])?;
    let dd = u32_from_str(&msrmnt_local_time_stamp[input_len - 8..input_len - 6])?;
    let hh = u32_from_str(&msrmnt_local_time_stamp[input_len - 6..input_len - 4])?;
    let mm = u32_from_str(&msrmnt_local_time_stamp[input_len - 4..input_len - 2])?;
    let ss = u32_from_str(&msrmnt_local_time_stamp[input_len - 2..])?;

    // CHECKME untested:
    let yyi32 = yy as i32
        + if yy >= 2000 {
            0
        } else {
            // Y2100 Y21C Y21K
            if yy >= 100 {
                return Err(anyhow!(
                    "no century for year {yy} in {msrmnt_local_time_stamp}"
                ));
            }
            // determine century for 2 digit year from current date
            let y2k_i32: i32 = 2000;
            let y2k_dt = NaiveDate::from_ymd_opt(y2k_i32, 1, 1).ok_or(anyhow!("y2k_dt"))?;
            let yyyy_since_y2k = now
                .date_naive()
                .years_since(y2k_dt)
                .ok_or(anyhow!("yyyy_since_y2k"))? as i32;
            let cc_since_y2k = yyyy_since_y2k / 100;
            let yy_since_y2k = (yyyy_since_y2k % 100) as u32;
            y2k_i32
                + 100
                    * (cc_since_y2k
                        + if yy == 99 && yy_since_y2k == 00 {
                            // yy from previous century
                            -1
                        } else if yy == 0 && yy_since_y2k == 99 {
                            // yy from next century
                            1
                        } else {
                            0
                        })
        };
    let Some(naive_date) = NaiveDate::from_ymd_opt(yyi32, mo, dd) else {
        return Err(anyhow!("no NaiveDate from {yyi32} {mo} {dd}"));
    };
    let Some(naive_time) = NaiveTime::from_hms_opt(hh, mm, ss) else {
        return Err(anyhow!("no NaiveTime from {hh} {mm} {ss}"));
    };
    let naive_date_time = NaiveDateTime::new(naive_date, naive_time);
    // This might infer the local time zone of the meter, but for now this is not reported/checked.
    let local_dt = match Local.from_local_datetime(&naive_date_time) {
        LocalResult::None => {
            return Err(anyhow!("no local time zone for {naive_date_time}"));
        }
        LocalResult::Single(ldt) => ldt,
        LocalResult::Ambiguous(ldt1, ldt2) => {
            // This happens when the local time is ambiguous, e.g. during daylight savings time changes.
            // See also strip_optional_suffix_from_time_stamp() for Winter/Summer suffix strip.
            warn!(
                "local measurement time {msrmnt_local_time_stamp} has ambiguous utc, ignoring {ldt1}, using {ldt2}"
            );
            ldt2
        }
    };
    Ok(local_dt.with_timezone(&Utc))
}

impl P1Telegram {
    fn measurement_infos(&self) -> Result<Vec<MeasurementInfo>> {
        self.measurement_infos_for_aggr(true, None)
    }

    fn log_msrmnts(&self) {
        match self.measurement_infos() {
            Err(e) => error!("{e:?}"),
            Ok(m_infos) => {
                for msrmnt_inf in m_infos {
                    use enmprv::bincoded::MeasurementInfo::*;
                    match msrmnt_inf {
                        DirectValue(msrmnt_type, value) => {
                            info!("{:>11} {msrmnt_type:?}", value.to_string(3));
                        }
                        EncryptedValue(msrmnt_type, value) => {
                            info!("{:>11} {msrmnt_type:?} encrypted", value.to_string(3));
                        }
                        MeterSerialNr(..) | Mutc(..) => {
                            info!("{msrmnt_inf:?}");
                        }
                    }
                }
            }
        }
    }
}

fn strip_optional_dst_from_time_stamp(stamp: &str) -> &str {
    // From DSMR 5.0.2,
    // P1 Companion Standard, Dutch Smart Meter Requirements
    // Section 6.4 Representation of COSEM objects:
    // Meter timestamps are an ASCII presentation of Time stamp with
    // Year, Month, Day, Hour, Minute, Second,
    // and an indication whether DST is active
    // (X=S) or DST is not active (X=W).
    // YYMMDDhhmmssX
    for suffix in ['W', 'S'] {
        // Suffix indicates Winter, Summer
        if let Some(stripped) = stamp.strip_suffix(suffix) {
            return stripped;
        }
    }
    stamp
}

fn decode_cosem_octets(cosem_octet_str: &str) -> Result<String> {
    // use an inverse of hexdump, normally used for meter serial numbers.
    if cosem_octet_str.len().is_odd() {
        return Err(anyhow!(
            "odd length {}: {cosem_octet_str}",
            cosem_octet_str.len()
        ));
    }
    let mut byte_pair: [u8; 2] = [0, 0];
    let mut unhexed = Vec::<u8>::new();
    for (idx, &b) in cosem_octet_str.as_bytes().iter().enumerate() {
        byte_pair[idx & 1] = b;
        if idx.is_even() {
            continue;
        };
        let byte_pair_str = str::from_utf8(&byte_pair)
            .map_err(|e| Error::new(e).context(format!("at {idx} from_utf8")))?;
        let hex_pair: u8 =
            u8::from_str(byte_pair_str).map_err(|e| anyhow!("{e:?} at {idx} from_str"))?;
        let char_pair = format!("{hex_pair:02}");
        unhexed.push(
            u8::from_str_radix(&char_pair, 16)
                .map_err(|e| Error::new(e).context(format!("at {idx} from_str_radix")))?,
        );
    }
    String::from_utf8(unhexed).map_err(|e| Error::new(e).context("final from_utf8"))
}

fn suffix_u8(obis_key: &str) -> Result<u8> {
    let Some(last_dot_pos) = obis_key.rfind('.') else {
        return Err(anyhow!("missing dot in obis_key {obis_key}"));
    };
    let suffix = &obis_key[last_dot_pos + 1..];
    u8::from_str(suffix).map_err(|_e| anyhow!("unexpected suffix {suffix} in obis_key {obis_key}"))
}

impl P1TelegramBuilder {
    fn new(
        last_logged_keys_values: HashMap<String, String>,
        logged_keys_min_max_values: HashMap<String, LoggedMinMaxValue>,
    ) -> Self {
        P1TelegramBuilder {
            used_obis_keys: HashSet::new(),
            obis_kf: new_obis_kf(),
            last_logged_keys_values,
            logged_keys_min_max_values,
            crc16: 0,
            input_errors: 0,
            meter_type_line_opt: None,
            creation_dt_utc: time_stamp_milli_secs(),
            last_time_stamp_opt: None,
            last_serial_number_opt: None,
            last_msrmnt_type_opt: None,
            msrmnts: Vec::new(),
        }
    }

    fn build(
        &self,
        window_msrmnt_time_start: &ChronoDuration,
        window_msrmnt_time_end: &ChronoDuration,
    ) -> Result<P1Telegram> {
        if self.meter_type_line_opt.is_none() {
            Err(anyhow!("no identity line"))
        } else {
            // verify that each measurement type occurs only once:
            let mut msrmnt_types = HashSet::<P1MeasurementType>::new();
            for msrmnt in &self.msrmnts {
                if !msrmnt_types.insert(msrmnt.p1_msrmnt_type) {
                    return Err(anyhow!("unit more than once: {:?}", msrmnt.p1_msrmnt_type));
                }
                // Ensure that the msrmnt.mrmnt_dt_utc is not too far into in the future.
                // Otherwise the check lateron that a later measurement comes after this one
                // might prevent use of only later measurements for a long time.
                if msrmnt.dt_utc + *window_msrmnt_time_start < self.creation_dt_utc {
                    return Err(anyhow!(
                        "measurement too early: {msrmnt}, window_msrmnt_time_start {window_msrmnt_time_start}"
                    ));
                }
                // Gas measurements can be 5 quarters of an hour late, allow a maximum delay.
                if msrmnt.dt_utc - *window_msrmnt_time_end > self.creation_dt_utc {
                    return Err(anyhow!(
                        "measurement too late: {msrmnt}, window_msrmnt_time_end {window_msrmnt_time_end}"
                    ));
                }
            }
            Ok(P1Telegram {
                msrmnts: self.msrmnts.to_vec(),
            })
        }
    }

    fn add_line(&mut self, line: &str) {
        if line.is_empty() {
            return;
        }
        if self.meter_type_line_opt.is_none() {
            // first line has the meter type
            self.meter_type_line_opt = Some(line.to_string());
        } else if line.starts_with('!') {
            // last telegram line
            if line.len() == 5 {
                warn!("last line {line} expecting crc {:04X}", self.crc16);
            }
        } else if let Err(e) = self.add_obis_line(line) {
            self.input_errors += 1;
            error!("{e:#} for line: {line}");
        }
    }

    fn add_crc(&mut self, b: u8) {
        // See https://www.netbeheernederland.nl/_upload/Files/Slimme_meter_15_2bff566986.pdf
        // section 5.2:
        // CRC is a CRC16 value calculated over the preceding characters in the data message
        // (from “/” to “!” using the polynomial: x^16+x^15+x^2+1).
        // CRC16 uses no XOR in, no XOR out and is computed with least significant bit first.
        // The value is represented as 4 hexadecimal characters (MSB first).
        self.crc16 ^= b as u16;
        for _ in 0..u8::BITS {
            if self.crc16 & 1 == 0 {
                self.crc16 >>= 1;
            } else {
                self.crc16 >>= 1;
                self.crc16 ^= 0xA001;
            }
        }
    }

    fn add_obis_line(&mut self, line: &str) -> Result<()> {
        // line with obis key and value, e.g. 0-0:96.1.1(43214321432143214321)
        // values can be repeated on the same line by appending (value) brackets. A value may be empty ().
        let Some(first_open_bracket_pos) = line.find('(') else {
            return Err(anyhow!("missing open bracket"));
        };
        if !line.ends_with(')') {
            return Err(anyhow!("missing end closing bracket"));
        }
        let obis_key = &line[..first_open_bracket_pos];
        let obis_value = &line[first_open_bracket_pos + 1..line.len() - 1];

        if obis_key.is_empty() {
            // allow multiple empty keys for more than one connected meter
            return self.add_empty_key(obis_key, obis_value);
        }

        if !self.used_obis_keys.insert(obis_key.to_string()) {
            return Err(anyhow!("duplicate key {obis_key}"));
        }

        let Some(first_dash_pos) = obis_key.find('-') else {
            return Err(anyhow!("missing dash for channel nr"));
        };
        let Some(first_colon_pos) = obis_key.find(':') else {
            return Err(anyhow!("missing colon for channel nr"));
        };
        if first_dash_pos + 2 != first_colon_pos {
            return Err(anyhow!("unknown size for channel nr"));
        }
        let Some(last_dot_pos) = obis_key.rfind('.') else {
            return Err(anyhow!("missing dot"));
        };
        if last_dot_pos <= first_colon_pos {
            return Err(anyhow!("no dot after colon"));
        }
        let channel = &obis_key[first_dash_pos + 1..first_colon_pos];
        match channel {
            "0" | "1" | "2" | "3" | "4" => {}
            _ => {
                return Err(anyhow!(
                    "unexpected channel number {channel} in key {obis_key}"
                ));
            }
        }
        // check suffix presence, suffix value not used here
        suffix_u8(obis_key)?;
        // use the channel number as n, and the suffix as m, and lookup the parsing function:
        let mut obis_key_n_channel_m_suffix = String::new();
        obis_key_n_channel_m_suffix.push_str(&obis_key[..first_dash_pos + 1]);
        obis_key_n_channel_m_suffix.push('n');
        obis_key_n_channel_m_suffix.push_str(&obis_key[first_colon_pos..last_dot_pos + 1]);
        obis_key_n_channel_m_suffix.push('m');
        let Some(p1t_from_key_value) = self.obis_kf.get(obis_key_n_channel_m_suffix.as_str())
        else {
            return Err(anyhow!(
                "unknown key {obis_key}, {obis_key_n_channel_m_suffix}"
            ));
        };
        p1t_from_key_value(self, obis_key, obis_value)
    }

    fn log_key_new_value_once(
        &mut self,
        message: &str,
        obis_key: &str,
        obis_value: &str,
    ) -> Result<()> {
        if self
            .last_logged_keys_values
            .get(obis_key)
            .is_none_or(|last_value| last_value != obis_value)
        {
            trace!("{message}, key: \"{obis_key}\", new value: \"{obis_value}\"");
            self.last_logged_keys_values
                .insert(obis_key.to_owned(), obis_value.to_owned());
        }
        Ok(())
    }

    fn log_key_min_max_value(
        &mut self,
        message: &str,
        obis_key: &str,
        obis_value: &str,
    ) -> Result<()> {
        let Some(star_pos) = obis_value.find('*') else {
            return Err(anyhow!("missing star * in obis_value {obis_value}"));
        };
        let value = MeasuredValue::try_from(&obis_value[..star_pos])?;
        let physical_unit = &obis_value[star_pos + 1..]; // kW, kWh, A, V, ...
        if let Some(logged_min_max_value) = self.logged_keys_min_max_values.get_mut(obis_key) {
            if logged_min_max_value.physical_unit != physical_unit {
                return Err(anyhow!(
                    "for key {obis_key} expected unit {} in {obis_value}",
                    logged_min_max_value.physical_unit
                ));
            }
            if value < logged_min_max_value.min_value {
                trace!("{message}, key: \"{obis_key}\", new minimum value: \"{obis_value}\"");
                logged_min_max_value.min_value = value;
            } else if value > logged_min_max_value.max_value {
                trace!("{message}, key: \"{obis_key}\", new maximum value: \"{obis_value}\"");
                logged_min_max_value.max_value = value;
            } // else within min/max, ignore
        } else {
            trace!("{message}, key: \"{obis_key}\", first min/max value: \"{obis_value}\"");
            self.logged_keys_min_max_values.insert(
                obis_key.to_owned(),
                LoggedMinMaxValue {
                    physical_unit: physical_unit.to_owned(),
                    min_value: value.clone(),
                    max_value: value,
                },
            );
        }
        Ok(())
    }

    fn add_power_time_stamp(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        let power_time_stamp = strip_optional_dst_from_time_stamp(obis_value);
        match utc_from_msrmnt_time_str(power_time_stamp, self.creation_dt_utc) {
            Ok(meter_dt_utc) => {
                let delta_meter_host = meter_dt_utc - self.creation_dt_utc;
                info!(
                    "time delta meter-host: {} sec",
                    delta_meter_host.num_milliseconds() as f64 / 1000f64
                );
                self.last_time_stamp_opt = Some(meter_dt_utc.min(self.creation_dt_utc));
                Ok(())
            }
            Err(e) => Err(e.context(format!("key: {obis_key} value: {obis_value}"))),
        }
    }

    fn add_meter_serial_number(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        // gas or electricity meter serial number
        let serial_nr = match decode_cosem_octets(obis_value) {
            Ok(s) => s,
            Err(e) => {
                error!(
                    "{e:?} for meter serial number at obis_key {obis_key}, using obis_value {obis_value}"
                );
                obis_value.to_string()
            }
        };
        self.last_serial_number_opt = Some(serial_nr);
        Ok(())
    }

    fn add_kwh_msrmnt_to_tariff(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        let tariff_type = match suffix_u8(obis_key)? {
            1 => P1MeasurementType::El_kWh_to_tariff_1,
            2 => P1MeasurementType::El_kWh_to_tariff_2,
            _ => return Err(anyhow!("no to tariff known for {obis_key}")),
        };
        self.add_kwh_msrmnt_type(obis_key, obis_value, tariff_type)
    }

    fn add_kwh_msrmnt_by_tariff(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        let tariff_type = match suffix_u8(obis_key)? {
            1 => P1MeasurementType::El_kWh_by_tariff_1,
            2 => P1MeasurementType::El_kWh_by_tariff_2,
            _ => return Err(anyhow!("no by tariff known for {obis_key}")),
        };
        self.add_kwh_msrmnt_type(obis_key, obis_value, tariff_type)
    }

    fn add_kwh_msrmnt_type(
        &mut self,
        obis_key: &str,
        obis_value: &str,
        msrmnt_type: P1MeasurementType,
    ) -> Result<()> {
        let Some(msrmnt_val_str) = obis_value.strip_suffix("*kWh") else {
            return Err(anyhow!("no *kWh at end of {obis_value} for key {obis_key}"));
        };
        let msrmnt_val = match MeasuredValue::try_from(msrmnt_val_str) {
            Ok(mv) => mv,
            Err(e) => {
                return Err(anyhow!(
                    "{e:?} for {msrmnt_val_str} in {obis_value} for key {obis_key}"
                ));
            }
        };
        if self.msrmnts.iter().any(|m| m.p1_msrmnt_type == msrmnt_type) {
            return Err(anyhow!(
                "more than one {msrmnt_type:?} measurement, key {obis_key} value {obis_value}"
            ));
        };
        let Some(ref last_serial_number) = self.last_serial_number_opt else {
            return Err(anyhow!(
                "no earlier meter serial number for key {obis_key} value {obis_value} type {msrmnt_type:?}"
            ));
        };
        let msrmnt_dt_utc = if let Some(last_dt_utc) = self.last_time_stamp_opt {
            last_dt_utc
        } else {
            self.creation_dt_utc
        };
        self.msrmnts.push(P1Measurement {
            meter_nr: last_serial_number.to_string(),
            p1_msrmnt_type: msrmnt_type,
            dt_utc: msrmnt_dt_utc,
            value: msrmnt_val,
        });

        Ok(())
    }

    fn add_kw_msrmnt_to(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.add_kw_msrmnt_type(obis_key, obis_value, P1MeasurementType::El_kW_to)
    }

    fn add_kw_msrmnt_by(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.add_kw_msrmnt_type(obis_key, obis_value, P1MeasurementType::El_kW_by)
    }

    fn add_kw_msrmnt_type(
        &mut self,
        obis_key: &str,
        obis_value: &str,
        msrmnt_type: P1MeasurementType,
    ) -> Result<()> {
        let Some(ref last_serial_number) = self.last_serial_number_opt else {
            return Err(anyhow!(
                "no earlier device id for key {obis_key} value {obis_value}"
            ));
        };
        let Some(msrmnt_val_str) = obis_value.strip_suffix("*kW") else {
            return Err(anyhow!("no *kW at end of {obis_value} for key {obis_key}"));
        };
        let msrmnt_val = match MeasuredValue::try_from(msrmnt_val_str) {
            Ok(br) => br,
            Err(e) => {
                return Err(anyhow!(
                    "{e:?} for {msrmnt_val_str} in {obis_value} for key {obis_key}"
                ));
            }
        };
        let time_stamp = if let Some(pts) = self.last_time_stamp_opt {
            pts
        } else {
            self.creation_dt_utc
        };
        self.msrmnts.push(P1Measurement {
            meter_nr: last_serial_number.to_string(),
            p1_msrmnt_type: msrmnt_type,
            dt_utc: time_stamp,
            value: msrmnt_val,
        });
        Ok(())
    }

    fn add_device_type(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        if obis_value.trim_start_matches('0') != "3" {
            // encountered gas device types: 03 and 003
            return Err(anyhow!(
                "{obis_value} not a gas device type, key {obis_key}"
            ));
        }
        let Some(device_id) = &self.last_serial_number_opt else {
            return Err(anyhow!(
                "no earlier device id for key {obis_key} value {obis_value}"
            ));
        };
        self.last_serial_number_opt = Some(device_id.to_string());
        Ok(())
    }

    fn add_time_and_unit(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        // obis_value example: 210925140000)(08)(60)(1)(0-1:24.2.0)(m3
        // Use this meter time stamp when it is not ahead of self.creation_time.
        let Some(close_bracket_pos) = obis_value.find(')') else {
            return Err(anyhow!(
                "missing closing bracket for meter time stamp for key {obis_key} value {obis_value}"
            ));
        };
        let msrmnt_time_str = strip_optional_dst_from_time_stamp(&obis_value[..close_bracket_pos]);
        match utc_from_msrmnt_time_str(msrmnt_time_str, self.creation_dt_utc) {
            Err(e) => {
                return Err(e.context(format!("key {obis_key} value {obis_value}")));
            }
            Ok(mutc) => {
                if mutc > self.creation_dt_utc {
                    error!(
                        "measurement at {mutc} is ahead of utc {}",
                        self.creation_dt_utc
                    );
                }
                self.last_time_stamp_opt = Some(mutc.min(self.creation_dt_utc));
            }
        }
        let m3_suffix = "m3"; // final closing bracket already used for obis_value
        if obis_value.ends_with(m3_suffix) {
            self.last_msrmnt_type_opt = Some(P1MeasurementType::NgNl_m3);
            Ok(())
        } else {
            Err(anyhow!(
                "no measurement unit for {obis_key} value {obis_value}",
            ))
        }
    }

    fn add_time_and_value(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        //     0-1:24.2.1(230826000000S)(09100.175*m3)
        let Some(close_bracket_pos) = obis_value.find(')') else {
            return Err(anyhow!(
                "missing closing bracket for meter time stamp for key {obis_key} value {obis_value}"
            ));
        };
        let utc_time_stamp = match utc_from_msrmnt_time_str(
            strip_optional_dst_from_time_stamp(&obis_value[..close_bracket_pos]),
            self.creation_dt_utc,
        ) {
            Err(e) => {
                return Err(e.context(format!("key {obis_key} value {obis_value}")));
            }
            Ok(mutc) => mutc,
        };
        let Some(ref last_serial_number) = self.last_serial_number_opt else {
            return Err(anyhow!(
                "missing earlier equipment id for value {obis_value}",
            ));
        };
        let m3_suffix = "*m3"; // closing bracket already used for obis_value
        let unit = if obis_value.ends_with(m3_suffix) {
            P1MeasurementType::NgNl_m3
        } else {
            return Err(anyhow!(
                "no measurement unit for {obis_key} value {obis_value}",
            ));
        };

        if Some("(") != obis_value.get(close_bracket_pos + 1..close_bracket_pos + 2) {
            return Err(anyhow!(
                "missing open bracket after closing bracket: {obis_value}",
            ));
        }
        let meas_val = if let Some(meas_value_str) =
            obis_value.get(close_bracket_pos + 2..obis_value.len() - m3_suffix.len())
        {
            match MeasuredValue::try_from(meas_value_str) {
                Ok(mv) => mv,
                Err(e) => {
                    return Err(anyhow!("{e:?} for {meas_value_str} in {obis_value}"));
                }
            }
        } else {
            return Err(anyhow!(
                "missing open bracket after closing bracket: {obis_value}",
            ));
        };
        self.msrmnts.push(P1Measurement {
            meter_nr: last_serial_number.clone(),
            dt_utc: utc_time_stamp,
            p1_msrmnt_type: unit,
            value: meas_val,
        });
        Ok(())
    }

    fn add_empty_key(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        let value = match MeasuredValue::try_from(obis_value) {
            Err(e) => {
                return Err(anyhow!("{e:?} for {obis_value} obis_key {obis_key}"));
            }
            Ok(v) => v,
        };
        let Some(ref last_serial_number) = self.last_serial_number_opt else {
            return Err(anyhow!(
                "missing earlier equipment id for value {obis_value}",
            ));
        };
        let Some(utc_time_stamp) = self.last_time_stamp_opt else {
            return Err(anyhow!("missing earlier time stamp for value {obis_value}",));
        };
        let Some(unit) = self.last_msrmnt_type_opt else {
            return Err(anyhow!("missing measurement type for value {obis_value}",));
        };
        self.msrmnts.push(P1Measurement {
            meter_nr: last_serial_number.clone(),
            dt_utc: utc_time_stamp,
            p1_msrmnt_type: unit,
            value,
        });
        Ok(())
    }

    fn log_kw_msrmnt_line(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_min_max_value("power l1/l2/l3", obis_key, obis_value)
    }

    fn log_el_current_line(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_min_max_value("current l1/l2/l3", obis_key, obis_value)
    }

    fn log_el_voltage_line(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_min_max_value("voltage l1/l2/l3", obis_key, obis_value)
    }

    fn log_version(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("version", obis_key, obis_value)
    }

    fn log_tariff(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("tariff", obis_key, obis_value)
    }

    fn log_text_message(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("text message", obis_key, obis_value)
    }

    fn log_power_failures(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("(long) power failures", obis_key, obis_value)
    }

    fn log_power_failure_event_log(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("power failure event log", obis_key, obis_value)
    }

    fn log_voltage_sags_swells_line(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("voltage sags/swells l1/l2/l3", obis_key, obis_value)
    }

    fn get_last_logged_keys_values(&self) -> HashMap<String, String> {
        // A reference in P1TelegramBuilder would avoid the need for this method.
        // But a reference needs a lifetime, and that makes the cast "as Akv" above
        // non primitive, not allowed.
        self.last_logged_keys_values.clone()
    }

    fn get_logged_keys_min_max_values(&self) -> HashMap<String, LoggedMinMaxValue> {
        // A reference in P1TelegramBuilder would avoid the need for this method.
        // But a reference needs a lifetime, and that makes the cast "as Akv" above
        // non primitive, not allowed.
        self.logged_keys_min_max_values.clone()
    }
}

enum TerminalError {
    TimeOut,            // no input at all
    Utf8Error,          // possible parity problem.
    Err(anyhow::Error), // other error
}

fn change_parity(serial_port: &mut SerialPort, even_parity: bool) -> Result<()> {
    let mut port_config = serial_port.get_configuration()?;
    port_config.set_parity(if even_parity {
        Parity::Even
    } else {
        Parity::Odd
    });
    serial_port.set_configuration(&port_config)?;
    info!("changed parity to {:?}", port_config.get_parity());
    Ok(())
}

fn change_baudrate(serial_port: &mut SerialPort, baudrates: &[u64]) -> Result<()> {
    // Check the baudrate, in case it is in the given baudrates before the last one,
    // change the baudrate to the next in baudrate,
    // otherwise change the baudrate to the first given one.
    if baudrates.len() < 2 {
        return Err(anyhow!("need at least 2 baudrates: {baudrates:?}"));
    }
    let mut port_config = serial_port.get_configuration()?;
    let current_baud_rate = port_config.get_baud_rate()?;

    let next_baud_rate = if let Some((pos, _)) = baudrates
        .iter()
        .enumerate()
        .find(|&(_, &br)| Some(br) == current_baud_rate.to_u64())
    {
        if let Some(nbr) = baudrates.get(pos + 1) {
            *nbr
        } else {
            baudrates[0]
        }
    } else {
        baudrates[0]
    };
    port_config.set_baud_rate(
        next_baud_rate
            .to_u32()
            .ok_or_else(|| anyhow!("next_baud_rate {next_baud_rate}"))?,
    )?;
    serial_port.set_configuration(&port_config)?;
    info!("changed baudrate from {current_baud_rate} to {next_baud_rate}");
    Ok(())
}

fn read_available_input(serial_port: &mut SerialPort) {
    let mut buf = [0u8; 1];
    let mut bytes_read = 0;
    if serial_port
        .set_read_timeout(Duration::from_secs(0))
        .is_err()
    {
        return;
    }
    while let Ok(n) = serial_port.read(buf.as_mut_slice()) {
        bytes_read += n;
    }
    if bytes_read > 0 {
        trace!("ignored {bytes_read} input bytes");
    }
}

fn read_p1_telegrams(
    serial_port: &mut SerialPort,
    config: &P1EncryptConfig,
    p1_sender: &tokio_mpsc::Sender<P1Telegram>,
    repeat_check_clock_sync: &RepeatCheckClockSync,
) -> TerminalError {
    let max_wait_for_byte_secs = 60;
    info!("max_wait_for_byte_secs: {max_wait_for_byte_secs}");
    if let Err(e) = serial_port.set_read_timeout(Duration::from_secs(max_wait_for_byte_secs)) {
        return TerminalError::Err(e.into());
    }
    let max_unicode_errors = 300;
    let mut unicode_errors = 0;

    let mut buf = [0u8; 1];

    let mut p1_telegram_builder_opt: Option<P1TelegramBuilder> = None;
    let mut last_line = String::new();

    let mut last_used_p1_dt_opt: Option<DateTime<Utc>> = None;

    let mut final_telegram_line = false; // allow for final line with crc: !6DF1

    let mut last_logged_keys_values = HashMap::<String, String>::new();
    let mut logged_keys_min_max_values = HashMap::<String, LoggedMinMaxValue>::new();
    let mut last_clear_logged_key_values = time_stamp_milli_secs();

    loop {
        let input_char = match serial_port.read(buf.as_mut_slice()) {
            Err(e) => {
                trace!("{e:?}");
                return match e.kind() {
                    ErrorKind::TimedOut => TerminalError::TimeOut,
                    _ => TerminalError::Err(Error::new(e).context("read")),
                };
            }
            Ok(num_bytes) => {
                if num_bytes != 1 {
                    return TerminalError::Err(anyhow!(
                        "unexpected number of read bytes {num_bytes}"
                    ));
                }
                match std::str::from_utf8(&buf) {
                    Err(_utf8_error) => {
                        unicode_errors += 1;
                        if let Some(ref mut p1_telegram_builder) = p1_telegram_builder_opt {
                            p1_telegram_builder.input_errors += 1;
                        }
                        if unicode_errors >= max_unicode_errors {
                            return TerminalError::Utf8Error;
                        }
                        // poll next byte
                        continue;
                    }
                    Ok(s) => {
                        if let Some(ch) = s.chars().next() {
                            ch
                        } else {
                            error!("buffer {buf:?} valid single byte string without char");
                            continue;
                        }
                    }
                }
            }
        };

        match input_char {
            '/' => {
                let mut p1_telegram_builder = P1TelegramBuilder::new(
                    last_logged_keys_values.clone(),
                    logged_keys_min_max_values.clone(),
                );
                p1_telegram_builder.add_crc(input_char as u8);
                final_telegram_line = false; // start a new p1 telegram
                trace!(
                    "starting p1 telegram, utc_time_stamp: {:?}",
                    p1_telegram_builder.creation_dt_utc
                );
                p1_telegram_builder_opt = Some(p1_telegram_builder);
                last_line.clear();
            }
            '!' => {
                if let Some(ref mut p1_telegram_builder) = p1_telegram_builder_opt {
                    p1_telegram_builder.add_crc(input_char as u8);
                }
                final_telegram_line = true;
            }
            '\n' | '\r' => {
                if final_telegram_line {
                    let telegram_ended_dt = time_stamp_milli_secs();
                    match p1_telegram_builder_opt.take() {
                        Some(ref mut p1_telegram_builder) => {
                            if !last_line.is_empty() {
                                trace!(
                                    "ignoring crc on last line {last_line}, expected {:04X}",
                                    p1_telegram_builder.crc16
                                );
                            }
                            let mut telegram_ok_late_enough = if p1_telegram_builder.input_errors
                                == 0
                            {
                                if let Some(last_used_p1_dt) = last_used_p1_dt_opt {
                                    if last_used_p1_dt + config.min_p1_interval.time_delta()
                                        < telegram_ended_dt
                                    {
                                        true
                                    } else {
                                        trace!(
                                            "ignoring too early p1 telegram, min_p1_interval {}",
                                            config.min_p1_interval
                                        );
                                        false
                                    }
                                } else {
                                    true
                                }
                            } else {
                                warn!(
                                    "ignoring p1 telegram with {} input error(s)",
                                    p1_telegram_builder.input_errors
                                );
                                false
                            };
                            if telegram_ok_late_enough {
                                // only use p1 telegram when system clock still in sync.
                                match repeat_check_clock_sync.recheck_wait() {
                                    Err(e) => {
                                        // error during sync, drop p1 telegram
                                        warn!("ignoring p1 telegram, error during sync {e:?}");
                                        telegram_ok_late_enough = false;
                                    }
                                    Ok(true) => {
                                        warn!("ignoring p1 telegram, clock was out of sync");
                                        read_available_input(serial_port);
                                        telegram_ok_late_enough = false;
                                    }
                                    Ok(false) => {} // not waited, clock still in sync
                                }
                            }
                            if telegram_ok_late_enough {
                                last_used_p1_dt_opt = Some(telegram_ended_dt);
                                // end a started P1Telegram.
                                match p1_telegram_builder.build(
                                    &config.window_msrmnt_time_start.time_delta(),
                                    &config.window_msrmnt_time_end.time_delta(),
                                ) {
                                    Ok(p1_telegram) => {
                                        p1_telegram.log_msrmnts();
                                        if let Err(queue_send_err) = p1_sender.try_send(p1_telegram)
                                        {
                                            match queue_send_err {
                                                tokio_mpsc::error::TrySendError::Full(
                                                    _full_error,
                                                ) => {
                                                    warn!("queue full_error, ignored p1_telegram");
                                                }
                                                tokio_mpsc::error::TrySendError::Closed(
                                                    _option_mkvs,
                                                ) => {
                                                    return TerminalError::Err(anyhow!(
                                                        "queue closed"
                                                    ));
                                                }
                                            }
                                        }
                                    }
                                    Err(e) => {
                                        error!("{e:#} at p1 telegram build");
                                    }
                                }
                                if last_clear_logged_key_values
                                    + config.log_ignored_values_interval.time_delta()
                                    > telegram_ended_dt
                                {
                                    // For update from next telegram:
                                    last_logged_keys_values =
                                        p1_telegram_builder.get_last_logged_keys_values();
                                    logged_keys_min_max_values =
                                        p1_telegram_builder.get_logged_keys_min_max_values()
                                } else {
                                    // Log interval begin/end and last/min/max values before clearing.
                                    info!(
                                        "Interval summary {last_clear_logged_key_values} - {telegram_ended_dt}"
                                    );
                                    last_clear_logged_key_values = telegram_ended_dt;
                                    for (key, val) in &p1_telegram_builder.last_logged_keys_values {
                                        info!("obis key {key}: last value {val}");
                                    }
                                    last_logged_keys_values.clear();
                                    let log_decimal_digits = 3;
                                    for (key, logged_min_max_value) in
                                        &p1_telegram_builder.logged_keys_min_max_values
                                    {
                                        info!(
                                            "obis key {key}: min {}, max {}, unit {}",
                                            logged_min_max_value
                                                .min_value
                                                .to_string(log_decimal_digits),
                                            logged_min_max_value
                                                .max_value
                                                .to_string(log_decimal_digits),
                                            logged_min_max_value.physical_unit,
                                        );
                                    }
                                    logged_keys_min_max_values.clear();
                                }
                            }
                        }
                        _ => {
                            // trace!("ending unstarted p1 telegram: {input_char}"); // ignore end char, no telegram started.
                        }
                    }
                    last_line.clear();
                }
                if !last_line.is_empty() {
                    match p1_telegram_builder_opt {
                        Some(ref mut p1_telegram_builder) => {
                            p1_telegram_builder.add_line(&last_line);
                        }
                        _ => {
                            info!("unstarted p1 telegram line: {last_line}");
                        }
                    }
                    last_line.clear();
                }
            }
            _ => {
                if let Some(ref mut p1_telegram_builder) = p1_telegram_builder_opt {
                    p1_telegram_builder.add_crc(input_char as u8);
                }
                last_line.push(input_char);
            }
        }
    }
}

pub(crate) fn read_p1_telegrams_changing_parity(
    config: &P1EncryptConfig,
    p1_sender: &tokio_mpsc::Sender<P1Telegram>,
    repeat_check_clock_sync: RepeatCheckClockSync,
) -> Result<()> {
    let excl_lock_addr_port = config.p1_excl_local_ip.addr_port_str()?;
    // Keep _udp_sock open to prevent other programs from opening it
    // before using the p1_terminal.
    let _udp_sock = udp_bind(&excl_lock_addr_port)
        .map_err(|e| e.context(format!("udp_bind, refuse to read {}", &config.p1_terminal)))?;
    loop {
        let mut serial_port = match SerialPort::open(&config.p1_terminal, KeepSettings) {
            Ok(sp) => sp,
            Err(e) => {
                error!("{e:?} at open() p1_terminal {}", &config.p1_terminal);
                error!("waiting 60 secs, is the P1 port connected?");
                thread::sleep(Duration::from_secs(60));
                continue;
            }
        };

        read_available_input(&mut serial_port);
        let mut parity_changes: u64 = 0;
        loop {
            match read_p1_telegrams(
                &mut serial_port,
                config,
                p1_sender,
                &repeat_check_clock_sync,
            ) {
                TerminalError::Err(e) => return Err(e.context("read_p1_telegrams")),
                TerminalError::TimeOut => {
                    warn!("timeout on P1 input {}, connection ok?", config.p1_terminal);
                    // switch terminal baudrate between 9600 and 115200:
                    if let Err(e) = change_baudrate(&mut serial_port, &config.baudrates) {
                        error!("{e:#} at change_baudrate");
                        error!("reopening p1_terminal {}", &config.p1_terminal);
                        break;
                    }
                }
                TerminalError::Utf8Error => {
                    if parity_changes > config.max_parity_changes {
                        error!("reached max_parity_changes {}", config.max_parity_changes);
                        error!("reopening p1_terminal {}", &config.p1_terminal);
                        break;
                    }
                    match change_parity(&mut serial_port, parity_changes % 2 == 1) {
                        Err(e) => {
                            error!("{e:#} at change_parity");
                            error!("reopening p1_terminal {}", &config.p1_terminal);
                            break;
                        }
                        _ => {
                            parity_changes += 1;
                        }
                    }
                }
            };
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use P1MeasurementType::*;
    use core::str;
    use enmprv::util::dt_utc_from_str;

    use std::{collections::BTreeMap, path::Path, sync::Once};
    static INIT: Once = Once::new();
    pub fn init_log4rs() {
        // init the log so it shows on stdout
        INIT.call_once(|| {
            // yaml file on local machine
            let log_config_file_path_buf = Path::new(file!())
                .parent()
                .unwrap()
                .join("p1etestlog4rs.yaml");
            let log_config_file_path = log_config_file_path_buf.as_path();
            // panic will poison INIT:
            log4rs::init_file(
                log_config_file_path,
                log4rs::config::Deserializers::default(),
            )
            .unwrap();
        });
    }

    fn test_parse_p1_text(
        p1_text: &str,
        exp_msrmnts: &[P1Measurement],
        test_now: DateTime<Utc>,
        exp_last_logged_keys_values: &[(&str, &str)],
        exp_last_logged_keys_min_max_values: &[(&str, &str)], // for testing: only 1 value needed for min and max.
    ) {
        init_log4rs();
        let mut p1b = P1TelegramBuilder::new(HashMap::new(), HashMap::new());
        {
            let mut started = false;
            for b in p1_text.bytes() {
                if b == b'/' {
                    started = true;
                }
                if started {
                    p1b.add_crc(b);
                }
                if b == b'!' {
                    break;
                }
            }
        }
        p1b.creation_dt_utc = test_now;
        for line in p1_text.lines() {
            p1b.add_line(line);
            assert_eq!(p1b.input_errors, 0);
        }
        for (i, (exp_msrmnt, result_msrmnt)) in exp_msrmnts.iter().zip(&p1b.msrmnts).enumerate() {
            let mes = format!("msrmnt {i}");
            assert_eq!(result_msrmnt.meter_nr, exp_msrmnt.meter_nr, "ref_id {mes}");
            assert_eq!(
                result_msrmnt.dt_utc, exp_msrmnt.dt_utc,
                "utc_time_stamp {mes}"
            );
            assert_eq!(
                result_msrmnt.p1_msrmnt_type, exp_msrmnt.p1_msrmnt_type,
                "unit {mes}"
            );
            assert_eq!(result_msrmnt.value, exp_msrmnt.value, "value {mes}");
        }
        assert_eq!(p1b.msrmnts.len(), exp_msrmnts.len());

        let mut exp_last_logged_keys_strings_map = BTreeMap::new();
        for (exp_key, exp_val) in exp_last_logged_keys_values {
            if let Some(prev_val) = exp_last_logged_keys_strings_map.insert(*exp_key, *exp_val) {
                panic!("last logged keys duplicate key {exp_key}, strings {prev_val} and {exp_val}")
            }
        }

        let mut exp_last_logged_keys_min_max_values_map = BTreeMap::new();
        for (exp_key, exp_val) in exp_last_logged_keys_min_max_values {
            if let Some(prev_val) =
                exp_last_logged_keys_min_max_values_map.insert(*exp_key, *exp_val)
            {
                panic!("last log min max duplicate key {exp_key}, strings {prev_val} and {exp_val}")
            }
        }

        let last_logged_keys_values = p1b.get_last_logged_keys_values();
        for (act_key, act_val) in &last_logged_keys_values {
            assert_eq!(
                Some(&act_val.as_str()),
                exp_last_logged_keys_strings_map.get(act_key.as_str()),
                "actual key for last logged: {act_key}",
            );
        }
        for (exp_key, exp_string) in &exp_last_logged_keys_strings_map {
            assert_eq!(
                last_logged_keys_values.get(*exp_key),
                Some(&exp_string.to_string()),
                "expected key last logged: {exp_key}",
            );
        }
        assert_eq!(
            exp_last_logged_keys_strings_map.len(),
            last_logged_keys_values.len(),
        );

        let logged_keys_min_max_values = p1b.get_logged_keys_min_max_values();
        for (act_key, act_min_max_val) in &logged_keys_min_max_values {
            let Some(exp_obis_value) =
                exp_last_logged_keys_min_max_values_map.get::<str>(act_key.as_ref())
            else {
                panic!("missing expected value for {act_key}, {act_min_max_val:?}");
            };
            let Some(star_pos) = exp_obis_value.find('*') else {
                panic!("missing star * in exp_obis_value {exp_obis_value}");
            };
            let Ok(exp_value) = MeasuredValue::try_from(&exp_obis_value[..star_pos]) else {
                panic!("no value available in {exp_obis_value}");
            };
            let physical_unit = &exp_obis_value[star_pos + 1..];

            assert_eq!(
                act_min_max_val.physical_unit, physical_unit,
                "actual key: {act_key}",
            );
            assert_eq!(
                act_min_max_val.min_value, exp_value,
                "actual key: {act_key} min value",
            );
            assert_eq!(
                act_min_max_val.max_value, exp_value,
                "actual key: {act_key} min value",
            );
        }
        assert_eq!(
            exp_last_logged_keys_min_max_values_map.len(),
            logged_keys_min_max_values.len(),
        );
    }

    fn msrmnt(
        meter_nr: &str,
        utc_stamp: &str,
        unit: P1MeasurementType,
        value: &str,
    ) -> P1Measurement {
        P1Measurement {
            meter_nr: meter_nr.to_string(),
            dt_utc: dt_utc_from_str(utc_stamp).unwrap(),
            p1_msrmnt_type: unit,
            value: MeasuredValue::try_from(value).unwrap(),
        }
    }

    #[test]
    fn test_p1_parse_a() {
        let p1_text = "/XMX5LGBBFFB231204853

1-3:0.2.8(42)
0-0:1.0.0(230826001741S)
0-0:96.1.1(4530303434313233343536373839303131)
1-0:1.8.1(012057.792*kWh)
1-0:2.8.1(000000.000*kWh)
1-0:1.8.2(014488.290*kWh)
1-0:2.8.2(000000.004*kWh)
0-0:96.14.0(0001)
1-0:1.7.0(00.165*kW)
1-0:2.7.0(00.000*kW)
0-0:96.7.21(00000)
0-0:96.7.9(00003)
1-0:99.97.0(3)(0-0:96.7.19)(170720162945S)(0000001598*s)(170326055407S)(0029785553*s)(160415120840S)(0037051525*s)
1-0:32.32.0(00000)
1-0:32.36.0(00000)
0-0:96.13.1()
0-0:96.13.0()
1-0:31.7.0(001*A)
1-0:21.7.0(00.165*kW)
1-0:22.7.0(00.000*kW)
0-1:24.1.0(003)
0-1:96.1.0(4730303434313233343536373839303132)
0-1:24.2.1(230826000000S)(09100.175*m3)
!6DF1
";
        let el_ref_id = "E0044123456789011";
        let el_stamp = "2023-08-25T22:17:41Z";
        let exp_msrmnts = vec![
            msrmnt(el_ref_id, el_stamp, El_kWh_to_tariff_1, "012057.792"),
            msrmnt(el_ref_id, el_stamp, El_kWh_by_tariff_1, "0.000"),
            msrmnt(el_ref_id, el_stamp, El_kWh_to_tariff_2, "014488.290"),
            msrmnt(el_ref_id, el_stamp, El_kWh_by_tariff_2, "0.004"),
            msrmnt(el_ref_id, el_stamp, El_kW_to, "0.165"),
            msrmnt(el_ref_id, el_stamp, El_kW_by, "0"),
            msrmnt(
                "G0044123456789012",
                "2023-08-25T22:00:00Z",
                NgNl_m3,
                "9100.175",
            ),
        ];
        let exp_last_logged_keys_values = &[
            ("0-0:96.7.9", "00003"),
            ("0-0:96.7.21", "00000"),
            ("0-0:96.13.0", ""),
            ("0-0:96.13.1", ""),
            ("0-0:96.14.0", "0001"),
            ("1-0:32.32.0", "00000"),
            ("1-0:32.36.0", "00000"),
            (
                "1-0:99.97.0",
                "3)(0-0:96.7.19)(170720162945S)(0000001598*s)(170326055407S)(0029785553*s)(160415120840S)(0037051525*s",
            ),
            ("1-3:0.2.8", "42"),
        ];
        let exp_last_logged_keys_min_max_values = &[
            ("1-0:21.7.0", "00.165*kW"),
            ("1-0:22.7.0", "00.000*kW"),
            ("1-0:31.7.0", "001*A"),
        ];
        test_parse_p1_text(
            p1_text,
            &exp_msrmnts,
            time_stamp_milli_secs(),
            exp_last_logged_keys_values,
            exp_last_logged_keys_min_max_values,
        );
    }

    #[test]
    fn test_p1_parse_b() {
        let p1_text = "/XMX5XMXABCE000062328

0-0:96.1.1(4530303434313233343536373839303135)
1-0:1.8.1(28194.060*kWh)
1-0:1.8.2(37773.828*kWh)
1-0:2.8.1(03016.402*kWh)
1-0:2.8.2(06355.026*kWh)
0-0:96.14.0(0001)
1-0:1.7.0(0003.12*kW)
1-0:2.7.0(0000.00*kW)
0-0:96.13.1()
0-0:96.13.0()
0-1:96.1.0(4730303434313233343536373839303133)
0-1:24.1.0(03)
0-1:24.3.0(230827170000)(08)(60)(1)(0-1:24.2.0)(m3)
(20204.264)
!
";
        let el_ref_id = "E0044123456789015";
        let el_stamp = "2023-08-27T20:31:45.531Z";
        let test_now = dt_utc_from_str(el_stamp).unwrap();
        let exp_msrmnts = vec![
            msrmnt(el_ref_id, el_stamp, El_kWh_to_tariff_1, "28194.060"),
            msrmnt(el_ref_id, el_stamp, El_kWh_to_tariff_2, "37773.828"),
            msrmnt(el_ref_id, el_stamp, El_kWh_by_tariff_1, "03016.402"),
            msrmnt(el_ref_id, el_stamp, El_kWh_by_tariff_2, "06355.026"),
            msrmnt(el_ref_id, el_stamp, El_kW_to, "3.12"),
            msrmnt(el_ref_id, el_stamp, El_kW_by, "0"),
            msrmnt(
                "G0044123456789013",
                "2023-08-27T15:00:00Z",
                NgNl_m3,
                "20204.264",
            ),
        ];
        let exp_last_logged_keys_values = &[
            ("0-0:96.13.0", ""),
            ("0-0:96.13.1", ""),
            ("0-0:96.14.0", "0001"),
        ];
        let exp_last_logged_keys_min_max_values = &[];
        test_parse_p1_text(
            p1_text,
            &exp_msrmnts,
            test_now,
            exp_last_logged_keys_values,
            exp_last_logged_keys_min_max_values,
        );
    }

    #[test]
    fn test_p1_parse_c() {
        let p1_text = "/XMX5XMXABCE000062328

0-0:96.1.1(4530303434313233343536373839303136)
1-0:1.8.1(28194.064*kWh)
1-0:1.8.2(37773.828*kWh)
1-0:2.8.1(03016.402*kWh)
1-0:2.8.2(06355.026*kWh)
0-0:96.14.0(0001)
1-0:1.7.0(0000.99*kW)
1-0:2.7.0(0000.00*kW)
0-0:96.13.1()
0-0:96.13.0()
0-1:96.1.0(4730303434313233343536373839303134)
0-1:24.1.0(03)
0-1:24.3.0(230827170000)(08)(60)(1)(0-1:24.2.0)(m3)
(20204.264)
!
";
        let el_ref_id = "E0044123456789016";
        let el_stamp = "2023-08-27T20:31:45.531Z";
        let test_now = dt_utc_from_str(el_stamp).unwrap();
        let exp_msrmnts = vec![
            msrmnt(el_ref_id, el_stamp, El_kWh_to_tariff_1, "28194.064"),
            msrmnt(el_ref_id, el_stamp, El_kWh_to_tariff_2, "37773.828"),
            msrmnt(el_ref_id, el_stamp, El_kWh_by_tariff_1, "03016.402"),
            msrmnt(el_ref_id, el_stamp, El_kWh_by_tariff_2, "06355.026"),
            msrmnt(el_ref_id, el_stamp, El_kW_to, "0.99"),
            msrmnt(el_ref_id, el_stamp, El_kW_by, "0"),
            msrmnt(
                "G0044123456789014",
                "2023-08-27T15:00:00Z",
                NgNl_m3,
                "20204.264",
            ),
        ];
        let exp_last_logged_keys_values = &[
            ("0-0:96.13.0", ""),
            ("0-0:96.13.1", ""),
            ("0-0:96.14.0", "0001"),
        ];
        let exp_last_logged_keys_min_max_values = &[];
        test_parse_p1_text(
            p1_text,
            &exp_msrmnts,
            test_now,
            exp_last_logged_keys_values,
            exp_last_logged_keys_min_max_values,
        );
    }

    #[test]
    fn test_p1_parse_dsmr5_a() {
        // added a backslash before \2 to have a literal backslash.
        let p1_text = "/ISK5\\2M550T-1014

1-3:0.2.8(50)
0-0:1.0.0(240611160648S)
0-0:96.1.1(4530303434313233343536373839303137)
1-0:1.8.1(000070.343*kWh)
1-0:1.8.2(000073.429*kWh)
1-0:2.8.1(000035.807*kWh)
1-0:2.8.2(000077.116*kWh)
0-0:96.14.0(0002)
1-0:1.7.0(00.000*kW)
1-0:2.7.0(00.821*kW)
0-0:96.7.21(00011)
0-0:96.7.9(00005)
1-0:99.97.0(3)(0-0:96.7.19)(230928115329S)(0000001333*s)(230928142346S)(0000009015*s)(230928143736S)(0000000211*s)
1-0:32.32.0(00000)
1-0:52.32.0(00000)
1-0:72.32.0(00005)
1-0:32.36.0(00003)
1-0:52.36.0(00003)
1-0:72.36.0(00003)
0-0:96.13.0()
1-0:32.7.0(238.5*V)
1-0:52.7.0(238.6*V)
1-0:72.7.0(239.0*V)
1-0:31.7.0(000*A)
1-0:51.7.0(001*A)
1-0:71.7.0(004*A)
1-0:21.7.0(00.094*kW)
1-0:41.7.0(00.249*kW)
1-0:61.7.0(00.000*kW)
1-0:22.7.0(00.000*kW)
1-0:42.7.0(00.000*kW)
1-0:62.7.0(01.166*kW)
0-1:24.1.0(003)
0-1:96.1.0(4730303434313233343536373839303135)
0-1:24.2.1(240611160458S)(00010.135*m3)
!33D3
";
        let el_ref_id = "E0044123456789017";
        let el_stamp = "2024-06-11T14:06:48.000Z"; // UTC, given as GMT+2.
        let test_now = dt_utc_from_str(el_stamp).unwrap();
        let exp_msrmnts = vec![
            msrmnt(el_ref_id, el_stamp, El_kWh_to_tariff_1, "70.343"),
            msrmnt(el_ref_id, el_stamp, El_kWh_to_tariff_2, "73.429"),
            msrmnt(el_ref_id, el_stamp, El_kWh_by_tariff_1, "35.807"),
            msrmnt(el_ref_id, el_stamp, El_kWh_by_tariff_2, "77.116"),
            msrmnt(el_ref_id, el_stamp, El_kW_to, "0.000"),
            msrmnt(el_ref_id, el_stamp, El_kW_by, "0.821"),
            msrmnt(
                "G0044123456789015",
                "2024-06-11T14:04:58Z", // utc, present as gmt+2
                NgNl_m3,
                "00010.135",
            ),
        ];
        let exp_last_logged_keys_values = &[
            ("0-0:96.7.9", "00005"),
            ("0-0:96.7.21", "00011"),
            ("0-0:96.13.0", ""),
            ("0-0:96.14.0", "0002"),
            ("1-0:32.32.0", "00000"),
            ("1-0:52.32.0", "00000"),
            ("1-0:72.32.0", "00005"),
            ("1-0:32.36.0", "00003"),
            ("1-0:52.36.0", "00003"),
            ("1-0:72.36.0", "00003"),
            (
                "1-0:99.97.0",
                "3)(0-0:96.7.19)(230928115329S)(0000001333*s)(230928142346S)(0000009015*s)(230928143736S)(0000000211*s",
            ),
            ("1-3:0.2.8", "50"),
        ];
        let exp_last_logged_keys_min_max_values = &[
            ("1-0:21.7.0", "00.094*kW"),
            ("1-0:22.7.0", "00.000*kW"),
            ("1-0:31.7.0", "000*A"),
            ("1-0:32.7.0", "238.5*V"),
            ("1-0:41.7.0", "00.249*kW"),
            ("1-0:42.7.0", "00.000*kW"),
            ("1-0:51.7.0", "001*A"),
            ("1-0:52.7.0", "238.6*V"),
            ("1-0:61.7.0", "00.000*kW"),
            ("1-0:62.7.0", "01.166*kW"),
            ("1-0:71.7.0", "004*A"),
            ("1-0:72.7.0", "239.0*V"),
        ];
        test_parse_p1_text(
            p1_text,
            &exp_msrmnts,
            test_now,
            exp_last_logged_keys_values,
            exp_last_logged_keys_min_max_values,
        );
    }

    #[test]
    fn test_decode_cosem_octets() {
        // Test case from example at https://github.com/energietransitie/dsmr-info, August 2024:
        let serial_nr_from_p1 = "4530303434313233343536373839303132";
        let equipment_id = "E0044123456789012";
        assert_eq!(
            decode_cosem_octets(serial_nr_from_p1).unwrap(),
            equipment_id
        );
    }
}
