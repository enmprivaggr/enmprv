use std::{
    collections::{BTreeMap, HashMap, hash_map},
    env,
    fs::File,
    io::Read,
    net::UdpSocket,
    process::exit,
    sync::{Arc, Mutex, mpsc},
    thread,
};

use anyhow::{Error, Result, anyhow};
use log::{error, info, trace};
use serde::Deserialize;
use tokio::{runtime::Builder as TokioRuntimeBuilder, sync::mpsc as tokio_mpsc};

use enmprv::{
    bincoded::ShareSeedRequest,
    checkclocksync::RepeatCheckClockSync,
    durationconfig::DurationConfig,
    measurement::P1Measurement,
    openssl::{OpenSslSigner, OpenSslSignerFiles, PubKeyHash},
    util::{IpAddressPortConfig, RunState},
};

use crate::{p1db::MeasurementIntervalKeepTimesConfig, p1ka::SharedSeedsConfig};

mod p1aggr;
mod p1db;
mod p1input;
mod p1ka;

#[cfg(feature = "simulate_meter_nrs")]
mod p1split;

#[cfg(feature = "simulate_meter_nrs")]
use crate::p1split::P1SplitConfig;

// P1 telegram as read from p1 terminal, and then queued for insertion into a db
#[derive(Debug, Clone)]
pub struct P1Telegram {
    msrmnts: Vec<P1Measurement>,
}

const CONFIG_FILE_NAME: &str = "p1encrypt.toml";
const LOG_CONFIG_FILE_NAME: &str = "p1elog4rs.yaml";

#[derive(Debug)]
pub enum P1TelegramOrSharedSeedRequest {
    P1Telegram(P1Telegram),
    ShareSeedRequest(ShareSeedRequest),
}

#[derive(Deserialize, Debug)]
pub struct P1EncryptConfig {
    wait_clock_sync: DurationConfig,
    repeat_wait_clock_sync: DurationConfig,
    p1_terminal: String,
    p1_excl_local_ip: IpAddressPortConfig,
    baudrates: Vec<u64>,
    max_parity_changes: u64,
    min_p1_interval: DurationConfig,
    window_msrmnt_time_start: DurationConfig,
    window_msrmnt_time_end: DurationConfig,
    log_ignored_values_interval: DurationConfig,
    priv_aggr_meters_local_ip: IpAddressPortConfig,
    priv_aggr_meters_remote_ip: IpAddressPortConfig,
    priv_aggr_meters_timeout: DurationConfig,
    key_adder_remote_ip: IpAddressPortConfig,
    key_adder_timeout: DurationConfig,
    send_non_encrypted: Option<bool>,
    max_p1_queue_size: u64,
    shared_seeds: SharedSeedsConfig,
    meter_sets_signers: BTreeMap<String, MeterSetSignerConfig>,
    temp_dir: String,
    key_adder_public_key: String,
    aggregator_public_key: String,
    database_url: String,
    measurement_interval_keep_times: MeasurementIntervalKeepTimesConfig,
    #[cfg(feature = "simulate_meter_nrs")]
    dev_el_meter_serial_nr_ag: Option<String>,
    #[cfg(feature = "simulate_meter_nrs")]
    dev_gas_meter_serial_nr_ag: Option<String>,
    #[cfg(feature = "simulate_meter_nrs")]
    p1_split: Option<P1SplitConfig>,
}

/// A set of meters measuring unique [`bincoded::P1MeasurementType`] values that are signed by a single key pair.
/// The P1 port of the electricity meter may also report gas measurements.
#[derive(Deserialize, Debug)]
struct MeterSetSignerConfig {
    // meter serial nrs for present meters
    electricity: String,
    gas: Option<String>,
    // signing key pair for the meters
    ssl_signer_files: OpenSslSignerFiles,
}

fn pkh_and_signer_by_meter_serial_nr_from_config(
    config: &P1EncryptConfig,
) -> Result<HashMap<String, (PubKeyHash, Arc<OpenSslSigner>)>> {
    use hash_map::Entry::*;
    let mut result_ok = HashMap::new();
    let mut config_name_by_pkh = HashMap::new();
    for (config_name, meter_set_signer) in &config.meter_sets_signers {
        let ssl_signer = OpenSslSigner::new(&meter_set_signer.ssl_signer_files, &config.temp_dir)?;
        let pkh = ssl_signer.pub_key_hash()?;
        match config_name_by_pkh.entry(pkh.clone()) {
            Vacant(c_ve) => {
                c_ve.insert(config_name);
            }
            Occupied(c_oe) => {
                return Err(anyhow!(
                    "non unique public key hash for {} and {config_name}",
                    c_oe.get()
                ));
            }
        }
        let ssl_signer_rc = Arc::new(ssl_signer);
        let mut meter_serial_nrs = vec![&meter_set_signer.electricity];
        meter_serial_nrs.extend(&meter_set_signer.gas);
        trace!(
            "config {config_name} {pkh} of {} for {}",
            meter_set_signer.ssl_signer_files.public_key_file_name,
            meter_serial_nrs
                .iter()
                .map(|&s| s.to_owned())
                .collect::<Vec<_>>()
                .join(" ")
        );
        for meter_serial_nr in meter_serial_nrs {
            match result_ok.entry(meter_serial_nr.to_owned()) {
                Vacant(r_ve) => {
                    r_ve.insert((pkh.to_owned(), ssl_signer_rc.clone()));
                }
                Occupied(_) => {
                    return Err(anyhow!(
                        "non unique meter serial nr {} at {}",
                        meter_serial_nr,
                        config_name,
                    ));
                }
            }
        }
    }
    Ok(result_ok)
}

fn usage_exit(exit_code: i32) -> ! {
    eprintln!("argument: -v");
    eprintln!("  show invocation and version number");
    eprintln!("arguments: [config_file [log_config_file]]");
    eprintln!("  config_file:     default {CONFIG_FILE_NAME}");
    eprintln!("  log_config_file: default {LOG_CONFIG_FILE_NAME}");
    exit(exit_code);
}

fn error_exit(e: Error) -> ! {
    error!("{e:#}, exiting");
    exit(1);
}

fn main() {
    let mut args = env::args();
    let binary_name = args.next().unwrap_or("p1 encryptor".to_owned());
    let config_file_name = args.next().unwrap_or_else(|| CONFIG_FILE_NAME.to_string());
    let version = env!("CARGO_PKG_VERSION");
    if config_file_name == "-v" {
        println!("{binary_name} version {version}");
        exit(0);
    }
    if config_file_name == "-h" {
        usage_exit(0);
    }
    let log_config_file_name = args
        .next()
        .unwrap_or_else(|| LOG_CONFIG_FILE_NAME.to_string());
    if args.next().is_some() {
        usage_exit(1);
    }
    log4rs::init_file(
        &log_config_file_name,
        log4rs::config::Deserializers::default(),
    )
    .unwrap_or_else(|e| {
        eprintln!("{log_config_file_name}: {e:?} at log4rs::init_file, exiting",);
        exit(1);
    });
    info!(
        "starting p1 encryptor version {version} as {binary_name} user {} pid {}",
        std::env::var("USER").unwrap_or("".to_owned()),
        std::process::id()
    );

    let run_state = match RunState::new() {
        Err(e) => error_exit(e.context("Runstate::new()")),
        Ok(r) => r,
    };
    run_state.start_exit_thread();

    let mut toml_buf = Vec::<u8>::new();
    {
        let mut config_file = File::open(&config_file_name).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(config_file_name.to_owned()));
        });
        let _ = config_file.read_to_end(&mut toml_buf).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(config_file_name.to_owned()));
        });
    }
    let toml_str = std::str::from_utf8(&toml_buf).unwrap_or_else(|e| {
        error_exit(Error::new(e).context(config_file_name.to_owned()));
    });
    let cfn = config_file_name.clone();
    let config = Arc::new(
        toml::from_str::<P1EncryptConfig>(toml_str).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(cfn));
        }),
    );

    if config
        .shared_seeds
        .validity_period
        .time_delta()
        .num_minutes()
        <= 0
    {
        error_exit(anyhow!(
            "{config_file_name} shared seed validity period less than 1 minute"
        ));
    }

    let shared_udp_socket: Arc<Mutex<Option<UdpSocket>>> = Arc::new(Mutex::new(None));

    let remote_aggr_meters_addr_port = match config.priv_aggr_meters_remote_ip.socket_address() {
        Ok(sa) => sa,
        Err(e) => error_exit(anyhow!("{e:?} at socket_address")),
    };

    let key_adder_socket_addr = match config.key_adder_remote_ip.socket_address() {
        Err(e) => {
            error_exit(anyhow!(
                "{e:?} key_adder_remote_ip {:?}",
                &config.key_adder_remote_ip
            ));
        }
        Ok(sa) => sa,
    };

    let pkh_and_signer_by_meter_serial_nr =
        match pkh_and_signer_by_meter_serial_nr_from_config(&config) {
            Ok(r) => r,
            Err(e) => error_exit(e.context("pkh_and_signer_by_meter_serial_nr_from_config")),
        };

    let repeat_check_clock_sync = match RepeatCheckClockSync::new(
        config.wait_clock_sync.std_duration(),
        config.repeat_wait_clock_sync.std_duration(),
    ) {
        Err(e) => {
            error_exit(e.context("RepeatCheckClockSync::new"));
        }
        Ok(rccs) => rccs,
    };

    // Start threads, and connect channels, in reverse order of data flow:

    let replies_shared_socket = Arc::clone(&shared_udp_socket);
    let config_clone = Arc::clone(&config);

    let _show_udp_replies_thread = thread::spawn(move || {
        p1aggr::show_udp_replies(
            &replies_shared_socket,
            remote_aggr_meters_addr_port,
            &config_clone,
        );
    });

    let max_p1_queue_size = match usize::try_from(config.max_p1_queue_size) {
        Ok(m) => m,
        Err(e) => {
            error_exit(Error::new(e).context(format!(
                "usize::try_from max_p1_queue_size {}",
                config.max_p1_queue_size
            )));
        }
    };

    // channel to encryption thread
    let (to_encrypt_chan, from_encrypt_chan) =
        mpsc::sync_channel::<P1TelegramOrSharedSeedRequest>(max_p1_queue_size);

    let encrypt_config = Arc::clone(&config);
    let encrypt_shared_udp_socket = Arc::clone(&shared_udp_socket);

    let share_seeds_pkh_and_signer_by_meter_serial_nr = Arc::new(pkh_and_signer_by_meter_serial_nr);
    let encrypt_pkh_and_signer_by_meter_serial_nr =
        Arc::clone(&share_seeds_pkh_and_signer_by_meter_serial_nr);

    let _encryptor_thread = thread::spawn(move || {
        if let Err(e) = p1aggr::encrypt_p1_telegrams_to_udp(
            &from_encrypt_chan,
            &encrypt_shared_udp_socket,
            remote_aggr_meters_addr_port,
            &encrypt_config,
            &encrypt_pkh_and_signer_by_meter_serial_nr,
        ) {
            error_exit(e.context("encrypt_p1_telegrams_to_udp"));
        }
    });

    let (to_share_seeds_chan, mut from_share_seeds_chan) =
        tokio_mpsc::channel::<P1Telegram>(max_p1_queue_size);

    let config_clone = Arc::clone(&config);

    let _share_seeds_thread = thread::spawn(move || {
        if let Err(e) = p1ka::share_seeds(
            &config_clone,
            &share_seeds_pkh_and_signer_by_meter_serial_nr,
            &mut from_share_seeds_chan,
            &key_adder_socket_addr,
            &to_encrypt_chan,
        ) {
            error_exit(e.context("share_seeds"));
        }
    });

    #[cfg(not(feature = "simulate_meter_nrs"))]
    let to_share_seeds_or_split_chan: tokio_mpsc::Sender<_> = to_share_seeds_chan;

    #[cfg(feature = "simulate_meter_nrs")]
    let to_share_seeds_or_split_chan: tokio_mpsc::Sender<_> = {
        let config_clone = Arc::clone(&config);
        if let Some(p1split_config) = &config_clone.p1_split {
            if p1split_config.extra_p1_copies > 0 {
                // channel to p1 splitting thread
                let (to_split_chan, mut from_split_chan) =
                    tokio_mpsc::channel::<P1Telegram>(max_p1_queue_size);
                let _p1_split_thread = thread::spawn(move || {
                    if let Err(e) = p1split::split_p1_telegrams(
                        &config_clone,
                        &mut from_split_chan,
                        &to_share_seeds_chan,
                    ) {
                        error_exit(e.context("split_p1_telegrams"));
                    }
                });
                to_split_chan
            } else {
                to_share_seeds_chan
            }
        } else {
            to_share_seeds_chan
        }
    };

    // channel to insert p1 into db thread: ports
    let config_clone = Arc::clone(&config);
    let (to_insert_db_chan, mut from_insert_db_chan) =
        tokio_mpsc::channel::<P1Telegram>(max_p1_queue_size);
    // See https://tokio.rs/tokio/topics/bridging
    let _p1_insert_db_thread = thread::spawn(move || {
        // Use only the current thread to implement async/await
        let runtime = TokioRuntimeBuilder::new_current_thread()
            .enable_all()
            .build()
            .unwrap_or_else(|e| {
                error_exit(Error::new(e).context("tokio failed to build runtime"));
            });
        runtime.block_on(async {
            if let Err(e) = p1db::p1_telegrams_pass_on_and_into_db(
                &config_clone,
                &mut from_insert_db_chan,
                &to_share_seeds_or_split_chan,
            )
            .await
            {
                error_exit(e.context("insert_p1_telegrams_into_db"));
            }
        });
    });

    let config_clone = Arc::clone(&config);
    if let Err(e) = p1input::read_p1_telegrams_changing_parity(
        &config_clone,
        &to_insert_db_chan,
        repeat_check_clock_sync,
    ) {
        error_exit(e.context("read_p1_telegrams_changing_parity"));
    }
    info!("normal exit");
    exit(0);
}
