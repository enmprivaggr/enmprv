use std::{
    collections::HashMap,
    net::{SocketAddr, UdpSocket},
    sync::{Arc, Mutex, mpsc},
    thread,
    time::{Duration, Instant},
};

use anyhow::{Context, Result, anyhow};
use bincode::serde::{decode_from_slice, encode_to_vec};
use log::{error, info, trace, warn};
use num::{BigInt, pow};

use enmprv::{
    bincoded::{AggrReplyToTelegram, MeasurementInfo, get_bincode_config},
    encryptionseed::{ShareSeedAddResult, SharedSeeds, encrypting_value},
    openssl::{OpenSslSigVerifier, OpenSslSigner, PubKeyHash},
    util::{dt_utc_to_string, ip_addr_in_wg_local_intf, time_stamp_milli_secs, udp_bind},
};

use crate::{P1EncryptConfig, P1Telegram, P1TelegramOrSharedSeedRequest};

impl P1Telegram {
    pub(crate) fn measurement_infos_for_aggr(
        &self,
        send_non_encrypted: bool,
        meter_pkh_and_shared_seeds_opt: Option<(&PubKeyHash, &SharedSeeds)>,
    ) -> Result<Vec<MeasurementInfo>> {
        let mut last_meter_nr_opt: Option<String> = None;
        let mut last_timestamp_opt: Option<String> = None;
        let mut msrmnt_infos: Vec<MeasurementInfo> = Vec::new();
        let mut some_value = false;
        use MeasurementInfo::*;
        for msrmnt in &self.msrmnts {
            let put_id = if let Some(ref last_meter_nr) = last_meter_nr_opt {
                if *last_meter_nr == msrmnt.meter_nr {
                    false
                } else {
                    last_meter_nr_opt = Some(msrmnt.meter_nr.clone());
                    true
                }
            } else {
                last_meter_nr_opt = Some(msrmnt.meter_nr.clone());
                true
            };
            let timestamp = dt_utc_to_string(&msrmnt.dt_utc);
            let put_timestamp = if let Some(ref last_timestamp) = last_timestamp_opt {
                if *last_timestamp == timestamp {
                    false
                } else {
                    last_timestamp_opt = Some(timestamp.clone());
                    true
                }
            } else {
                last_timestamp_opt = Some(timestamp.clone());
                true
            };
            if put_id {
                msrmnt_infos.push(MeterSerialNr(msrmnt.meter_nr.to_owned()));
            }
            if put_timestamp {
                msrmnt_infos.push(Mutc(timestamp.clone()));
            }
            if send_non_encrypted {
                msrmnt_infos.push(DirectValue(msrmnt.p1_msrmnt_type, msrmnt.value.clone()));
                some_value = true;
            }
            if let Some((meter_pkh, shared_seeds)) = meter_pkh_and_shared_seeds_opt {
                match shared_seeds.get_encryption_info(meter_pkh, &timestamp) {
                    Err(e) => {
                        if !send_non_encrypted {
                            warn!("{e:#}");
                            warn!("no shared seed at {timestamp}");
                        }
                    }
                    Ok(shared_seed_encr_info) => {
                        let enc_modulus = pow(
                            BigInt::from(10),
                            shared_seed_encr_info.ten_pow_modulus as usize,
                        );
                        let encrypted_value = (&msrmnt.value)
                            + encrypting_value(
                                &shared_seed_encr_info.seed,
                                meter_pkh,
                                &timestamp,
                                msrmnt.p1_msrmnt_type,
                                &enc_modulus,
                                shared_seed_encr_info.ten_pow_divisor,
                            );
                        msrmnt_infos.push(EncryptedValue(msrmnt.p1_msrmnt_type, encrypted_value));
                        some_value = true;
                    }
                }
            }
        }
        if msrmnt_infos.is_empty() {
            Err(anyhow!("empty"))
        } else if !some_value {
            Err(anyhow!("no measurement value"))
        } else {
            Ok(msrmnt_infos)
        }
    }
}

pub(crate) fn encrypt_p1_telegrams_to_udp(
    from_encrypt_chan: &mpsc::Receiver<P1TelegramOrSharedSeedRequest>,
    shared_udp_socket: &Mutex<Option<UdpSocket>>,
    remote_aggr_address_port: SocketAddr,
    config: &P1EncryptConfig,
    pkh_and_signer_by_meter_serial_nr: &HashMap<String, (PubKeyHash, Arc<OpenSslSigner>)>,
) -> Result<()> {
    let keep_interval = config.shared_seeds.keep_interval.time_delta();
    let mut shared_seeds = SharedSeeds::new();
    let mut udp_socket_opt: Option<UdpSocket> = None;
    loop {
        if udp_socket_opt.is_none() {
            match ip_addr_in_wg_local_intf(&config.priv_aggr_meters_local_ip.address) {
                Err(e) => {
                    error!(
                        "{e:#} at: ip_addr_in_wg_local_intf {}",
                        &config.priv_aggr_meters_local_ip.address
                    );
                }
                Ok(res) => {
                    if res {
                        let bind_addr_port_aggr_meters =
                            config.priv_aggr_meters_local_ip.addr_port_str()?;
                        match udp_bind(&bind_addr_port_aggr_meters) {
                            Ok(udp_socket) => {
                                match udp_socket.try_clone() {
                                    Ok(udp_socket_clone) => match shared_udp_socket.lock() {
                                        Ok(mut udp_sock_opt_guard) => {
                                            *udp_sock_opt_guard = Some(udp_socket_clone);
                                        }
                                        Err(poison) => {
                                            error!("{poison:?} at lock");
                                        }
                                    },
                                    Err(e) => {
                                        error!("{e:?} at try_clone");
                                    }
                                }
                                udp_socket_opt = Some(udp_socket);
                            }
                            Err(e) => {
                                if let Some(ioe) = e.downcast_ref::<std::io::ErrorKind>() {
                                    if ioe == &std::io::ErrorKind::AddrInUse {
                                        return Err(
                                            e.context("udp_bind, no way to connect to aggregator")
                                        );
                                    }
                                }
                                error!("{e:#} at udp_bind {bind_addr_port_aggr_meters}");
                            }
                        }
                    } else {
                        warn!(
                            "address {} is not at a wireguard interface",
                            &config.priv_aggr_meters_remote_ip.address
                        );
                    }
                }
            };
        }

        match from_encrypt_chan.recv().context("recv")? {
            P1TelegramOrSharedSeedRequest::ShareSeedRequest(shared_seed_req) => {
                let current_time_dt = time_stamp_milli_secs();
                let keep_oldest_dt = current_time_dt - keep_interval;
                match shared_seeds.add_share_seed_request(
                    &shared_seed_req,
                    &keep_oldest_dt,
                    &current_time_dt,
                ) {
                    Ok(ShareSeedAddResult::Ok) => {}
                    Ok(ShareSeedAddResult::Duplicate) => {
                        trace!("duplicate shared_seed_req");
                    }
                    Err(e) => {
                        error!("{e:#} at add_share_seed_request");
                    }
                }
            }
            P1TelegramOrSharedSeedRequest::P1Telegram(p1_telegram) => {
                match signed_telegram_for_aggr_0(
                    &p1_telegram,
                    config.send_non_encrypted.unwrap_or(false),
                    &shared_seeds,
                    pkh_and_signer_by_meter_serial_nr,
                ) {
                    Err(e) => {
                        // this can be before the first shared seed interval
                        warn!("{e:#} at sign_telegram_to_aggr_0, ignore");
                        continue; // next p1_telegram_or_shared_seed
                    }
                    Ok((signed_msrmnts_infos, sign_duration_opt)) => {
                        match udp_socket_opt {
                            Some(ref udp_socket) => {
                                match udp_socket
                                    .send_to(&signed_msrmnts_infos, remote_aggr_address_port)
                                {
                                    Ok(size) => {
                                        if size != signed_msrmnts_infos.len() {
                                            error!(
                                                "sent {size} bytes of {}",
                                                signed_msrmnts_infos.len()
                                            );
                                        }
                                    }
                                    Err(e) => {
                                        error!("{e:?} for length {}", signed_msrmnts_infos.len())
                                    }
                                }
                            }
                            _ => {
                                warn!("no udp socket available to send p1_telegram")
                            }
                        }
                        if let Some(sign_duration) = sign_duration_opt {
                            trace!(
                                "{} measurements, total bytes including signature {}, sign_duration {} ms",
                                p1_telegram.msrmnts.len(),
                                signed_msrmnts_infos.len(),
                                sign_duration.as_millis(),
                            );
                        } else {
                            error!(
                                "{} measurements, total bytes including signature {}, but no sign duration",
                                p1_telegram.msrmnts.len(),
                                signed_msrmnts_infos.len()
                            );
                        };
                    }
                }
            }
        }
    }
}

fn signed_telegram_for_aggr_0(
    p1_telegram: &P1Telegram,
    send_non_encrypted: bool,
    shared_seeds: &SharedSeeds,
    pkh_and_signer_by_meter_serial_nr: &HashMap<String, (PubKeyHash, Arc<OpenSslSigner>)>,
) -> Result<(Vec<u8>, Option<Duration>)> {
    // It is possible to need more than one OpenSslSigner and meter PubKeyHash for a single P1Telegram.
    // But that is not easily implementable here, because it would need more than one signature on a udp packet.
    // So require that all measurements have the same signer.
    let mut pkh_and_ssl_signer_opt: Option<(&PubKeyHash, &OpenSslSigner)> = None;
    for msrmnt in &p1_telegram.msrmnts {
        if let Some(pkh_and_signer) = pkh_and_signer_by_meter_serial_nr.get(&msrmnt.meter_nr) {
            if let Some((pkh, _)) = pkh_and_ssl_signer_opt {
                if &pkh_and_signer.0 != pkh {
                    return Err(anyhow!(
                        "different signer for meter {pkh} and {} ",
                        pkh_and_signer.0
                    ));
                }
            } else {
                pkh_and_ssl_signer_opt = Some((&pkh_and_signer.0, &pkh_and_signer.1));
            }
        } else {
            return Err(anyhow!("no signer for meter nr {}", msrmnt.meter_nr));
        }
    }
    let Some((meter_pkh, ssl_signer)) = pkh_and_ssl_signer_opt else {
        return Err(anyhow!("no signer at all for {p1_telegram:?}")); // empty telegram
    };

    let msrmnt_infos = p1_telegram
        .measurement_infos_for_aggr(send_non_encrypted, Some((meter_pkh, shared_seeds)))?;

    let bincode_config = enmprv::bincoded::get_bincode_config();
    let msrmnt_infos_bytes = encode_to_vec(&msrmnt_infos, bincode_config)?;

    let sign_start_time = Instant::now();
    let signature = ssl_signer
        .generate_signature(&msrmnt_infos_bytes)
        .context("generate_signature")?;
    let sign_duration_opt = Instant::now().checked_duration_since(sign_start_time);

    let version_bytes: [u8; 2] = [0, 0];
    let mut bytes = Vec::<u8>::new();
    bytes.extend(version_bytes);
    let msrmnt_infos_bytes_len = msrmnt_infos_bytes.len();
    // encode as two bytes, least significant first.
    if msrmnt_infos_bytes_len > u16::MAX as usize {
        return Err(anyhow!(
            "msrmnt_infos_bytes length {msrmnt_infos_bytes_len} is longer than u16::MAX"
        ));
    }
    bytes.extend([
        msrmnt_infos_bytes_len as u8,
        (msrmnt_infos_bytes_len >> 8) as u8,
    ]);
    bytes.extend(msrmnt_infos_bytes);
    bytes.extend(signature);
    Ok((bytes, sign_duration_opt))
}

pub(crate) fn show_udp_replies(
    shared_udp_sock: &Arc<Mutex<Option<UdpSocket>>>,
    remote_aggr_addr_port: SocketAddr,
    config: &P1EncryptConfig,
) -> ! {
    // return error message, if any.
    const BUF_SIZE: usize = 1350; // just over normal udp max packet size
    let mut receive_buffer = [0u8; BUF_SIZE];
    let mut udp_socket_opt: Option<UdpSocket> = None;
    let mut waited_once: bool = false;
    let aggr_sig_verifier_opt =
        match OpenSslSigVerifier::new(&config.aggregator_public_key, &config.temp_dir) {
            Ok(sig_verifier) => Some(sig_verifier),
            Err(e) => {
                error!("{e:?}");
                None
            }
        };

    loop {
        match shared_udp_sock.lock() {
            Ok(mut udp_sock_opt_guard) => match udp_sock_opt_guard.take() {
                Some(udp_socket) => {
                    udp_socket_opt = Some(udp_socket);
                }
                None => {
                    if udp_socket_opt.is_none() && waited_once {
                        warn!("no socket available");
                    }
                }
            },
            Err(poison) => {
                error!("{poison:?} at lock");
            }
        }
        match udp_socket_opt {
            Some(ref udp_sock) => {
                match udp_sock.recv_from(&mut receive_buffer) {
                    Err(sock_err) => {
                        error!("{sock_err:?} at recv_from");
                        udp_socket_opt = None; // try and take a new socket from shared_udp_sock
                    }
                    Ok((num_bytes_received, src_addr_port)) => {
                        let Some(ref aggr_sig_verifier) = aggr_sig_verifier_opt else {
                            error!("no signature verifier for received udp packet");
                            continue;
                        };
                        if src_addr_port != remote_aggr_addr_port {
                            warn!(
                                "unexpected address_port {src_addr_port}, message length {num_bytes_received}"
                            );
                        } else if num_bytes_received == BUF_SIZE {
                            warn!("buffer full, message length {num_bytes_received}");
                        } else {
                            if num_bytes_received < 6 {
                                error!("received udp data too short length {num_bytes_received}");
                                continue;
                            }
                            if receive_buffer[..2] != [0, 0] {
                                error!("unexpected version bytes {:?}", &receive_buffer[..2]);
                                continue;
                            }
                            let mes_size =
                                receive_buffer[2] as usize + ((receive_buffer[3] as usize) << 8);
                            if mes_size + 4 + 2 > receive_buffer.len() {
                                error!(
                                    "message size {mes_size} too long for {}",
                                    receive_buffer.len()
                                );
                                continue;
                            }
                            let mes_bytes = &receive_buffer[4..][..mes_size];
                            let sig_bytes = &receive_buffer[4..num_bytes_received][mes_size..];
                            match aggr_sig_verifier.verify_signature(mes_bytes, sig_bytes) {
                                Err(e) => {
                                    error!("{e:?}");
                                    continue;
                                }
                                Ok(verified) => {
                                    if !verified {
                                        error!(
                                            "signature verification by {:?} did not pass",
                                            aggr_sig_verifier
                                        );
                                        continue;
                                    }
                                }
                            }
                            match decode_from_slice::<AggrReplyToTelegram, _>(
                                mes_bytes,
                                get_bincode_config(),
                            ) {
                                Err(e) => {
                                    error!("{e:?}");
                                    continue;
                                }
                                Ok((aggr_reply, size)) => {
                                    if size != mes_bytes.len() {
                                        error!(
                                            "aggr_reply decode_from_slice incomplete, {} of {}",
                                            size,
                                            mes_bytes.len()
                                        );
                                        continue;
                                    }
                                    use AggrReplyToTelegram::*;
                                    match aggr_reply {
                                        NotVerified => {
                                            error!("last telegram not verified by aggregator")
                                        }
                                        InternalError => {
                                            error!("aggregator internal error for last telegram")
                                        }
                                        LastAggregationWithMeterSerialNr(
                                            aggr_msrmnt_anon_infos,
                                        ) => {
                                            if aggr_msrmnt_anon_infos.is_empty() {
                                                info!(
                                                    "no aggregated anonymous measurement infos for last telegram"
                                                )
                                            } else {
                                                for msrmnt_inf in aggr_msrmnt_anon_infos {
                                                    use enmprv::bincoded::AggrAnonMeasurementInfo::*;
                                                    match msrmnt_inf {
                                                        AggrValue(msrmnt_type, value) => {
                                                            info!(
                                                                "{:>11} {msrmnt_type:?}",
                                                                value.to_string(5)
                                                            );
                                                        }
                                                        AggrId(..) | AggrUtc(..)
                                                        | TotalDelayMs(..) | NumAggregated(..) => {
                                                            info!("{msrmnt_inf:?}");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            _ => {
                thread::sleep(config.priv_aggr_meters_timeout.std_duration());
                waited_once = true;
            }
        }
    }
}
