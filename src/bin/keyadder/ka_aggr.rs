use std::{
    collections::{
        BTreeSet, HashMap, HashSet,
        hash_map::Entry::{Occupied, Vacant},
    },
    io,
    iter::FromIterator,
    net::{TcpListener, TcpStream},
    sync::{
        Arc,
        mpsc::{self, SyncSender},
    },
    thread,
    time::{Duration, Instant},
};

use anyhow::{Context, Error, Result, anyhow};
use bincode::serde::decode_from_slice;
use log::{error, info, warn};
use num::{BigInt, FromPrimitive, pow};
use polling::{Event, Events, Poller};

use enmprv::{
    bincoded::{
        EncKeyAddReply, EncKeyAddRequest, EncryptionResult, IncrementalRequest,
        MeasurementTypeTotalEncKey, P1MeasurementType, get_bincode_config,
    },
    checkclocksync::RepeatCheckClockSync,
    encryptionseed::{SharedSeeds, encrypting_value},
    measuredvalue::MeasuredValue,
    openssl::{OpenSslSigVerifier, OpenSslSigner, PubKeyHash},
    util::{CtrlC, receive_signed_buffer, send_signed_message, time_stamp_milli_secs},
};

use crate::{KeyAdderConfig, KeyAdderQueuedMessage};

fn receive_key_add_requests_for_interval(
    aggr_meters_tcp_stream: &mut TcpStream,
    aggr_sig_verifier: &Arc<OpenSslSigVerifier>,
    read_timeout_seconds: u64,
    key_add_sender: &mpsc::SyncSender<KeyAdderQueuedMessage>,
) -> Result<()> {
    let bincode_config = get_bincode_config();
    loop {
        let key_add_req_buffer = match receive_signed_buffer(
            aggr_meters_tcp_stream,
            read_timeout_seconds,
            aggr_sig_verifier,
        ) {
            Err(e) => {
                if let Some(ioe) = e.downcast_ref::<io::Error>() {
                    if ioe.kind() == io::ErrorKind::UnexpectedEof {
                        return Ok(());
                    }
                }
                return Err(e.context("receive_signed_buffer"));
            }
            Ok(b) => b,
        };
        let (key_add_request, size) = decode_from_slice(&key_add_req_buffer, bincode_config)
            .context("deserialize EncKeyAddRequest")?;
        if size != key_add_req_buffer.len() {
            return Err(anyhow!(
                "key_add_request decode_from_slice incomplete, {} of {}",
                size,
                key_add_req_buffer.len()
            ));
        }
        key_add_sender
            .send(KeyAdderQueuedMessage::EncKeyAddRequest(key_add_request))
            .context("key_add_sender.send")?;
    }
}

pub(crate) fn connect_from_aggregator(
    config: &KeyAdderConfig,
    key_add_sender: &SyncSender<KeyAdderQueuedMessage>,
) -> Result<()> {
    const AGGR_KEY: usize = 7;
    let local_addr_port = config.priv_aggr_local_ip.addr_port_str()?;
    let accept_timeout_seconds = config.tcp_accept_timeout.std_duration().as_secs();
    let read_timeout_seconds = config.tcp_read_timeout.std_duration().as_secs();

    let aggr_sig_verifier = Arc::new(OpenSslSigVerifier::new(
        &config.priv_aggr_public_key,
        &config.temp_dir,
    )?);

    loop {
        let listener: TcpListener = match TcpListener::bind(local_addr_port.clone()) {
            Err(e) => {
                error!("{e:?} at bind local_addr_port: {local_addr_port}");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                continue; // next listener
            }
            Ok(l) => l,
        };
        if let Err(e) = listener.set_nonblocking(true) {
            error!("{e:?} at set_nonblocking listener {listener:?}");
            thread::sleep(Duration::from_secs(accept_timeout_seconds));
            continue; // next listener
        }
        let mut start_instant = Instant::now();

        let poller = match Poller::new() {
            Ok(p) => p,
            Err(e) => {
                error!("{e:?} at Poller::new");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                continue; // next listener
            }
        };

        // Set interest in the first events.

        let add_res;
        unsafe {
            add_res = poller.add(&listener, Event::readable(AGGR_KEY));
            // needs poller.delete() below for safefy.
        }
        if let Err(e) = add_res {
            error!("{e:?} poller.add()");
            thread::sleep(Duration::from_secs(accept_timeout_seconds));
            continue; // next listener
        }

        loop {
            let mut events = Events::new();
            if let Err(e) = poller.wait(&mut events, Some(Duration::new(accept_timeout_seconds, 0)))
            {
                error!("{e:?} at poller.wait");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                break; // next listener
            }
            if events.is_empty() {
                error!("no event from {listener:?} via {poller:?}");
            } else {
                for ev in events.iter() {
                    if ev.key != AGGR_KEY {
                        error!("poller.wait() unknown event key in {ev:?}");
                        thread::sleep(Duration::from_secs(accept_timeout_seconds));
                        break; // next listener
                    }
                    match listener.accept() {
                        Ok((mut interval_stream, _peer_addr)) => {
                            let interval_stream_clone = match interval_stream.try_clone() {
                                Ok(stream) => stream,
                                Err(e) => {
                                    return Err(Error::new(e).context("try_clone"));
                                }
                            };
                            match key_add_sender
                                .send(KeyAdderQueuedMessage::TcpStream(interval_stream_clone))
                            {
                                Ok(()) => {}
                                Err(e) => {
                                    error!("{e:?}");
                                }
                            }

                            if let Err(e) = receive_key_add_requests_for_interval(
                                &mut interval_stream,
                                &aggr_sig_verifier,
                                read_timeout_seconds,
                                key_add_sender,
                            ) {
                                if e.downcast_ref::<CtrlC>().is_some() {
                                    return Err(e);
                                } else {
                                    warn!("{e:#}");
                                }
                            }
                        }
                        Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                            if start_instant.elapsed() > Duration::new(accept_timeout_seconds, 0) {
                                info!("time out on {listener:?}");
                                start_instant = Instant::now();
                            }
                        }
                        Err(e) => {
                            error!("listener.accept() {e:?}");
                            thread::sleep(Duration::from_secs(accept_timeout_seconds));
                            break; // next listener
                        }
                    }
                }
            }
            // Set interest in the next events.
            if let Err(e) = poller.modify(&listener, Event::readable(AGGR_KEY)) {
                error!("poller.modify() {e:?}");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                break; // next listener
            }
        }
        poller.delete(&listener)?; // for safety of poller.add()
    }
}

// For building up a MeasurementTypeTotalEncKey from partial results per eq_id/ms_dt for a single measurement type.
// This is initialized and maintained per aggregation interval
// The total_encryption_key is maintained also for meters serial nrs that are initially refused
// as long there are not enough meters available in the interval.
struct PartialEncKey {
    total_encryption_key: MeasuredValue,
    total_measurements: u64,
    // maximum decimal digits encountered in encryption infos used in this interval.
    // used for formatting the encryption key in the reply.
    max_decimal_digits: u32,
    refused_meter_pkhs: Vec<PubKeyHash>, // Clear this when total meters exceed the configured minimum.
    // Allow a later received msrmnt for the same meter to replace an earlier msrmnt:
    enc_key_by_meter_pkh: HashMap<PubKeyHash, MeasuredValue>, // Last encryption key by meter serial nr, added into total_encryption_key,
}

// Aggregate encryption keys for an interval
struct IntervalEncryptionKeysAggr {
    min_meters_for_decryption: u64,
    partial_enc_key_by_msrmnt_type: HashMap<P1MeasurementType, PartialEncKey>,
    meter_pkhs_no_encryption_info: HashSet<PubKeyHash>,
}

impl IntervalEncryptionKeysAggr {
    fn new(min_meters_for_decryption: u64) -> Self {
        IntervalEncryptionKeysAggr {
            min_meters_for_decryption,
            partial_enc_key_by_msrmnt_type: HashMap::new(),
            meter_pkhs_no_encryption_info: HashSet::new(),
        }
    }
    fn earlier_missing_encryption_info(&self, meter_pkh: &PubKeyHash) -> bool {
        self.meter_pkhs_no_encryption_info.contains(meter_pkh)
    }

    fn missing_encryption_info_for_meter(&mut self, meter_pkh: &PubKeyHash) {
        self.meter_pkhs_no_encryption_info.insert(meter_pkh.clone());
        // In this interval the meter_nr may have had an earlier measurement with encryption info available,
        // Remove this meter_nr/msrmnt_type from partial results for all msmrnt types
        for (msrmnt_type, partial_enc_key) in &mut self.partial_enc_key_by_msrmnt_type {
            if let Some(enc_key) = partial_enc_key.enc_key_by_meter_pkh.remove(meter_pkh) {
                partial_enc_key.total_encryption_key -= enc_key;
                warn!("removing earlier encryption key for meter {meter_pkh} and {msrmnt_type:?}");
            }
        }
    }

    fn add_encryption_key(
        &mut self,
        meter_pkh: &PubKeyHash,
        msrmnt_type: P1MeasurementType,
        enc_key: MeasuredValue,
        max_decimal_digits: u32,
    ) {
        match self.partial_enc_key_by_msrmnt_type.entry(msrmnt_type) {
            Vacant(entry) => {
                // first measurement of this type
                let partial_enc_key = PartialEncKey {
                    total_encryption_key: enc_key.clone(),
                    max_decimal_digits,
                    total_measurements: 1,
                    refused_meter_pkhs: if self.min_meters_for_decryption > 1 {
                        vec![meter_pkh.to_owned()]
                    } else {
                        // no meter value privacy
                        Vec::new()
                    },
                    enc_key_by_meter_pkh: HashMap::from_iter([(meter_pkh.clone(), enc_key)]),
                };
                entry.insert(partial_enc_key);
            }
            Occupied(mut entry) => {
                // later measurement of this type
                let partial_enc_key = entry.get_mut();
                match partial_enc_key
                    .enc_key_by_meter_pkh
                    .entry(meter_pkh.to_owned())
                {
                    Vacant(entry) => {
                        // first measurement of this type for this meter serial nr
                        entry.insert(enc_key.clone());
                        partial_enc_key.total_encryption_key += enc_key;
                        if max_decimal_digits > partial_enc_key.max_decimal_digits {
                            partial_enc_key.max_decimal_digits = max_decimal_digits;
                        }
                        partial_enc_key.total_measurements += 1;
                        if partial_enc_key.total_measurements < self.min_meters_for_decryption {
                            partial_enc_key
                                .refused_meter_pkhs
                                .push(meter_pkh.to_owned());
                        } else {
                            partial_enc_key.refused_meter_pkhs.clear();
                        }
                    }
                    Occupied(mut entry) => {
                        // later received measurement of this type for this meter serial nr, discard earlier
                        let enc_key_eqid_ref = entry.get_mut();
                        let prev_enc_key = enc_key_eqid_ref.clone();
                        *enc_key_eqid_ref = enc_key.clone();
                        partial_enc_key.total_encryption_key -= prev_enc_key;
                        partial_enc_key.total_encryption_key += enc_key;
                        // same meter serial nr: leave partial_enc_key.total_measurements unchanged
                    }
                }
            }
        }
    }

    fn total_key_by_msrmnt_type(
        &self,
        msrmnt_type: P1MeasurementType,
        min_eq_ids_decrypt: u64,
    ) -> Option<MeasurementTypeTotalEncKey> {
        if let Some(partial_enc_key) = self.partial_enc_key_by_msrmnt_type.get(&msrmnt_type) {
            let total_encryption_key = if partial_enc_key.total_measurements < min_eq_ids_decrypt {
                info!(
                    "need measurements from at least {} grid connections, only {} available for {msrmnt_type:?}",
                    min_eq_ids_decrypt, partial_enc_key.total_measurements
                );
                EncryptionResult::PrivacyViolation
            } else {
                EncryptionResult::TotalEncryptionKey(partial_enc_key.total_encryption_key.clone())
            };

            for refused_meter_pkh in &partial_enc_key.refused_meter_pkhs {
                info!("refused meter set {refused_meter_pkh} for {msrmnt_type:?}");
            }

            Some(MeasurementTypeTotalEncKey {
                msrmnt_type,
                total_encryption_key,
                total_measurements: partial_enc_key.total_measurements,
                refused_meter_set_pkhs: partial_enc_key.refused_meter_pkhs.clone(),
            })
        } else {
            None
        }
    }
}

fn send_key_add_reply_for_interval(
    signer: &OpenSslSigner,
    config: &KeyAdderConfig,
    key_add_receiver: &mpsc::Receiver<KeyAdderQueuedMessage>,
    shared_seeds: &mut SharedSeeds,
) -> Result<()> {
    let Some(ten_bi) = BigInt::from_usize(10) else {
        return Err(anyhow!("BigInt::from_usize(10)"));
    };

    let mut aggr_meters_tcp_stream_opt: Option<TcpStream> = None; // use the last one from the queue.
    let mut key_add_request_opt: Option<EncKeyAddRequest> = None; // use the last one from the queue.

    let mut interval_keys_aggr = IntervalEncryptionKeysAggr::new(config.min_meters_decrypt);

    let shared_seeds_keep_duration = config.shared_seeds_keep_duration.time_delta();

    // to log the meter serial nrs for which an encryption key was computed:
    let mut meter_pkhs_enc_key: BTreeSet<PubKeyHash> = BTreeSet::new(); // BTreeSet order for logging

    loop {
        let request = key_add_receiver.recv().context("recv")?;

        match request {
            KeyAdderQueuedMessage::TcpStream(s) => {
                // for sending the key add reply
                aggr_meters_tcp_stream_opt = Some(s);
            }
            KeyAdderQueuedMessage::EncKeyAddRequest(kar) => {
                key_add_request_opt = Some(kar);
            }
            KeyAdderQueuedMessage::ShareSeedRequest(share_seed_request) => {
                let current_time_stamp = time_stamp_milli_secs();
                let keep_oldest_dt = current_time_stamp - shared_seeds_keep_duration;
                match shared_seeds.add_share_seed_request(
                    &share_seed_request,
                    &keep_oldest_dt,
                    &current_time_stamp,
                ) {
                    Ok(shared_seed_add_result) => {
                        info!("add_share_seed_request {shared_seed_add_result:?}");
                    }
                    Err(e) => {
                        error!("{e:#} at add_share_seed_request");
                    }
                }
            }
        }

        let Some(ref key_add_request) = key_add_request_opt else {
            continue;
        };

        // Check the measurement types in the key_add_request
        for meter_nr_meas_dt_types in &key_add_request.meter_pkh_meas_dt_types_vec {
            let msrmnt_type_set = &meter_nr_meas_dt_types.msrmnt_types;
            if msrmnt_type_set.is_empty() {
                return Err(anyhow!(
                    "no requested measurement types in {meter_nr_meas_dt_types:?}"
                ));
            }
        }

        for meter_pkh_measurement_dt_types in &key_add_request.meter_pkh_meas_dt_types_vec {
            match shared_seeds.get_encryption_info(
                &meter_pkh_measurement_dt_types.meter_pkh,
                &meter_pkh_measurement_dt_types.meas_dt_stamp,
            ) {
                Err(e) => {
                    warn!("{e:#}, no encryption info for {meter_pkh_measurement_dt_types}");
                    interval_keys_aggr.missing_encryption_info_for_meter(
                        &meter_pkh_measurement_dt_types.meter_pkh,
                    );
                }
                Ok(encryption_info) => {
                    if interval_keys_aggr
                        .earlier_missing_encryption_info(&meter_pkh_measurement_dt_types.meter_pkh)
                    {
                        warn!(
                            "earlier measurement from meter {} had no encryption info available",
                            &meter_pkh_measurement_dt_types.meter_pkh
                        );
                    } else {
                        let enc_modulo_digits = usize::from(encryption_info.ten_pow_modulus);
                        let encryption_modulus = pow(ten_bi.clone(), enc_modulo_digits);
                        let ten_pow_divisor = encryption_info.ten_pow_divisor;

                        let max_decimal_digits = u32::from(ten_pow_divisor);

                        let msrmnt_type_set = &meter_pkh_measurement_dt_types.msrmnt_types;
                        for msrmnt_type in P1MeasurementType::iter() {
                            if msrmnt_type_set.contains(msrmnt_type) {
                                let enc_key = encrypting_value(
                                    &encryption_info.seed,
                                    &meter_pkh_measurement_dt_types.meter_pkh,
                                    &meter_pkh_measurement_dt_types.meas_dt_stamp,
                                    *msrmnt_type,
                                    &encryption_modulus,
                                    ten_pow_divisor,
                                );

                                interval_keys_aggr.add_encryption_key(
                                    &meter_pkh_measurement_dt_types.meter_pkh,
                                    *msrmnt_type,
                                    enc_key,
                                    max_decimal_digits,
                                );

                                meter_pkhs_enc_key
                                    .insert(meter_pkh_measurement_dt_types.meter_pkh.to_owned());
                            }
                        }
                    }
                }
            }
        }

        match key_add_request.request_stage {
            IncrementalRequest::Intermediate => {
                continue;
            }
            IncrementalRequest::Final => break,
        }
    }

    // log the meter serial nrs for which an encryption key was computed:
    if !meter_pkhs_enc_key.is_empty() {
        info!(
            "{} meters: {}",
            meter_pkhs_enc_key.len(),
            meter_pkhs_enc_key
                .into_iter()
                .map(|pkh| format!("{pkh}"))
                .collect::<Vec<_>>()
                .join(" ")
        );
    }

    // avoid HashMap random order
    let msrmnt_type_total_key_vec = P1MeasurementType::iter()
        .filter_map(|msrmnt_type| {
            interval_keys_aggr.total_key_by_msrmnt_type(*msrmnt_type, config.min_meters_decrypt)
        })
        .collect::<Vec<MeasurementTypeTotalEncKey>>();

    let mut meter_pkhs_without_encryption_info =
        Vec::<PubKeyHash>::from_iter(interval_keys_aggr.meter_pkhs_no_encryption_info);
    meter_pkhs_without_encryption_info.sort();

    let Some(mut aggr_meters_tcp_stream) = aggr_meters_tcp_stream_opt else {
        return Err(anyhow!(
            "send_key_add_replies_for_interval: no tcp stream to answer"
        ));
    };

    if let Err(e) = send_signed_message(
        &EncKeyAddReply {
            msrmnt_type_total_key_vec: msrmnt_type_total_key_vec.clone(), // clone for reporting below
            meter_pkhs_no_encryption_info: meter_pkhs_without_encryption_info,
        },
        signer,
        &mut aggr_meters_tcp_stream,
    ) {
        dbg!(e);
    }

    use EncryptionResult::*;
    for mttk in &msrmnt_type_total_key_vec {
        match &mttk.total_encryption_key {
            PrivacyViolation => {
                info!(
                    "{:>3} {:<18?} msrmnt(s), privacy violation",
                    mttk.total_measurements, mttk.msrmnt_type,
                );
            }
            TotalEncryptionKey(_) => {}
        }
    }
    if msrmnt_type_total_key_vec
        .iter()
        .any(|mttk| matches!(mttk.total_encryption_key, TotalEncryptionKey(_),))
    {
        info!("decryption keys:");
        for mttk in &msrmnt_type_total_key_vec {
            match &mttk.total_encryption_key {
                TotalEncryptionKey(decryption_key) => {
                    let decimal_digits = 3; // CHECKME: take from some config
                    let msrmnt_type_str = format!("{:?}", mttk.msrmnt_type);
                    info!(
                        "{:>3} {:>18} msrmnts: {:>18}",
                        mttk.total_measurements,
                        msrmnt_type_str,
                        decryption_key.to_string(decimal_digits),
                    );
                }
                PrivacyViolation => {}
            }
        }
    }
    Ok(())
}

pub(crate) fn send_key_add_replies_for_intervals(
    signer: &OpenSslSigner,
    config: &KeyAdderConfig,
    repeat_check_clock_sync: &RepeatCheckClockSync,
    key_add_receiver: &mpsc::Receiver<KeyAdderQueuedMessage>,
) -> Result<()> {
    let mut shared_seeds = SharedSeeds::new();
    loop {
        send_key_add_reply_for_interval(signer, config, key_add_receiver, &mut shared_seeds)
            .context("send_key_add_reply_for_interval")?;
        repeat_check_clock_sync.recheck_wait()?;
    }
}
