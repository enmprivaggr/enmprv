use std::{
    collections::BTreeSet,
    env,
    fs::File,
    io::Read,
    net::TcpStream,
    process::exit,
    sync::{Arc, mpsc},
    thread,
};

use anyhow::Error;
use log::{error, info, warn};
use serde::Deserialize;

use enmprv::{
    bincoded::{EncKeyAddRequest, ShareSeedRequest},
    checkclocksync::RepeatCheckClockSync,
    durationconfig::DurationConfig,
    openssl::{OpenSslSigner, OpenSslSignerFiles},
    util::{IpAddressPortConfig, RunState, ip_addr_in_wg_local_intf},
};

mod ka_aggr;
mod ka_p1;

enum KeyAdderQueuedMessage {
    TcpStream(TcpStream),
    EncKeyAddRequest(EncKeyAddRequest),
    ShareSeedRequest(ShareSeedRequest),
}

const CONFIG_FILE_NAME: &str = "keyadder.toml";
const LOG_CONFIG_FILE_NAME: &str = "keyaddlog4rs.yaml";

#[derive(Deserialize, Debug)]
pub(crate) struct KeyAdderConfig {
    wait_clock_sync: DurationConfig,
    repeat_wait_clock_sync: DurationConfig,
    min_meters_decrypt: u64,
    tcp_accept_timeout: DurationConfig,
    tcp_read_timeout: DurationConfig,
    sync_queue_size: u64,
    priv_aggr_local_ip: IpAddressPortConfig,
    p1_encryptors_local_ip: IpAddressPortConfig,
    shared_seeds_keep_duration: DurationConfig,
    temp_dir: String,
    priv_aggr_public_key: String,
    meter_set_public_keys: BTreeSet<String>,
    ssl_signer_files: OpenSslSignerFiles,
}

fn usage_exit(exit_code: i32) -> ! {
    eprintln!("argument: -v");
    eprintln!("  show invocation and version number");
    eprintln!("arguments: [config_file [log_config_file]]");
    eprintln!("  config_file:     default {CONFIG_FILE_NAME}");
    eprintln!("  log_config_file: default {LOG_CONFIG_FILE_NAME}");
    exit(exit_code)
}

fn error_exit(e: Error) -> ! {
    error!("{e:#}, exiting");
    exit(1)
}

fn main() {
    let mut args = env::args();
    let binary_name = args.next().unwrap_or("key adder".to_string());
    let config_file_name = args.next().unwrap_or_else(|| CONFIG_FILE_NAME.to_string());
    let version = env!("CARGO_PKG_VERSION");
    if config_file_name == "-v" {
        println!("{binary_name} version {version}");
        exit(0);
    }
    if config_file_name == "-h" {
        usage_exit(0);
    }
    let log_config_file_name = args
        .next()
        .unwrap_or_else(|| LOG_CONFIG_FILE_NAME.to_string());
    if args.next().is_some() {
        usage_exit(1);
    }
    log4rs::init_file(
        &log_config_file_name,
        log4rs::config::Deserializers::default(),
    )
    .unwrap_or_else(|e| {
        eprintln!("{log_config_file_name}: {e:?} at log4rs::init_file , exiting",);
        exit(1);
    });
    info!(
        "starting key adder version {version} as {binary_name} user {} pid {}",
        std::env::var("USER").unwrap_or("".to_owned()),
        std::process::id()
    );

    let run_state = match RunState::new() {
        Err(e) => error_exit(e.context("RunState::new()")),
        Ok(r) => r,
    };
    run_state.start_exit_thread();

    let mut toml_buf = Vec::<u8>::new();
    {
        let mut config_file = File::open(&config_file_name).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(config_file_name.to_owned()));
        });
        let _ = config_file.read_to_end(&mut toml_buf).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(format!("read_to_end {config_file_name}")));
        });
    }
    let toml_str = std::str::from_utf8(&toml_buf).unwrap_or_else(|e| {
        error_exit(Error::new(e).context(config_file_name.to_owned()));
    });

    let config = Arc::new(
        toml::from_str::<KeyAdderConfig>(toml_str).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(format!("toml::from_str {config_file_name}")));
        }),
    );

    let repeat_check_clock_sync = match RepeatCheckClockSync::new(
        config.wait_clock_sync.std_duration(),
        config.repeat_wait_clock_sync.std_duration(),
    ) {
        Err(e) => error_exit(e.context("RepeatCheckClockSync::new")),
        Ok(rccs) => rccs,
    };

    match ip_addr_in_wg_local_intf(&config.priv_aggr_local_ip.address) {
        Err(e) => error_exit(e.context(format!(
            "ip_addr_in_wg_local_intf {}",
            &config.priv_aggr_local_ip.address
        ))),
        Ok(res) => {
            if !res {
                warn!(
                    "no local wireguard ip interface for {}",
                    &config.priv_aggr_local_ip.address
                );
            }
        }
    }

    match ip_addr_in_wg_local_intf(&config.p1_encryptors_local_ip.address) {
        Err(e) => error_exit(e.context(format!(
            "ip_addr_in_wg_local_intf {}",
            &config.p1_encryptors_local_ip.address
        ))),
        Ok(res) => {
            if !res {
                warn!(
                    "no local wireguard ip interface for {}",
                    &config.p1_encryptors_local_ip.address
                );
            }
        }
    }

    let queue_size = config.sync_queue_size as usize;
    let (key_add_sender, key_add_receiver) =
        mpsc::sync_channel::<KeyAdderQueuedMessage>(queue_size);

    // start threads
    let signer = Arc::new(
        OpenSslSigner::new(&config.ssl_signer_files, &config.temp_dir).unwrap_or_else(|e| {
            error_exit(e.context("OpenSslSigner::new()"));
        }),
    );

    let signer_clone = Arc::clone(&signer);
    let config_clone = Arc::clone(&config);
    let _key_add_reply_sender_thread = thread::spawn({
        move || loop {
            if let Err(e) = ka_aggr::send_key_add_replies_for_intervals(
                &signer_clone,
                &config_clone,
                &repeat_check_clock_sync,
                &key_add_receiver,
            ) {
                error!("send_key_add_replies_for_intervals() {e:?}");
            }
        }
    });

    let share_seed_sender = key_add_sender.clone(); // extra queue producer

    let config_clone = Arc::clone(&config);
    let _share_seed_thread = thread::spawn({
        move || match ka_p1::listen_seed_share_requests(&config_clone, &signer, &share_seed_sender)
        {
            Err(e) => error_exit(e.context("listen_seed_share_requests")),
            Ok(()) => {
                error!("serve_seed_share_requests thread stopped");
            }
        }
    });

    match ka_aggr::connect_from_aggregator(&config, &key_add_sender) {
        Ok(()) => {
            info!("connect_from_aggregator returned, exiting");
        }
        Err(e) => {
            error_exit(e.context("connect_from_aggregator"));
        }
    }
}
