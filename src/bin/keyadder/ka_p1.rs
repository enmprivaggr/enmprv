use std::{
    collections::HashMap,
    io,
    net::{TcpListener, TcpStream},
    rc::Rc,
    sync::mpsc::SyncSender,
    thread,
    time::{Duration, Instant},
};

use anyhow::{Context, Result, anyhow};
use bincode::serde::decode_from_slice;
use log::{Level, error, info, log, trace, warn};
use polling::{Event, Events, Poller};

use enmprv::{
    bincoded::{ShareSeedReply, ShareSeedRequest, get_bincode_config},
    openssl::{OpenSslSigVerifier, OpenSslSigner, PubKeyHash},
    util::{
        receive_version_size_message, send_signed_message, sig_verifier_by_meter_pkh_from_config,
    },
};

use crate::{KeyAdderConfig, KeyAdderQueuedMessage};

pub(crate) fn listen_seed_share_requests(
    config: &KeyAdderConfig,
    key_adder_signer: &OpenSslSigner,
    share_seed_sender: &SyncSender<KeyAdderQueuedMessage>,
) -> Result<()> {
    const P1_KEY: usize = 9;
    let sig_verifier_by_meter_pkh =
        sig_verifier_by_meter_pkh_from_config(&config.meter_set_public_keys, &config.temp_dir)?;

    let local_addr_port = config.p1_encryptors_local_ip.addr_port_str()?;
    let accept_timeout_seconds = config.tcp_accept_timeout.std_duration().as_secs();
    let read_timeout_seconds = config.tcp_read_timeout.std_duration().as_secs();
    loop {
        let listener: TcpListener = match TcpListener::bind(local_addr_port.clone()) {
            Err(e) => {
                error!("{e:?} at bind local_addr_port: {local_addr_port}");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                continue; // new listener
            }
            Ok(l) => l,
        };
        if let Err(e) = listener.set_nonblocking(true) {
            error!("{e:?} at set_nonblocking listener {listener:?}");
            thread::sleep(Duration::from_secs(accept_timeout_seconds));
            continue; // new listener
        }
        let mut start_instant = Instant::now();

        let poller = match Poller::new() {
            Ok(p) => p,
            Err(e) => {
                error!("{e:?} at Poller::new");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                continue; // new listener
            }
        };

        // CHECKME: merging this poller with the one in ka_aggr might save one thread.

        // Set interest in the first events.
        let add_res;
        unsafe {
            add_res = poller.add(&listener, Event::readable(P1_KEY));
            // needs poller.delete() below for safefy.
        }
        if let Err(e) = add_res {
            error!("{e:?} at poller.add");
            thread::sleep(Duration::from_secs(accept_timeout_seconds));
            continue; // new listener
        }

        loop {
            let mut events = Events::new();
            if let Err(e) = poller.wait(&mut events, Some(Duration::new(accept_timeout_seconds, 0)))
            {
                error!("{e:?} at poller.wait");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                break; // new listener
            }
            if events.is_empty() {
                trace!("no event from {listener:?} via {poller:?}");
            } else {
                for ev in events.iter() {
                    if ev.key != P1_KEY {
                        error!("poller.wait() unknown event key in {ev:?}");
                        thread::sleep(Duration::from_secs(accept_timeout_seconds));
                        break; // poll again.
                    }
                    match listener.accept() {
                        Ok((mut seed_share_stream, _peer_addr)) => {
                            if let Err(e) = serve_seed_share_requests(
                                &mut seed_share_stream,
                                read_timeout_seconds,
                                &sig_verifier_by_meter_pkh,
                                key_adder_signer,
                                share_seed_sender,
                            ) {
                                let mut level: Level = Level::Warn;
                                for cause in e.chain() {
                                    if let Some(ioe) = cause.downcast_ref::<io::Error>() {
                                        if ioe.kind() == io::ErrorKind::UnexpectedEof {
                                            level = Level::Trace;
                                            break;
                                        }
                                    }
                                }
                                log!(level, "{e:#}");
                            }
                        }
                        Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                            if start_instant.elapsed() > Duration::new(accept_timeout_seconds, 0) {
                                info!("time out on {listener:?}");
                                start_instant = Instant::now();
                            }
                        }
                        Err(e) => {
                            error!("{e:?} at listener.accept");
                            thread::sleep(Duration::from_secs(accept_timeout_seconds));
                            break; // new listener
                        }
                    }
                }
            }
            // Set interest in the next events.
            // From the docs: interest in I/O events needs to be re-enabled using modify() again
            // after an event is delivered if we’re interested in the next event of the same kind.
            if let Err(e) = poller.modify(&listener, Event::readable(P1_KEY)) {
                error!("{e:?} at poller.modify");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                break; // new listener
            }
        }
        poller.delete(&listener)?; // for safety of poller.add()
    }
}

fn serve_seed_share_requests(
    seed_share_stream: &mut TcpStream,
    read_timeout_seconds: u64,
    sig_verifier_by_meter_pkh: &HashMap<PubKeyHash, Rc<OpenSslSigVerifier>>,
    key_adder_signer: &OpenSslSigner,
    share_seed_sender: &SyncSender<KeyAdderQueuedMessage>,
) -> Result<()> {
    let bincode_config = get_bincode_config();
    loop {
        let share_seed_request_res: Result<ShareSeedRequest, String> = {
            match receive_version_size_message(seed_share_stream, read_timeout_seconds) {
                Err(e) => {
                    for cause in e.chain() {
                        if let Some(ioe) = cause.downcast_ref::<io::Error>() {
                            if ioe.kind() == io::ErrorKind::UnexpectedEof {
                                // no more requests
                                return Ok(());
                            }
                        }
                    }
                    Err(format!("{e:?} at receive_version_size_message"))
                }
                Ok(seed_share_request_buf) => {
                    let signature =
                        receive_version_size_message(seed_share_stream, read_timeout_seconds)
                            .context("receive_version_size_message signature")?;

                    let (seed_share_request, size) = decode_from_slice::<ShareSeedRequest, _>(
                        &seed_share_request_buf,
                        bincode_config,
                    )
                    .context("decode_from_slice seed_share_request_buf")?;
                    if size != seed_share_request_buf.len() {
                        return Err(anyhow!(
                            "seed_share_request decode_from_slice incomplete, {} of {}",
                            size,
                            seed_share_request_buf.len()
                        ));
                    }

                    trace!("received: {seed_share_request}");

                    if seed_share_request.encryption_info.hash_function_name != "blake2s256" {
                        return Err(anyhow!(
                            "unknown hash_function {}",
                            &seed_share_request.encryption_info.hash_function_name
                        ));
                    }
                    if seed_share_request.interval.not_before_dt_stamp
                        > seed_share_request.interval.not_after_dt_stamp
                    {
                        return Err(anyhow!(
                            "inconsistent interval {}",
                            seed_share_request.interval
                        ));
                    };

                    let sig_verifier = sig_verifier_by_meter_pkh
                        .get(&seed_share_request.meter_pkh)
                        .with_context(|| {
                            format!(
                                "no signature verifier for meter {}",
                                &seed_share_request.meter_pkh
                            )
                        })?;

                    trace!("{sig_verifier}");

                    if sig_verifier
                        .verify_signature(&seed_share_request_buf, &signature)
                        .context("verify_signature")?
                    {
                        Ok(seed_share_request)
                    } else {
                        Err("verify_signature failed".to_string())
                    }
                }
            }
        };

        let share_seed_reply = ShareSeedReply {
            message: match share_seed_request_res {
                Err(e) => Some(format!("{e:#} in shared_seed_request_res")),
                Ok(share_seed_request) => {
                    trace!("received {share_seed_request}");
                    match share_seed_sender
                        .send(KeyAdderQueuedMessage::ShareSeedRequest(share_seed_request))
                    {
                        Err(e) => Some(format!(" {e:?} at SyncSender send(share_seed_request)")),
                        Ok(()) => None,
                    }
                }
            },
        };

        if let Some(ref mes) = share_seed_reply.message {
            warn!("shared_seed_reply.message {mes}");
        }

        send_signed_message(&share_seed_reply, key_adder_signer, seed_share_stream)?;
    }
}
