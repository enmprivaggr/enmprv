use std::fmt::{self, Display};

use chrono::{DateTime, Utc};

use crate::{bincoded::P1MeasurementType, measuredvalue::MeasuredValue, util::dt_utc_to_string};

pub type MeasurementTime = DateTime<Utc>;

#[derive(Debug, Clone)]
pub struct P1Measurement {
    pub meter_nr: String,                  // meter serial number
    pub dt_utc: MeasurementTime,           // in millisecond resolution
    pub p1_msrmnt_type: P1MeasurementType, // implied in P1 telegram.
    pub value: MeasuredValue,
}

impl Display for P1Measurement {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Measurement(meter {}, at {}, {} {:?})",
            self.meter_nr,
            dt_utc_to_string(&self.dt_utc),
            self.value.to_string(5),
            self.p1_msrmnt_type,
        )
    }
}
