//! The number type used for possibly encrypted measured values.
//!
//! These values are read from and written to strings with the usual decimal digits.
//!
//! Another typical use for measured values is encryption by adding a large number as encryption key.
//! These large encrypted values are summed into an encrypted sum.
//! Decryption is done by subtracting the key sum from the encrypted sum.

use std::{
    cmp::{Ord, Ordering},
    convert::From,
    fmt,
    iter::Sum,
    ops::{Add, AddAssign, Div, Mul, Neg},
    str::FromStr,
};

use anyhow::{Error, Result};
use custom_derive;
use num::bigint::Sign;
use num::{BigInt, Zero};
use serde::de::{SeqAccess, Visitor};
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use sqlx::types::BigDecimal;

custom_derive! {
    #[derive(NewtypeAdd, NewtypeSub, NewtypeAddAssign, NewtypeSubAssign, Debug, Clone, )]
    pub struct MeasuredValue(BigDecimal);
}

impl MeasuredValue {
    /// A [`MeasuredValue`] with the value of the given [`BigInt`]
    /// times 10 to the power `-ten_pow_divisor`.
    pub fn from_bi_minus_ten_pow(bi: BigInt, ten_pow_divisor: i32) -> Self {
        MeasuredValue(BigDecimal::new(bi, i64::from(ten_pow_divisor)))
    }

    pub fn abs(&self) -> Self {
        MeasuredValue(self.0.abs())
    }

    pub fn to_string(&self, decimal_digits: u32) -> String {
        bd_to_string(&self.0, decimal_digits)
    }

    pub fn to_big_decimal(&self) -> BigDecimal {
        self.0.clone()
    }
}

impl TryFrom<&str> for MeasuredValue {
    type Error = anyhow::Error;
    fn try_from(s: &str) -> Result<MeasuredValue> {
        match bd_from_str(s) {
            Err(e) => Err(e),
            Ok(bd) => Ok(MeasuredValue(bd)),
        }
    }
}

impl TryFrom<&String> for MeasuredValue {
    type Error = anyhow::Error;
    fn try_from(s: &String) -> Result<MeasuredValue> {
        Self::try_from(s.as_str())
    }
}

impl Zero for MeasuredValue {
    fn zero() -> Self {
        MeasuredValue(BigDecimal::zero())
    }

    fn is_zero(&self) -> bool {
        self.0.is_zero()
    }
}

impl Neg for MeasuredValue {
    type Output = Self;
    fn neg(self) -> Self {
        MeasuredValue(self.0.neg())
    }
}

impl Add<MeasuredValue> for &MeasuredValue {
    type Output = MeasuredValue;
    fn add(self, rhs: MeasuredValue) -> MeasuredValue {
        MeasuredValue(self.0.clone() + rhs.0)
    }
}

impl AddAssign<BigDecimal> for MeasuredValue {
    fn add_assign(&mut self, rhs: BigDecimal) {
        self.0 += rhs;
    }
}

impl Mul<i32> for &MeasuredValue {
    type Output = MeasuredValue;
    fn mul(self, rhs: i32) -> MeasuredValue {
        MeasuredValue(self.0.clone() * BigDecimal::from(BigInt::from(rhs)))
    }
}

impl Mul<BigDecimal> for MeasuredValue {
    type Output = MeasuredValue;
    fn mul(self, rhs: BigDecimal) -> MeasuredValue {
        MeasuredValue(self.0.clone() * rhs)
    }
}

impl Div<i32> for &MeasuredValue {
    type Output = MeasuredValue;
    fn div(self, rhs: i32) -> MeasuredValue {
        MeasuredValue(self.0.clone() / rhs)
    }
}

/// Taking the sum of many large encrypted values is a performance critical operation.
impl<'a> Sum<&'a MeasuredValue> for MeasuredValue {
    fn sum<I: Iterator<Item = &'a MeasuredValue>>(iter: I) -> Self {
        MeasuredValue(iter.map(|e| &e.0).sum())
    }
}

impl PartialEq for MeasuredValue {
    fn eq(&self, rhs: &MeasuredValue) -> bool {
        self.0.eq(&rhs.0)
    }
}

impl PartialOrd for MeasuredValue {
    fn partial_cmp(&self, rhs: &MeasuredValue) -> std::option::Option<std::cmp::Ordering> {
        Some(self.cmp(rhs))
    }
}

impl Eq for MeasuredValue {}

impl Ord for MeasuredValue {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}

/// Provide optional sign, whole number digits, optional dot,
/// and round to given number of decimal digits.
#[must_use]
fn bd_to_string(bd: &BigDecimal, decimal_digits: u32) -> String {
    bd.round(decimal_digits.into()).to_string()
}

/// Takes sign, digits, and optional (dot, digits)
fn bd_from_str(input_str: &str) -> Result<BigDecimal> {
    BigDecimal::from_str(input_str).map_err(Error::from)
}

impl Serialize for MeasuredValue {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("MeasuredValue", 3)?; // scale e, sign s, mantissa m
        let (bi, scale) = self.0.as_bigint_and_exponent();
        state.serialize_field("e", &scale)?;
        let (sign, bytes) = bi.to_bytes_be();
        let sign_byte: i8 = match sign {
            Sign::Minus => -1,
            Sign::NoSign => 0,
            Sign::Plus => 1,
        };
        state.serialize_field("s", &sign_byte)?;
        state.serialize_field("m", &bytes)?;
        state.end()
    }
}

struct MeasuredValueVisitor;

impl<'de> Deserialize<'de> for MeasuredValue {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_struct("MeasuredValue", &["e", "s", "m"], MeasuredValueVisitor)
    }
}

impl<'de> Visitor<'de> for MeasuredValueVisitor {
    type Value = MeasuredValue;
    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a MeasuredValue")
    }

    fn visit_seq<V>(self, mut seq: V) -> Result<MeasuredValue, V::Error>
    where
        V: SeqAccess<'de>,
    {
        use serde::de;
        let scale: i64 = seq
            .next_element()?
            .ok_or_else(|| de::Error::invalid_length(0, &"e"))?;
        let sign_byte: i8 = seq
            .next_element()?
            .ok_or_else(|| de::Error::invalid_length(1, &"s"))?;
        let sign = match sign_byte {
            -1 => Sign::Minus,
            0 => Sign::NoSign,
            1 => Sign::Plus,
            _ => {
                return Err(de::Error::invalid_value(
                    de::Unexpected::Signed(sign_byte as i64),
                    &"-1, 0, 1",
                ));
            }
        };
        let bytes: Vec<u8> = seq
            .next_element()?
            .ok_or_else(|| de::Error::invalid_length(2, &"m"))?;
        let bi = BigInt::from_bytes_be(sign, &bytes);
        let bd = BigDecimal::new(bi, scale);
        Ok(MeasuredValue(bd))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use bincode::serde::{decode_from_slice, encode_to_vec};
    use num::{BigInt, One, pow::Pow};

    /// Round to 10 ** pow_ten
    fn round_tenpow(bd: &BigDecimal, pow_ten: i32) -> BigDecimal {
        bd.round(-pow_ten as i64)
    }

    #[test]
    fn from_str_err() {
        assert!(bd_from_str("").is_err());
        assert!(bd_from_str("+").is_err());
        assert!(bd_from_str("-").is_err());
        assert!(bd_from_str("a").is_err());
        assert!(bd_from_str("5d.0").is_err());
        assert!(bd_from_str("0.b").is_err());
        assert!(bd_from_str(".c").is_err());
    }

    #[test]
    fn zero_to_string() {
        let z = &BigDecimal::zero();
        assert_eq!(bd_to_string(z, 0), "0");
        // assert_eq!(bd_to_string(z, 1), "0.0");
        // assert_eq!(bd_to_string(z, 2), "0.00");
    }

    #[test]
    fn one_to_string() {
        let r = &BigDecimal::one();
        assert_eq!(bd_to_string(r, 0), "1");
        // assert_eq!(bd_to_string(r, 1), "1.0");
        // assert_eq!(bd_to_string(r, 2), "1.00");
        let r = &-BigDecimal::one();
        assert_eq!(bd_to_string(r, 0), "-1");
        // assert_eq!(bd_to_string(r, 1), "-1.0");
        // assert_eq!(bd_to_string(r, 2), "-1.00");
    }

    fn test_ok(parse_res: Result<BigDecimal>, exp_value: &BigDecimal) {
        match parse_res {
            Ok(bd) => {
                assert_eq!(&bd, exp_value);
            }
            Err(e) => {
                panic!("{e:?} for {exp_value:?}");
            }
        }
    }

    #[test]
    fn from_str_zero() {
        let z = &BigDecimal::zero();
        test_ok(bd_from_str("0"), z);
        test_ok(bd_from_str("0.0"), z);
        test_ok(bd_from_str("0.00"), z);
        test_ok(bd_from_str("+0"), z);
        test_ok(bd_from_str("+0.0"), z);
        test_ok(bd_from_str("+0.00"), z);
        test_ok(bd_from_str("-0"), z);
        test_ok(bd_from_str("-0.0"), z);
        test_ok(bd_from_str("-0.00"), z);
    }

    #[test]
    fn whole_from_string() {
        for i in 0..101 {
            test_ok(
                bd_from_str(&format!("{}", i)),
                &BigDecimal::from(BigInt::from(i)),
            );
            test_ok(
                bd_from_str(&format!("{}", -i)),
                &(BigDecimal::from(BigInt::from(-i))),
            );
        }
    }

    #[test]
    fn test_to_string() {
        for pow in [1i32, 2, 3, 4] {
            for i in 1..101 {
                let ip = &BigDecimal::new(BigInt::from(i), -(pow as i64));
                assert_eq!(
                    bd_to_string(ip, 4).parse::<f32>().unwrap(),
                    (i as f32) * 10f32.pow(pow),
                );
                let imp = &BigDecimal::new(BigInt::from(i), pow as i64);
                assert_eq!(
                    bd_to_string(imp, 4).parse::<f32>().unwrap(),
                    ((i as f32) * 10f32.pow(-pow) * 10000f32).round() / 10000f32,
                );
            }
        }
    }

    #[test]
    fn test_from_str() {
        for pow in [1i32, 2, 3, 4] {
            for i in 1..101 {
                let ip = &BigDecimal::new(BigInt::from(i), -(pow as i64));
                let ip_str = format!("{}", (i as f32) * 10f32.pow(pow));
                test_ok(bd_from_str(&ip_str), ip);
                let imp = &BigDecimal::new(BigInt::from(i), pow as i64);
                let imp_str = format!("{}", (i as f32) / 10f32.pow(pow));
                test_ok(bd_from_str(&imp_str), imp);
            }
        }
    }

    fn bfs(s: &str) -> BigDecimal {
        bd_from_str(s).unwrap()
    }

    #[test]
    fn test_from_str2() {
        assert!(bfs("0.1") < bfs("0.11"));
        assert!(bfs("+0.1") <= bfs("0.11"));
        assert!(bfs("0.11") > bfs("0.1"));
        assert!(bfs("0.11") >= bfs("0.1"));
        assert!(bfs("-0.1") > bfs("-0.11"));
        assert!(bfs("-0.1") >= bfs("-0.11"));
        assert!(bfs("-0.11") < bfs("-0.1"));
        assert!(bfs("-0.11") <= bfs("-0.1"));
    }
    #[test]
    fn test_from_str_corner_cases() {
        assert_eq!(bfs("+.0"), BigDecimal::zero());
        assert_eq!(bfs("-0."), BigDecimal::zero());
        assert_eq!(bfs(".9"), bfs("9") / bfs("10"));
        assert_eq!(bfs("9."), bfs("9"));
    }

    #[test]
    fn test_sum() {
        let bd_ar: [BigDecimal; 4] = [bfs("40"), bfs("+3.00"), bfs("-0.01"), bfs("0.29")];
        let s1 = Ok(bd_ar.iter().sum());
        test_ok(s1, &bfs("43.28"));
    }

    #[test]
    fn test_round_tenpow() {
        assert_eq!(round_tenpow(&(bfs("1") / bfs("3")), -4), bfs("0.3333"));
        assert_eq!(round_tenpow(&(bfs("2") / bfs("3")), -4), bfs("0.6667"));
    }

    #[test]
    fn test_from_to_string_big_small() {
        for pow in 1u32..100 {
            for i in 1..4 {
                let ip = &BigDecimal::new(BigInt::from(i), -(pow as i64));
                let ip_str = bd_to_string(ip, 1);
                test_ok(bd_from_str(&ip_str), ip);

                let mut ip_str_no_exp: String = String::from(char::from_digit(i, 10).unwrap());
                for _ in 0..pow {
                    ip_str_no_exp.push('0');
                }
                test_ok(bd_from_str(&ip_str_no_exp), ip);

                let imp = &BigDecimal::new(BigInt::from(i), pow as i64);
                let imp_str = bd_to_string(imp, pow + 1);
                test_ok(bd_from_str(&imp_str), imp);

                let mut imp_str_no_exp: String = String::from("0.");
                for _ in 0..pow - 1 {
                    imp_str_no_exp.push('0');
                }
                imp_str_no_exp.push(char::from_digit(i, 10).unwrap());
                test_ok(bd_from_str(&imp_str_no_exp), imp);
            }
        }
    }

    fn test_serde_mv(expect_mv: MeasuredValue) {
        let bincode_config = crate::bincoded::get_bincode_config();
        let mv_bytes =
            encode_to_vec::<MeasuredValue, _>(expect_mv.clone(), bincode_config).unwrap();
        // dbg!(expect_mv, &mv_bytes.len()); 3 digits -> 10 bits, plus 3 bytes.
        let (actual_mv, size) =
            decode_from_slice::<MeasuredValue, _>(&mv_bytes, bincode_config).unwrap();
        assert_eq!(size, mv_bytes.len());
        assert_eq!(actual_mv, expect_mv);
    }

    #[test]
    fn test_serde_measured_values() {
        use num::FromPrimitive;
        for expect_mv in [0, 1, -1]
            .map(|i| MeasuredValue::from_bi_minus_ten_pow(BigInt::from_i64(i).unwrap(), 0))
        {
            test_serde_mv(expect_mv);
        }

        let mv_one = || MeasuredValue::from_bi_minus_ten_pow(BigInt::from_i64(1).unwrap(), 0);

        for ten_pow in 1..101 {
            let mv_tp =
                || MeasuredValue::from_bi_minus_ten_pow(BigInt::from_i64(1).unwrap(), -ten_pow);
            test_serde_mv(mv_tp() - mv_one());
            test_serde_mv(mv_tp());
            test_serde_mv(mv_tp() + mv_one());
            test_serde_mv(-mv_tp() - mv_one());
            test_serde_mv(-mv_tp());
            test_serde_mv(-mv_tp() + mv_one());
        }
    }
}
