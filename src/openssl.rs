use std::{
    fmt::{self, Display},
    fs::{File, create_dir_all, remove_file},
    io::{Read, Write},
    iter::repeat_with,
    path::{self, MAIN_SEPARATOR, Path},
    process, thread,
};

use anyhow::{Error, Result, anyhow};
use base64::{display::Base64Display, engine::general_purpose::STANDARD as BASE64_STANDARD};
use blake2::{Blake2s256, Digest};
use rand::random;
use serde::{Deserialize, Serialize};

use crate::util::bash_cmd_output;

fn thread_id_string() -> String {
    let thread_id_dbg = format!("{:?}", thread::current().id());
    if let Some(open_pos) = thread_id_dbg.find('(') {
        if let Some(close_pos) = thread_id_dbg[open_pos + 2..].find(')') {
            return thread_id_dbg[open_pos + 1..open_pos + 2 + close_pos].to_string();
        }
    }
    thread_id_dbg
}

fn expect_empty(stream_name: &str, mes: &[u8]) -> Result<()> {
    if mes.is_empty() {
        Ok(())
    } else if let Ok(s) = std::str::from_utf8(mes) {
        Err(anyhow!("{stream_name} non empty: {s}"))
    } else {
        Err(anyhow!("{stream_name} non utf8: {mes:?}"))
    }
}

fn rm_file(file_path: &Path) -> Result<()> {
    remove_file(file_path).map_err(Error::new)
}

fn random_data(num_bytes: usize) -> Vec<u8> {
    repeat_with(random).take(num_bytes).collect()
}

#[derive(Deserialize, Debug, Clone)]
pub struct OpenSslSignerFiles {
    pub private_key_file_name: String,
    pub public_key_file_name: String, // verify key pair at initialization
    pub key_pass_phrase: Option<String>,
}

const SIG_FILE_SUFFIX: &str = "sig";
const DATA_FILE_SUFFIX: &str = "data";

fn thread_unique_tmp_file_name(temp_dir_name: &str, suffix: &str) -> String {
    format!(
        "{temp_dir_name}{}{}-{}-{suffix}",
        path::MAIN_SEPARATOR,
        process::id(),
        thread_id_string()
    )
}

pub struct OpenSslSigner {
    private_key_file_name: String,
    public_key_file_name: String,
    key_pass_phrase: Option<String>,
    temp_dir_name: String,
}

impl Display for OpenSslSigner {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.pub_key_hash() {
            Ok(pkh) => write!(f, "OpenSslSigner({pkh} {})", self.public_key_file_name),
            Err(e) => write!(f, "OpenSslSigner({e:?} {})", self.public_key_file_name),
        }
    }
}

const OPENSSL: &str = "openssl";
const SHA_SIZE: u16 = 384; // use secure hash SHA384 for signing
const PUB_KEY_HASH_SIZE: usize = 8; // bytes vec size for hash of public key.

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, Clone)]
pub struct PubKeyHash(pub Vec<u8>);

impl Display for PubKeyHash {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "PubKeyHash({})",
            Base64Display::new(&self.0, &BASE64_STANDARD)
        )
    }
}

fn file_content_hash(file_name: &str) -> Result<PubKeyHash> {
    let mut content = Vec::<u8>::new();
    {
        let mut file = match File::open(file_name) {
            Ok(f) => f,
            Err(e) => return Err(Error::new(e).context("File::open")),
        };
        if let Err(e) = file.read_to_end(&mut content) {
            return Err(Error::new(e).context("read_to_end"));
        }
    }
    let mut hasher: Blake2s256 = Blake2s256::new();
    hasher.update(&content);
    let hash = hasher.finalize();
    Ok(PubKeyHash(if hash.len() >= PUB_KEY_HASH_SIZE {
        hash[..PUB_KEY_HASH_SIZE].to_vec()
    } else {
        hash.to_vec()
    }))
}

impl OpenSslSigner {
    pub fn new(ssl_signer_files: &OpenSslSignerFiles, temp_dir: &str) -> Result<Self> {
        if let Err(e) = create_dir_all(temp_dir) {
            Err(Error::new(e).context(format!("create_dir_all {temp_dir}")))
        } else {
            let priv_key_path = home_path(&ssl_signer_files.private_key_file_name)?;
            let pub_key_path = home_path(&ssl_signer_files.public_key_file_name)?;
            if (!Path::new(&priv_key_path).exists()) || (!Path::new(&pub_key_path).exists()) {
                return Err(anyhow!(
                    "{priv_key_path} and/or {pub_key_path} do not exist"
                ));
            }
            let signer = OpenSslSigner {
                private_key_file_name: priv_key_path.to_owned(),
                public_key_file_name: pub_key_path.to_owned(),
                key_pass_phrase: ssl_signer_files.key_pass_phrase.to_owned(),
                temp_dir_name: temp_dir.to_owned(),
            };
            // verify a test data signature:
            let verifier = OpenSslSigVerifier {
                public_key_file_name: pub_key_path.to_owned(),
                temp_dir_name: temp_dir.to_owned(),
            };
            let test_data = random_data(5);
            let test_signature = signer.generate_signature(&test_data)?;
            match verifier.verify_signature(&test_data, &test_signature) {
                Err(e) => Err(e.context("verify_signature")),
                Ok(false) => Err(anyhow!(
                    "signature verification failed: {priv_key_path} {pub_key_path}"
                )),
                Ok(true) => Ok(signer),
            }
        }
    }

    pub fn generate_signature(&self, data: &[u8]) -> Result<Vec<u8>> {
        let data_file_name = thread_unique_tmp_file_name(&self.temp_dir_name, DATA_FILE_SUFFIX);
        {
            let mut data_file = match File::create(&data_file_name) {
                Ok(f) => f,
                Err(e) => return Err(anyhow!("{e:?} at File::create {data_file_name}")),
            };
            if let Err(e) = data_file.write_all(data) {
                return Err(anyhow!("{e:?} at write_all {data_file_name}"));
            }
        }
        let cmd_first_part = format!(
            "{OPENSSL} dgst -sha{SHA_SIZE} -sign {}",
            &self.private_key_file_name,
        );
        let mut cmd_parts: Vec<&str> = vec![&cmd_first_part];
        let pass_phrase_part: String;
        if let Some(ref pass_phrase) = self.key_pass_phrase {
            pass_phrase_part = format!("-passin pass:{pass_phrase}");
            cmd_parts.push(&pass_phrase_part);
        }
        let sig_file_name = thread_unique_tmp_file_name(&self.temp_dir_name, SIG_FILE_SUFFIX);
        let cmd_final_part = format!("-out {sig_file_name} {data_file_name}");
        cmd_parts.push(&cmd_final_part);
        let cmd = cmd_parts.join(" ");

        let output = bash_cmd_output(&cmd)?;
        expect_empty("stdout", &output.stdout)?;
        expect_empty("stderr", &output.stderr)?;
        let mut signature = Vec::<u8>::new();
        {
            let mut sig_file = match File::open(&sig_file_name) {
                Ok(f) => f,
                Err(e) => return Err(anyhow!("{e:?} at File::open {sig_file_name}")),
            };
            if let Err(e) = sig_file.read_to_end(&mut signature) {
                return Err(anyhow!("{e:?} at read_to_end {sig_file_name}"));
            }
        }
        rm_file(Path::new(&sig_file_name))?;
        rm_file(Path::new(&data_file_name))?;
        if output.status.success() {
            Ok(signature)
        } else {
            Err(anyhow!("cmd {cmd} status: {:?}", output.status))
        }
    }

    pub fn pub_key_hash(&self) -> Result<PubKeyHash> {
        file_content_hash(&self.public_key_file_name)
    }
}

#[derive(PartialEq, Debug)]
pub struct OpenSslSigVerifier {
    public_key_file_name: String,
    temp_dir_name: String,
}

impl Display for OpenSslSigVerifier {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.pub_key_hash() {
            Ok(pkh) => write!(f, "OpenSslSigVerifier({pkh} {})", self.public_key_file_name),
            Err(e) => write!(f, "OpenSslSigVerifier({e:?} {})", self.public_key_file_name),
        }
    }
}

impl OpenSslSigVerifier {
    pub fn new(public_key_file_name: &str, temp_dir_name: &str) -> Result<Self> {
        let pub_key_path = home_path(public_key_file_name)?;
        if Path::new(&pub_key_path).exists() {
            // ignore regular file and readability modes
            Ok(OpenSslSigVerifier {
                public_key_file_name: pub_key_path,
                temp_dir_name: temp_dir_name.to_string(),
            })
        } else {
            Err(anyhow!("{pub_key_path} does not exist"))
        }
    }

    pub fn verify_signature(&self, data: &[u8], signature: &[u8]) -> Result<bool> {
        let sig_file_name = thread_unique_tmp_file_name(&self.temp_dir_name, SIG_FILE_SUFFIX);
        {
            let mut sig_file = match File::create(&sig_file_name) {
                Ok(f) => f,
                Err(e) => return Err(Error::new(e)),
            };
            if let Err(e) = sig_file.write_all(signature) {
                return Err(Error::new(e));
            }
        }
        let data_file_name = thread_unique_tmp_file_name(&self.temp_dir_name, DATA_FILE_SUFFIX);
        {
            let mut data_file = match File::create(&data_file_name) {
                Ok(f) => f,
                Err(e) => return Err(anyhow!("{e:?} at File::create {data_file_name}")),
            };
            if let Err(e) = data_file.write_all(data) {
                return Err(anyhow!("{e:?} at write_all {data_file_name}"));
            }
        }
        let cmd = format!(
            "{OPENSSL} dgst -sha{SHA_SIZE} -verify {} -signature {sig_file_name} {data_file_name}",
            &self.public_key_file_name,
        );
        let output = bash_cmd_output(&cmd)?;
        rm_file(Path::new(&sig_file_name))?;
        rm_file(Path::new(&data_file_name))?;
        match (output.stdout.as_slice(), output.status.code()) {
            (b"Verified OK\n", Some(0)) => Ok(true),
            (b"Verification failure\n", Some(1)) => Ok(false), // opensll on x86
            (b"Verification Failure\n", Some(1)) => Ok(false), // openssl on arm
            _ => {
                if output.stderr.as_slice() == b"Error Verifying Data\n" {
                    // e.g. too long signature on arm, an input format problem.
                    Ok(false)
                } else {
                    Err(anyhow!(
                        "{cmd} status {:?} code {:?} stdout {:?} stderr {:?}",
                        output.status,
                        output.status.code(),
                        String::from_utf8(output.stdout),
                        String::from_utf8(output.stderr),
                    ))
                }
            }
        }
    }

    pub fn pub_key_hash(&self) -> Result<PubKeyHash> {
        file_content_hash(&self.public_key_file_name)
    }
}

fn home_path(path: &str) -> Result<String> {
    if path.starts_with('/') {
        Ok(path.to_string())
    } else if let Ok(home) = std::env::var("HOME") {
        Ok(format!("{home}{}{path}", MAIN_SEPARATOR))
    } else {
        Err(anyhow!("HOME not available for {path}"))
    }
}

#[cfg(test)]
mod tests {
    use super::{
        OPENSSL, OpenSslSigVerifier, OpenSslSigner,
        path::{Path, PathBuf},
        random_data,
    };
    use crate::{openssl::OpenSslSignerFiles, util::bash_cmd_stdout};
    use std::fs::{create_dir_all, remove_dir_all};

    const TEST_TEMP_DIR: &str = "test-enmprv/openssl"; // in home directory
    /// Create directory for test data as a subdir of TEST_TEMP_DIR in home.
    fn init_test_dir(sub_dir: &str) -> PathBuf {
        let home = std::env::var("HOME").unwrap();
        let test_dir_path = Path::new(&home).join(TEST_TEMP_DIR).join(sub_dir);
        if !test_dir_path.is_dir() {
            if let Err(e) = create_dir_all(&test_dir_path) {
                panic!("create_dir_all {e:?} from {test_dir_path:?}");
            }
        };
        test_dir_path
    }

    fn do_cmd(cmd: &str) {
        match bash_cmd_stdout(cmd) {
            Ok(stdout) => {
                if !stdout.is_empty() {
                    dbg!(stdout);
                }
            }
            Err(e) => {
                panic!("{e:?} for {cmd}");
            }
        }
    }

    fn test_on_random_data(
        test_dir_path: &PathBuf,
        private_key_file_name: &str,
        public_key_file_name: &str,
        allow_repeated_signature: bool,
    ) {
        // generate random data
        let data_size = 32;
        let mut test_data = random_data(data_size);

        let test_dir_name = &test_dir_path.to_string_lossy();
        // sign the data with the private key
        let signer = OpenSslSigner::new(
            &OpenSslSignerFiles {
                private_key_file_name: private_key_file_name.to_owned(),
                public_key_file_name: public_key_file_name.to_owned(),
                key_pass_phrase: None,
            },
            test_dir_name,
        )
        .unwrap();
        let signature = signer.generate_signature(&test_data).unwrap();

        // verify the signature
        let sig_verifier = OpenSslSigVerifier::new(public_key_file_name, test_dir_name).unwrap();
        let ok = sig_verifier
            .verify_signature(&test_data, &signature)
            .unwrap();
        assert!(ok);

        let signature2 = signer.generate_signature(&test_data).unwrap();
        if allow_repeated_signature {
            assert_eq!(signature, signature2); // need nonce on signed data
        } else {
            // this fails when using rsa keys:
            assert_ne!(signature, signature2); // minimum condition to not need nonce on signed data.
        }

        test_data[0] = !test_data[0]; // bit wise complement first byte
        let not_ok = sig_verifier
            .verify_signature(&test_data, &signature)
            .unwrap();
        assert!(!not_ok);

        if let Err(e) = remove_dir_all(test_dir_path) {
            panic!("{e:?}");
        }
    }

    #[test]
    fn test_rsa_signature_1() {
        let test_dir_path = init_test_dir("test_rsa_signature_1");
        let rsa_key_bit_size = 3072u16;

        // generate private rsa key without passphrase
        let private_key_file_path = test_dir_path.join(format!("private{rsa_key_bit_size}.pem"));
        let private_key_file_name = private_key_file_path.to_string_lossy();
        do_cmd(&format!(
            "{OPENSSL} genrsa -out {private_key_file_name} {rsa_key_bit_size}"
        ));

        // generate the public rsa key
        let public_key_file_path = test_dir_path.join(format!("public{rsa_key_bit_size}.pem"));
        let public_key_file_name = public_key_file_path.to_string_lossy();
        do_cmd(&format!(
            "{OPENSSL} rsa -in {private_key_file_name} -pubout -out {public_key_file_name}"
        ));

        test_on_random_data(
            &test_dir_path,
            &private_key_file_name,
            &public_key_file_name,
            true,
        );
    }

    #[test]
    fn test_ec_signature_1() {
        let test_dir_path = init_test_dir("test_ec_signature_1");

        let bit_size = 384u16; // for both curve and sha hash.
        let elliptic_curve_name = "brainpoolP384r1"; // or brainpoolP512r1

        // generate private ec key without passphrase, use default PEM output format
        let private_key_file_path = test_dir_path.join(format!("private{bit_size}.pem"));
        let private_key_file_name = private_key_file_path.to_string_lossy();
        do_cmd(&format!(
            "{OPENSSL} ecparam -name {elliptic_curve_name} -genkey -out {private_key_file_name}"
        ));

        // generate the public key
        let public_key_file_path = test_dir_path.join(format!("public{bit_size}.pem"));
        let public_key_file_name = public_key_file_path.to_string_lossy();
        do_cmd(&format!(
            "{OPENSSL} ec -in {private_key_file_name} -pubout -out {public_key_file_name}"
        ));

        test_on_random_data(
            &test_dir_path,
            &private_key_file_name,
            &public_key_file_name,
            false,
        );
    }
}
