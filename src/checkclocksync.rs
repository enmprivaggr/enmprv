use std::{
    sync::Mutex,
    thread,
    time::{Duration, Instant},
};

use anyhow::{Result, anyhow};
use log::{trace, warn};

use crate::util::bash_cmd_stdout;

pub struct RepeatCheckClockSync {
    wait_for_sync: Duration,
    repeat_interval: Duration,
    last_sync_check: Mutex<Instant>,
}

impl RepeatCheckClockSync {
    pub fn new(wait_for_sync: Duration, repeat_interval: Duration) -> Result<Self> {
        let now = Instant::now();
        let res = RepeatCheckClockSync {
            wait_for_sync,
            repeat_interval,
            last_sync_check: Mutex::new(now),
        };
        res.wait_clock_synchronized(&now)?;
        Ok(res)
    }

    fn wait_clock_synchronized(&self, now: &Instant) -> Result<bool> {
        // return Ok(false) when no waiting is needed, Ok(true) when sync in given time.
        let start_time = *now;
        let prop = "NTPSynchronized";
        let cmd = &format!("timedatectl show --property={prop}");
        let expect_out = &format!("{prop}=yes");
        let mut waited = false;
        loop {
            let stdout = bash_cmd_stdout(cmd)?;
            if stdout.contains(expect_out) {
                trace!("{expect_out}");
                return match self.last_sync_check.lock() {
                    Err(e) => Err(anyhow!("{e:?} at set last_sync_check")),
                    Ok(mut last_sync_check_ref) => {
                        *last_sync_check_ref = Instant::now();
                        Ok(waited)
                    }
                };
            }
            warn!("{}: waiting for clock synchronization", stdout.trim()); // remove end of line
            thread::sleep(Duration::new(10, 0));
            waited = true;
            if let Some(wait_duration) = Instant::now().checked_duration_since(start_time) {
                if wait_duration > self.wait_for_sync {
                    return Err(anyhow!(
                        "clock not synchronized after {:?}",
                        self.wait_for_sync
                    ));
                }
            } else {
                thread::sleep(Duration::new(10, 0)); // hope for recovery
                return Err(anyhow!("non monotic clock")); // prefer better times.
            }
        }
    }

    pub fn recheck_wait(&self) -> Result<bool> {
        // return Ok(false) when clock still in sync, or no sync check needed.
        // return Ok(true) when waited for sync within repeat_interval.
        // Avoid the synchronised system clock here, use Instant
        let last_sync_check = match self.last_sync_check.lock() {
            Ok(last_sync_check_ref) => *last_sync_check_ref,
            Err(e) => return Err(anyhow!("{e:?} at get last_sync_check")),
        };

        let now = Instant::now();
        if let Some(since_last_sync_check) = now.checked_duration_since(last_sync_check) {
            Ok(if since_last_sync_check < self.repeat_interval {
                // assume clock still in sync, no waiting needed
                false
            } else {
                self.wait_clock_synchronized(&now)?
            })
        } else {
            thread::sleep(Duration::new(10, 0)); // hope for recovery
            Err(anyhow!("non monotic clock")) // prefer better times.
        }
    }
}
