use std::{
    collections::{BTreeSet, HashMap},
    fmt::Display,
    io::{Read, Write},
    net::{IpAddr, SocketAddr, TcpStream, UdpSocket},
    process::{Command, Output},
    rc::Rc,
    str::FromStr,
    sync::{
        Arc,
        atomic::{AtomicBool, Ordering},
    },
    thread,
    time::{Duration, SystemTime},
};

use anyhow::{Context, Error, Result, anyhow};
use bincode::serde::encode_to_vec;
use blake2::{Blake2s256, Digest};
use chrono::{SecondsFormat, Timelike};
use ctrlc;
use log::{error, trace, warn};
use serde::{Deserialize, Serialize};
use serde_json;
use sqlx::types::chrono::{DateTime, NaiveDateTime, Utc};

use crate::{
    bincoded::get_bincode_config,
    openssl::{OpenSslSigVerifier, OpenSslSigner, PubKeyHash},
};

/// Let bash execute the given `cmd` and provide the result as [`Result<Output>`]
pub fn bash_cmd_output(cmd: &str) -> Result<Output> {
    match Command::new("/bin/bash").arg("-c").arg(cmd).output() {
        Err(e) => Err(anyhow!("{cmd} {e:?}")),
        Ok(output) => Ok(output),
    }
}

/// Let bash execute the given `cmd` and provide the standard output as [`Result<String>`]
/// when the error output is empty and cmd exited normally.
pub fn bash_cmd_stdout(cmd: &str) -> Result<String> {
    let result = bash_cmd_output(cmd)?;
    if !result.status.success() {
        Err(anyhow!(
            "cmd {cmd} result.status.success() is false {result:?}"
        ))
    } else if let Ok(s) = std::str::from_utf8(&result.stdout) {
        Ok(s.to_string())
    } else {
        Err(anyhow!(
            "from_utf8 failed on result.stdout, cmd {cmd} result {result:?}"
        ))
    }
}

#[derive(Deserialize, Debug)]
pub struct IpAddressPortConfig {
    pub address: String,
    pub port: u16,
}

impl IpAddressPortConfig {
    pub fn addr_port_str(&self) -> Result<String> {
        match IpAddr::from_str(&self.address) {
            Err(e) => Err(Error::new(e).context(self.address.to_owned())),
            Ok(IpAddr::V4(_)) => Ok(format!("{}:{}", self.address, self.port)),
            Ok(IpAddr::V6(_)) => {
                // See RFC2732, example [2001:db8:4006:812::200e]:8080
                Ok(format!("[{}]:{}", self.address, self.port))
            }
        }
    }

    pub fn socket_address(&self) -> Result<SocketAddr> {
        match IpAddr::from_str(&self.address) {
            Err(e) => Err(Error::new(e).context(self.address.to_owned())),
            Ok(IpAddr::V4(addr_v4)) => Ok(SocketAddr::new(IpAddr::V4(addr_v4), self.port)),
            Ok(IpAddr::V6(addr_v6)) => Ok(SocketAddr::new(IpAddr::V6(addr_v6), self.port)),
        }
    }
}

pub fn udp_bind(local_addr_port: &str) -> Result<UdpSocket> {
    UdpSocket::bind(local_addr_port).with_context(|| format!("bind for {local_addr_port}"))
}

fn same_prefix_bits<const N: usize>(a1: [u8; N], a2: [u8; N], num_bits: u64) -> bool {
    // check octets of ip addresses for equality of prefix bits.
    let mut unchecked_bits = num_bits;
    if num_bits == 0 {
        true
    } else if num_bits as usize > N * (u8::BITS as usize) {
        false
    } else {
        !a1.into_iter()
            .zip(a2)
            .take(((num_bits as usize - 1) / (u8::BITS as usize)) + 1)
            .any(|(b1, b2)| {
                // look for first unequal byte pair, check fewer bits when unchecked_bits < 8
                if unchecked_bits >= 8 {
                    if b1 == b2 {
                        unchecked_bits -= 8;
                        false
                    } else {
                        true
                    }
                } else {
                    (b1 ^ b2) >> (8 - unchecked_bits) != 0
                }
            })
    }
}

pub fn ip_addr_in_wg_local_intf(ip_addr_str: &str) -> Result<bool> {
    let ip_addr = match IpAddr::from_str(ip_addr_str) {
        Err(e) => return Err(anyhow!("{e:?} for input address {ip_addr_str}")),
        Ok(ia) => ia,
    };
    let cmd = "ip -json -details addr show"; // no need for --pretty
    let stdout = bash_cmd_stdout(cmd)?;
    // Partial output of:
    // ip --json --pretty -d addr show

    // [
    // ...
    //  {
    // ...
    //     "ifname": "wgenmprv0",
    // ...
    //     "linkinfo": {
    //         "info_kind": "wireguard"
    //     },
    // ...
    //     "addr_info": [ {
    //             "family": "inet",
    //             "local": "10.1.0.1",
    //             "prefixlen": 32,
    // ...
    //         },{
    //             "family": "inet6",
    //             "local": "fde2:8b02:e204::a01:1",
    //             "prefixlen": 128,
    // ...
    //         } ]
    // } ]
    type JsonValue = serde_json::Value;
    let ip_json: JsonValue = match serde_json::from_str(&stdout) {
        Ok(json) => json,
        Err(e) => {
            return Err(anyhow!(
                "{e:?} for serde_json::from_str for output of {cmd}"
            ));
        }
    };
    let JsonValue::Array(intfs_array) = ip_json else {
        return Err(anyhow!("expected json array, got {ip_json:?}"));
    };
    for intf_value in &intfs_array {
        let JsonValue::Object(intf_object) = intf_value else {
            return Err(anyhow!(
                "expected json object, got {intf_value:?} for {intfs_array:?}"
            ));
        };
        // if let Some(JsonValue::String(if_name)) = intf_object.get("ifname") {
        //     dbg!(if_name);
        // }
        let Some(JsonValue::Object(link_info_object)) = intf_object.get("linkinfo") else {
            continue;
        };
        if Some(&JsonValue::String("wireguard".to_string())) != link_info_object.get("info_kind") {
            continue;
        }
        let Some(JsonValue::Array(addr_infos_array)) = intf_object.get("addr_info") else {
            warn!("no addr_info on wireguard interface {intf_object:?}");
            continue;
        };
        for addr_info_value in addr_infos_array {
            let JsonValue::Object(addr_info_object) = addr_info_value else {
                return Err(anyhow!(
                    "expected json Object, got {addr_info_value:?} in {addr_infos_array:?}"
                ));
            };
            let Some(JsonValue::String(family)) = addr_info_object.get("family") else {
                warn!("no address family for {addr_info_object:?} in {intf_object:?}");
                continue;
            };
            let Some(JsonValue::String(local_addr_string)) = addr_info_object.get("local") else {
                warn!("no local address for {addr_info_object:?} in {intf_object:?}");
                continue;
            };
            let local_addr = match IpAddr::from_str(local_addr_string) {
                Err(e) => {
                    return Err(anyhow!(
                        "{e:?} for {local_addr_string:?} in {addr_info_object:?}"
                    ));
                }
                Ok(ip_addr) => ip_addr,
            };
            let prefix_len_key = "prefixlen";
            let Some(JsonValue::Number(prefix_len_number)) = addr_info_object.get(prefix_len_key)
            else {
                return Err(anyhow!(
                    "no Number for {prefix_len_key} in {addr_info_object:?}"
                ));
            };
            let Some(prefix_len) = prefix_len_number.as_u64() else {
                return Err(anyhow!(
                    "{prefix_len_number:?} not u64 for {prefix_len_key} in {addr_info_object:?}"
                ));
            };
            match (family.as_str(), ip_addr, local_addr) {
                ("inet", IpAddr::V4(given_v4), IpAddr::V4(addr_v4))
                    if same_prefix_bits(given_v4.octets(), addr_v4.octets(), prefix_len) =>
                {
                    return Ok(true);
                }
                ("inet6", IpAddr::V6(given_v6), IpAddr::V6(addr_v6))
                    if same_prefix_bits(given_v6.octets(), addr_v6.octets(), prefix_len) =>
                {
                    return Ok(true);
                }
                ("inet" | "inet6", _, _) => {} // ipv4/ipv6 or ip6/ipv4
                _ => {
                    trace!(
                        "ignoring family {family:?}, local_address {local_addr:?}, and {prefix_len_key} {prefix_len:?} for ip_addr {ip_addr:?}"
                    );
                }
            }
        }
    }
    Ok(false)
}

pub fn tcp_stream_in_wg_intf(socket_addr: &SocketAddr, time_out_secs: u64) -> Result<TcpStream> {
    let timeout = Duration::new(time_out_secs, 0);
    match TcpStream::connect_timeout(socket_addr, timeout) {
        Err(e) => Err(Error::new(e).context(format!("TcpStream::connect_timeout {socket_addr:?}"))),
        Ok(tcp_stream) => match tcp_stream.local_addr() {
            Err(e) => Err(Error::new(e).context("tcp_stream.local_addr")),
            Ok(local_addr_port) => {
                let local_ip_addr_str = local_addr_port.ip().to_string();
                match ip_addr_in_wg_local_intf(&local_ip_addr_str) {
                    Err(e) => {
                        Err(e.context(format!("ip_addr_in_wg_local_intf {local_ip_addr_str}")))
                    }
                    Ok(res) => {
                        if !res {
                            warn!(
                                "no wireguard ip interface for local address {local_ip_addr_str}"
                            );
                        }
                        Ok(tcp_stream)
                    }
                }
            }
        },
    }
}

pub fn send_version_size_message(message: &[u8], tcp_stream: &mut TcpStream) -> Result<()> {
    let mes_size = message.len();
    if mes_size > u16::MAX.into() {
        Err(anyhow!("message too long: {mes_size}"))
    } else {
        tcp_stream
            .set_nonblocking(false)
            .context("set_nonblocking")?;
        // prefix 2 zero version bytes and 2 length bytes
        let mut version_size: [u8; 4] = [0; 4];
        version_size[2] = mes_size as u8;
        version_size[3] = (mes_size >> 8) as u8;
        tcp_stream
            .write_all(&version_size)
            .context("write_all version_size")?;
        tcp_stream.write_all(message).context("write_all message")?;
        Ok(())
    }
}

pub fn receive_version_size_message(
    tcp_stream: &mut TcpStream,
    read_timeout_seconds: u64,
) -> Result<Vec<u8>> {
    if let Err(e) = tcp_stream.set_nonblocking(false) {
        return Err(Error::new(e).context("set_nonblocking"));
    }
    let receive_timeout = Duration::new(read_timeout_seconds, 0);
    if let Err(e) = tcp_stream.set_read_timeout(Some(receive_timeout)) {
        return Err(Error::new(e).context("set_read_timeout"));
    }
    let mut version_size: [u8; 4] = [0; 4];
    if let Err(e) = tcp_stream.read_exact(&mut version_size) {
        return Err(Error::new(e).context("read_exact version_size"));
    }
    if version_size[0] != 0 || version_size[1] != 0 {
        return Err(anyhow!("unexpected version bytes {:?}", &version_size[..2]));
    }
    let message_size = version_size[2] as usize + ((version_size[3] as usize) << 8);
    trace!("receive_version_size_message size {message_size}");
    let mut message = vec![0; message_size];
    if let Err(e) = tcp_stream.read_exact(&mut message) {
        Err(Error::new(e).context("read_exact"))
    } else {
        Ok(message)
    }
}

pub fn send_signed_message<T>(
    message: &T,
    signer: &OpenSslSigner,
    stream: &mut TcpStream,
) -> Result<(), anyhow::Error>
where
    T: Serialize,
{
    let bincode_config = get_bincode_config();
    let buf = encode_to_vec(message, bincode_config)?;
    send_version_size_message(&buf, stream).context("send_version_size_message")?;
    let sig_buf = signer
        .generate_signature(&buf)
        .context("generate_signature")?;
    send_version_size_message(&sig_buf, stream).context("send_version_size_message signature")?;
    Ok(())
}

pub fn receive_signed_buffer(
    stream: &mut TcpStream,
    read_timeout_seconds: u64,
    sig_verifier: &OpenSslSigVerifier,
) -> Result<Vec<u8>> {
    let reply_buf = receive_version_size_message(stream, read_timeout_seconds)
        .context("receive_version_size_message reply_buf")?;
    // verify the signature of the reply
    let signature = receive_version_size_message(stream, read_timeout_seconds)
        .context("receive_version_size_message signature")?;
    let sig_ok = sig_verifier
        .verify_signature(&reply_buf, &signature)
        .context("verify_signature")?;
    if !sig_ok {
        return Err(anyhow!("verify_signature failed"));
    }
    Ok(reply_buf)
}
#[must_use]
pub fn time_stamp_milli_secs() -> DateTime<Utc> {
    let now = SystemTime::now();
    let now_utc = DateTime::<Utc>::from(now);
    let date = now_utc.naive_utc().date();
    let time = now_utc.time();
    let millis = time.nanosecond() / 1_000_000;
    let nanos = millis * 1_000_000;
    if let Some(naive_time) = time.with_nanosecond(nanos) {
        DateTime::from_naive_utc_and_offset(NaiveDateTime::new(date, naive_time), Utc)
    } else {
        error!("time_stamp_milli_secs no NaiveTime in milli secs from {now_utc:?}");
        now_utc
    }
}

pub fn dt_utc_to_string(dt_utc: &DateTime<Utc>) -> String {
    // As RFC 3339, always including milliseconds, even when zero, and with a trailing Z.
    // RFC 9557 requires that a trailing Z implies that there is no preferred time zone,
    // which is consistent with the intended use here: plain UTC without a time zone.
    let use_z: bool = true;
    dt_utc.to_rfc3339_opts(SecondsFormat::Millis, use_z)
}

pub fn dt_utc_from_str(s: &str) -> Result<DateTime<Utc>> {
    match NaiveDateTime::parse_from_str(s, "%Y-%m-%dT%H:%M:%S%.3fZ") {
        Ok(ndt) => Ok(DateTime::<Utc>::from_naive_utc_and_offset(ndt, Utc)),
        Err(e) => Err(Error::new(e).context(format!("NaiveDateTime::parse_from_str {s}"))),
    }
}

#[derive(Debug)]
pub struct CtrlC {}

impl Display for CtrlC {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        f.write_str("ctrl-C")
    }
}

#[derive(Clone)]
pub struct RunState(Arc<AtomicBool>);

impl RunState {
    pub fn new() -> Result<Self> {
        let res = RunState(Arc::new(AtomicBool::new(true)));
        let r = res.0.clone();
        let handler = {
            move || {
                r.store(false, Ordering::SeqCst);
            }
        };
        match ctrlc::set_handler(handler) {
            Err(e) => Err(Error::new(e).context("set_handler")),
            Ok(_) => Ok(res),
        }
    }

    pub fn is_running(&self) -> Result<()> {
        if self.0.load(Ordering::SeqCst) {
            Ok(())
        } else {
            Err(Error::msg("ctrl-C"))
        }
    }

    pub fn start_exit_thread(self) {
        let _ctrlc_thread = thread::spawn(move || {
            loop {
                thread::sleep(Duration::from_secs(5));
                if let Err(e) = self.is_running() {
                    error!("{e:?} exiting");
                    std::process::exit(1);
                }
            }
        });
    }
}

pub type SigVerifierByMeterPubKeyHash = HashMap<PubKeyHash, Rc<OpenSslSigVerifier>>;

pub fn sig_verifier_by_meter_pkh_from_config(
    meter_set_public_keys: &BTreeSet<String>,
    temp_dir_name: &str,
) -> Result<SigVerifierByMeterPubKeyHash> {
    let mut res = SigVerifierByMeterPubKeyHash::new();
    for public_key_file_name in meter_set_public_keys {
        let sig_verifier = Rc::new(OpenSslSigVerifier::new(
            public_key_file_name,
            temp_dir_name,
        )?);
        let pkh = sig_verifier.pub_key_hash()?;
        trace!("config: {pkh} of {public_key_file_name}");
        res.insert(pkh, sig_verifier);
    }
    Ok(res)
}

pub fn hash_u32(bytes: &[u8]) -> u32 {
    let mut hasher: Blake2s256 = Blake2s256::new();
    hasher.update(bytes);
    let hashed: &[u8] = &hasher.finalize();
    let ar: [u8; 4] = hashed[..4].try_into().unwrap();
    u32::from_le_bytes(ar)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_ts(ts: &str) {
        let dt = dt_utc_from_str(ts).unwrap();
        let dts = dt_utc_to_string(&dt);
        assert_eq!(ts, dts);
        let dtst = dt_utc_from_str(&dts).unwrap();
        assert_eq!(dt, dtst);
    }

    #[test]
    fn test_time_stamp_string() {
        test_ts("2023-03-25T01:01:03.999Z");
        test_ts("2023-03-25T01:02:03.000Z"); // this used to fail before 2023, suppressing the .000
        test_ts("2023-03-25T01:02:03.001Z");
    }

    #[test]
    fn test_same_prefix_bits() {
        assert!(same_prefix_bits([0], [0], 8));
        assert!(!same_prefix_bits([0], [1], 8));
        assert!(same_prefix_bits([0], [1], 7));
        assert!(!same_prefix_bits([0, 0], [0, 3], 16));
        assert!(same_prefix_bits([0, 0], [0, 3], 14));
    }

    fn test_ok_bool(r: Result<bool>, b: bool) -> bool {
        r.is_ok_and(|rb| rb == b)
    }

    #[test]
    #[ignore]
    fn test_ip_addr_in_wg_intf() {
        // Only run this with a wg link with local addresses 10.1.0.1 and fde2:8b02:e204::a01:1
        assert!(test_ok_bool(ip_addr_in_wg_local_intf("10.1.0.1"), true));
        assert!(test_ok_bool(ip_addr_in_wg_local_intf("10.1.0.2"), false));
        assert!(test_ok_bool(
            ip_addr_in_wg_local_intf("fde2:8b02:e204::a01:1"),
            true
        ));
        assert!(test_ok_bool(
            ip_addr_in_wg_local_intf("fde2:8b02:e204::a01:0"),
            false
        ));
    }

    #[test]
    fn test_hashers() {
        let bytes_refs: [&[u8]; 6] = [
            &[],
            &[1u8],
            &[1u8, 2],
            &[1u8, 2, 3],
            &[1u8, 2, 3, 4],
            &[1u8, 2, 3, 4, 5],
        ];
        for bytes_ref in bytes_refs {
            let _ = hash_u32(bytes_ref);
        }
        // verify same results on different platforms:
        assert_eq!(hash_u32(&[]), 813310313);
    }
}
