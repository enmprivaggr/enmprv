#! /bin/bash -ev
set -u

# In the local repository run:
# - cargo format in check mode
# - cargo check 
# - cargo clippy
# - cargo test
# Use --all-features where possible to check all code features.

cd "$(dirname "$0")"/.. # cd to repository

cargo fmt --check # show diffs that are needed

cargo check --tests --all-features

# on any clippy warning, show the warnings and exit 1
ws="$(cargo clippy --tests --all-features 2>&1 | (grep warning: || true))"
if [[ "$ws" != "" ]] ; then
   cargo clippy --tests --all-features
   exit 1
fi

cargo test --all-features -- --nocapture
# example to run a single test case:
# cargo test openssl::tests::test_ec_signature_1 -- --nocapture
