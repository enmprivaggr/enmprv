#! /bin/bash -ev
set -u

# On a remote p1encryptor, control:
# - building, testing and running a development version, with the simulate_meter_nrs feature.
#   this includes copying ssl keys and wg config files for development to the
#   remote p1encryptor,
# - building and running a release version from a git repo on the remote p1encryptor with no code features, and 
# - enabling and disabling the release version as systemctl service.
#
# To compile the rust sqlx code offline (using sqlx .json files instead of accessing a database with tables)
# the user doing the compilation should not have a user role in an active postgresql cluster.
# In case there is such a role, the compilation will try to access a database
# instead of using the .json files, and fail with an error message about a missing database with
# the same name as the user.
# Because of this the remote compilation is done by a user that has no role in the postgres cluster, 
# and running the built program is done by another user that owns the cluster database that will be used 
# by the built program to create and use its tables.
# CHECKME: from the sqlx docs:
# To make sure an accidentally-present DATABASE_URL environment variable or .env file does not result 
# in cargo build (trying to) access the database, you can set the SQLX_OFFLINE environment variable to true.
#
# When disabling the remote systemctl service:
# - stop the service and disable it for next reboot.
# Otherwise, the target directories for rsync copying should exist:
# - copy rust code for p1encrypt-rs, and some test configuration, from the local source workspace
#   into the remote workspace ~/enmprv/ on the target raspberry
# - copy the Cargo.lock file to remote.
# - cargo check on the target
# - cargo clippy
# Finally, one of:
# - build debug target, and cargo run or test
# - build and cargo run the release target
# - start and enable the release build target as systemd service

# Remote p1encryptor ssh host with the user building the program.
# This user should not have a db role on the remote host. 
raspy_host=raspyp1
raspy="$raspy_host" 
# For a test run of the built program, this user will sudo -u as the run user:
raspy_run_user=mmenc
# This run user is also the user running the built program as a systemctl service.


# Function chosen by 1st arg:
# By default do a normal run without the tests.
devsimmode=0
releasemode=0 # cargo build --release, and run this in foreground.
enablemode=0 # cargo build --release, and systemctl start/enable.
disablemode=0 # systemctl stop/disable
statusmode=0 # show systemctl status
if [ "$#" == 0 ]; then
    echo "Use $0: devsim | release | enable | disable | status"
    exit 1
elif [ "$1" = 'devsim' ]; then
    devsimmode=1 # development run with simulate_meter_nrs feature
elif [ "$1" = 'release' ]; then
    releasemode=1 # build a release, and run the release binary in foreground.
elif [ "$1" = 'enable' ]; then
    enablemode=1 # systemctl
elif [ "$1" = 'disable' ]; then
    disablemode=1 # systemctl
elif [ "$1" = 'status' ]; then
    statusmode=1 # systemctl
elif [ "$1" != '' ]; then
    echo "$0: unknown argument $1"
    exit 1
fi

release_repo_url="https://gitlab.com/privacysamen/enmprv.git" # clone the release source from here.
release_tag="v0.2.0" # check out for a release build on remote p1encryptor.
release_branch="master" # release tag should be on this branch


ssh $raspy pwd

# use the remote sudoer user instead of mmenc: 
# CHECKME the --prompt may not be needed, and in that case the ssh -t is also not needed.
ssh_sudo_args=(-t "$raspy" sudo --prompt=sudo-passwd-"$raspy":)

remote_binary="p1encrypt" # binary name
service_name=$remote_binary"-rs" # prefix of .service file

# pipe systemctl status through cat to avoid systemctl using less on the terminal stdout
systemctl_status_cmd="systemctl status $service_name | cat"

if [[ $statusmode -eq 1 ]]; then
    # show full systemctl status
    ssh "${ssh_sudo_args[@]}" "$systemctl_status_cmd"
    exit
fi

if [[ $disablemode -eq 1 ]]; then
    # Check for a remote running p1encrypt-rs process:
    grep_exit=0
    (ssh $raspy "ps ax" | grep $remote_binary) || grep_exit=$?
    # show full systemctl status anyway
    ssh "${ssh_sudo_args[@]}" "$systemctl_status_cmd"
    if [[ $grep_exit -ne 0 ]]; then
        # grep exit non zero, no process running
        echo "no $remote_binary remote process running to be disabled"
        exit 1
    fi
    echo "Stop $service_name service"
    ssh "${ssh_sudo_args[@]}" systemctl stop $service_name
    echo "Do not start $service_name at next boot"
    ssh "${ssh_sudo_args[@]}" systemctl disable $service_name
    ssh "${ssh_sudo_args[@]}" "$systemctl_status_cmd"
    exit
fi

# Local source code repository directory:
repo_name="enmprv"
local_repo=~/gitrepos/$repo_name
rsyncopts=(--relative --times)
target_repo=$repo_name # in remote home directory
release_repo_dir=$repo_name"-rel" # in remote sudoer home directory

# Create remote directories for rust package with only p1encrypt, openssl keys and wg tunnel configs:
service_dir=$service_name # in remote home directory, for running as systemctl service.

# Create remote directories for rust package with only p1encrypt, openssl keys
target_package_dir=$target_repo/src/bin/$remote_binary
for dir in \
    $target_package_dir \
    keys \
    $service_dir
do
    (ssh $raspy "mkdir --parents $dir" ) || true
done

p1e_config_file="p1encrypt.toml"
log_config_file_name=p1elog4rs.yaml # foreground log config
log_bg_config_file_name=p1ebglog4rs.yaml # background log config
dev_cargo_features="simulate_meter_nrs" # for development only.

if [[ $devsimmode -eq 1 ]]; then
    # copy Cargo.toml and Cargo.lock for p1encrypt to the remote package:
    cd $local_repo
    rsync "${rsyncopts[@]}" Cargo.lock Cargo.toml "$raspy":"$target_repo"
    # Copy development files, system service files, and rust sources to raspy
    # copy the .sqlx files for remote compilation with database schema info:
    sqlx_dir_name=".sqlx"
    rsync --recursive --times --delete "$sqlx_dir_name"/ "$raspy":"$target_repo"/"$sqlx_dir_name"/
    cd $local_repo/src/bin/"$remote_binary"
    rsync "${rsyncopts[@]}" $p1e_config_file "$raspy":"$target_package_dir"/
    # copy the log config file after some substitutions, see below.
    # rust sources and included sql files for p1encrypt:
    rust_files=(\
        main.rs \
        p1input.rs \
        p1db.rs \
        p1split.rs \
        p1aggr.rs \
        p1ka.rs \
        meter.sql \
        measurement.sql )
    rsync "${rsyncopts[@]}" "${rust_files[@]}" "$raspy":"$target_package_dir"
    # rust sources for the library in the package:
    cd $local_repo/src
    lib_files=(\
        lib.rs \
        bincoded.rs \
        checkclocksync.rs \
        durationconfig.rs \
        encryptionseed.rs \
        measuredvalue.rs \
        measurement.rs \
        openssl.rs \
        util.rs)
    rsync "${rsyncopts[@]}" "${lib_files[@]}" "$raspy":"$target_repo"/src

    # cargo check step:
    cargo_args="check --features $dev_cargo_features"
    echo "$cargo_args"
    time ssh -t $raspy "(cd $target_repo; cargo $cargo_args)"

    # cargo clippy step
    cargo_args="clippy --features $dev_cargo_features"
    cmd="ssh -t $raspy (cd $target_repo; cargo $cargo_args)"
    echo "$cmd"
    time $cmd
fi # not enabling system service

raspy_build_user=$(ssh $raspy whoami)
echo Build user at $raspy is "$raspy_build_user"

release_build_target="target/release/p1encrypt" # in $source_dir on $raspy

if [[ $enablemode -eq 0 ]]; then
    # development run, use debug or release target.

    # set terminal to incorrect parity for testing parity change without reboot:
    # ssh $raspy stty -F /dev/ttyUSB0 -evenp

    # build p1encrypt on raspy, and prepare cargo run command:
    if [[ $releasemode -eq 0 ]]; then
        ssh -t $raspy "(cd $target_repo;cargo build --features $dev_cargo_features)"
        cargo_build_cmd="build --features $dev_cargo_features"
        source_dir=$target_repo
        target_dir="target/debug"
    else
        # check for presence of enmprv-rel git repo on raspy
        # build/run release from release_repo_dir
        dir_present=$(ssh $raspy ls -d $release_repo_dir || true)
        if [[ $dir_present == '' ]]; then
           echo $release_repo_dir on $raspy
           ssh $raspy git clone $release_repo_url $release_repo_dir
        fi
        echo Checking out $release_tag, should be at $release_branch branch.
        ssh $raspy "(cd $release_repo_dir; \
                     git fetch --all; \
                     git checkout master; \
                     git pull $release_repo_url $release_branch; \
                     git pull $release_repo_url $release_tag; \
                     git checkout $release_tag)"
        ssh -t $raspy "(cd $release_repo_dir; cargo clippy --all-features)"
        cargo_build_cmd="build --bin p1encrypt --release" # build release without cargo features
        source_dir=$release_repo_dir
        target_dir="target/release"
        log_config_file_name=$log_bg_config_file_name
    fi

    # in the log config change the host name marker p1enc-host to $raspy
    tmp_log_config_file=/tmp/$log_config_file_name
    cd $local_repo/src/bin/p1encrypt
    sed --expression="{
        s/p1enc-user/$raspy_run_user/g
        s/p1enc-host/$raspy_host/g
    }" $log_config_file_name > $tmp_log_config_file
    # Copy test run and log config files to $raspy, avoid rsync --relative
    rsync --times $p1e_config_file $tmp_log_config_file "$raspy":"$source_dir"
    echo "cd $source_dir; cargo $cargo_build_cmd"
    ssh -t $raspy "(cd $source_dir; cargo $cargo_build_cmd)"
    
    # # test: change tty settings before running
    # stty_cmd="stty -F /dev/ttyUSB0 9600" # stty fails with -oddp here
    # echo "$stty_cmd"
    # ssh -t $raspy "$stty_cmd"

    # run the development/release binary as user mmenc:
    run_cmd="(cd $source_dir; sudo -u $raspy_run_user $target_dir/p1encrypt $p1e_config_file $log_config_file_name)"
    echo "$run_cmd"
    ssh -t $raspy "$run_cmd"
    exit
fi

# enablemode=1

remote_sudoer_home=$(ssh $raspy pwd)
echo remote_sudoer_home "$remote_sudoer_home"

remote_run_home=$(ssh $raspy_run_user@$raspy pwd)
echo remote_run_home "$remote_run_home"

echo "Check existence of release binary $release_repo_dir/$release_build_target"
ssh $raspy ls -l "$release_repo_dir/$release_build_target"

# link the release target into service_dir
ssh "${ssh_sudo_args[@]}" ln --force "$release_repo_dir/$release_build_target" "$remote_run_home/$service_dir"

cd $local_repo/src/bin/p1encrypt
# copy the .service, the log config file and the p1e config file into service_dir:
p1e_service_name="$service_name".service
tmp_p1e_service_file=/tmp/$p1e_service_name
sed --expression="{
    s/p1enc-user/$raspy_run_user/g
}" $p1e_service_name > $tmp_p1e_service_file

log_config_name=$log_bg_config_file_name # background log config
tmp_log_config_file=/tmp/$log_config_name
# replace host name marker with $raspy in log config file:
sed --expression="{
    s/p1enc-user/$raspy_run_user/g
    s/p1enc-host/$raspy/g
}" $log_config_name > $tmp_log_config_file

p1e_service_start_files=(
    "$tmp_p1e_service_file"
    "$tmp_log_config_file"
    "$p1e_config_file"
)
# avoid rsync --relative because of $tmp_log_config_file:
rsync --times "${p1e_service_start_files[@]}" "$raspy_run_user@$raspy":"$service_dir"
ssh $raspy_run_user@$raspy ls -lt $service_dir

# Start as systemctl service:
ssh $raspy ls -lt "$target_repo"
# remote symbolic link from /etc/systemd/system/ to absolute path service file:
remote_service_file="$remote_run_home"/"$service_dir"/"$p1e_service_name"
ssh "${ssh_sudo_args[@]}" ln --symbolic --force "$remote_service_file" /etc/systemd/system
ssh "${ssh_sudo_args[@]}" ls -lt /etc/systemd/system

# reload possibly changed p1encrypt.service
ssh "${ssh_sudo_args[@]}" systemctl daemon-reload
# start as systemd service
ssh "${ssh_sudo_args[@]}" systemctl start $service_name
# enable on next boot
ssh "${ssh_sudo_args[@]}" systemctl enable $service_name
# show status
ssh "${ssh_sudo_args[@]}" "$systemctl_status_cmd"
exit
