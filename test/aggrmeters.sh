#! /bin/bash -ev
set -u
# Run the aggregator for development testing
cd ~/gitrepos/enmprv
clear
cargo clippy
cargo build --bin aggrmeters

user_name=prvaggr
binary=~/gitrepos/enmprv/target/debug/aggrmeters
runconfig=src/bin/aggrmeters/aggrmeters.toml

# Patch the log configuration:
log_config_name=agrmlog4rs.yaml
log_config_path=src/bin/aggrmeters/$log_config_name
tmp_log_config_path=/tmp/$log_config_name
sed_script="{
s/prvag-user/$user_name/g
s/prvag-host/$HOSTNAME/g
}"
sed --expression="$sed_script" $log_config_path > $tmp_log_config_path

time sudo -u $user_name $binary $runconfig $tmp_log_config_path

