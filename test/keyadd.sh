#! /bin/bash -ev
set -u 
# Run the key adder locally for development testing
cd ~/gitrepos/enmprv
clear
cargo clippy
cargo build --bin keyadder

binary=~/gitrepos/enmprv/target/debug/keyadder
runconfig=src/bin/keyadder/keyadder.toml

# Patch the log config for local use:
log_config_name=keyaddlog4rs.yaml
log_config_path=src/bin/keyadder/$log_config_name
tmp_log_config_path=/tmp/$log_config_name

user_name=keyadder

# Patch the log configuration::
sed_script="{
s/keyadd-user/$user_name/g
s/keyadd-host/$HOSTNAME/g
}"
sed --expression="$sed_script" $log_config_path > $tmp_log_config_path

time sudo -u $user_name $binary $runconfig $tmp_log_config_path
