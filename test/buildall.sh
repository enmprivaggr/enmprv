#! /bin/bash -ev
set -u

# Perform cargo update and build on all rust programs and library code in enmprv locally and on raspy2 and p1e3

repodir=~/gitrepos/enmprv
#p1encryptors="raspy2 p1e3"
p1encryptors="raspy2"

dorelease=0

if [ "$1" = 'clean' ]; then
    cargo_build_commands="clean update check clippy build"
elif [ "$1" = 'update' ]; then
    cargo_build_commands="update check clippy build"
elif [ "$1" = 'release' ]; then
    cargo_build_commands="check clippy"
    dorelease=1
    echo $dorelease
else
    cargo_build_commands="check clippy build"
fi

git_checks () {
    if (git status --porcelain | grep ' M '); then pwd;exit 1; fi
    if (git status --porcelain | grep '\?\? '); then pwd;exit 1; fi
    if (git status --porcelain --branch | grep 'ahead '); then pwd;exit 1; fi
}

cargo_update_build () {
    for cargo_cmd in $cargo_build_commands
    do
        echo "$cargo_cmd"
        cargo "$cargo_cmd"
    done
    if [ $dorelease = 1 ]; then
        cargo build --release
    fi
}


# code must be clean in repo:
cd $repodir
git_checks

# cargo fmt and build steps:
for rustpack in enmplib-rs keyadder-rs aggrmeters-rs
do
    echo $rustpack
    cd $repodir/$rustpack
    cargo fmt
    # code must be clean in repo after cargo fmt
    git_checks
    cargo_update_build
done

if [ $dorelease = 1 ]; then
    for rustprog in keyadder-rs aggrmeters-rs
    do
        echo $rustprog
        cd $repodir/$rustprog
        cargo build --release
    done
fi

database_url="postgres:///p1edb"

# build p1encrypt-rs on $p1encryptors:
# CHECKME: copy code to each p1encryptor just as in p1encrypt-rs/test/p1encrypt-rs.sh
for cargo_cmd in $cargo_build_commands
do
    echo "$cargo_cmd"
    for p1enc in $p1encryptors
    do
        echo $p1enc
        ssh $p1enc "(cd p1encrypt-rs;DATABASE_URL=$database_url cargo $cargo_cmd)"
    done
done
if [ $dorelease = 1 ]; then
    for p1enc in $p1encryptors
    do
        echo $p1enc
        ssh $p1enc "(cd p1encrypt-rs;DATABASE_URL=$database_url cargo build --release)"
    done
fi

