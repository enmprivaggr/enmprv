# Development scripts


## Setting up the development environment

The development environment consists of a development machine running Linux, 
and a Raspberry Pi that is connected to the P1 port of a smart meter.
The development machine is also referred to as the local machine, and 
and the Raspberry Pi is also called the remote machine.

The user on the local development machine should be a sudoer.
This can be checked by this command:
```bash
sudo echo ok
```
The Raspberry Pi should be ssh reachable from the development machine by this command:
```bash
ssh raspyp1
```
This can be prepared on the development machine in the file `~/.ssh/config`.
In this file, under Host `raspyp1`, the Hostname should be the one that was used in the operating system installation [here]( https://www.raspberrypi.com/documentation/computers/getting-started.html#install-an-operating-system).
The ssh user on the Raspberry is a sudoer, 
and this is normally the result of setting up the Raspberry with SSH enabled and allowing public-key authentication.

A more detailed hardware setup for the Raspberry Pi is [here](SetupP1Encryptor.md).

The [setupemp.py](dev/setupemp.py) script sets of the 
software of the development environment.
This script uses two `.toml` files with configuration values. 
Most of these configuration values have a default value
that is defined in [setupempdefault.toml](dev/setupempdefault.toml).

The IP address of the Raspberry must to be provided in another `.toml` file
for the wireguard tunnel that will be created
between the development machine and the Raspberry. 
This `.toml` file looks like:

  ```toml
  [wg_tunnel]
  p1e_end_point = { address = "...", port = 51282 }
  ```
The address ... value here is the IP address that is used
by wireguard to connect to the Raspberry, 
and this can normally be found by the command 
```bash
host hostname
```
where the hostname is the one chosen for the Raspberry Pi at the operating system installation. 
It is recommended to put this extra `.toml` file in a private repository,
for example under the name `enmprefs.toml`.

Make sure that this Raspberry Pi
has `/dev/ttyUSB0` (left upper USB port) connected to the P1 port of the smart meter.

Running the set up script will do `sudo apt-get update` and `sudo apt=get upgrade`
on the local machine and on the remote machine.
This is to make sure that all available safety updates are applied
before starting development.
Since these apt-get steps can take quite some time, doing these steps
manually beforehand will let running the set up script take less time.

After these preparations run the set up script from the development machine:
```bash
dev/setupemp.py -l ../path/to/enmprefs.toml
```
(The shell working directory should be the top level of the repository.)

The configuration value for the tunnel endpoint has the default value some_ipv4or6, 
and when this is not overridden in the `enmprefs.toml` file, the setup script will end with this error message:
`ValueError: 'some_ipv4or6' does not appear to be an IPv4 or IPv6 address`

Setting up the development environment will take a few minutes,
even when the apt-get steps have already been done.

To check the setup, continue through the following steps until aggregation output is
available.

## Local checks on the Rust code

Running the script [test/local-enmprv.sh](test/local-enmprv.sh) will:
```bash
# In the local repository run:
# - cargo format in check mode
# - cargo check 
# - cargo clippy
# - cargo test
```
The intended use is that this passes before all commits in the repository.

When this is run for the first time, be prepared to wait for the rust build steps.

## Locally starting the keyadder and aggrmeters rust programs 

In the prepared development environment the keyadder and aggrmeters programs run
locally on the development machine.

The script 
```bash
test/keyadd.sh
``` 
is started as development user.
The script is best run from its own terminal window.
It will build the keyadder program, and then run it as the user `keyadder` that has been created by the setup above.

Similarly, the script 
```bash
test/aggrmeters.sh
``` 
is started as development user
in its own terminal window. It will build the aggrmeters program,
and then run it as the user `prvaggr`.

These programs produce log output on stdout and in the log/ directory
in their user home directory.
Both programs can be stopped by typing Ctrl-C in their terminal window.

## Running the p1encrypt program on the p1encryptor Raspberry

The script `test/p1encrypt.sh` is run from the development machine.
It requires one argument as shown below:

```bash
test/p1encrypt.sh devsim
```
This:
  - copies source code and configuration files to the p1encryptor host, 
  - builds a development version of the p1encrypt program
    with the `simulate_meter_nrs` feature.
    (The first build will take a few minutes.)
  - runs the program in foreground as the `mmenc` user.

  The configuration files and the `simulate_meter_nrs` feature
  combine here to simulate a second meter with the same, but slightly
  delayed, measurements from the P1 port.


  The program encrypts these measurements and connects, via the wireguard tunnel,
  to the development versions of the
  keyadder and aggrmeters programs running on the development machine.


  When both these keyadder and aggrmeters programs are also running, 
  after a few minutes a decrypted aggregation of the meters
  is shown in the log of the aggrmeters program, for example:

```text
,,,,,,,,,,,,,,***********************************
2025-02-15T21:06:45.949Z v110 INFO aggrmeters::aggrka:817 collected 7 measurement messages, with 49 measurements, total 2237 bytes
2025-02-15T21:06:45.999Z v110 TRACE enmprv::util:298 receive_version_size_message size 139
2025-02-15T21:06:46.050Z v110 TRACE enmprv::util:298 receive_version_size_message size 103
2025-02-15T21:06:46.098Z v110 INFO aggrmeters::aggrka:866 decryption delay: 149 ms
2025-02-15T21:06:46.101Z v110 INFO aggrmeters::aggrka:985 AggrId("aggr_01_dev")
2025-02-15T21:06:46.101Z v110 INFO aggrmeters::aggrka:988 AggrUtc("2025-02-15T21:06:46.099Z")
2025-02-15T21:06:46.102Z v110 INFO aggrmeters::aggrka:977   2 meter serial nrs: E1234567812345678 E1234567812345678-1
2025-02-15T21:06:46.102Z v110 INFO aggrmeters::aggrka:991 TotalDelayMs(8040)
2025-02-15T21:06:46.102Z v110 INFO aggrmeters::aggrka:970     0.43000 El_kW_to_aggr
2025-02-15T21:06:46.103Z v110 INFO aggrmeters::aggrka:970     0.00000 El_kW_by_aggr
2025-02-15T21:06:46.103Z v110 INFO aggrmeters::aggrka:970  2204.35400 El_kWh_to_tariff_1_aggr
2025-02-15T21:06:46.103Z v110 INFO aggrmeters::aggrka:970   630.00000 El_kWh_by_tariff_1_aggr
2025-02-15T21:06:46.104Z v110 INFO aggrmeters::aggrka:970  3049.32200 El_kWh_to_tariff_2_aggr
2025-02-15T21:06:46.104Z v110 INFO aggrmeters::aggrka:970  1275.04400 El_kWh_by_tariff_2_aggr
2025-02-15T21:06:46.105Z v110 INFO aggrmeters::aggrka:970     0.00000 El_kW_coll
2025-02-15T21:06:46.105Z v110 INFO aggrmeters::aggrka:977   2 meter serial nrs: G1234567012345670 G1234567012345670-1
2025-02-15T21:06:46.105Z v110 INFO aggrmeters::aggrka:991 TotalDelayMs(216198)
2025-02-15T21:06:46.105Z v110 INFO aggrmeters::aggrka:970  2231.06000 NgNl_m3_aggr
```  
These aggregated values are decrypted from measured values 
that are read from the P1 port and encrypted by the p1encryptor.
In this development scenario simulated meter numbers are used.
At the end of the aggregation interval (the first INFO log line above) 
the aggrmeters program requested the decryption keys from the keyadder program,
and this caused a decryption delay of 149 ms.

### The P1 input lock

Normally the toml configuration file 
[src/bin/p1encrypt/p1encrypt.toml]
(src/bin/p1encrypt/p1encrypt.toml)
of the p1encrypt program contains the device file from which
from the p1 telegrams are read, and a socket bind lock:
```toml
p1_terminal = '/dev/ttyUSB0' # read p1 telegrams from here
# bind a socket to this loopback address/udp-port to exclude other programs from reading p1_terminal:
p1_excl_local_ip = { address = '127.126.1.1', port = 52312 }
```
This socket is bound at startup and the program terminates immediately
when this binding fails with a log line like this:
```text
2025-02-19T18:58:57.772Z raspyp1 ERROR p1encrypt:159 read_p1_telegrams_changing_parity: udp_bind, refuse to read /dev/ttyUSB0: bind for 127.126.1.1:52312: Address already in use (os error 98), exiting
```

The reason to use this lock is that when two programs read the 
same character device file at the same time, the operating system
randomly divides the available characters between these programs.
Such input is never correct, and the bind lock is used to avoid this situation.

During development it is relatively easy to forget about
a p1encrypt program that is running as a systemctl service in background.
Therefore always check the log of a p1encrypt program when appears
not to be starting up.


### Running the p1encrypt program as a systemd service

The following steps will build a release binary of the p1encryptor program
and control it as systemd service:

```bash
test/p1encrypt.sh release
```
  This:
  - checks out the latest git release
    in a separate git repository on the p1encryptor host, 
  - does a release build of the p1encrypt program with no code features,
  - runs this program in foregroundas the `mmenc` user.
    No meter is simulated, and the log will show errors about
    missing signing keys for the actual meter numbers.

```bash
test/p1encrypt.sh enable
```
  This starts and enables the release version as a systemctl service
  running (in background) as the `mmenc` user.

```bash
test/p1encrypt.sh disable
```
  This stops and disables this systemctl service.

```bash
test/p1encrypt.sh status
```
  This shows the status of the systemctl service.


### Logs and stdout of the p1encrypt program

When the `p1encrypt` program runs in foreground, it produces log info on stdout,
and it can be stopped by Ctrl-C.
When running in foreground or in background, log files are produced
in the `log/` directory of the `mmenc` user.


## Database development steps

### Rust code compilation

The Rust code here uses sqlx to deal with the Postgresql database.
Currently only the code in `src/bin/p1encrypt/p1db.rs` accesses a database.
See [here](https://lib.rs/crates/sqlx-cli) for more details on sqlx-cli.
Before using the steps below, it is necessary to install sqlx-cli
from the command line:

```bash
cargo install sqlx-cli
```


The compilation of the rust code here needs either:
- online access to the database table definitions in the database, or:
- in offline compilation mode, use `query*.json` files the `.sqlx/` directory.

Because the rust code normally also creates the tables in this database there is
a circular dependency to set this up.
To break this circle, the table definitions are stored in `.sql` files in the code
repository, and the `dev/sqlxemp.py` script also uses these `.sql` files to create the tables
in a local database that is only used for compilation.

The configuration for this is stored in the file `dev/setupempdefault.toml`
in the `[local_host]` section:

```toml
all_tables_db_name = "sqlx_all_tables_db"
all_tables_sql_defs = [
    # in src/bin/ in the git repo.
    # Order any table with a foreign key after the table with the referenced primary key:
    "p1encrypt/meter.sql",
    "p1encrypt/measurement.sql",
]
```
As an example of creating a table, the `p1encrypt/meter.sql` file is shown here:
```sql
CREATE TABLE IF NOT EXISTS meter (
    local_meter_nr                     int         PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY,
    meter_serial_nr                    varchar     NOT NULL CHECK (meter_serial_nr <> ''),
    CONSTRAINT unique_meter_serial_nr
        UNIQUE(meter_serial_nr));
```

Using `dev/sqlxemp.py` from the command line:
```bash
dev/sqlxemp.py --schema
```
will use the configuration to create the tables in the local database. 
This will also generate a `.env` file with the postgres URL of the database for further sqlx online rust code compilation.

When the code that performs the queries on the database tables is ready:
```bash
dev/sqlxemp.py --prepare
```
will use `sqlx-cli` to prepare the `.slqx/query*.json` files
for offline compilation. 
This will also delete the `.env` file from the previous step.

On the master branch only offline compilation should be used.

### Plotting the measured values from the database

It may be necessary to install matplotlib:
```bash
sudo apt install python3-matplotlib
```

To show the content of the database use the script `dev/meterplots.py`.
This will plot electricity and gas power usage in quarter of an hour intervals.
By (un)commenting a few lines of code in the `do_main()` function other intervals and time periods can be used.

On stdout meterplots.py provides the meter numbers, the last meter readings,
and actual intervals and total energy values.


### Some useful database queries

To locally show the total disk usage by the remote database:
```bash
ssh raspyp1 sudo du -sh /var/lib/postgresql
```

With this command start a shell for user mmenc on the raspberry:
```bash
ssh mmenc@raspyp1
```
And from there use psql:
```bash
psql --dbname=p1encdb
```
This will show the prompt `p1encdb=> `.

**Be careful, do not make any change to this database manually.
Only use sql `select` queries.**

From this prompt the following sql queries can be useful:
```sql
select count(*) from measurement;
```
This number will grow up to a maximum determined by the configuration in `src/bin/p1encrypt/p1encrypt.toml`, in the section `[measurement_interval_keep_times]`.
Together with the size of the database the number of storage bytes per measurement
can be estimated. After some weeks of continuous running of the p1encrypt program, this should normally go down to below 200 bytes/measurement.

```sql
select interval_border, count(*), min(msrmnt_dt_utc) from measurement group by interval_border;
```
This will show the oldest measurement and the number of measurements
for each interval border. These oldest measurements should be no older than
the measurement interval keep time.
