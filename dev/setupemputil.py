"""
Helper code for setupemp.py
"""

import ipaddress
import logging
from logging.handlers import RotatingFileHandler
import os
import socket

try:
    import serial
except ImportError:
    # The python3-serial apt package is normally installed at the first usage of setupemp.py,
    # and the serial module is only actually used after the first usage.
    pass

import sys
import subprocess
import threading
import time
from pathlib import Path
from typing import Callable, List, Optional

logger = logging.getLogger("__main__." + __name__)

# -t terminal for remotely tunning programs
SSH_CMD = "ssh -t"

SSH_USER_CONFIG_DIR_NAME = ".ssh"
SSH_AUTH_KEYS = "authorized_keys"

SUDO_GROUP_NAME = "sudo"


class SetupException(Exception):
    """ Raised here with a description of the reason. """


def init_logger(
        logger: logging.Logger,
        log_file_name_stem: str,
) -> None:
    """
    Set the `logger` level to `logging.DEBUG` and add a `RotatingFileHandler` to it
    that saves 10 log files of at most 7 MB each in the log/ directory in the home directory.
    The initial name of the log files for the RotatingFileHandler is `log_file_name_stem`.log .
    The logged lines have:
    - the UTC time in milliseconds,
    - the hostname,
    - the log level,
    - the source file and line number of the logger call, and
    - the logged message.
    """
    log_dir_name = "log"
    log_dir_path = Path.home().joinpath(log_dir_name)
    if not log_dir_path.exists():
        log_dir_path.mkdir(parents=True)
    hostname = do_command_stdout("hostname").strip()
    if not log_dir_path.is_dir():
        raise SetupException(
            "{0} at {1} is not a directory".format(
                log_dir_path,
                hostname))
    log_file_path = log_dir_path.joinpath(
        log_file_name_stem).with_suffix(".log")

    class UtcTimeFormatter(logging.Formatter):
        """ Logging formatter using UTC time """
        converter = time.gmtime

    # Use the same format as the enmprv rust programs:
    # log date/time as 2024-02-11T15:16:02.183Z,
    asctime_format = "%Y-%m-%dT%H:%M:%S"
    # add msecs to asctime
    log_format = " ".join((
        "%(asctime)s.%(msecs)03dZ",
        hostname,
        "%(levelname)s",
        "%(filename)s:%(lineno)d",
        "%(message)s"))

    formatter = UtcTimeFormatter(log_format, datefmt=asctime_format)
    max_log_file_size = 7000 * 1024  # 7000 kB, same as for the enmprv rust programs
    file_handler = RotatingFileHandler(
        filename=log_file_path,
        maxBytes=max_log_file_size,
        backupCount=10)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.setLevel(logging.DEBUG)


def do_command_return_code(command: str):
    """ Wait for shell execution of `command`, use the normal stdin, stdout and stderr.
        Return the exit code of the command that is provided by the shell.
    """
    logger.debug(command)
    completion = subprocess.run(
        command,
        shell=True,
        stdin=sys.stdin,
        stdout=sys.stdout,
        stderr=sys.stderr,
        check=False,
    )
    sys.stderr.flush()
    sys.stdout.flush()
    return completion.returncode


def do_command_return_code_no_stdout_stderr(command: str):
    """ Wait for shell execution of `command` with the normal stdin,
        and send stdout and stderr to the log.
        Return the exit code of the command that is provided by the shell.
    """
    logger.debug(command)
    proc = subprocess.Popen(
        command,
        shell=True,
        stdin=sys.stdin,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    out_bytes, err_bytes = proc.communicate(input=None, timeout=None)
    if err_bytes:
        logger.warning("stderr: %s", err_bytes.decode().strip())
    if out_bytes:
        logger.info("stdout: %s", out_bytes.decode().strip())
    return proc.returncode


def do_command(command: str):
    """ Pass `command` to `do_command_return_code`.
        Raise `SetupException` when the returned exit code is non zero.
    """
    return_code = do_command_return_code(command)
    if return_code != 0:
        raise SetupException(f"return_code {return_code} from: {command}")


def do_command_no_stdout_stderr(command: str) -> None:
    """ Wait for shell execution of `command` with the normal stdin,
        and send stdout and stderr to the log.
        Raise `SetupException` when the exit code is non zero.
    """
    return_code = do_command_return_code_no_stdout_stderr(command)
    if return_code != 0:
        raise SetupException(f"return_code {return_code} from: {command}")


def do_command_stdout(
        command: str,
        ignore_stderr: bool = False,
) -> str:
    """ Wait for shell execution of `command`, use the normal stdin,
        Log stderr.
        Log stdout as ignored on non zero exit of command.
        Raise `SetupException` when:
        - the stderr from the command is not empty, except when `ignore_stderr`, or
        - the exit code from the command is non zero.
        Otherwise return the stdout of the command that is provided by the shell.
    """
    logger.debug(command)
    proc = subprocess.Popen(
        command,
        shell=True,
        stdin=sys.stdin,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    out_bytes, err_bytes = proc.communicate(input=None, timeout=None)
    if err_bytes:
        logger.warning("stderr: %s", err_bytes.decode().strip())
    stdout_str = out_bytes.decode()
    if proc.returncode != 0:
        logger.warning("ignoring stdout: %s", stdout_str)
        mes = f"return_code {proc.returncode} from: {command}"
        logger.error(mes)
        raise SetupException(mes)
    if (not ignore_stderr) and err_bytes:
        raise SetupException(
            f"command {command}: normal return code, non empty stderr")
    return stdout_str


def test_p1_connection(
        p1_file_name: str,
        lock_ip_addr_str: str,
        lock_ip_udp_port: int,
) -> None:
    """ Raise `SetupException` when the p1 file does not produce 2 p1 telegrams in 30 seconds,
    or when binding the lock_ip_addr_str:lock_ip_udp_port fails.
    """

    # lock for exclusive read access:
    xlock = XLockSocket(lock_ip_addr_str, int(lock_ip_udp_port))
    if not xlock.try_aquire(max_wait_secs=10):
        raise SetupException("cannot acquire lock {0}:{1} to read {2}".format(
            lock_ip_addr_str,
            lock_ip_udp_port,
            p1_file_name))

    try:
        # A p1 telegram is normally read once per second, or once per ten seconds.
        # Read the port for 60 seconds or until a p1 telegram was read.
        # After 15 seconds without input change the terminal baud rate to 115200 or 9600.
        # See p1input.rs for more details.
        # The first line of a p1 telegram starts with a slash /, e.g.: /ISK5\\2M550T-1014
        # The last line starts with an exclamation mark !, e.g.: !33D3
        stty_settings = do_command_stdout(f"sudo stty -F {p1_file_name}")
        stty_settings_log_msg = " ".join(stty_settings.splitlines())
        logger.debug(stty_settings_log_msg)
        current_baudrate = int(stty_settings.split()[1])
        serial_port = serial.Serial(p1_file_name, baudrate=current_baudrate)
        serial_port.timeout = 3  # seconds
        if current_baudrate != 115200:
            next_baud_rate = 115200
        else:
            next_baud_rate = 9600
        num_start_lines = 0
        num_stop_lines = 0
        now_secs = time.time()
        timeout_secs = 60  # normally at least 6 telegrams started, but only with correct baudrate
        timeout_at_secs = now_secs + timeout_secs
        change_baudrate_secs = timeout_secs / 4
        change_baudrate_at_secs = now_secs + change_baudrate_secs
        while now_secs < timeout_at_secs:
            line_bytes = serial_port.readline()  # note: serial_port.timeout was set
            if not line_bytes:
                logger.debug("no input from %s", p1_file_name)
            else:
                try:
                    line = line_bytes.decode()
                except UnicodeDecodeError:
                    logger.error("UnicodeDecodeError for %s", repr(line_bytes))
                    # CHECKME: change terminal parity after a max 300 character errors, see p1input.rs
                else:
                    line = line.strip()  # remove \r and \n
                    if line.startswith("/"):
                        logger.debug("p1 telegram start line: %s", line)
                        num_start_lines += 1
                    elif line.startswith("!"):
                        logger.debug("p1 telegram stop  line: %s", line)
                        num_stop_lines += 1
                    if num_start_lines >= 2 and num_stop_lines >= 2:
                        # read some p1 telegram start and end lines:
                        return

            now_secs = time.time()
            if now_secs >= change_baudrate_at_secs:
                do_command(f"sudo stty -F {p1_file_name} {next_baud_rate}")
                if next_baud_rate == 115200:
                    next_baud_rate = 9600
                else:
                    next_baud_rate = 115200
                change_baudrate_at_secs += change_baudrate_secs
    finally:
        xlock.release()

    raise SetupException("no p1 telegram from {} after {} secs".format(
        p1_file_name,
        timeout_secs))


def apt_upgrade_install(
        install_packages: List[str],
) -> None:
    """ - perform apt update/upgrade/autoremove,
        - apt install the install_packages,

        Redirect stdout and stderr from apt to the log.
    """
    apt_get_cmd_format = "sudo apt-get --assume-yes {}"

    def do_apt_get(sub_cmd: str) -> None:
        do_command_no_stdout_stderr(apt_get_cmd_format.format(sub_cmd))

    do_apt_get("update")
    do_apt_get("upgrade")
    do_apt_get("autoremove")

    for package in install_packages:
        do_apt_get(f"install {package}")


def sudo_file_exists(file_path: Path) -> bool:
    """ As sudoer check for file existence """
    bash_cmd = f"[[ -e {file_path} ]]"
    return do_command_return_code(f"sudo bash -c '{bash_cmd}'") == 0


def sudo_dir_exists(dir_path: Path) -> bool:
    """ As sudoer check for directory existence """
    bash_cmd = f"[[ -d {dir_path} ]]"
    return do_command_return_code(f"sudo bash -c '{bash_cmd}'") == 0


def wait_clock_synchronized() -> None:
    """ Wait for ntp clock synchronization for at most 30 seconds.
        When this fails, raise a `SetupException`.
    """
    max_wait_secs = 30
    end_time_secs = time.time() + max_wait_secs
    prop = "NTPSynchronized"
    cmd = f"timedatectl show --property={prop}"
    expect_out = f"{prop}=yes"
    while True:
        if do_command_stdout(cmd).find(expect_out) != -1:
            logger.debug(expect_out)
            return
        time.sleep(2)
        if time.time() > end_time_secs:
            raise SetupException("{} not after {} secs".format(
                expect_out,
                max_wait_secs
            ))


def get_user_names() -> set[str]:
    list_users_cmd = "awk -F':' '{print $1}' /etc/passwd"
    user_names = set().union(do_command_stdout(list_users_cmd).strip().splitlines())
    return user_names


def create_system_user(
    user_name: str,
    enmprv_home: str,
    file_name_group_access: Optional[str] = None,
    copy_ssh_auth_keys: bool = False,
) -> None:
    """
    When the user `user_name` does not exist, create it as system user in its own group.
    This system user will not have a password for login, and will not be in the sudo group.
    When `file_name_group_access` is given, put the user in the group of this file.

    When `copy_auth_keys` is True the user will receive
    a copy of the ssh authorized keys of the current user, 
    and /bin/bash as as login shell for use by sshd.
    Otherwise the user will not have a login shell.

    The home directory for the user will be `enmprv_home`/`user_name`.
    In case this directory does not exist it will be created.
    """

    if user_name in get_user_names():
        logger.info(f"{user_name} already exists")
        return

    enmprv_path = Path(enmprv_home)
    if not sudo_dir_exists(enmprv_path):
        do_command_no_stdout_stderr(f"sudo mkdir -p {enmprv_path}")

    # ssh login needs this:
    # CHECKME: can og+rx be reduced?
    do_command_no_stdout_stderr(f"sudo chmod og+rx {enmprv_path}")

    user_home_path = enmprv_path.joinpath(user_name)
    if not sudo_dir_exists(user_home_path):
        add_user_cmd_words = [
            "sudo adduser",
            "--system",  # no password for login
            "--group",  # in group with same name as user
            "--home",
            str(user_home_path),
        ]
        if copy_ssh_auth_keys is True:
            # shell for sshd
            add_user_cmd_words.append("--shell /bin/bash")
        add_user_cmd_words.append(user_name)
        do_command_no_stdout_stderr(" ".join(add_user_cmd_words))
        # CHECKME: adduser also adds directory ~/.local/ for the system user
        # with some mysterious files in there, why?, perhaps see man adduser.local

    if not sudo_dir_exists(user_home_path):
        raise SetupException(
            "missing home directory {} for user {}".format(user_home_path, user_name))

    user_groups = do_command_stdout(f"groups {user_name}").strip().split()[2:]
    if SUDO_GROUP_NAME in user_groups:
        raise SetupException("user {} should not be in group {}".format(
            user_name,
            SUDO_GROUP_NAME))

    if file_name_group_access is not None:
        # typical command for group plugdev of /dev/ttyUSB0:
        # ls -l /dev/ttyUSB0
        # crw-rw----+ 1 root plugdev 188, 0 Nov 19 15:10 /dev/ttyUSB0
        ls_stdout = do_command_stdout(
            f"ls -l {file_name_group_access}").strip()
        logger.debug(ls_stdout)
        file_group_name = ls_stdout.split()[3]
        if file_group_name == SUDO_GROUP_NAME:
            raise SetupException("cannot use group {} of file {} for user {}".format(
                file_group_name,
                file_name_group_access,
                user_name
            ))
        add_user_to_group_cmd = f"sudo adduser {user_name} {file_group_name}"
        do_command_no_stdout_stderr(add_user_to_group_cmd)

    if copy_ssh_auth_keys is True:
        # create the .ssh directory of the user when needed:
        ssh_config_dir_path = user_home_path.joinpath(SSH_USER_CONFIG_DIR_NAME)
        if not sudo_dir_exists(ssh_config_dir_path):
            mkdir_cmd = f"sudo mkdir {ssh_config_dir_path}"
            do_command_no_stdout_stderr(mkdir_cmd)
        # change the owner and group to user_name
        chown_ssh_dir_cmd = "sudo chown {0}:{0} {1}".format(
            user_name,
            ssh_config_dir_path)
        do_command_no_stdout_stderr(chown_ssh_dir_cmd)

        # copy the ssh authorized keys from the current user:
        source_auth_keys_path = Path.home().joinpath(
            SSH_USER_CONFIG_DIR_NAME).joinpath(SSH_AUTH_KEYS)
        target_auth_keys_path = user_home_path.joinpath(
            SSH_USER_CONFIG_DIR_NAME).joinpath(SSH_AUTH_KEYS)
        if source_auth_keys_path == target_auth_keys_path:
            raise SetupException(source_auth_keys_path)
        copy_cmd = f"sudo cp {source_auth_keys_path} {target_auth_keys_path}"
        do_command_no_stdout_stderr(copy_cmd)
        # change the owner and group to user_name
        chown_auth_keys_cmd = "sudo chown {0}:{0} {1}".format(
            user_name,
            target_auth_keys_path)
        do_command_no_stdout_stderr(chown_auth_keys_cmd)


def patch_bashrc_no_early_exit():
    """ In the .bashrc file add shell comments to the non interactive early exit. """
    # To use rustup via the bash path, the curl rustup install appends this
    # line to the end of the .bashrc file:
    #
    #   . "$HOME/.cargo/env"
    #
    # But for non interactive use, the .bashrc file does an early return near the top:
    #
    #   # If not running interactively, don't do anything
    #   case $- in
    #       *i*) ;;
    #         *) return;;
    #   esac
    #
    # Comment this early exit code in the .bashrc file with bash hashes.
    # This can be done more than once, because there is no matching line after the first patch.
    #
    bashrc_path = Path.home().joinpath(".bashrc")
    if bashrc_path.exists():
        # from the case $- line to the next esac line, prepend a bash comment hash:
        # python needs double backslash.
        sed_expr = "/^case \\$-/,/^esac/s/^/# /"
        sed_cmd = "sed --expression='{0}' --in-place=.sed {1}".format(
            sed_expr,
            bashrc_path)
        do_command_no_stdout_stderr(sed_cmd)
    else:
        # a system user does not have a .bashrc,
        # so put .cargo/bin on the path:
        bash_rc_line = 'export PATH="$HOME/.cargo/bin:$PATH"\\n'
        with bashrc_path.open('w', encoding="utf-8") as f:
            f.write(bash_rc_line)
        do_command_no_stdout_stderr(f"sync {bashrc_path}")


def install_rust(install_sqlx_cli: bool = False) -> None:
    """ Install or update rustup and rust for the current user.
    """
    rustup_program = ".cargo/bin/rustup"
    if Path.home().joinpath(rustup_program).exists():
        do_command_no_stdout_stderr("rustup update")
    else:
        # Rust install via curl rustup install:
        # See https://www.rust-lang.org/tools/install
        # See https://rust-lang.github.io/rustup/installation/other.html for help installing
        # Added -- -y to always use the defaults without interaction:
        rust_install_cmd = "curl --proto =https --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y"
        # Note: uninstall by: rustup self uninstall.
        do_command_no_stdout_stderr(rust_install_cmd)
        # to have cargo on non interactive path.
        patch_bashrc_no_early_exit()

    if install_sqlx_cli:
        # install/update sqlx-cli for offline compilation of sqlx related code.
        # do this via bash to have cargo on the path:
        do_command_no_stdout_stderr("bash -c 'cargo install sqlx-cli'")

    # For cargo to compile libc, apt package cmake is also needed,
    # see https://github.com/alacritty/alacritty/issues/1440


def delete_system_user(
    user_name: str,
) -> None:
    """ When user `user_name` exists, kill all processes of the user
    and delete this system user and its group.
    CHECKME: how to do disable systemctl services of the user?
    """
    user_names = get_user_names()
    if user_name not in user_names:
        logger.info(f"{user_name} does not exist")
        return

    kill_all_cmd = "sudo killall --user {}".format(user_name)
    kill_all_exit_code = do_command_return_code_no_stdout_stderr(kill_all_cmd)
    # the user may or may not have running processes:
    if kill_all_exit_code not in {0, 1}:
        raise SetupException("return code {} from {}".format(
            kill_all_exit_code, kill_all_cmd))
    return_code = do_command_return_code_no_stdout_stderr(
        "sudo deluser --remove-home --system {}".format(user_name))
    if return_code not in [0, 8]:
        raise SetupException(
            "unknown return code {0} from deluser for {1}".format(
                return_code,
                user_name))
    # no need for delgroup, the single user group is deleted with the user.


def save_atomically(
    do_save: Callable[[Path], None],
    target_path: Path,
) -> None:
    """
    Use a callable `do_save`to save content to a Path.
    Save the result atomically in `target_path`.

    This uses a temporary file in the same directory as `target_path`
    that has a process/thread unique suffix to the name of the `target_path`.
    """
    temp_file_path = Path("{}.{}.{}".format(
        target_path,
        os.getpid(),
        threading.get_ident()))
    try:
        # save to temp path, leave the original content in target path
        do_save(temp_file_path)
        # sync to save the content
        do_command_no_stdout_stderr(f"sync {temp_file_path}")
        # atomic move to the target
        do_command_no_stdout_stderr(
            f"mv {temp_file_path} {target_path}")
        # sync directory after mv
        do_command_no_stdout_stderr(f"sync {target_path.parent}")
    finally:
        temp_file_path.unlink(missing_ok=True)


class XLockSocket:
    """ An exclusive lock based on a socket.
    The exclusion is obtained by a binding socket to listen on
    an IP address/port. """

    def __init__(
        self,
        ip_addr_str: str,
        udp_port: int,
    ):
        """ Create an exclusive lock from a loopback IP address and a UDP port. """
        try:
            ip_addr = ipaddress.ip_address(ip_addr_str)
        except ValueError as ve:
            raise SetupException(f"{ve} for {ip_addr_str}")
        if not ip_addr.is_loopback:
            raise SetupException(f"not a loopback address: {ip_addr_str}")
        if not (0 <= udp_port <= 2**16):
            raise SetupException(f"not a udp port number: {udp_port}")

        self.ip_addr_str = ip_addr_str
        self.ip_port = udp_port
        self.address_family = socket.AF_INET if ip_addr.version == 4 else socket.AF_INET6

    def try_aquire(self, max_wait_secs: int) -> bool:
        """ Try and acquire the lock by binding a socket to the address/port.
        Return True when this succeeds within max_wait_secs, False otherwise.
        """
        end_time = time.time() + max_wait_secs
        sleep_secs = 1
        while True:
            try:
                # do not reuse a socket that caused an IOError:
                sock = socket.socket(
                    family=self.address_family,
                    type=socket.SOCK_DGRAM)
                sock.bind((self.ip_addr_str, self.ip_port))
                self.sock = sock  # for release()
                return True
            except IOError as ioe:
                logger.debug(ioe)
                time.sleep(sleep_secs)
                if time.time() > end_time:
                    return False

    def release(self) -> None:
        """ Release the lock by closing the bound socket. """
        self.sock.close()
