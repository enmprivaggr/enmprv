import sys
import subprocess
from pathlib import Path
import unittest


def do_command_return_code(command: str):
    """ Wait for shell execution of `command`, use the normal stdin, stdout and stderr.
        Return the exit code of the command that is provided by the shell.
    """
    print(command)
    completion = subprocess.run(
        command,
        shell=True,
        stdin=sys.stdin,
        stdout=sys.stdout,
        stderr=sys.stderr)
    sys.stderr.flush()
    sys.stdout.flush()
    return completion.returncode


def do_command(command: str):
    """ Pass `command` to `do_command_return_code`.
        Raise an `Exception` when the returned exit code is non zero.
    """
    return_code = do_command_return_code(command)
    if return_code != 0:
        raise Exception(f"return_code {return_code} from: {command}")


class TestECDH(unittest.TestCase):
    test_data_dir = "/tmp/test_echd/"
    key_file_ext = ".pem"
    ec_curve = "brainpoolP512r1"

    def prv_key_file_name(self, name: str) -> str:
        return f"{self.test_data_dir}prv_{name}{self.key_file_ext}"

    def pub_key_file_name(self, name: str) -> str:
        return f"{self.test_data_dir}pub_{name}{self.key_file_ext}"

    def secret_file_name(self, name: str) -> str:
        return f"{self.test_data_dir}secret_{name}"

    def gen_priv_pub_key_files(self, prvkey: str, pubkey: str):
        do_command(
            f"openssl ecparam -name {self.ec_curve} -genkey -noout -out {prvkey}")
        do_command(f"openssl ec -in {prvkey} -pubout -out {pubkey}")

    def derive_ecdh_secret(self, prv1: str, pub2: str, secret: str):
        do_command(
            f"openssl pkeyutl -derive -inkey {prv1} -peerkey {pub2} -out {secret}")

    def test_ecdh_512(self):
        if not Path(self.test_data_dir).exists():
            Path(self.test_data_dir).mkdir()

        name1 = "alice"
        prv1 = self.prv_key_file_name(name1)
        pub1 = self.pub_key_file_name(name1)
        self.gen_priv_pub_key_files(prv1, pub1)
        name2 = "bob"
        prv2 = self.prv_key_file_name(name2)
        pub2 = self.pub_key_file_name(name2)
        self.gen_priv_pub_key_files(prv2, pub2)

        secret1 = self.secret_file_name(name1)
        secret2 = self.secret_file_name(name2)

        self.derive_ecdh_secret(prv1, pub2, secret1)
        self.derive_ecdh_secret(prv2, pub1, secret2)

        do_command(f"ls -lt {secret1} {secret2}")

        self.assertEqual(do_command_return_code(
            f"diff {secret1} {secret2}"), 0)


if __name__ == "__main__":
    unittest.main()
