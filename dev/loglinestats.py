#! /usr/bin/python3

"""
Show log line stats over source code lines.
"""

from collections import defaultdict


def do_main():
    fname = "../p1e-raspyp1.log.tmp"  # copied from p1 encryptor host.
    source_lines_count = defaultdict(int)
    with open(fname) as f:
        line_count = 0
        for rl in f.readlines():
            line_count += 1
            rlsplit = rl.split()
            if len(rlsplit) >= 4:
                source_lines_count[rlsplit[3]] += 1

    print(fname, line_count)
    # most frequent first:
    sl_counts = sorted(source_lines_count.items(), key=lambda sl: -sl[1])
    for sl, count in sl_counts[:20]:
        print(sl, count)


if __name__ == "__main__":
    do_main()
