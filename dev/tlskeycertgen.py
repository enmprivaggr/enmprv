#! /usr/bin/python3
import datetime
import os
from pathlib import Path
import subprocess
import sys

"""
Generate keys and certicates for use by tlsclient and tlsserver.
keys are generated in: ~/keys
certs in: ~/keys/certs
Generate:
- root key, self certified for distinguished name Tenant
- aggrmeters key as tls server, certified by Tenant to serve at host v110.fritz.box

Later, also generate:
- p1encrypt key as tls client, certified by Tenant to be client.

"""

"""
Inspired by:
https://www.golinuxcloud.com/openssl-create-client-server-certificate/

The list of steps to be followed to generate server client certificate using OpenSSL
and perform further verification using Apache HTTPS:

    Create server certificate
        Generate server key
        Generate Certificate Signing Request (CSR) with server key
        Generate and Sign the server certificate using CA key and certificate
    Create client certificate
        Generate client key
        Generate Certificate Signing request (CSR) with client key
        Generate and Sign the client certificate using CA key and certificate
    Configure Apache with SSL
    Verify openssl server client certificates

And replace the last two steps by using tlsclient.rs and tlsserver.rs instead of the web server.

"""


home = os.environ["HOME"]
keys_dir_name = home + "/keys"
certs_dir_name = keys_dir_name + "/certs"
pem_extension = ".pem"

def mtime(file_name):
    return Path.stat(Path(file_name)).st_mtime_ns

def file_exists(file_name):
    return Path.is_file(Path(file_name))

def needs_update(derived_file, source_file):
    assert file_exists(source_file)
    return (not file_exists(derived_file)) or (mtime(derived_file) < mtime(source_file))

def execute_command(cmd):
    completion = subprocess.run(cmd, shell=True)
    if completion.returncode != 0:
        raise Exception(str(completion))

def execute_command_input_text(cmd, cmd_input):
    completion = subprocess.run(cmd, shell=True, input=cmd_input, text=True)
    if completion.returncode != 0:
        raise Exception(str(completion))

def create_file_from_text(file_name, text):
    with open(file_name, "w") as f:
        assert f.write(text) == len(text)

def run_command(cmd):
    return subprocess.run(cmd, shell=True, stdin=sys.stdin, stdout=sys.stdout)

def time_command(cmd):
    start_time = datetime.datetime.now()
    completion = run_command(cmd)
    run_time = (datetime.datetime.now() - start_time).total_seconds()
    print("time_command " + str(run_time) + " secs: " + cmd)
    if completion.returncode != 0:
        raise Exception(str(completion))

def key_file_name(name_prefix, is_priv, key_size):
    p = "private" if is_priv else "public"
    return name_prefix + "-" + p + str(key_size) + pem_extension

def gen_private_key_command(private_key_file, key_size):
    return " ".join(["openssl genrsa -out", private_key_file, str(key_size)])

def gen_public_key_command(private_key_file, public_key_file):
    return " ".join(["openssl rsa -in", private_key_file, "-pubout -out", public_key_file])

def gen_certif_req_command(private_key_file, certif_req_file):
    return " ".join(["openssl req -new -key", private_key_file, "-out", certif_req_file])

def gen_cert_req_cnf_command(private_key_file, req_cfg_file, certif_req_file):
    # CHECKME: to use a private key with a password, add -passin file:mypass.enc
    return " ".join([gen_certif_req_command(private_key_file, certif_req_file), "-config", req_cfg_file])

def gen_cert_command(certif_req_file, priv_key_file, certif_file):
    # Example command
    # openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt -passin file:mypass.enc
    # leave out the days and password for now:
    # openssl x509 -req -in server.csr -signkey server.key -out server.crt -passin file:mypass.enc
    return " ".join(["openssl x509 -req -in", certif_req_file, "-signkey", priv_key_file, "-out",  certif_file])

"""
And use this as -config argument:
openssl req -new -key server.key -out server.csr -passin file:mypass.enc -config self_signed_certificate.cnf
"""

def certif_req_content(country_code, state_name, location_name, company_name, organisational_unit_name, common_name):
    return os.linesep.join([
        "[req]",
        "distinguished_name = requester_name",
        "prompt = no",
        "[requester_name]",
        "C = " + country_code,
        "ST = " + state_name,
        "L = " + location_name,
        "O = " + company_name,
        "OU = " + organisational_unit_name,
        "CN = " + common_name,
        "",
    ])

def gen_tls_keys_certs():
    print("gen_tls_key_certs")
    key_size = 3072
    cert_sign_request_extension = ".csr"
    cert_req_suffix = "_req.cnf"

    ca_root_prefix = "/ca_root"
    ca_root_key_prefix = keys_dir_name + ca_root_prefix
    ca_root_private_key_file = key_file_name(ca_root_key_prefix, True, key_size)
    ca_root_public_key_file = key_file_name(ca_root_key_prefix, False, key_size)
    ca_self_certif_req_file = ca_root_key_prefix + cert_sign_request_extension
    ca_self_cert_file = certs_dir_name + ca_root_prefix + "_self_cert" + pem_extension

    sub_ca_prefix = "/sub_ca"
    sub_ca_key_prefix = keys_dir_name + sub_ca_prefix
    sub_ca_private_key_file = key_file_name(sub_ca_key_prefix, True, key_size)
    sub_ca_public_key_file = key_file_name(sub_ca_key_prefix, False, key_size)
    sub_ca_certif_req_file = sub_ca_key_prefix + cert_sign_request_extension
    sub_ca_cert_file = certs_dir_name + sub_ca_prefix + "_cert" + pem_extension

    if not file_exists(ca_root_private_key_file):
        time_command(gen_private_key_command(ca_root_private_key_file, key_size))

    if needs_update(ca_root_public_key_file, ca_root_private_key_file):
        time_command(gen_public_key_command(ca_root_private_key_file, ca_root_public_key_file))

    country_code = "NL"
    state_name = "provincie"
    location_name = "plaats"
    company_name = "Energy Meter Privacy"
    organisational_unit_name = "ICT"
    host_name_root = "fritz.box"
    host_name = "v110.fritz.box" # the server client TCP handshake will fail if the hostname of the server cert does not match CN
    # host_name = "v110"

    if needs_update(ca_self_cert_file, ca_root_private_key_file):
        ca_self_certif_req_conf_file = ca_root_key_prefix + cert_req_suffix

        create_file_from_text(ca_self_certif_req_conf_file,
            certif_req_content(
                country_code, 
                state_name, 
                location_name, 
                company_name, 
                organisational_unit_name, 
                host_name_root))

        execute_command("cat " + ca_self_certif_req_conf_file)
        time_command(gen_cert_req_cnf_command(ca_root_private_key_file, ca_self_certif_req_conf_file, ca_self_certif_req_file))
        time_command(gen_cert_command(ca_self_certif_req_file, ca_root_private_key_file, ca_self_cert_file))


    if not file_exists(sub_ca_private_key_file):
        time_command(gen_private_key_command(sub_ca_private_key_file, key_size))

    if needs_update(sub_ca_public_key_file, sub_ca_private_key_file):
        time_command(gen_public_key_command(sub_ca_private_key_file, sub_ca_public_key_file))

    if (needs_update(sub_ca_cert_file, sub_ca_private_key_file) or
        needs_update(sub_ca_cert_file, ca_root_private_key_file)):
        sub_ca_certif_req_conf_file = sub_ca_key_prefix + cert_req_suffix
        create_file_from_text(sub_ca_certif_req_conf_file,
            certif_req_content(
                country_code, 
                state_name, 
                location_name, 
                company_name, 
                organisational_unit_name, 
                host_name))
        time_command(gen_cert_req_cnf_command(ca_root_private_key_file, sub_ca_certif_req_conf_file, sub_ca_certif_req_file))
        time_command(gen_cert_command(sub_ca_certif_req_file, ca_root_private_key_file, sub_ca_cert_file))
        # CHECKME: the expiration date of the sub_ca certificate should be
        #          before the expiration date of the root certificate, how?
        # CHECKME: how is the sub_ca public key certified here, i.e. how is the chain of trust implemented?
        # 
        raise Exception("no chain of trust, and expiration dates uncontrolled")



    return

    aggr_key_prefix = "aggrmeters01tls"
    aggr_private_key_file = key_file_name(aggr_key_prefix, True, key_size)
    aggr_public_key_file = key_file_name(aggr_key_prefix, False, key_size)
    certif_req_file = aggr_key_prefix + ".csr"


    if not file_exists(aggr_private_key_file):
        time_command(gen_private_key_command(aggr_private_key_file, key_size))

    if needs_update(aggr_public_key_file, aggr_private_key_file):
        time_command(gen_public_key_command(aggr_private_key_file, aggr_public_key_file))

    if needs_update(certif_req_file, aggr_private_key_file):
        cmd_input = os.linesep.join([
            "NL", # Country Name, 2 letter code
            "", # State or province name
            "", # locality, city
            "Energy Meter Privacy", # organization name
            "", # organisational unit, department
            host_name, # CN common name, server host name (FQDN), or person name.
            "", # email address
            "", # challenge password
            "", # optional company name
            "", # final end of line
        ])
        # alternative for cmd_input: use openssl config file
        execute_command_input_text(
            gen_certif_request_command(aggr_private_key_file, certif_req_file),
            cmd_input
        )



if __name__ == "__main__":
    gen_tls_keys_certs()
