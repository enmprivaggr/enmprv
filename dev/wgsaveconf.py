#! /usr/bin/python3

"""
For all wg interfaces generate the configuration from wg showconf every hour,
and save a new version when there is any change.
Needs sudo privileges. Works in the current directory.
This effectively monitors for ip address changes at the wg tunnel.
"""

import datetime
import time
import subprocess
import sys
import json


def do_cmd(cmd, stdout=sys.stdout, stderr=sys.stderr):
    cmd_process = subprocess.Popen(cmd.split(), stdout=stdout, stderr=stderr)
    cmd_process.wait()
    if cmd_process.returncode != 0:
        raise Exception(
            "command {cmd} non zero return code {cmd_process.returncode}")


def do_cmd_stdout_to_file(cmd, stdout_name):
    fw = open(stdout_name, "w")
    try:
        do_cmd(cmd, fw)
    finally:
        fw.close()


def do_cmd_stdout(cmd):
    return subprocess.run(cmd, shell=True, capture_output=True).stdout


def equal_files(f1, f2):
    cmd = f"diff {f1} {f2}"
    cmd_process = subprocess.Popen(
        cmd.split(), stdout=sys.stdout, stderr=sys.stderr)
    cmd_process.wait()
    if cmd_process.returncode == 0:
        return True
    if cmd_process.returncode == 1:
        return False
    raise Exception(
        f"command {cmd} non zero/one return code {cmd_process.returncode}")


def wg_link_names_list():
    def is_wg_link(intf_dict):
        link_info = intf_dict.get("linkinfo")
        return link_info and link_info.get("info_kind") == "wireguard"

    return list(map(lambda intf_dict: intf_dict.get("ifname"),
                    filter(is_wg_link,
                           json.loads(do_cmd_stdout("ip --json -d link show")))))


def do_main():
    wg_interface_names = wg_link_names_list()
    print("wg_interface_names:", wg_interface_names)
    prev_file_by_intf_name = {}
    for wg_interface in wg_interface_names:
        file_name_prefix = f"{wg_interface}-conf"
        cmd = f"ls -t {file_name_prefix}.* | head -1"
        prev_file_name = do_cmd_stdout(cmd).decode().strip()
        if prev_file_name:
            print("prev_file_name", prev_file_name)
            prev_file_by_intf_name[wg_interface] = prev_file_name

    while True:
        for wg_interface in wg_interface_names:
            now_utc = datetime.datetime.now(datetime.timezone.utc)
            print(f"checking {wg_interface} at {now_utc}")
            time_extension = "{0:4}{1:02}{2:02}{3:02}{4:02}{5:02}".format(
                now_utc.year, now_utc.month, now_utc.day,
                now_utc.hour, now_utc.minute, now_utc.second)
            file_name_prefix = f"{wg_interface}-conf"
            file_name = f"{file_name_prefix}.{time_extension}"
            do_cmd_stdout_to_file(f"wg showconf {wg_interface}", file_name)
            prev_file_name = prev_file_by_intf_name.get(wg_interface)
            if prev_file_name is None:
                prev_file_by_intf_name[wg_interface] = file_name
            elif equal_files(prev_file_name, file_name):
                do_cmd(f"rm {file_name}")
            else:
                prev_file_by_intf_name[wg_interface] = file_name

        time.sleep(3599)  # 1 hour minus 1 second

        new_wg_interface_names = wg_link_names_list()
        if new_wg_interface_names != wg_interface_names:
            print(f"new_wg_interface_names: {new_wg_interface_names}")
            wg_interface_names = new_wg_interface_names


if __name__ == "__main__":
    do_main()
