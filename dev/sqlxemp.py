"""
Perform the sqlx database manipulations to compile the rust code after a database related change.
Use this after setting up a rust development environment by setupemp.py.
This uses the same configuration as setupemp.py, and creates a log in the ~/log directory.
Use this from its normal place in a git repository, other files in the same repository are used.


The sqlx database manipulations are:

1. Prepare a local database with all table definitions, but no data in the tables.
   This database is used as the schema definitions for compilation of sqlx related rust code
   in a DATABASE_URL environment variable.
   
2. Once the code compiles and passes tests, prepare a .sqlx/ directory in the repository 
   with .json files to be checked into the repository so the sqlx related rust code can be compiled
   without the DATABASE_URL.
"""

import argparse
import logging
from pathlib import Path
import sys

from setupemputil import init_logger
from setupempconfig import SetupEmpConfig
from setupemppg import refresh_schema_db, do_sqlx_prepare_offline


logger = logging.getLogger(__name__)


def do_main(log_file_name):
    parser = argparse.ArgumentParser(
        description="Sqlx related rust development steps: refresh the schema db, or prepare the .json files",
        epilog=f"After the --prepare step, check the log at ~/log/{log_file_name} for messages about version control.")
    parser.add_argument(
        "-l",
        "--local",
        help="local toml configuration values. These add to, and override, the default",
        required=True)
    default_path = Path.cwd().joinpath(
        sys.argv[0]).parent.joinpath("setupempdefault.toml")
    parser.add_argument(
        "-d",
        "--default",
        help="default toml configuration values, defaults here to {}".format(
            default_path),
        default=default_path)
    parser.add_argument(
        "-s",
        "--schema",
        help="do a schema db refresh, and generate a .env file for compilation of sqlx related code using this db",
        action="store_true")
    parser.add_argument(
        "-p",
        "--prepare",
        help="prepare the sqlx json files from the schema db and the rust code, and remove the .env file",
        action="store_true")

    args = parser.parse_args()

    if args.schema == args.prepare:
        print("use (only) one of: --schema or --prepare")
        parser.print_help()
        sys.exit(1)

    config = SetupEmpConfig.from_files(args.local, args.default)

    if args.schema:
        refresh_schema_db(config)
    elif args.prepare:
        do_sqlx_prepare_offline(config)


if __name__ == "__main__":
    invocation = " ".join(sys.argv)
    try:
        # logs to rotating files with the same name as this script:
        log_file_name_stem = Path(sys.argv[0]).stem
        init_logger(logger, log_file_name_stem)
        logger.debug("starting: %s", invocation)
        # creates private keys and wg .conf files:
        do_main(log_file_name=f"{log_file_name_stem}.log")
        logger.debug("finished: %s", invocation)
    except Exception as exception:
        logger.critical("exception during: %s", invocation)
        logger.exception(exception)
        raise exception  # and exit non zero
