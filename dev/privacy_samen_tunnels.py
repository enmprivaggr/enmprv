# Compute 56 bits in hex for global id (40 bits) and subnet id (16 bits)
# in ipv6 local unicast addresses.

import ipaddress

def privacy_samen_as_56bits():
    # This replaces the pseudorandom generator of RFC4193, par 3.2.2
    letter_ords = list(ord(c) for c in 'privacysamen')
    lmin = min(letter_ords)
    f = max(letter_ords) - lmin + 1
    num = 0
    for n in letter_ords:
        num = num * f + (n - lmin)
    # num %= 2**40 # 40 bits instead of 56
    return num

def privacy_samen_tunnels_network():
    global_and_subnet_id = privacy_samen_as_56bits() * 2**64
    # combine this with the LUA prefix and make this an IPv6Network:
    lua_prefix_rfc4193 = 2**(128-8) * 0xfd
    base_addr = ipaddress.IPv6Address(
        lua_prefix_rfc4193 | global_and_subnet_id)
    return ipaddress.IPv6Network("{}/{}".format(base_addr, 64))


if __name__ == "__main__":
    print(privacy_samen_tunnels_network())
