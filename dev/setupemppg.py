
import logging
from pathlib import Path
import sys

from setupempconfig import SetupEmpConfig, PG_USER
from setupemputil import SetupException, do_command_no_stdout_stderr, do_command_stdout

logger = logging.getLogger("__main__." + __name__)


class PsqlCmds:

    SHOW_TABLE_NAMES_SQL = """SELECT table_name 
    FROM information_schema.tables
    WHERE table_schema='public';
    """

    PSQL_OPTIONS = (
        # output non aligned
        "--no-align",
        # no columns/headers/footers
        "--tuples-only",
        # failed SQL commands to stderr
        "--echo-errors",
    )

    def su_pg_cmd(
            self,
            psql_cmds: str) -> str:
        # double quotes for the shell around psql_cmds, pipe into psql
        return 'echo "{}" | sudo -u {} psql {}'.format(
            psql_cmds,
            PG_USER,
            " ".join(self.PSQL_OPTIONS),
        )

    def connect_db_cmd(
            self,
            db_name: str,
            psql_cmds: str) -> str:
        # connect to the database owned by current user.
        # double quotes for the shell around psql_cmds, pipe into psql
        return 'echo "{0}" | psql --dbname={1} {2}'.format(
            psql_cmds,
            db_name,
            " ".join(self.PSQL_OPTIONS),
        )

    def dotenv_path(self, repo_path: Path) -> Path:
        return repo_path.joinpath(".env")

    def dotenv_text(self, db_name: str) -> str:
        db_url = f"postgres:///{db_name}"
        return f"DATABASE_URL={db_url}"


def create_role_and_database(
    user_name: str,
    db_name: str,
) -> None:
    """ Create a user role without a database password, and create a database with the user as owner.
    The current user must be sudoer.
    """

    psql = PsqlCmds()
    create_user_cmd = "CREATE USER {0} LOGIN PASSWORD NULL;".format(
        user_name)
    do_command_no_stdout_stderr(psql.su_pg_cmd(create_user_cmd))

    list_user_cmd = "SELECT oid, rolname FROM pg_authid WHERE rolname='{0}'".format(
        user_name)
    do_command_no_stdout_stderr(psql.su_pg_cmd(list_user_cmd))

    create_db_cmd = "CREATE DATABASE {0} WITH OWNER {1};".format(
        db_name,
        user_name)
    do_command_no_stdout_stderr(psql.su_pg_cmd(
        create_db_cmd))
    show_user_dbs_cmd = """SELECT datname FROM pg_database, pg_authid
    WHERE pg_database.datdba=pg_authid.oid
      AND pg_database.datistemplate = false
      AND pg_authid.rolname='{}';
    """.format(user_name)
    do_command_no_stdout_stderr(psql.su_pg_cmd(show_user_dbs_cmd))


def drop_empty_database_and_role(
    db_name: str,
    user_name: str,
) -> None:
    """ Drop an empty database that is the only database owned by the user, 
    and then drop the user role.
    The current user must be sudoer.
    """
    # Check for presence of the user role:
    psql = PsqlCmds()
    list_user_cmd = "SELECT rolname FROM pg_authid WHERE rolname='{0}'".format(
        user_name)
    present_user_role = do_command_stdout(
        psql.su_pg_cmd(list_user_cmd)).strip()
    if present_user_role != user_name:
        logger.info("user role %s not present: %r",
                    user_name, present_user_role)
        return

    # Check for presence of db_name as owned by user_name,
    # return when this db does not exist.
    show_user_dbs_cmd = """SELECT datname FROM pg_database, pg_authid
    WHERE pg_database.datdba=pg_authid.oid
      AND pg_database.datistemplate = false
      AND pg_authid.rolname='{}';
    """.format(user_name)
    user_dbs = do_command_stdout(psql.su_pg_cmd(
        show_user_dbs_cmd)).strip().splitlines()

    if len(user_dbs) != 1:
        logger.warning("user %s has no, or too many, databases: %r",
                       user_name, user_dbs)
        return

    if user_dbs[0] != db_name:
        logger.warning("user %s has database: %s, but expected database %s",
                       user_name, user_dbs[0], db_name)
        return

    # Check for any existing table in db_name
    table_names = do_command_stdout(psql.connect_db_cmd(
        db_name,
        psql.SHOW_TABLE_NAMES_SQL)).strip().splitlines()

    logger.info("%d tables: %r", len(table_names), table_names)
    all_tables_empty = True
    for table_name in table_names:
        show_is_empty_cmd = "SELECT count(*) = 0 FROM {};".format(table_name)
        is_empty_stdout = do_command_stdout(psql.connect_db_cmd(
            db_name,
            show_is_empty_cmd)).strip()
        if is_empty_stdout == 't':
            logger.info("table %s is empty, is_empty_stdout: %r",
                        table_name, is_empty_stdout)
        else:
            logger.warning("table %s is not empty, is_empty_stdout: %r",
                           table_name, is_empty_stdout)
            all_tables_empty = False

    if not all_tables_empty:
        raise SetupException(f"some tables of user {user_name} are not empty")

    # all tables empty, drop the tables:
    for table_name in table_names:
        drop_table_cmd = "DROP TABLE {};".format(table_name)
        do_command_no_stdout_stderr(psql.connect_db_cmd(
            db_name,
            drop_table_cmd))

    # check that there are no tables:
    table_names = do_command_stdout(psql.connect_db_cmd(
        db_name,
        psql.SHOW_TABLE_NAMES_SQL)).strip().splitlines()
    if len(table_names) > 0:
        raise SetupException("failed to drop tables %r", table_names)

    # drop the db
    drop_db_cmd = "DROP DATABASE {};".format(db_name)
    do_command_no_stdout_stderr(psql.su_pg_cmd(drop_db_cmd))

    # drop the owning user
    drop_user_cmd = "DROP USER {};".format(user_name)
    do_command_no_stdout_stderr(psql.su_pg_cmd(drop_user_cmd))


class SqlxDevException(Exception):
    """ Raised here during sqlx development steps, with a description of the reason. """


def refresh_schema_db(
    config: SetupEmpConfig,
) -> None:
    # db in the local postgres cluster:
    schema_db_name = config.local_host.all_tables_db_name

    # The create table sql files for the tables.
    # These are used here to create the schema db with all tables,
    # and this schema db is used for compilation.
    # These are also included in the rust sources to create the tables for actual use.
    repo_path = Path(sys.argv[0]).parent.parent
    src_bin_path = repo_path.joinpath("src").joinpath("bin")
    schema_tables_sql_files = config.local_host.all_tables_sql_defs

    # Check that all the table definition files are readable
    for create_table_sql_file_name in schema_tables_sql_files:
        create_table_path = src_bin_path.joinpath(create_table_sql_file_name)
        try:
            create_table_path.read_text()
        except IOError:
            raise SqlxDevException("cannot read {}".format(create_table_path))

    # drop the existing tables from schema_db_name
    psql = PsqlCmds()
    existing_table_names = do_command_stdout(psql.connect_db_cmd(
        schema_db_name,
        psql.SHOW_TABLE_NAMES_SQL)).strip().splitlines()
    for table_name in existing_table_names:
        # drop the table and any foreign key referencing this table:
        drop_table_cmd = "DROP TABLE {} CASCADE;".format(table_name)
        do_command_no_stdout_stderr(
            psql.connect_db_cmd(schema_db_name, drop_table_cmd))

    # create the tables in schema_tables_sql_files in schema_db_name:
    # these must be in dependency order,
    # for example a foreign key should only be defined on an already existing primary key.
    for create_table_sql_file_name in schema_tables_sql_files:
        create_table_path = src_bin_path.joinpath(create_table_sql_file_name)
        create_table_sql = create_table_path.read_text()
        # create the table:
        do_command_no_stdout_stderr(
            psql.connect_db_cmd(schema_db_name, create_table_sql))

    # Create a .env file with DATABASE_URL=...
    # This database is used when sqlx code is compiled to verify the tables/columns.
    dotenv_path = psql.dotenv_path(repo_path)
    dotenv_text = psql.dotenv_text(schema_db_name)
    dotenv_path.write_text("{}\n".format(dotenv_text))
    logger.info("created {0} with {1}".format(dotenv_path, dotenv_text))


def do_sqlx_prepare_offline(
    config: SetupEmpConfig,
) -> None:
    repo_path = Path(sys.argv[0]).parent.parent
    # db in the local postgres cluster
    schema_db_name = config.local_host.all_tables_db_name
    psql = PsqlCmds()
    # CHECKME: also use sqlx --check option to check into version control?
    prepare_cmd = "cd {0};{1} cargo sqlx prepare -- --all-targets --all-features".format(
        repo_path,
        psql.dotenv_text(schema_db_name))
    do_command_no_stdout_stderr(prepare_cmd)

    # sqlx prepare ok: remove the .env file
    dotenv_path = psql.dotenv_path(repo_path)
    # cargo sqlx prepare above did not use the .env file
    if dotenv_path.exists():
        dotenv_path.unlink(missing_ok=False)
        logger.info("deleted {0}".format(dotenv_path))
