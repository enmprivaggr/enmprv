"""
Helper code for setupemp.py to do wireguard manipulations.
"""

import ipaddress
import json
import logging
from pathlib import Path
from typing import Dict, List, Optional

from setupempconfig import SetupEmpConfig
from setupemputil import SSH_CMD, SetupException, save_atomically
from setupemputil import do_command, do_command_no_stdout_stderr, do_command_stdout
from setupemputil import do_command_return_code, sudo_file_exists

WG_ETC_DIR_NAME = "/etc/wireguard"

ALLOWED_WG_TUNNEL_NETWORKS = (
    "10.0.0.0/8",  # ipv4 private use
    "172.16.0.0/12",  # ipv4 private use
    "192.168.0.0/16",  # ipv4 private use
    "FD00::/8",  # ipv6 local unicast address, locally assigned, RFC 4193
)


logger = logging.getLogger("__main__." + __name__)


def wg_public_key_from_private_key(priv_key_path: Path) -> str:
    """ Let wg compute a public key from a private key. """
    # avoid logging the private key:
    cmd = f"sudo bash -c 'wg pubkey < {priv_key_path}'"
    return do_command_stdout(cmd).strip()


def wg_private_key_file_path(
        wg_conf_dir_path: Path,
        wg_intf_name: str,
        wg_priv_key_extension: str,
) -> Path:
    """ File path for the private key file for wireguard interface:
        `wg_conf_dir_path`/`wg_intf_name`.`wg_priv_key_extension`
    """
    return wg_conf_dir_path.joinpath(Path(wg_intf_name).with_suffix(wg_priv_key_extension))


def generate_wg_keys(
        wg_conf_dir_path: Path,
        wg_intf_name: str,
        wg_priv_key_extension: str,
) -> str:
    """
    When the wg private key file 
    `wg_conf_dir_path`/`wg_intf_name`.`wg_priv_key_extension` does not exist,
    generate a new private key in this file.
    Return the wg public key.
    Redirect all unused stdout and stderr to the log.
    """
    assert wg_conf_dir_path.is_dir(), wg_conf_dir_path
    private_key_path = wg_private_key_file_path(
        wg_conf_dir_path,
        wg_intf_name,
        wg_priv_key_extension)
    if not private_key_path.exists():
        # generate and save the private key
        wg_private_key = do_command_stdout("wg genkey").strip()

        def save_wg_private_key(target_path: Path):
            with target_path.open('w', encoding="utf-8") as f:
                f.write(wg_private_key)

        save_atomically(save_wg_private_key, private_key_path)

        logger.debug("created %s", private_key_path)

    return wg_public_key_from_private_key(private_key_path)


def get_wg_conf_file_path(wg_intf_name: str) -> Path:
    """ Provide a path with .conf added to the interface name """
    return Path(wg_intf_name).with_suffix(".conf")


def get_wg_etc_conf_file_path(wg_intf_name: str) -> Path:
    """ The path for wg interface configuration file used by the wg daemon """
    return Path(WG_ETC_DIR_NAME).joinpath(get_wg_conf_file_path(wg_intf_name))


def active_wg_interfaces() -> list[str]:
    show_wg_intfs_cmd = "sudo wg show interfaces"
    lines = do_command_stdout(show_wg_intfs_cmd).strip().splitlines()
    return " ".join(lines).split()


def setup_wg_with_peer(
        wg_conf_dir_path: Path,
        wg_intf_name: str,
        wg_priv_key_extension: str,
        listen_port: Optional[int],
        wg_intf_address: str,
        peer_allowed_ip: str,
        peer_public_key: str,
        peer_persistent_keep_alive: Optional[int],
        peer_end_point: Optional[str],
) -> None:
    """ Use the private key in `wg_conf_dir_name`/`wg_intf_name`.privatekey
        to set up a wg tunnel device with `listen_port`.
        At the device create a tunnel peer with `peer_public_key`, `peer_allowed_ips`
        and `peer_persistent_keep_alive`.
    """
    wg_intf_is_up = wg_intf_name in active_wg_interfaces()
    if wg_intf_is_up:
        wg_intf_down_cmd = f"sudo ip link delete dev {wg_intf_name}"
        do_command(wg_intf_down_cmd)
        wg_intf_is_up = wg_intf_name in active_wg_interfaces()
        if wg_intf_is_up:
            raise SetupException("{} active after {}".format(
                wg_intf_name,
                wg_intf_down_cmd,
            ))

    assert not wg_intf_is_up, wg_intf_name

    private_key_path = wg_private_key_file_path(
        wg_conf_dir_path,
        wg_intf_name,
        wg_priv_key_extension)
    assert private_key_path.exists(), private_key_path

    wg_intf_ip_address = ipaddress.ip_address(wg_intf_address)
    if type(wg_intf_ip_address) is ipaddress.IPv4Address:
        ip_address_type_option = "-4"
    else:
        ip_address_type_option = "-6"
    prefixlen = wg_intf_ip_address.max_prefixlen

    # command series borrowed from wg-quick up for an interface that is down:
    sudo_cmds = [f"ip link add {wg_intf_name} type wireguard"]
    set_intf_cmd_parts = [f"wg set {wg_intf_name}"]
    set_intf_cmd_parts.append(f"private-key {private_key_path}")
    if listen_port is not None:
        set_intf_cmd_parts.append(f"listen-port {listen_port}")
    sudo_cmds.append(" ".join(set_intf_cmd_parts))
    sudo_cmds.append("ip {} address add {}/{} dev {}".format(
        ip_address_type_option,
        wg_intf_address,
        prefixlen,
        wg_intf_name
    ))
    # For the peer with peer_conf set the PublicKey to wg_peer_pub_key.
    # A peer public key identifies the peer in a wg interface,
    # so remove the peer with the existing public key
    # and add a new peer with the remaining existing configuration.
    set_peer_cmd_words = []
    peer_allowed_ips = f"{peer_allowed_ip}/{prefixlen}"
    set_peer_cmd_words.extend(('allowed-ips', peer_allowed_ips))
    if peer_end_point is not None:
        set_peer_cmd_words.extend(
            ('endpoint', peer_end_point))
    if peer_persistent_keep_alive is not None:
        set_peer_cmd_words.extend(
            ('persistent-keepalive', str(peer_persistent_keep_alive)))
    wg_peer_cmds = " ".join(set_peer_cmd_words)
    wg_set_pub_key_command = "wg set {0} peer {1} {2}".format(
        wg_intf_name,
        peer_public_key,
        wg_peer_cmds)
    sudo_cmds.append(wg_set_pub_key_command)
    # wg-quick does these two with one command: ip link set mtu 1420 up dev ...
    sudo_cmds.append(f"ip link set mtu 1420 dev {wg_intf_name}")
    sudo_cmds.append(f"ip link set up dev {wg_intf_name}")
    # add route after bringing the interface up:
    sudo_cmds.append("ip {} route add {}/{} dev {}".format(
        ip_address_type_option,
        peer_allowed_ip,
        prefixlen,
        wg_intf_name
    ))
    for sudo_cmd in sudo_cmds:
        do_command_no_stdout_stderr(f"sudo {sudo_cmd}")

    if wg_intf_name not in active_wg_interfaces():
        raise SetupException(f"{wg_intf_name} not active after setup")


def get_wg_unique_ip_address(
        intf_name: str,
        ssh_host: Optional[str] = None,
) -> ipaddress.IPv4Address | ipaddress.IPv6Address:
    """ From the ip interface `intf_name` provide the local address.
        When `ssh_host` is given provide the address from that host.
        An assertion error is raised when the interface has more than one local address.
    """
    ip_addr_cmd = f"ip --json --pretty addr show {intf_name}"
    if ssh_host is not None:
        ip_addr_cmd = f"{SSH_CMD} {ssh_host} {ip_addr_cmd}"
    ip_addr_json = do_command_stdout(
        ip_addr_cmd,
        # ssh may warn about closed connection
        ignore_stderr=(ssh_host is not None),
    )
    ip_addr_obj = json.loads(ip_addr_json)
    # requested a single wg_interface
    assert len(ip_addr_obj) == 1, len(ip_addr_obj)
    ip_addr_wg_intf_obj = ip_addr_obj[0]
    assert ip_addr_wg_intf_obj["ifname"] == intf_name, repr(
        ip_addr_wg_intf_obj)
    addr_info = ip_addr_wg_intf_obj['addr_info']
    # only use the first cidr here:
    assert len(addr_info) == 1, repr(addr_info)
    first_addr_info = addr_info[0]
    local_adr = first_addr_info['local']
    prefixlen = int(first_addr_info['prefixlen'])
    intf_ip_address = ipaddress.ip_address(local_adr)
    # check single address in allowed ips:
    assert intf_ip_address.max_prefixlen == prefixlen, addr_info
    return intf_ip_address


def wg_conf_from_text(wg_showconf_text: str) -> tuple[Dict[str, str], List[Dict[str, str]]]:
    """ From a wg configuration text as shown by sudo wg showconf int_name, 
        provide a tuple with a dictionary for the interface,
        and list of dictionaries for the peers.
    """
    """ Example wg showconf configuration text, with redacted private key:
[Interface]
ListenPort = 51008
PrivateKey = oAZ=

[Peer]
PublicKey = PSFyNqUZCWJ1oCu9ApjOH6wJ6ueSNsNdON8N/Drmzm4=
AllowedIPs = 10.1.0.3/32
Endpoint = 185.238.129.202:51281
PersistentKeepalive = 25

[Peer]
...
    """
    intf_dict = {}
    in_intf = False
    peer_dicts = []
    for conf_line in wg_showconf_text.splitlines():
        if not conf_line:
            continue
        if conf_line == "[Interface]":
            in_intf = True
        elif conf_line == "[Peer]":
            in_intf = False
            peer_dicts.append({})  # empty dict for next peer
        else:
            key, value = conf_line.split(" = ")
            if in_intf:
                intf_dict[key] = value
            else:
                peer_dicts[-1][key] = value
    return (intf_dict, peer_dicts)


def get_intf_pub_key_from_show(wg_show_text: str) -> str:
    """ 
    Return the wg interface public key from a text like this, 
    normally from `sudo wg show intf_name`:

interface: intf_name
  public key: MWQhAOBECquwSa0dwlgbdRZmi4Oxbjedfzre8y/tkSI=
  private key: (hidden)
...
peer: ....
...
    """
    for line in wg_show_text.splitlines():
        parts = line.split(':')
        if len(parts) != 2:
            continue
        if parts[0].find("public key") >= 0:
            return parts[1].strip()
        if parts[0].find("peer") >= 0:
            break
    raise SetupException(f"no public key in : {wg_show_text}")


def get_wg_public_keys(
        intf_name: str,
        peer_ip_addr: ipaddress.IPv4Address | ipaddress.IPv6Address,
        ssh_host: Optional[str] = None,
) -> tuple[str, str]:
    """
    Return the wg public key of the given interface `intf_name`, 
    and the wg public key used by the peer with `peer_ip_addr` allowed at the interface.
    Use the local interface, or the interface on ssh_host when this is given.
    """

    wg_show_cmd = f"sudo wg show {intf_name}"
    if ssh_host is not None:
        wg_show_cmd = f"{SSH_CMD} {ssh_host} {wg_show_cmd}"
    wg_show_text = do_command_stdout(
        wg_show_cmd,
        ignore_stderr=(ssh_host is not None),
    )
    intf_public_key = get_intf_pub_key_from_show(wg_show_text)

    wg_showconf_cmd = f"sudo wg showconf {intf_name}"
    if ssh_host is not None:
        wg_showconf_cmd = f"{SSH_CMD} {ssh_host} {wg_showconf_cmd}"
    wg_showconf_text = do_command_stdout(
        wg_showconf_cmd,
        ignore_stderr=(ssh_host is not None),
    )
    _, peer_dicts = wg_conf_from_text(wg_showconf_text)
    allowed_ips_key = "AllowedIPs"
    public_key_key = "PublicKey"
    for peer_dict in peer_dicts:
        if ((allowed_ips_key in peer_dict) and
                (public_key_key in peer_dict)):
            allowed_ips = peer_dict[allowed_ips_key]
            peer_network = ipaddress.ip_network(allowed_ips)
            if peer_network.max_prefixlen != peer_ip_addr.max_prefixlen:
                logger.warning("mixing ipv4 and ipv6")
            if peer_network.network_address == peer_ip_addr:
                peer_public_key = peer_dict[public_key_key]
                return (intf_public_key, peer_public_key)

    raise SetupException(
        "public key for {} not found in: {}".format(
            peer_ip_addr, wg_show_text
        ))


def test_tunnel_both_ways(
        local_wg_address: ipaddress.IPv4Address | ipaddress.IPv6Address,
        remote_wg_address: ipaddress.IPv4Address | ipaddress.IPv6Address,
        p1e_ssh_host: str,
        wg_intf_name: str,
) -> bool:
    """ 
    Verify that:
    - on the local host and on `p1e_ssh_host` the `wg_intf_name` is up
    - this interface uses the *_wg_address on its host, and
    - the wg peers with these addresses use the wg public key of other host.

    Also ping test a wg tunnel to `p1e_ssh_host` both ways by pinging `remote_wg_address` locally
    and `local_wg_address` from `p1e_ssh_host`.
    Return True when both ways can be pinged.
    Return False when the wg interface is not up or when a ping fails indicating no other error.
    Raise an exception for any other error encountered.
    """
    if wg_intf_name not in active_wg_interfaces():
        logger.debug("no active local wg interface %s", wg_intf_name)
        return False

    # inline active_wg_interfaces() for remote command:
    p1e_intfs_lines = do_command_stdout(
        f"{SSH_CMD} {p1e_ssh_host} sudo wg show interfaces",
        ignore_stderr=True,  # ssh may warn about closed connection
    ).strip().splitlines()
    p1e_active_interfaces = " ".join(p1e_intfs_lines).split()
    if wg_intf_name not in p1e_active_interfaces:
        logger.debug("no active wg interface %s at %s",
                     wg_intf_name, p1e_ssh_host)
        return False

    # check that the addresses are at the interfaces.
    local_intf_addr = get_wg_unique_ip_address(wg_intf_name)
    if local_intf_addr != local_wg_address:
        logger.debug("expected local interface address %s, got %s",
                     local_wg_address,
                     local_intf_addr)
        return False

    p1e_intf_addr = get_wg_unique_ip_address(wg_intf_name, p1e_ssh_host)
    if p1e_intf_addr != remote_wg_address:
        logger.debug("expected remote interface address %s, got %s, at %s",
                     remote_wg_address,
                     p1e_intf_addr,
                     p1e_ssh_host)
        return False

    # Check that mutual wg public keys are used:
    local_wg_pubkey, local_wg_peer_pubkey = get_wg_public_keys(
        wg_intf_name,
        p1e_intf_addr)
    p1e_wg_pubkey, p1e_wg_peer_pubkey = get_wg_public_keys(
        wg_intf_name,
        local_intf_addr,
        p1e_ssh_host)
    if not ((local_wg_pubkey == p1e_wg_peer_pubkey) and
            (p1e_wg_pubkey == local_wg_peer_pubkey)):
        mes = "intf/peer wg public key pairs not both equal\n:{} {}\n{} {}".format(
            local_wg_pubkey, p1e_wg_peer_pubkey,
            p1e_wg_pubkey, local_wg_peer_pubkey,
        )
        logger.debug(mes)
        return False

    # Send a ping every second, receive 2 pings, wait at most 40 secs.
    # When the wg tunnel is starting up, this may delay by PersistenKeepAlive secs, normally 25.
    ping_cmd_ip_format = "ping -c 2 -w 40 {}"
    # Exit status of ping, see man ping:
    # If ping does not receive any reply packets at all it will exit with code 1.
    # If a packet count and deadline are both specified, and fewer than count packets are received
    # by the time the deadline has arrived, it will also exit with code 1.
    # On other error it exits with code 2. Otherwise it exits with code 0.
    # This makes it possible to use the exit code to see if a host is alive or not.

    local_ping_cmd = ping_cmd_ip_format.format(remote_wg_address)
    local_exit_code = do_command_return_code(local_ping_cmd)
    if local_exit_code == 1:
        return False
    elif local_exit_code != 0:
        raise SetupException("return_code {0} from: {1}".format(
            local_exit_code, local_ping_cmd
        ))

    remote_ping_cmd = ping_cmd_ip_format.format(local_wg_address)
    # ssh -t shows ping output.
    remote_exit_code = do_command_return_code(
        f"{SSH_CMD} {p1e_ssh_host} {remote_ping_cmd}")
    if remote_exit_code == 1:
        return False
    elif remote_exit_code != 0:
        raise SetupException("return_code {0} from host: {1} command: {2}".format(
            remote_exit_code,
            p1e_ssh_host,
            remote_ping_cmd
        ))

    return True


def save_wg_config_for_restart(wg_intf_name: str) -> None:
    """ Save the current wg configuration in the file used by the wg daemon and enable this daemon. """
    wg_etc_conf_path = get_wg_etc_conf_file_path(wg_intf_name)

    if sudo_file_exists(wg_etc_conf_path):
        do_command_no_stdout_stderr(
            f"sudo wg-quick save {wg_intf_name}")  # works atomically
    else:
        wg_quick_saved = False
        try:
            # touch the .conf file to fool wg-quick into saving it:
            do_command_no_stdout_stderr(f"sudo touch {wg_etc_conf_path}")
            do_command_no_stdout_stderr(
                f"sudo wg-quick save {wg_intf_name}")  # works atomically
            wg_quick_saved = True
        finally:
            if not wg_quick_saved:
                # remove possibly empty wg_etc_conf_path
                wg_etc_conf_path.unlink(missing_ok=True)

    # save the wg systemctl directory for restart: CHECKME: is this needed?
    do_command_no_stdout_stderr(f"sudo sync {WG_ETC_DIR_NAME}")

    # Start the service when the interface is not there:
    system_ctl_cmd = "sudo systemctl"
    option_quiet = "--quiet"
    wg_intf_service = f"wg-quick@{wg_intf_name}"
    if wg_intf_name not in active_wg_interfaces():
        do_command_no_stdout_stderr("{} start {} {}".format(
            system_ctl_cmd,
            option_quiet,
            wg_intf_service,
        ))
    # Enable the wg service at next boot:
    intf_enabled_cmd = "{} is-enabled {} {}".format(
        system_ctl_cmd,
        option_quiet,
        wg_intf_service,
    )
    intf_enabled = do_command_return_code(intf_enabled_cmd) == 0
    if not intf_enabled:
        do_command_no_stdout_stderr("{} enable {} {}".format(
            system_ctl_cmd,
            option_quiet,
            wg_intf_service,
        ))


def tunnel_ip_address_errmsg(
        tunnel_addr: ipaddress.IPv4Address | ipaddress.IPv6Address
) -> None | str:
    """
    Check that a given address can be used in a wireguard tunnel here.
    Return None when ok, otherwise an error message.
    This is a restricted version of python `(IPv4Address | IPv6Address).is_private`.

    For background see:
    https://www.iana.org/assignments/iana-ipv4-special-registry/iana-ipv4-special-registry.xhtml
    https://www.iana.org/assignments/iana-ipv6-special-registry/iana-ipv6-special-registry.xhtml
    https://www.rfc-editor.org/rfc/rfc4193.html
    """
    # Use an ip_network of only tunnel_addr to allow check by subnet_of:
    tunnel_addr_network = ipaddress.ip_network(
        "{}/{}".format(tunnel_addr, tunnel_addr.max_prefixlen))
    for network in ALLOWED_WG_TUNNEL_NETWORKS:
        try:
            if tunnel_addr_network.subnet_of(ipaddress.ip_network(network)):
                return None
        except TypeError:
            # not the same ip address version
            pass
    return "{} should be from {}".format(tunnel_addr, " or ".join(ALLOWED_WG_TUNNEL_NETWORKS))


def check_setup_wg_tunnel(
        config: SetupEmpConfig,
        p1e_wg_public_key: str,
) -> None:
    """ Ping test the wg tunnel from the configuration in both directions.
        When the ping test fails set up this wg tunnel and repeat the test.
        Raise a `SetupException` in case the second test fails.
    """
    local_wg_address = ipaddress.ip_address(config.wg_tunnel.local_ip)
    remote_wg_address = ipaddress.ip_address(config.wg_tunnel.p1e_ip)

    if type(local_wg_address) is not type(remote_wg_address):
        raise SetupException(
            "tunnel cannot have the mixed ipv4/ipv6 addresses: {} {}".format(
                local_wg_address,
                remote_wg_address))

    # check the the tunnel addresses
    local_errmsg = tunnel_ip_address_errmsg(local_wg_address)
    if local_errmsg is not None:
        raise SetupException(f"local tunnel address {local_errmsg}")
    remote_errmsg = tunnel_ip_address_errmsg(remote_wg_address)
    if remote_errmsg is not None:
        raise SetupException(f"remote tunnel address {remote_errmsg}")

    if local_wg_address == remote_wg_address:
        raise SetupException(
            "tunnel cannot have the same local and remote address: {}".format(local_wg_address))
    remote_invocation = config.get_remote_sudoer_invocation()
    if test_tunnel_both_ways(
        local_wg_address,
        remote_wg_address,
        config.p1_encryptor.ssh_host,
        config.wg_tunnel.intf_name,
    ):
        logger.info("ping test passed")
    else:
        logger.debug("ping test failed, setting up wg tunnel")

        # generate local wg private key if necessary, and provide the public key:
        wg_pub_key = generate_wg_keys(
            Path.home().joinpath(config.wg_tunnel.config_dir),
            config.wg_tunnel.intf_name,
            config.wg_tunnel.priv_key_extension)

        peer_end_point = "{}:{}".format(
            config.wg_tunnel.p1e_end_point.address,
            config.wg_tunnel.p1e_end_point.port,
        )
        setup_wg_with_peer(
            wg_conf_dir_path=Path.home().joinpath(config.wg_tunnel.config_dir),
            wg_intf_name=config.wg_tunnel.intf_name,
            wg_priv_key_extension=config.wg_tunnel.priv_key_extension,
            listen_port=None,
            wg_intf_address=config.wg_tunnel.local_ip,
            peer_allowed_ip=config.wg_tunnel.p1e_ip,
            peer_public_key=p1e_wg_public_key,
            peer_persistent_keep_alive=config.wg_tunnel.local_persistent_keep_alive,
            peer_end_point=peer_end_point,
        )

        # See https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers
        # Do not use a system port or a port that can be IANA registered:
        peer_end_point_port = config.wg_tunnel.p1e_end_point.port
        assert 49152 <= peer_end_point_port <= 65535, peer_end_point_port
        # assume the port is forwarded to the other tunnel end
        do_command("{0} {1} {2} {3} {4} {5} {6} {7} {8}".format(
            remote_invocation,
            "setupwgpeer",
            config.wg_tunnel.config_dir,
            config.wg_tunnel.intf_name,
            config.wg_tunnel.priv_key_extension,
            str(peer_end_point_port),
            config.wg_tunnel.p1e_ip,
            wg_pub_key,
            config.wg_tunnel.local_ip,
        ))

        if test_tunnel_both_ways(
                local_wg_address,
                remote_wg_address,
                config.p1_encryptor.ssh_host,
                config.wg_tunnel.intf_name):
            logger.info("ping test ok after setup")
        else:
            raise SetupException(
                "ping test failed after wg tunnel setup")

    # save the new local wg config for use after restart:
    save_wg_config_for_restart(config.wg_tunnel.intf_name)
    # idem, remote:
    do_command("{0} {1} {2}".format(
        remote_invocation,
        "savewgconfig",
        config.wg_tunnel.intf_name))


def tear_down_wg_tunnel(wg_intf_name: str) -> None:
    """ 
    - disable and stop the wg-quick service for config.wg_tunnel.intf_name
    - delete the wg-quick config file from /etc/wireguard/
    - delete the ip interface config.wg_tunnel.intf_name
    - remove the directory config.wg_tunnel.config_dir from the home directory.
    """
    # Undo save_wg_config_for_restart():
    # Stop the service
    system_ctl_cmd = "sudo systemctl"
    option_quiet = "--quiet"
    wg_intf_service = f"wg-quick@{wg_intf_name}"
    if wg_intf_name in active_wg_interfaces():
        do_command_no_stdout_stderr("{0} stop {1} {2}".format(
            system_ctl_cmd,
            option_quiet,
            wg_intf_service,
        ))
    # Disable the wg service at next boot:
    intf_enabled_cmd = "{0} is-enabled {1} {2}".format(
        system_ctl_cmd,
        option_quiet,
        wg_intf_service,
    )
    intf_enabled = do_command_return_code(intf_enabled_cmd) == 0
    if intf_enabled:
        do_command_no_stdout_stderr("{0} disable {1} {2}".format(
            system_ctl_cmd,
            option_quiet,
            wg_intf_service,
        ))

    if wg_intf_name in active_wg_interfaces():
        do_command_no_stdout_stderr(
            "sudo ip link delete dev {}".format(wg_intf_name))

    wg_etc_conf_path = get_wg_etc_conf_file_path(wg_intf_name)
    if sudo_file_exists(wg_etc_conf_path):
        do_command_no_stdout_stderr(f"sudo rm {wg_etc_conf_path}")
    # sync the deletion for reboot:
    do_command_no_stdout_stderr(f"sudo sync {WG_ETC_DIR_NAME}")
