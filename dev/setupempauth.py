""" Handle openssl private/public keys for authorisations """

import os
from pathlib import Path

from setupempconfig import SetupEmpConfig
from setupemputil import SetupException, XLockSocket
from setupemputil import do_command, do_command_no_stdout_stderr, save_atomically

OPENSSL_CMD = "openssl"


def dev_key_file_name(
    user_name: str,
    is_private_key: bool,
    key_num: int,
) -> str:
    private_or_public = "private" if is_private_key else "public"
    return "dev-{0}-{1}-{2}.pem".format(
        user_name,
        private_or_public,
        key_num)


def generate_ec_key_pairs(
    keys_dir_name: str,
    ec_curve_name: str,
    lock_ip_addr: str,
    lock_ip_port: int,
    num_key_pairs: int,
) -> None:
    """
    Generate an openssl private/public key pair for the given elleptic curve `ec_curve_name`
    into the directory ~/`keys_dir_name` of the current user.
    For the current user `user_name`, the following file names will be used:
    - dev-`user_name`-private.pem
    - dev-`user_name`-public.pem

    In case `num_key_pairs` is bigger than 1, the following file names are also used,
    with 1 <= `n`  <= `num_key_pairs - 1`:
    - dev-`user_name`-private-`n`.pem
    - dev-`user_name`-public-`n`.pem

    Generation is skipped when these files already exist.
    When this function returns normally, the key pair files 
    are retrievable after a crash or reboot.
    """
    keys_dir_path = Path.home().joinpath(keys_dir_name)
    if not keys_dir_path.exists():
        os.mkdir(keys_dir_path)

    user_name = os.getenv("USER")
    if user_name is None:
        raise SetupException(
            "no USER environment variable available")  # non debian?

    for key_pair_num in range(1, num_key_pairs + 1):

        priv_key_path = keys_dir_path.joinpath(dev_key_file_name(
            user_name=user_name,
            is_private_key=True,
            key_num=key_pair_num))

        pub_key_path = keys_dir_path.joinpath(dev_key_file_name(
            user_name=user_name,
            is_private_key=False,
            key_num=key_pair_num))

        # obtain an exclusive lock by binding the socket
        lock = XLockSocket(lock_ip_addr, lock_ip_port)
        if not lock.try_aquire(max_wait_secs=10):
            raise SetupException(
                "Failed to acquire lock for key pair generation")

        try:
            if not priv_key_path.exists():
                # an existing public key will be invalidated by the new private key:
                pub_key_path.unlink(missing_ok=True)

                def open_ssl_gen_priv_key(target_path: Path) -> None:
                    do_command_no_stdout_stderr(
                        "{0} ecparam -name {1} -genkey -out {2}".format(
                            OPENSSL_CMD,
                            ec_curve_name,
                            target_path))

                # this also syncs the removal of the public key from the same directory:
                save_atomically(open_ssl_gen_priv_key, priv_key_path)

            if not pub_key_path.exists():
                def open_ssl_gen_pub_key(target_path: Path) -> None:
                    do_command_no_stdout_stderr(
                        "{0} ec -in {1} -pubout -out {2}".format(
                            OPENSSL_CMD,
                            priv_key_path,
                            target_path))

                save_atomically(open_ssl_gen_pub_key, pub_key_path)
        finally:
            lock.release()


def generate_authorization_keys(
        config: SetupEmpConfig,
        remote_p1e_invocation: str,
) -> None:
    p1e_num_key_pairs = config.p1_encryptor.simulated_meter_sets + 1
    if p1e_num_key_pairs < 2:
        raise SetupException("p1_encryptor.simulated_meter_sets is too small: {}".format(
            config.p1_encryptor.simulated_meter_sets
        ))

    do_command("{0} {1} {2} {3} {4} {5} {6}".format(
        remote_p1e_invocation,
        "generateeckeypairs",
        config.user_auth_keys.keys_dir,
        config.user_auth_keys.elliptic_curve,
        config.excl_socket_lock.address,
        config.excl_socket_lock.port,
        p1e_num_key_pairs,
    ))

    num_key_pairs = 1  # for both key_adder_user and prv_aggr_user
    subprocess_genkeypair_args = [
        "generateeckeypairs",
        config.user_auth_keys.keys_dir,
        config.user_auth_keys.elliptic_curve,
        config.excl_socket_lock.address,
        str(config.excl_socket_lock.port),
        str(num_key_pairs)]
    for user_name in (
            config.local_host.key_adder_user,
            config.local_host.prv_aggr_user):
        do_command(config.get_local_user_invocation(
            user_name=user_name,
            python_args=subprocess_genkeypair_args,
        ))

    # In case this becomes necessary:
    # Generate authorization keys for local test runs of p1_encryptor.
    # The current user name and the p1_encryptor user name must differ,
    # otherwise the key file names will conflict:
    #
    # local_user_name = os.getenv("USER")
    # assert local_user_name != config.p1_encryptor.user, local_user_name
    # generate_ec_key_pairs(
    #     config.user_auth_keys.keys_dir,
    #     config.excl_socket_lock.address,
    #     config.excl_socket_lock.port,
    #     config.p1_encryptor.simulated_meter_sets + 1,
    # )


def distribute_public_authorization_keys(config: SetupEmpConfig) -> None:
    """ Make all public authorization keys available to each user.

        For remote keys, copying is done via directory `config.user_auth_keys.keys_dir`
        in the home directory. This directory is created in case it does not exist.
    """

    rsync_cmd = "{} {} {}".format(
        # sudo rsync is not available for ssh access of remote host:
        "rsync",
        # use only content checksum to skip files,
        # i.e. always check the content, because we are copying key files:
        "--checksum",
        # keep modification times, both hosts have been checked here to be ntp synced:
        "--times",
    )
    # for local use, keep modification time.
    sudo_rsync_cmd = f"sudo {rsync_cmd}"
    sudo_chown_cmd = "sudo chown"

    keys_dir_path = Path(config.user_auth_keys.keys_dir)

    # use rsync always to/from local home keys directory, otherwise a sudo rsync is needed:
    home_keys_dir_path = Path.home().joinpath(keys_dir_path)
    home_keys_dir_path.mkdir(exist_ok=True)

    # Public keys of p1_encryptor.user to key_adder_user and prv_aggr_user:
    for p1e_pub_key_path in [
        keys_dir_path.joinpath(dev_key_file_name(
            user_name=config.p1_encryptor.user,
            is_private_key=False,
            key_num=key_num)
        )
        for key_num in range(1, config.p1_encryptor.simulated_meter_sets + 2)
    ]:
        for target_user in (
                config.local_host.key_adder_user,
                config.local_host.prv_aggr_user):
            # rsync pub_key_path from config.p1_encryptor.ssh_host
            # via home_keys_dir_path,
            # to ~target_user/keys_dir_path on local host,
            # and chown to ~target_user.
            via_path = Path.home().joinpath(p1e_pub_key_path)
            cmd = "{0} {1}@{2}:{3} {4}".format(
                rsync_cmd,
                config.p1_encryptor.user,
                config.p1_encryptor.ssh_host,
                p1e_pub_key_path,
                via_path,
            )
            do_command(cmd)
            target_path = Path(f"~{target_user}").joinpath(p1e_pub_key_path)
            cmd = "{0} {1} {2}".format(
                sudo_rsync_cmd,
                via_path,
                target_path,
            )
            do_command(cmd)
            # change owner to target_user and group:
            cmd = "{0} {1}:{1} {2}".format(
                sudo_chown_cmd,
                target_user,
                target_path,
            )
            do_command(cmd)

    # Public key of key_adder_user to p1_encryptor.user:
    ka_pub_key_path = keys_dir_path.joinpath(dev_key_file_name(
        user_name=config.local_host.key_adder_user,
        is_private_key=False,
        key_num=1,
    ))
    from_path = Path(f"~{config.local_host.key_adder_user}").joinpath(
        ka_pub_key_path)
    via_path = Path.home().joinpath(ka_pub_key_path)
    cmd = "{} {} {}".format(
        sudo_rsync_cmd,  # local copy
        from_path,
        via_path,
    )
    do_command(cmd)
    # via_path is owned by root, change owner to current user:
    current_user = os.getenv("USER")
    assert current_user is not None
    cmd = "{0} {1}:{1} {2}".format(
        sudo_chown_cmd,
        current_user,
        via_path,
    )
    do_command(cmd)

    cmd = "{0} {1} {2}@{3}:{4}".format(
        rsync_cmd,  # copy to remote
        via_path,
        config.p1_encryptor.user,
        config.p1_encryptor.ssh_host,
        ka_pub_key_path,
    )
    # remote rsync also sets the owner to config.p1_encryptor.user
    do_command(cmd)

    # Public key of key_adder_user to prv_aggr_user (skip the local keys directory)
    cmd = "{0} ~{1}/{2} ~{3}/{4}".format(
        sudo_rsync_cmd,  # local mode
        config.local_host.key_adder_user,
        ka_pub_key_path,
        config.local_host.prv_aggr_user,
        ka_pub_key_path,
    )
    do_command(cmd)
    # change owner to prv_aggr_user:
    cmd = "{0} {1}:{1} ~{1}/{2}".format(
        sudo_chown_cmd,
        config.local_host.prv_aggr_user,
        ka_pub_key_path,
    )
    do_command(cmd)

    # Public key of prv_aggr_user via home_keys_dir_path to p1_encryptor.user
    ag_pub_key_path = keys_dir_path.joinpath(dev_key_file_name(
        user_name=config.local_host.prv_aggr_user,
        is_private_key=False,
        key_num=1,
    ))
    from_path = Path(f"~{config.local_host.prv_aggr_user}").joinpath(
        ag_pub_key_path)
    via_path = Path.home().joinpath(ag_pub_key_path)
    cmd = "{} {} {}".format(
        sudo_rsync_cmd,  # local copy
        from_path,
        via_path,
    )
    do_command(cmd)

    # via_path is owned by root, change owner to current user:
    cmd = "{0} {1}:{1} {2}".format(
        sudo_chown_cmd,
        current_user,
        via_path,
    )
    do_command(cmd)

    cmd = "{0} {1} {2}@{3}:{4}".format(
        rsync_cmd,  # copy to remote
        via_path,
        config.p1_encryptor.user,
        config.p1_encryptor.ssh_host,
        ag_pub_key_path,
    )
    # rsync also sets owner to p1_encryptor.user
    do_command(cmd)

    # Public key of prv_aggr_user to key_adder_user (skip local keys directory)
    cmd = "{0} ~{1}/{2} ~{3}/{4}".format(
        sudo_rsync_cmd,  # in local mode
        config.local_host.prv_aggr_user,
        ag_pub_key_path,
        config.local_host.key_adder_user,
        ag_pub_key_path,
    )
    do_command(cmd)
    # change owner to key_adder_user:
    cmd = "{0} {1}:{1} ~{1}/{2}".format(
        sudo_chown_cmd,
        config.local_host.key_adder_user,
        ag_pub_key_path,
    )
    do_command(cmd)
