

def exception(message):
    raise Exception(message)


def get_meter_readings(equipment_ean_combi, msrmnt_type, interval_border, start_dt, end_dt):
    # import from meterplots.py when actually needed:
    get_meter_readings_by_type(((msrmnt_type, equipment_ean_combi),), interval_border, start_dt, end_dt)[msrmnt_type]

def plot_diff_kW_kWh():
    # untested, still needs arguments.
    # difference between kW and kWh values, check meter accuracy
    interval_border = IntervalBorder.Second
    meterreadings_kW = get_meter_readings(el_equipment_ean_combi, MeasurementType.El_kW, interval_border, start_dt, end_dt)
    meterreadings_kWh_to = get_meter_readings(el_equipment_ean_combi, MeasurementType.El_kWh_to, interval_border, start_dt, end_dt)
    meterreadings_kWh_by = get_meter_readings(el_equipment_ean_combi, MeasurementType.El_kWh_by, interval_border, start_dt, end_dt)
    meterreadings_kWh = [
                    (mbdt, mtval - mbval) if mbdt == mtdt else exception("unequal kWh dts: {} {}".format(mbdt, mtdt))
                    for ((mbdt, mbval), (mtdt, mtval)) in zip(meterreadings_kWh_by, meterreadings_kWh_to)]
    meterreadings_diff_kW_kWh = []
    prev_mdt = None
    prev_kWh = None
    total_diff_kWh = 0
    for (mdt_kWh, kWh_value) in meterreadings_kWh:
        (mdt_kW, kW_value) = meterreadings_kW.pop(0)
        if mdt_kW != mdt_kWh:
            exception("unequal kW and kWh dts: {} {}".format(mdt_kW, mdt_kWh))
        if prev_mdt is not None:
            deltat = (mdt_kW - prev_mdt) / one_hour_delta
            kWh_from_kW = float(prev_kWh) + deltat * float(kW_value)
            diff_kWh = kWh_from_kW - float(kWh_value)
            total_diff_kWh += diff_kWh
            # print("prev_mdt {} mdt_kW {} deltat {} prev_kWh {} kWh_from_kW {}".format(prev_mdt, mdt_kW, deltat, prev_kWh, kWh_from_kW))
            meterreadings_diff_kW_kWh.append((mdt_kW, total_diff_kWh))
        prev_mdt = mdt_kW
        prev_kWh = kWh_value
    print("total_diff_kWh {}".format(total_diff_kWh))
    scale = 1
    plot_values(meterreadings_diff_kW_kWh, ax1, legends, scale, colours[6], "diff_kW_kWh")

