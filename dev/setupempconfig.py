""" Define the configuration attributes and read them from a toml file. """

import ipaddress
from pathlib import Path
import sys
import tomllib
from typing import Any, Dict, List, Tuple

from setupemputil import SSH_CMD, SetupException

PG_USER = "postgres"  # avoid circular import from setupemppg.py


class FromDict:
    config_attrs: Tuple[str] = ()

    def check_values(self) -> None:
        """ Override to check the assigned values and raise an error when not ok. """

    def set_from_dict(self, values_by_keys: Dict[str, Any]) -> None:
        """ Check that the keys in `values_by_keys` coincide with the attributes in `config_attrs`.
            For each corresponding attribute 
            - recurse when the initial value implements `FromDict`, otherwise
            - change the initial value to the given value from `values_by_keys`,
            except when:
              -- the type of the given value is not the type of the initial value, or
              -- the given value is an empty string or contains whitespace.
        """
        assert len(self.config_attrs) > 0  # must be overriden in subclass
        for exp_key in self.config_attrs:
            if exp_key not in values_by_keys:
                raise SetupException(f"{self!r} missing key {exp_key}")
        for (key, value) in values_by_keys.items():
            if key not in self.config_attrs:
                raise SetupException(f"{self!r} unknown key {key}")
            try:
                init_value = getattr(self, key)
            except AttributeError:
                raise SetupException(
                    f"{self!r} attribute {key} should have been set by __init__")
            if isinstance(init_value, FromDict):
                init_value.set_from_dict(value)
            else:
                if type(init_value) is not type(value):
                    raise SetupException(
                        "{0} value {1} for attribute {2} expected type {3}, actual type: {4}".format(
                            repr(self),
                            repr(value),
                            key,
                            type(init_value),
                            type(value)))
                if isinstance(value, str):
                    if len(value) == 0:
                        raise SetupException(
                            f"{self!r} value for attribute {key} is an empty string: {value!r}")
                    for c in value:
                        if c.isspace():
                            raise SetupException(
                                f"{self!r} value for attribute {key} contains whitespace: {value!r}")
                setattr(self, key, value)
        self.check_values()


class P1EncryptorConfig(FromDict):
    config_attrs = (
        "ssh_host",
        "python_cmd",
        "apt_packages",
        "p1_terminal",
        "user",
        "simulated_meter_sets",
        "db_name",
    )

    def __init__(self):
        self.ssh_host = ''
        self.python_cmd = ''
        self.apt_packages: list[str] = []
        self.p1_terminal = ''
        self.user = ''
        self.simulated_meter_sets = -1
        self.db_name = ''

    def check_values(self):
        assert self.simulated_meter_sets >= 1  # for development


class IpAddressPortConfig(FromDict):
    config_attrs = (
        "address",
        "port",
    )

    def __init__(self):
        self.address = ''
        self.port = -1

    def check_values(self):
        ipaddress.ip_address(self.address)  # raise a value error when not ok.
        assert 0 <= self.port < 2**16, self.port


class WgTunnelConfig(FromDict):
    config_attrs = (
        "intf_name",
        "config_dir",
        "priv_key_extension",
        "local_ip",
        "local_persistent_keep_alive",
        "p1e_end_point",
        "p1e_ip",
    )

    def __init__(self):
        self.intf_name = ''
        self.config_dir = ''
        self.priv_key_extension = ''
        self.local_ip = ''
        self.local_persistent_keep_alive = -1
        self.p1e_end_point = IpAddressPortConfig()
        self.p1e_ip = ''

    def check_values(self):
        ipaddress.ip_address(self.local_ip)  # raise a value error when not ok
        assert 0 <= self.local_persistent_keep_alive <= 60  # normally 25
        ipaddress.ip_address(self.p1e_ip)  # raise a value error when not ok


class LocalHostConfig(FromDict):
    config_attrs = (
        "key_adder_user",
        "prv_aggr_user",
        "all_tables_db_name",
        "all_tables_sql_defs",
    )

    def __init__(self):
        self.key_adder_user = ''
        self.prv_aggr_user = ''
        self.all_tables_db_name = ''
        self.all_tables_sql_defs: list[str] = []


class UserAuthKeysConfig(FromDict):
    config_attrs = (
        "keys_dir",
        "elliptic_curve",
    )

    def __init__(self):
        self.keys_dir = ''
        self.elliptic_curve = ''  # could check_value() against openssl ecparam --list_curves


class SetupEmpConfig(FromDict):
    """ Get configuration values from a toml file """
    config_attrs = (
        "enmprv_home",
        "apt_packages",
        "excl_socket_lock",
        "local_host",
        "p1_encryptor",
        "wg_tunnel",
        "user_auth_keys",
    )

    def __init__(self):
        self.enmprv_home = ''
        self.apt_packages: list[str] = []
        self.excl_socket_lock = IpAddressPortConfig()
        self.local_host = LocalHostConfig()
        self.p1_encryptor = P1EncryptorConfig()
        self.wg_tunnel = WgTunnelConfig()
        self.user_auth_keys = UserAuthKeysConfig()

    @staticmethod
    def from_files(
        pref_file_name: str,
        defaults_file_path: Path | None,
    ) -> 'SetupEmpConfig':
        config = SetupEmpConfig()
        config.set_from_toml(
            Path(pref_file_name),
            defaults_file_path,
        )
        return config

    def set_from_toml(
            self,
            pref_toml_path: Path,
            defaults_toml_path: Path | None
    ) -> None:
        """ Parse hash tables with configuration values 
            from toml files `pref_toml_path` and `defaults_toml_path`.
            Configuration values defined in `pref_toml_path` 
            will override values from `defaults_toml_path`.

            The union of the toml keys should coincide with `config_attrs`.
            This method will set the corresponding python attributes on `self`.
        """
        with open(pref_toml_path, 'rb') as f:
            pref_dict = tomllib.load(f)
        if defaults_toml_path is None:
            self.set_from_dict(pref_dict)
        else:
            with open(defaults_toml_path, 'rb') as f:
                defaults_dict = tomllib.load(f)
            self.set_from_dict(SetupEmpConfig.merge_dicts(
                pref_dict, defaults_dict))

    @staticmethod
    def merge_dicts(
        pref_dict: Dict[str, Any],
        defaults_dict: Dict[str, Any],
    ) -> Dict[str, Any]:
        res = {}
        for (pref_key, pref_val) in pref_dict.items():
            if type(pref_val) is not type({}):
                res[pref_key] = pref_val
            elif pref_key not in defaults_dict:
                res[pref_key] = pref_val
            else:
                defaults_val = defaults_dict[pref_key]
                if type(defaults_val) is type({}):
                    res[pref_key] = SetupEmpConfig.merge_dicts(
                        pref_val, defaults_val)
                else:
                    raise SetupException(
                        f"key {pref_key!r} cannot have default value {defaults_val!r}")
        for (default_key, default_val) in defaults_dict.items():
            if default_key not in res:
                res[default_key] = default_val
        return res

    def check_user_names(
        self,
        local_sudoer_name: str,
        p1e_host_sudoer_name: str,
    ) -> None:
        enmprv_user_names = (
            self.p1_encryptor.user,
            self.local_host.key_adder_user,
            self.local_host.prv_aggr_user,
        )
        names_set = set().union(enmprv_user_names)
        if len(names_set) != 3:
            raise SetupException(
                "user names are not all 3 different: {}".format(enmprv_user_names))
        not_allowed_names = set().union((
            "root",
            PG_USER,
            # these two names may be equal:
            local_sudoer_name,
            p1e_host_sudoer_name,
        ))
        for user_name in enmprv_user_names:
            if not (user_name.islower() and user_name.isalpha()):
                raise SetupException(
                    "non alpha/lowercase user name: {!r}".format(user_name))
            if user_name in not_allowed_names:
                raise SetupException(
                    "user name {!r} is among the not allowed names: {!r}".format(
                        user_name,
                        not_allowed_names))

    sub_process_script_name = "setupemp_ppc.py"

    def get_remote_sudoer_invocation(self) -> str:
        """ Provide the ssh command to invoke setupemp_ppc.py on the remote host
            as the sudo user on the raspberry that is configured as
            p1e_ssh_host.
            This script is assumed to be copied into the configured wg_tunnel.config_dir
            in the sudoer home directory on the p1encryptor.
        """
        remote_script_path = Path(
            self.wg_tunnel.config_dir,
            self.sub_process_script_name,
        )
        remote_invocation = "{0} {1} {2} {3}".format(
            SSH_CMD,
            self.p1_encryptor.ssh_host,
            self.p1_encryptor.python_cmd,
            remote_script_path,
        )
        return remote_invocation

    def get_remote_p1e_invocation(self, sudoer_home_dir_name: str) -> str:
        """ Provide the ssh command to invoke setupemp_ppc.py on the p1_encryptor.ssh_host
            as the p1_encryptor.user.
            This script is assumed to be copied into the configured wg_tunnel.config_dir
            in the sudoer home directory on p1_encryptor.ssh_host .
        """
        remote_script_path = Path(
            sudoer_home_dir_name,
            self.wg_tunnel.config_dir,
            self.sub_process_script_name,
        )
        remote_p1e_invocation = "{0} {1}@{2} {3} {4} --subprocess".format(
            SSH_CMD,
            self.p1_encryptor.user,
            self.p1_encryptor.ssh_host,
            self.p1_encryptor.python_cmd,
            remote_script_path,
        )
        return remote_p1e_invocation

    def get_local_user_invocation(
        self,
        user_name: str,
        python_args: List[str],
    ) -> str:
        allowed_users = (
            self.local_host.key_adder_user,
            self.local_host.prv_aggr_user,
        )
        assert user_name in allowed_users, user_name
        local_python = sys.orig_argv[0]
        # full path to this script, python will be started in user_name home:
        invocation = "sudo su {0} --shell=/bin/bash -c '{1} {2} {3}'".format(
            user_name,
            local_python,
            self.sub_process_script_name,
            " ".join(python_args)
        )
        return invocation
