#! /usr/bin/python3

"""
Show P1 measurements as present in a postgres db.
"""

from enum import Enum
from itertools import pairwise  # python 3.10

import logging

from datetime import datetime, timedelta, timezone
from pathlib import Path
import sys
from matplotlib.axes import Axes
import matplotlib.pyplot as plt

import warnings
import argparse

from setupemputil import do_command, do_command_stdout, init_logger

logger = logging.getLogger(__name__)


remote_host = "raspyp1"
remote_user = "mmenc"
remote_db = "p1encdb"


class RemotePsql:
    PSQL_OPTIONS = (
        # output non aligned
        "--no-align",
        # no columns/headers/footers
        "--tuples-only",
        # failed SQL commands to stderr
        "--echo-errors",
    )

    @staticmethod
    def sql_timestamp_sec(dt_utc: datetime):
        return "(timestamp '{0:0>4}-{1:0>2}-{2:0>2} {3:0>2}:{4:0>2}:{5:0>2}')".format(
            dt_utc.year,
            dt_utc.month,
            dt_utc.day,
            dt_utc.hour,
            dt_utc.minute,
            dt_utc.second,
        )

    @staticmethod
    def record_lines(sql_query: str) -> list[str]:
        cmd = 'echo "{}" | ssh {}@{} psql --dbname={} {}'.format(
            sql_query,
            remote_user,
            remote_host,
            remote_db,
            " ".join(RemotePsql.PSQL_OPTIONS),
        )
        return do_command_stdout(cmd).strip().splitlines()

    # split lines into columns, should not be used in retrieved values.
    FIELD_SEP = "|"


ONE_SECOND_DELTA = timedelta(seconds=1)
FIVE_SECOND_DELTA = timedelta(seconds=5)
TEN_SECOND_DELTA = timedelta(seconds=10)
FIFTEEN_SECOND_DELTA = timedelta(seconds=15)
ONE_MINUTE_DELTA = timedelta(minutes=1)
ONE_QUARTER_DELTA = timedelta(minutes=15)
ONE_HOUR_DELTA = timedelta(hours=1)
ONE_DAY_DELTA = timedelta(days=1)
ONE_MONTH_DELTA = timedelta(days=30, hours=11)  # not exact
ONE_YEAR_DELTA = timedelta(days=365, hours=6)  # not exact

# for kW scale on Y axis: msrmnt values are in kWh and m3.
SECS_PER_HOUR = 3600
KWH_PER_M3 = 9.769
HOURS_PER_DAY = 24


class P1MeasurementType(Enum):
    # See rust bincoded::P1MeasurementType
    # This is stored as a column value with each measurement
    NgNl_m3 = 0   # Natural Gas, NL, cumulative, 9.769 kWh per normalized m3.
    El_kW_to = 1  # delivered to the meter from the grid
    El_kW_by = 2  # delivered by meter to the grid
    El_kWh_to_tariff_1 = 3
    El_kWh_to_tariff_2 = 4
    El_kWh_by_tariff_1 = 5
    El_kWh_by_tariff_2 = 6
    El_voltage_l1 = 7
    El_voltage_l2 = 8
    El_voltage_l3 = 9
    El_current_l1 = 10
    El_current_l2 = 11
    El_current_l3 = 12

    @staticmethod
    def from_value(value):
        return P1MeasurementType.by_value[value]


# cannot assign when defined in class scope.
P1MeasurementType.by_value = dict()
# need defined class to iterate for initialisation:
for msrmnt_type in P1MeasurementType:
    P1MeasurementType.by_value[msrmnt_type.value] = msrmnt_type

CumulativeP1MeasurementTypes = {
    P1MeasurementType.NgNl_m3,
    P1MeasurementType.El_kWh_to_tariff_1,
    P1MeasurementType.El_kWh_to_tariff_2,
    P1MeasurementType.El_kWh_by_tariff_1,
    P1MeasurementType.El_kWh_by_tariff_2,
}


class IntervalBorder(Enum):
    # See rust p1db::IntervalType
    # This attribute is stored with each pair of consecutive measurements
    # to indicate that the interval between this pair contains
    # the border of the longest such interval:
    YearUtc = 0
    YearLocal = 1
    MonthUtc = 2
    MonthLocal = 3
    DayUtc = 4
    DayLocal = 5
    Hour = 6
    Quarter = 7
    Minute = 8
    TenSecond = 9
    Second = 10

    def from_value(value):
        return IntervalBorder.by_value[value]


IntervalBorder.by_value = dict()  # cannot assign when defined in class scope.
# need defined class to iterate for initialisation:
for interval_border in IntervalBorder:
    IntervalBorder.by_value[interval_border.value] = interval_border


def filter_meter_readings(
    dt_value_pairs: list[tuple[datetime, float]],
    max_interval_border: IntervalBorder,
) -> list[tuple[datetime, float]]:
    if (len(dt_value_pairs) <= 2) or (max_interval_border == IntervalBorder.Second):
        return dt_value_pairs

    # for intervals bigger than TenSecond, remove the msrmnts starting the next interval
    min_delta = {
        IntervalBorder.YearUtc: ONE_HOUR_DELTA,
        IntervalBorder.YearLocal: ONE_HOUR_DELTA,
        IntervalBorder.MonthUtc: ONE_HOUR_DELTA,
        IntervalBorder.MonthLocal: ONE_HOUR_DELTA,
        IntervalBorder.DayUtc: ONE_HOUR_DELTA,
        IntervalBorder.DayLocal: ONE_HOUR_DELTA,
        IntervalBorder.Hour: ONE_HOUR_DELTA - FIFTEEN_SECOND_DELTA,
        IntervalBorder.Quarter: FIFTEEN_SECOND_DELTA,
        IntervalBorder.Minute: FIFTEEN_SECOND_DELTA,
        IntervalBorder.TenSecond: FIVE_SECOND_DELTA,
        IntervalBorder.Second: ONE_SECOND_DELTA,
    }[max_interval_border]
    res = dt_value_pairs[0:1]  # keep first msrmnt
    prev_dt = dt_value_pairs[0][0]
    for (dt, val) in dt_value_pairs[1:-1]:
        if (dt - prev_dt) > min_delta:
            res.append((dt, val))
            prev_dt = dt
    res.append(dt_value_pairs[-1])  # keep last msrmnt
    return res


def get_meter_readings_by_types(
    msrmnt_types: set[P1MeasurementType],
    max_interval_border: IntervalBorder,
    start_dt_utc: datetime,
    end_dt_utc: datetime,
) -> dict[tuple[int, P1MeasurementType], list[tuple[datetime, float]]]:
    start_query_dt = datetime.now()
    msrmnt_type_values = {msrmnt_type.value for msrmnt_type in msrmnt_types}

    rpsql = RemotePsql()
    sql_query = \
        """SELECT local_meter_nr, msrmnt_type, msrmnt_dt_utc, msrmnt_value
FROM measurement
WHERE {start_dt} <= msrmnt_dt_utc AND msrmnt_dt_utc <= {end_dt}
  AND msrmnt_type IN ({msrmnt_type_values})
  AND interval_border <= {max_interval_border};""".format(
            start_dt=rpsql.sql_timestamp_sec(start_dt_utc),
            end_dt=rpsql.sql_timestamp_sec(end_dt_utc),
            msrmnt_type_values=", ".join(map(str, msrmnt_type_values)),
            max_interval_border=str(max_interval_border.value),
        )
    sql_query = " ".join(sql_query.splitlines())
    outlines = rpsql.record_lines(sql_query)
    end_query_dt = datetime.now()
    elapsed = (end_query_dt - start_query_dt).total_seconds()
    print("Selected {0} {1} measurements from {3} to {4} UTC, in {2} secs".format(
        len(outlines),
        max_interval_border.name,
        elapsed,
        start_dt_utc.replace(tzinfo=None),
        end_dt_utc.replace(tzinfo=None),
    ))
    logger.debug("retrieved %d msrmnts", len(outlines))
    # local_meter_nr, msrmnt_type, msrmnt_dt_utc, msrmnt_value:
    meter_nr_type_dt_value_quads: list[tuple[int,
                                             int, datetime, float]] = []
    for l1 in outlines:
        local_meter_nr, msrmnt_type, msrmnt_dt_utc, msrmnt_value = l1.split(
            rpsql.FIELD_SEP)
        meter_nr_type_dt_value_quads.append((
            int(local_meter_nr),
            int(msrmnt_type),
            datetime.fromisoformat(msrmnt_dt_utc),
            float(msrmnt_value),
        ))

    res: dict[tuple[int, P1MeasurementType],
              list[tuple[datetime, float]]] = dict()
    for local_meter_nr, msrmnt_type_value, msrmnt_dt_utc, msrmnt_value in meter_nr_type_dt_value_quads:
        msrmnt_type = P1MeasurementType.from_value(msrmnt_type_value)
        k = (local_meter_nr, msrmnt_type)
        dt_value = (msrmnt_dt_utc, msrmnt_value)
        if k in res:
            res[k].append(dt_value)
        else:
            res[k] = [dt_value]

    for (local_meter_nr, msrmnt_type), dt_value_pairs in res.items():
        dt_value_pairs.sort()
        filtered_dt_value_pairs = filter_meter_readings(
            dt_value_pairs, max_interval_border)
        res[(local_meter_nr, msrmnt_type)] = filtered_dt_value_pairs
        print("{0:>4} filtered to {1:>4} {2:<20} interval end measurements from meter {3}".format(
            len(dt_value_pairs),
            len(filtered_dt_value_pairs),
            msrmnt_type.name,
            local_meter_nr,
        ))

    return res


def plot_values(
    meter_readings: list[tuple[datetime, float]],
    ax1,
    legends: list[str],
    scale: int,
    color: int,
    legend: str,
) -> None:
    # non cumulative meter reading values
    if not meter_readings:
        print("plot_values: no meter_readings for {}".format(legend))
        return
    plot_points = [(dt, value * scale) for (dt, value) in meter_readings]
    (dts, values) = zip(*plot_points)
    ax1.plot(dts, values, color=color, linewidth=0.8)
    legends.append(legend)


def vertical_lines(horizontal_intervals):
    vert_lines = []
    for (_prev_start_dt, _prev_end_dt, prev_value), (start_dt, _end_dt, value) in pairwise(horizontal_intervals):
        vert_lines.append((start_dt, prev_value, value))
    return vert_lines


def plot_decumul_values(meter_readings_cumul, ax1, legends, value_scale, color, legend):
    # meter reading values are cumulative
    if not meter_readings_cumul:
        print("plot_cumul_values: no meter_readings_cumul for {}".format(legend))
        return

    total_av_W = None
    if len(meter_readings_cumul) >= 2:
        # Report totals, compute avarages
        total_start_dt, total_start_val = meter_readings_cumul[0]
        total_end_dt, total_end_val = meter_readings_cumul[-1]
        total_secs = (total_end_dt - total_start_dt).total_seconds()
        total_val = float(total_end_val - total_start_val) * value_scale
        if total_secs > 0:
            total_av_kW = float(total_val / total_secs)
            total_av_W = round(total_av_kW * 1000)
            print("Plot avarage {:>6} W {:<20} from {} to {} UTC, total {:>4} kWh".format(
                total_av_W, legend,
                total_start_dt.replace(tzinfo=None),
                total_end_dt.replace(tzinfo=None),
                round(total_val / 3600)))
            ax1.hlines((total_av_kW,), (total_start_dt,),
                       (total_end_dt,), color=color, linestyles='dotted')
            legends.append("_")  # no legend for avarages

    plot_lines = []
    for (prev_start_dt, prev_cumul_value), (start_dt, cumul_value) in pairwise(meter_readings_cumul):
        duration = (start_dt - prev_start_dt).total_seconds()
        value_period = float(cumul_value - prev_cumul_value) * \
            value_scale  # decumulate
        av_value = value_period / duration
        plot_lines.append((prev_start_dt, start_dt, av_value))

    if plot_lines:
        (start_dts, end_dts, av_values) = zip(*plot_lines)
        ax1.hlines(av_values, start_dts, end_dts, color=color)
        if total_av_W is None:
            legends.append(legend)
        elif legend == P1MeasurementType.NgNl_m3.name:
            legends.append("{} (av. {} m3/day)".format(legend,
                           round(total_av_W * HOURS_PER_DAY / KWH_PER_M3) / 1000))
        else:
            legends.append("{} (av. {} W)".format(legend, total_av_W))
        vert_lines = vertical_lines(plot_lines)
        if vert_lines:
            (start_dts, prev_values, values) = zip(*vert_lines)
            ax1.vlines(start_dts, prev_values, values,
                       color=color, linewidth=0.3)
            legends.append("_")  # no legend for vertical lines


def plot_xy_info(
    value_name: str,
    value_unit: str,
    start_dt_utc: datetime,
    end_dt_utc: datetime,
    axes: Axes,
    legends: list[str],
    max_interval_border: IntervalBorder,
) -> None:
    plt.title("{} from P1 port, {} intervals".format(
        value_name, max_interval_border.name))
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        # ax1.legend(legends, ncol=1+len(legends)//2, loc="best")
        axes.legend(legends, loc="center left")
    axes.grid(axis="y", linestyle="dotted")
    plt.ylabel(value_unit)
    axes.xaxis.grid(linestyle="dotted")
    plt.xlabel("From {} to {} UTC".format(
        start_dt_utc.replace(tzinfo=None),
        end_dt_utc.replace(tzinfo=None),
    ))

    ax2 = axes.twinx()
    ax2.set_ylabel("m3/day")
    y1, y2 = axes.get_ylim()
    yfac = HOURS_PER_DAY / KWH_PER_M3
    ax2.set_ylim(y1 * yfac, y2 * yfac)


def report_meters():
    rpsql = RemotePsql()
    meters_query = "SELECT local_meter_nr, meter_serial_nr FROM meter;"
    for l1 in rpsql.record_lines(meters_query):
        local_meter_nr, meter_serial_nr = l1.split(rpsql.FIELD_SEP)
        print(f"meter {local_meter_nr}: {meter_serial_nr}")

    print("Last meter readings:")
    last_dts_by_type_query = """SELECT msrmnt_type, MAX(msrmnt_dt_utc) FROM measurement GROUP BY msrmnt_type;"""
    for l2 in rpsql.record_lines(last_dts_by_type_query):
        msrmnt_type, last_dt_utc = l2.split(rpsql.FIELD_SEP)
        last_value_query = \
            """SELECT msrmnt_value, local_meter_nr, msrmnt_dt_utc FROM measurement 
            WHERE msrmnt_type = {} AND msrmnt_dt_utc >= {}
            ORDER BY msrmnt_dt_utc;""".format(
                msrmnt_type,
                rpsql.sql_timestamp_sec(datetime.fromisoformat(last_dt_utc)))
        # use last line from ORDER BY:
        last_line = rpsql.record_lines(last_value_query)[-1]
        msrmnt_value, local_meter_nr, msrmnt_dt_utc = last_line.split(
            rpsql.FIELD_SEP)
        print("{:<20} {:>12} from meter {} at {}".format(
            P1MeasurementType.from_value(int(msrmnt_type)).name,
            msrmnt_value,
            local_meter_nr,
            datetime.fromisoformat(msrmnt_dt_utc)))


def plot_meter_readings_types(
    msrmnt_types_scales_colours: list[tuple[P1MeasurementType, float, str]],
    max_interval_border: IntervalBorder,
    start_dt_utc: datetime,
    end_dt_utc: datetime,
    ax1: Axes,
    legends: list[str],
) -> None:
    report_meters()

    msrmnt_types = {msrmnt_type for (
        msrmnt_type, scale, colour) in msrmnt_types_scales_colours}
    meterreadings_by_type = get_meter_readings_by_types(
        msrmnt_types,
        max_interval_border,
        start_dt_utc,
        end_dt_utc)

    local_meter_nrs = {local_meter_nr for (
        local_meter_nr, msrmnt_type) in meterreadings_by_type.keys()}

    for (msrmnt_type, scale, colour) in msrmnt_types_scales_colours:
        total_msrmnts_for_type = 0
        for local_meter_nr in local_meter_nrs:
            kmr = (local_meter_nr, msrmnt_type)
            if kmr not in meterreadings_by_type:
                continue
            total_msrmnts_for_type += len(meterreadings_by_type[kmr])
            if msrmnt_type in CumulativeP1MeasurementTypes:
                # decumulate per local_meter_nr
                value_plot_fn = plot_decumul_values
            else:
                value_plot_fn = plot_values
            value_plot_fn(
                meterreadings_by_type[kmr], ax1, legends, scale, colour, msrmnt_type.name)
        if total_msrmnts_for_type == 0:
            print(f"no measurements for {msrmnt_type}")


def do_main():
    parser = argparse.ArgumentParser(description='Show meter plots.',
                                     epilog="Executed commands are logged in ~/log/meterplots.log")
    parser.add_argument('-n', dest='non_blocking',
                        action="store_const", const=True, default=False,
                        help='close plots immediately')
    args = parser.parse_args()

    # Show current date/time in utc:
    do_command("date --utc")

    # max_interval_border = IntervalBorder.Second # not needed, interval between P1 telegrams is just over 10 secs.

    # show kWh quantization
    plot_last_days, plot_last_hours, max_interval_border = 0, 2, IntervalBorder.Second
    # show kWh quantization
    plot_last_days, plot_last_hours, max_interval_border = 0, 20, IntervalBorder.TenSecond
    # last 2 days in minutes, some quantization
    plot_last_days, plot_last_hours, max_interval_border = 0, 48, IntervalBorder.Minute
    # last week in quarters
    plot_last_days, plot_last_hours, max_interval_border = 7, None, IntervalBorder.Quarter
    # last month in hours
    # plot_last_days, plot_last_hours, max_interval_border = 32, None, IntervalBorder.Hour
    # last year in days
    # plot_last_days, plot_last_hours, max_interval_border = 366, None, IntervalBorder.DayUtc
    # last 2 years in months
    # plot_last_days, plot_last_hours, max_interval_border = 510, None, IntervalBorder.MonthUtc

    end_dt_utc = (datetime.now(timezone.utc) + timedelta(minutes=1)
                  ).replace(second=0, microsecond=0)  # whole minute

    if plot_last_hours is not None:
        plot_total_time = timedelta(
            days=plot_last_days,
            hours=plot_last_hours)
        start_dt_utc = end_dt_utc - plot_total_time
    else:
        plot_total_time = timedelta(days=plot_last_days)
        start_dt_utc = end_dt_utc - plot_total_time
        start_dt_utc = datetime(
            year=start_dt_utc.year, month=start_dt_utc.month, day=start_dt_utc.day)  # whole days

    colours = "blue green red cyan magenta yellow black white grey".split()
    legends: list[str] = []

    figure, axes = plt.subplots()

    msrmnt_types_scales_clrs: list[tuple[int, P1MeasurementType, float, str]] = [
        (P1MeasurementType.NgNl_m3, KWH_PER_M3 * SECS_PER_HOUR, colours[2]),

        (P1MeasurementType.El_kWh_to_tariff_1, SECS_PER_HOUR, colours[0]),

        (P1MeasurementType.El_kWh_to_tariff_2, SECS_PER_HOUR, colours[1]),

        # El_kWh_by* has many zero values, plot colour overwrites zeros mostly from NgNl_m3 above.
        (P1MeasurementType.El_kWh_by_tariff_1, -SECS_PER_HOUR, colours[0]),

        (P1MeasurementType.El_kWh_by_tariff_2, -SECS_PER_HOUR, colours[1]),
    ]

    # interval of ten seconds or less, i.e. all P1 telegrams
    if max_interval_border.value >= IntervalBorder.TenSecond.value:
        msrmnt_types_scales_clrs.extend((
            (P1MeasurementType.El_kW_to, 1, colours[5]),
            (P1MeasurementType.El_kW_by, -1, colours[5]),
        ))

    plot_meter_readings_types(
        msrmnt_types_scales_clrs,
        max_interval_border,
        start_dt_utc,
        end_dt_utc,
        axes,
        legends)

    plot_xy_info(
        "Electricity and gas",
        "kW (dotted: avarages)",
        start_dt_utc,
        end_dt_utc,
        axes,
        legends,
        max_interval_border)
    # see also matplotlib Figure.set_size_inches(w, h) or ...((w,h))
    figure.set_figwidth(13.5)
    try:
        plt.show(block=not args.non_blocking)
    except KeyboardInterrupt:  # avoid traceback
        print("")  # end line with ^C


if __name__ == "__main__":
    init_logger(logger, Path(sys.argv[0]).stem)
    do_main()
