#! /usr/bin/python3
"""
On a local host and an ssh reachable remote host, set up rust development environment for
energy meter privacy aggregation.
This environment consists of
- a p1encrypt-rs program running on the remote host
  that reads a smart meter p1 port connected to the remote host,
  and encrypts the measurements read from the p1 port.
- a keyadder-rs program on the local host that adds encryption keys,
  into sum keys to allow decryption/aggregation.
- an aggrmeter-rs program on the local host that receives encrypted measurements,
  and aggregates the measurement values by using the sum keys.

The local host runs a debian linux, e.g. ubuntu.
This script should be run from an enmprv repository clone. It will access other 
files from inside this repository clone relative to its own location,
a.o. the default configuration file setupempdefault.toml in the same directory.

The remote host is assumed to be a freshly installed raspbian host,
with ssh reachability as provided by a standard raspbian setup.
Both the user of this script and this remote ssh user need to be sudoers.

The set up operations on the remote host are done by copying 
setupemp_ppc.py and the modules that it uses to the remote host and 
by executing setupemp_ppc.py there via ssh with arguments provided
from this script running at the local host.
To run locally as another user setupemp_ppc.py is also used.

Setting up this development environment consists of the following steps,
that may exit with an exception in case of failure:

- check the p1 port connection on the remote host for streaming p1 telegrams.

- check the ntp clock synchronisation on both hosts.

- on both hosts do an apt upgrade to update the OS installations,
  and install/update wireguard.
- install/update a configured list of apt packages.
- afer these apt steps are done a reboot may be needed after this,  
  so an exception is thrown.
  The output of apt is logged on each host only after apt finished,
  so two initial runs of this script may take some time to upgrade both the hosts.

- initialize a wireguard (wg) tunnel between the local host
    and the remote host for the communication between the p1encrypt-rs,
    keyadder-rs and aggrmeters-rs programs.
    Private wg keys are generated on each host as needed.
    Private keys are never communicated between the hosts.

- on the local host and on the remote host:
    install/update rust via rustup as the sudoer user,
    so these users can build rust programs on both hosts.

- on the remote host create the measurements encryptor user
    to run the p1encrypt-rs program.
    This user will not be a sudoer.
    Copy the ssh authorized keys from the remote sudo user to allow ssh access as
    the measurements encryptor user.
    Running as the measurements encryptor user on the remote host, the following substeps are done:
    - generate an openssl key pair for authorizing/signing the encrypted measurements
      provided by the p1encrypt-rs program.

- on the local host, create a user for the keyadder-rs program,
  and generate an openssl key pair to authorize/sign the key sums.

- on the local host, create a user for the aggrmeters-rs program,
  and generate an openssl key pair for this user to authorize/sign aggregation results.

- copy the public openssl keys between the hosts
  for use by the local keyadder-rs and aggrmeters-rs programs,
  and for use by the remote p1encrypt-rs program,
  to verify all received signatures.

- for rust compilation of sqlx related code on the local host: 
   - create a postgres user role for the current user,
   - create the configured all_tables_db_name database owned by this role
     in the public postgresql schema of the database.

- to run the compiled rust code on the remote host: 
   - create a postgres user role for the configured p1_encryptor.user_name,
   - create the configured p1_encryptor.db_name database owned by this role
     in the public postgresql schema of the database.

After setting up the development environment,
a development run of each of these programs can be started.

TBD: create the configuration files for each program from the
configuration here.

Two toml configuration files are used to provide preferred and default values
for the following configuration attributes.
The configuraton values are explained in the setupempdefault.toml file.

The wg private key files are generated in case they do not exist.
The wg interfaces are created/updated to use the private key, and the peer wg public keys.
The local wg peer public key is replaced by a possibly newly generated public key from the remote host.
The wg tunnel is tested and when this passes
the wireguard config files are saved to /etc/wireguard/... for systemctl to find it.

Logging is done into the log/ directory in the user home directory.
This means that this script provides logs in 5 places:
- on the local host for the sudoer user starting this script,
- on the local host for the created key adder user,
- on the local host for the created aggregator user,
- on the remote ssh host for the sudoer user, and
- on the remote ssh host for the p1encryptor user.
See init_logger() for the details of the logging configuration.

All created files are only accessible by the user running this script,
or by the users generated here.

The uninstall command to this script works in reverse order:
In the postgres clusters on both hosts:
- drop empty tables from the created databases,
- drop the databases, except when they contain non empty tables,
- drop user roles when they do not own any databases.
In case there is a non empty table, the uninstall will stop with an exception.
Otherwise, on both hosts:
Remove the added users, including their home directories
with the log files from this script in these home directories.
This uninstall does not undo the apt upgrades and the rust install/updates on both hosts.
"""

import argparse
import logging
import os
from pathlib import Path
import sys

from setupempconfig import SetupEmpConfig

from setupemputil import init_logger, SetupException
from setupemputil import SSH_CMD, do_command, do_command_return_code, do_command_stdout
from setupemputil import apt_upgrade_install, wait_clock_synchronized
from setupemputil import create_system_user, delete_system_user, install_rust

from setupempwg import check_setup_wg_tunnel
from setupempwg import tear_down_wg_tunnel, wg_private_key_file_path

from setupempauth import distribute_public_authorization_keys, generate_authorization_keys

from setupemppg import create_role_and_database, drop_empty_database_and_role

# for copying local python sources from this directory to remote host to also run there:
IMPORTED_LOCAL_MODULES = (
    "setupemputil",
    "setupempconfig",
    "setupempwg",
    "setupempauth",
    "setupemppg",
    "setupemp_ppc",  # used when called as subprocess
)

_dev_env_text = """
    This script was developed in vscode with these extensions:
    - Python, default settings, used a.o. for formatting python code via Shift-Ctrl-I.
    - Pylint with this item added to allow using .format() for python strings:
       --disable=C0209
      and with "." added to the pylint path for the local python modules.
    This was added via File/Preferences/Settings, search pylint, add under Args.

    The reason to allow using .format() is a difference between
    python 3.11.2 on the raspberry p1encryptor host, and
    python 3.12.3 on the ubuntu machine used to develop this.

    Python 3.12 introduced https://peps.python.org/pep-0701/
    "Syntactic formalization of f-strings."
    This change allows newlines within expression brackets of f"{}" strings.
    Such newlines can be introduced by formatting the python code via Shift-Ctrl-I,
    leaving the python code valid under 3.12,
    but on python 3.11.2 (on the raspberry) these newlines
    give a syntax error at python startup.
    The code formatting can introduce newlines into longer f"{}"strings here,
    so .format() is used here to avoid introducing these newlines.
"""

_ssh_config_example_text = """
    # Use this in ~/.ssh/config to improve performance of sequences of ssh commands:
    # use actual values instead of ... :

    Host raspyp1 # p1_encryptor.ssh_host as in config file here.
    Hostname ... # dns name or ip address of Host
    User ... # sudoer ssh user on Host

    Host *
    ConnectTimeout 15
    ControlMaster auto
    # socket file name to keep ssh connection to host:
    ControlPath   /home/%u/.ssh/%h_%p_%r
    # for direct sequences of ssh uses with at most 10 seconds between remote commands:
    ControlPersist 10
    # disconnect after one minute of broken connection:
    ServerAliveInterval 30
    ServerAliveCountMax 1
"""

ALLOW_ONLY_USER_UMASK = 0o077  # for created files

INVOCATION = " ".join(sys.argv)

logger = logging.getLogger(__name__)


def copy_python_sources_to_remote(config: SetupEmpConfig):
    # check that remote host is available, log the remote python version
    python_version = do_command_stdout("{0} {1} {2} --version".format(
        SSH_CMD,
        config.p1_encryptor.ssh_host,
        config.p1_encryptor.python_cmd),
        ignore_stderr=True,
    ).strip()
    logger.debug("%s has %s", config.p1_encryptor.ssh_host, python_version)
    # rsync this python file and the imported modules to wg_tunnel.config_dir on the remote host,
    # this ignores the umask settings:
    modules_dir_path = Path(os.path.dirname(sys.argv[0]))
    sources_paths = [modules_dir_path.joinpath(f"{mod_name}.py")
                     for mod_name in IMPORTED_LOCAL_MODULES]
    sources_names = " ".join(str(p) for p in sources_paths)
    # this also creates the remote directory config.wg_tunnel.config_dir:
    do_command("rsync --archive {0} {1}:{2}/".format(
        sources_names,
        config.p1_encryptor.ssh_host,
        config.wg_tunnel.config_dir,
    ))


def do_local_main_install(
    config: SetupEmpConfig,
    apt_option: bool,
    p1_ntp_option: bool,
    wg_option: bool,
    create_users_option: bool,
    rust_option: bool,
    auth_keys_option: bool,
    databases_option: bool,
) -> None:
    """ The main function of this script when invoked locally.
        This uses the configuration from `config_toml_path`,
        performs various local commands,
        a.o. to copy this script to the remote host and invoke it there.
    """
    if apt_option:
        # do a local apt upgrade and install the configured local packages
        apt_upgrade_install(install_packages=config.apt_packages)

    copy_python_sources_to_remote(config)

    # make the remote home directory executable by a+rx,
    # so the copied python code can be used later by another user:
    do_command("{0} {1} sudo chmod a+rx .".format(
        SSH_CMD,
        config.p1_encryptor.ssh_host))

    # let rsync list the remote directory to stdout:
    do_command("rsync {0}:{1}/".format(
        config.p1_encryptor.ssh_host,
        config.wg_tunnel.config_dir))

    remote_sudoer_invocation = config.get_remote_sudoer_invocation()

    if p1_ntp_option:
        do_command("{0} {1} {2} {3} {4}".format(
            remote_sudoer_invocation,
            "testp1connection_clocksync",
            config.p1_encryptor.p1_terminal,
            config.excl_socket_lock.address,
            str(config.excl_socket_lock.port)))
        wait_clock_synchronized()

    if apt_option:
        # on the p1_encryptor do an apt upgrade and install the configured packages
        remote_cmd_words = ["aptupgrade"]
        remote_cmd_words.extend(config.p1_encryptor.apt_packages)
        upgraded_res = do_command_stdout(
            "{0} {1}".format(
                remote_sudoer_invocation,
                " ".join(remote_cmd_words)
            ),
            ignore_stderr=True).strip()
        if upgraded_res != "":
            raise SetupException(
                f"Unexpected stdout from aptupgrade: {upgraded_res}")

    if wg_option:
        p1e_wg_public_key = do_command_stdout(
            "{0} {1} {2} {3} {4}".format(
                remote_sudoer_invocation,
                "genwgkeys",
                config.wg_tunnel.config_dir,
                config.wg_tunnel.intf_name,
                config.wg_tunnel.priv_key_extension,
            ),
            ignore_stderr=True,
        ).strip()
        logger.info(f"p1e_wg_public_key: {p1e_wg_public_key}")
        check_setup_wg_tunnel(config, p1e_wg_public_key)

    if create_users_option:
        do_command("{0} {1} {2} {3} {4}".format(
            remote_sudoer_invocation,
            "createsystemuser",
            config.p1_encryptor.user,
            # put this user in the group of the serial port:
            config.p1_encryptor.p1_terminal,
            config.enmprv_home))

        # p1e_user should be ssh reachable now also via the wg tunnel:
        for remote_host in [
            config.p1_encryptor.ssh_host,
            config.wg_tunnel.p1e_ip,
        ]:
            # let ssh accept new host config.wg_tunnel.p1e_ip,
            # this is ok because public wg keys have been exchanged:
            ssh_pwd_cmd = "{0} -o StrictHostKeyChecking=off {1}@{2} {3}".format(
                SSH_CMD,
                config.p1_encryptor.user,
                remote_host,
                "pwd")
            remote_p1e_home = do_command_stdout(
                ssh_pwd_cmd,
                ignore_stderr=True,
            ).strip()
            logger.debug(
                "user %s home at %s is %s",
                config.p1_encryptor.user,
                remote_host,
                remote_p1e_home
            )

        # Create keyadder and prvaggr system users on the local host.
        # These users can only start a program via su or systemctl.
        echo_home_cmd = "echo ~"
        for user_name in (
            config.local_host.key_adder_user,
            config.local_host.prv_aggr_user,
        ):
            create_system_user(
                user_name,
                config.enmprv_home,
                file_name_group_access=None,
                copy_ssh_auth_keys=None)
            local_user_home = do_command_stdout(
                # let su run /bin/bash as the user and pass the echo_home command:
                "sudo su {0} --shell=/bin/bash --command='{1}'".format(
                    user_name,
                    echo_home_cmd
                )).strip()
            actual_home_path = Path(local_user_home)
            expected_home_path = Path(config.enmprv_home).joinpath(user_name)
            if actual_home_path != expected_home_path:
                raise SetupException("expected home of {0} is {1} but actual home is {2}".format(
                    user_name,
                    expected_home_path,
                    actual_home_path,
                ))
            logger.debug("user %s local home is %s",
                         user_name, local_user_home)

    # determine remote sudoer home directory for remote p1_encryptor.user invocations
    ssh_pwd_cmd = "{0} {1} pwd".format(
        SSH_CMD,
        config.p1_encryptor.ssh_host,
    )
    remote_sudoer_home = do_command_stdout(
        ssh_pwd_cmd, ignore_stderr=True).strip()
    remote_p1e_invocation = config.get_remote_p1e_invocation(
        remote_sudoer_home)

    if rust_option:
        # for the remote sudoer to compile remotely without sqlx-cli,
        do_command(f"{remote_sudoer_invocation} installrust")
        # on the local host also install sqlx-cli
        install_rust(install_sqlx_cli=True)

    if auth_keys_option:
        generate_authorization_keys(config, remote_p1e_invocation)
        distribute_public_authorization_keys(config)

    if databases_option:
        """ In the local postgres cluster create 
        - a user role for the local sudoer user, and
        - the database `config.local_host.all_tables_db_name` owned by this user.
        """
        local_user = os.getenv("USER")
        create_role_and_database(
            local_user, config.local_host.all_tables_db_name)

        """ In the remote postgres cluster create:
        - a user role for config.p1_encryptor.user, and
        - the database `config.p1_encryptor.db_name` owned by this user.
        """
        do_command("{0} {1} {2} {3}".format(
            remote_sudoer_invocation,
            "createroledatabase",
            config.p1_encryptor.user,
            config.p1_encryptor.db_name))


def do_local_main_uninstall(config: SetupEmpConfig) -> None:
    """ The main function of this script when invoked locally to uninstall.
        This undoes the steps of do_local_main_install() except:
         - the apt upgrade of the system on local and remote host, and
         - the rust installation.

        This uses the configuration from `config_toml_path`.
    """
    copy_python_sources_to_remote(config)

    # In reverse order of installation:
    """
    For the remote pg cluster with p1_encryptor.user/p1_encryptor.db_name, and
    from the local pg cluster sudoer user/ all_tables_db_name:
    - drop the non empty tables in the database,
    - drop this databases when it is empty,
    - drop the user role when it has no responsibilities.
    """
    remote_sudoer_invocation = config.get_remote_sudoer_invocation()
    do_command("{0} {1} {2} {3}".format(
        remote_sudoer_invocation,
        "dropdatabaserole",
        config.p1_encryptor.db_name,
        config.p1_encryptor.user,
    ))
    local_user = os.getenv("USER")
    drop_empty_database_and_role(
        config.local_host.all_tables_db_name,
        local_user)

    delete_system_user(config.local_host.prv_aggr_user)
    delete_system_user(config.local_host.key_adder_user)
    remote_sudoer_invocation = config.get_remote_sudoer_invocation()
    do_command("{0} {1} {2}".format(
        remote_sudoer_invocation,
        "deletesystemuser",
        config.p1_encryptor.user))

    # at config.p1_encryptor.ssh_host:
    # - tear down the wg interface
    do_command("{0} {1} {2}".format(
        remote_sudoer_invocation,
        "teardownwgtunnel",
        config.wg_tunnel.intf_name))
    # - remove the directory config.wg_tunnel.config_dir, this will also delete the copied python sources.
    # remotely this also contains the python source files so do no more remote commands after this:
    bash_is_dir_cmd = "[[ -d {} ]]".format(config.wg_tunnel.config_dir)
    cmd = "{0} {1} bash -c \"'{2}'\"".format(
        SSH_CMD,
        config.p1_encryptor.ssh_host,
        bash_is_dir_cmd
    )
    remote_dir_exists_ret_code = do_command_return_code(cmd)
    if remote_dir_exists_ret_code not in (0, 1):
        raise SetupException("return code {} from {}".format(
            remote_dir_exists_ret_code, cmd))
    remote_dir_exists = remote_dir_exists_ret_code == 0
    if remote_dir_exists:
        do_command("{0} {1} rm -r {2}".format(
            SSH_CMD,
            config.p1_encryptor.ssh_host,
            config.wg_tunnel.config_dir,
        ))

        # at local host:
        # - tear down the tunnel config.wg_tunnel.intf_name
        tear_down_wg_tunnel(config.wg_tunnel.intf_name)
        # - remove the wg private key file:
        priv_key_path = wg_private_key_file_path(
            Path(config.wg_tunnel.config_dir),
            config.wg_tunnel.intf_name,
            config.wg_tunnel.priv_key_extension)
        priv_key_path.unlink(missing_ok=True)


def do_main():
    parser = argparse.ArgumentParser(
        description="Install development environment for energy meter privacy aggregation",
        epilog="When all of the -n* options are given only the configuration is checked.")
    parser.add_argument(
        "-l",
        "--local",
        help="toml file with local configuration values. These add to, and override, the default")
    default_path = Path.cwd().joinpath(
        sys.argv[0]).parent.joinpath("setupempdefault.toml")
    parser.add_argument(
        "-d",
        "--default",
        help="toml file with default configuration values, defaults here to {}".format(
            default_path),
        default=default_path)
    parser.add_argument(
        "-u",
        "--uninstall",
        help="uninstall a previously installed environment, the -n* options are ignored",
        action="store_true")
    # only for case with config file:
    parser.add_argument(
        "-na",
        "--no-apt",
        help="skip apt upgrade/install step on both hosts, otherwise reboots may be needed",
        action="store_true")
    parser.add_argument(
        "-np",
        "--no-p1ntp",
        help="skip checking for p1 telegrams on remote host, and skip checking for ntp synchronisation on both hosts",
        action="store_true")
    parser.add_argument(
        "-nw",
        "--no-wg",
        help="skip setting up the wireguard tunnel",
        action="store_true")
    parser.add_argument(
        "-nr",
        "--no-rust",
        help="skip install/update rust on the remote host",
        action="store_true")
    parser.add_argument(
        "-nu",
        "--no-users",
        help="skip creating system users",
        action="store_true")
    parser.add_argument(
        "-nk",
        "--no-auth-keys",
        help="skip generating and distributing authorization keys",
        action="store_true")
    parser.add_argument(
        "-nd",
        "--no-databases",
        help="skip creating databases and database user roles",
        action="store_true")

    args = parser.parse_args()

    if args.local is not None:
        config = SetupEmpConfig.from_files(args.local, args.default)
    else:
        config = SetupEmpConfig.from_files(args.default, None)

    local_sudoer_name = os.getenv("USER")
    p1e_host_sudoer_name = do_command_stdout("{0} {1} whoami".format(
        SSH_CMD,
        config.p1_encryptor.ssh_host),
        ignore_stderr=True,
    ).strip()
    config.check_user_names(local_sudoer_name, p1e_host_sudoer_name)

    if not args.uninstall:
        do_local_main_install(
            config,
            apt_option=not args.no_apt,
            p1_ntp_option=not args.no_p1ntp,
            wg_option=not args.no_wg,
            create_users_option=not args.no_users,
            rust_option=not args.no_rust,
            auth_keys_option=not args.no_auth_keys,
            databases_option=not args.no_databases,
        )
    else:
        do_local_main_uninstall(config)


if __name__ == "__main__":
    prev_umask = os.umask(ALLOW_ONLY_USER_UMASK)
    try:
        # logs to rotating files with the same name as this script:
        log_file_name_stem = Path(sys.argv[0]).stem
        init_logger(logger, log_file_name_stem)
        logger.debug("starting: %s", INVOCATION)
        # creates private keys and wg .conf files:
        do_main()
        logger.debug("finished: %s", INVOCATION)
    except Exception as exception:
        logger.critical("exception during: %s", INVOCATION)
        logger.exception(exception)
        raise exception  # and exit non zero
    finally:
        os.umask(prev_umask)
