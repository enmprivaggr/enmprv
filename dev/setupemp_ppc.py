"""
setupemp_ppc: the process procedure calls for setupemp.py .
This is invoked as a process and the command line arguments are used 
to determine the procedure to be run and its arguments.

For logging, see setupemp.py.

All created files are only accessible by the user running this script,
or by the users generated here.
"""

import logging
import os
from pathlib import Path
import sys

from setupemputil import init_logger, SetupException
from setupemputil import apt_upgrade_install, test_p1_connection, wait_clock_synchronized
from setupemputil import create_system_user, delete_system_user, install_rust

from setupempwg import generate_wg_keys
from setupempwg import setup_wg_with_peer, save_wg_config_for_restart
from setupempwg import tear_down_wg_tunnel

from setupempauth import generate_ec_key_pairs

from setupemppg import create_role_and_database, drop_empty_database_and_role

ALLOW_ONLY_USER_UMASK = 0o077  # for created files

logger = logging.getLogger(__name__)


def do_main() -> None:
    """ The main function of this script when invoked as a subprocess.
        The command line arguments are normally used to pass the configuration values.
        Results are provided on stdout as expected by the calling execution.
        This can run as a process on a remote host or on the local host.
    """
    cmd_word = sys.argv[1]
    args = sys.argv[2:]

    def testp1connection_clocksync(
        p1_file_name,
        lock_ip_addr_str,
        lock_ip_port,
    ):
        test_p1_connection(
            p1_file_name=p1_file_name,
            lock_ip_addr_str=lock_ip_addr_str,
            lock_ip_udp_port=int(lock_ip_port))
        wait_clock_synchronized()

    def aptupgrade(*args):
        assert len(args) > 0
        # apt_upgrade_install() avoids stdout
        apt_upgrade_install(install_packages=args)

    def genwgkeys(
        wg_conf_dir_name,
        wg_intf_name,
        wg_priv_key_extension
    ):
        # generate_wg_keys() avoids stdout
        pub_key = generate_wg_keys(
            Path.home().joinpath(wg_conf_dir_name),
            wg_intf_name,
            wg_priv_key_extension)
        sys.stdout.write(pub_key)  # pass back to caller via stdout

    def setupwgpeer(
        wg_conf_dir_name,
        wg_intf_name,
        wg_priv_key_extension,
        listen_port,
        wg_intf_address,
        peer_public_key,
        peer_allowed_ip
    ):
        setup_wg_with_peer(
            wg_conf_dir_path=Path.home().joinpath(wg_conf_dir_name),
            wg_intf_name=wg_intf_name,
            wg_priv_key_extension=wg_priv_key_extension,
            listen_port=int(listen_port),
            wg_intf_address=wg_intf_address,
            peer_public_key=peer_public_key,
            peer_allowed_ip=peer_allowed_ip,
            peer_persistent_keep_alive=None,
            peer_end_point=None
        )

    def savewgconfig(intf_name):
        save_wg_config_for_restart(intf_name)

    def createsystemuser(
        p1e_user,
        p1e_p1_terminal,
        enmprv_home,
    ):
        create_system_user(
            user_name=p1e_user,
            enmprv_home=enmprv_home,
            file_name_group_access=p1e_p1_terminal,
            copy_ssh_auth_keys=True)

    def installrust():
        # normally done as config.p1_encryptor.user
        # assert len(args) == 0, repr(args)
        install_rust()

    def deletesystemuser(user_name):
        # normally done as sudoer user
        delete_system_user(user_name)

    def teardownwgtunnel(intf_name):
        # normally done as sudoer user
        tear_down_wg_tunnel(intf_name)

    def generateeckeypairs(
        keys_dir_name,
        ec_curve_name,
        lock_ip_addr,
        lock_ip_port,
        num_key_pairs,
    ):
        # normally done as p1_encryptor.user, key_adder_user or prv_aggr_user,
        # this user name is passed via the environment.
        generate_ec_key_pairs(
            keys_dir_name,
            ec_curve_name,
            lock_ip_addr,
            int(lock_ip_port),
            int(num_key_pairs),
        )

    def createroledatabase(user_name, db_name):
        create_role_and_database(user_name, db_name)

    def dropdatabaserole(db_name, user_name):
        drop_empty_database_and_role(db_name, user_name)

    f = locals().get(cmd_word)  # may be None
    if type(f) is not type(lambda: None):
        raise SetupException(
            f"cmd_wrd {cmd_word} is not a local function, arguments {args!r}")

    f(*args)


if __name__ == "__main__":
    prev_umask = os.umask(ALLOW_ONLY_USER_UMASK)
    invocation = " ".join(sys.argv)
    try:
        # logs to rotating files with the same name as this script:
        log_file_name_stem = Path(sys.argv[0]).stem
        init_logger(logger, log_file_name_stem)
        logger.debug("starting: %s", invocation)
        # creates private keys and wg .conf files:
        do_main()
        logger.debug("finished: %s", invocation)
    except Exception as exception:
        logger.critical("exception during: %s", invocation)
        logger.exception(exception)
        raise exception  # and exit non zero
    finally:
        os.umask(prev_umask)
